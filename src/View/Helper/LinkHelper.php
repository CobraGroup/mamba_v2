<?php


namespace App\View\Helper;

use Cake\View\Helper;

class LinkHelper extends Helper {

    public $helpers = ['Html'];

    public function makeAdd($title, $url) {
        // Logic to create specially formatted link goes here...

		$link = $this->Html->link($title, $url, ['class' => 'add']);
		
		return '<div class="addOuter">'. $link .'</div>';		
    }
    
    public function makeEdit($title, $url) {
        // Logic to create specially formatted link goes here...

		$link = $this->Html->link($title, $url, ['class' => 'edit']);
		
		return '<div class="editOuter">'. $link .'</div>';		
    }

}
