<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;

class UsersController extends AppController
{
	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

	public function initialize() {
	    parent::initialize();
	    $this->loadComponent('Csrf');
	}

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['register', 'logout']);
    }

	public function login() {
		$this->viewBuilder()->layout('login');
		#echo (new DefaultPasswordHasher)->hash('C0br4');
		if(env('HTTP_HOST') != LOGIN_URL) {
			return $this->redirect('/');
		}
	    if ($this->request->is('post')) {
	        $user = $this->Auth->identify();
	        if ($user) {
		        $this->lastLogin($user['id']);
	            $this->Auth->setUser($user);
	            return $this->redirect($this->Auth->redirectUrl());
	        } else {
	            $this->Flash->error(
	                __('Username or password is incorrect'),
	                'default',
	                [],
	                'auth'
	            );
	        }
	    }
	}

	public function register() {
	    $user = $this->Users->newEntity($this->request->data);
	    if ($this->Users->save($user)) {
	        $this->Auth->setUser($user->toArray());
	        return $this->redirect([
	            'controller' => 'Users',
	            'action' => 'home'
	        ]);
	    }
	}

	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}

	protected function lastLogin($id) {
		$user = $this->Users->get($id);
		$entity = $this->Users->patchEntity($user, ['last_login' => Time::now()]);
		$this->Users->save($entity);
	}

}