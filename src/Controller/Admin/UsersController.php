<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class UsersController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		if($this->Auth->user('role') != 'Admin') {
			$this->Auth->config('authorize', ['Controller']);
	    }
	}

    public function beforeRender(Event $event) {
    	if(!$this->request->session()->check('Breadcrumbs.Users')) {
    		$this->request->session()->write("Breadcrumbs.Users", "/admin/users");
    	}
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

    public function index() {
        $this->set('users', $this->paginate());
    }

    public function view($id) {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid user'));
	    }
	    $user = $this->Users->findById($id)
	    	->contain(['Websites'])
	    	->first();

	    if ($this->request->is(['post', 'put'])) {
	        $this->Users->patchEntity($user, $this->request->data);
	        if ($this->Users->save($user)) {
	            $this->Flash->success(__('Your user has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your user.'));
	    }

	    $data = $user->toArray();
	    $selected_sites = Hash::combine($data['websites'], '{n}.id', '{n}.name');

	    $sites = TableRegistry::get('Websites')->find('list')->order(['name' => 'ASC'])->toArray();
	    $available_sites = Hash::diff($sites, $selected_sites);

	    $this->set('roles', array('Editor' => 'Editor', 'Admin' => 'Admin'));
	    $this->set(compact('user', 'selected_sites', 'available_sites'));
	}

    public function add() {
        $user = $this->Users->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
	    $this->set('roles', array('Editor' => 'Editor', 'Admin' => 'Admin'));
        $this->set('user', $user);
    }

	public function delete($id = null) {
	    $this->request->allowMethod(['post', 'delete']);

		if(!$id) {
			throw new NotFoundException(__('Invalid user'));
		}

	    $user = $this->Users->get($id);
	    if ($this->Users->delete($user)) {
	        $this->Flash->success(__('The user with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

    public function roles() {
        // static view
    }

}