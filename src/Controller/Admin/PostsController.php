<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class PostsController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form'],
		'Paginator' => ['templates' => 'paginator-template']
	];

	public $paginate = [
		'contain' => ['Categories'],
		'conditions' => ['status' => 'Published'],
		'order' => ['created' => 'DESC'],
		'limit' => 25
	];

  public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->websiteSettingsAdmin();
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

    public function index() {
        $crumbs = $this->request->session()->read('Breadcrumbs');
        $this->request->session()->write('Breadcrumbs', array_splice($crumbs, 0, 2) + ['Posts' => '/admin/posts']);
        $this->set('posts', $this->paginate());
    }

    public function unpublished() {
        $this->paginate['conditions'] = ['status' => 'Unpublished'];
        $this->set('posts', $this->paginate());
    }

    public function draft() {
        $this->paginate['conditions'] = ['status' => 'Draft'];
        $this->set('posts', $this->paginate());
    }

    public function view($category_id = null) {
        $this->set('posts', $this->Posts->find()->where(['category_id' => $category_id]));
        $this->set('category', $this->Posts->Categories->find()->where(['id' => $category_id])->first());
    }

    public function filter($category_id = null, $block_id = null) {
	    $this->layout = false;
	    $posts = $this->Posts->find('list', ['valueField' => 'title'])->where(['category_id' => $category_id, ['status' => 'Published']])->toArray();
	    $selected = null;
	    if($block_id) {
			$reducer = function ($output, $value) {
			    if (!in_array($value, $output)) {
			        $output[] = $value;
			    }
			    return $output;
			};
	    	$selected = TableRegistry::get('Blocks')
	    		->findById($block_id)
	    		->extract('id')
	    		->reduce($reducer,[]);

	    	$blocks = TableRegistry::get('Blocks')->findById($block_id)->first();

	    	$this->set('selected', $blocks->data['post_ids']);
	    }
        $this->set(compact('posts'));
    }

    public function add($category_id = null) {
        $post = $this->Posts->newEntity($this->request->data);
        if ($this->request->is('post')) {

            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Post could not be saved, please try again.'));
        }

 	    $this->set('status', array('Published','Draft','Unpublished'));
 	    $this->set('categories', $this->Posts->Categories->find('list')->toArray());
		$this->set(compact('post', 'category_id'));
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid post'));
	    }
	    $post = $this->Posts->find()
	    	->where(['Posts.id' => $id])
	    	->contain(['Categories'])
	    	->first();

	    if ($this->request->is(['post', 'put'])) {
	        $this->Posts->patchEntity($post, $this->request->data);
	        if ($this->Posts->save($post)) {
	            $this->Flash->success(__('Post has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your post.'));
	    }

		$languages = TableRegistry::get('Websites')->find('languages');

		if($languages) {
			foreach($languages as $language)
				{
					$exists[] = $this->Posts
							->findByTitle($post['title'])
							->select(['locale', 'title', 'website_id'])
							->first()->toArray();
				}

		} else {
			$exists[] = $this->Posts
						->findByTitle($post['title'])
						->select(['locale', 'title', 'website_id'])
						->first()->toArray();
		}

/*
		foreach($languages as $language)
		{
			$exists[] = $this->Posts
						->findByTitleAndWebsiteId($post['title'], $language['website_id'])
						->select(['locale', 'title', 'website_id'])
						->first();
		}
*/
		$this->set('exists',$exists);
 	    $this->set('status', array('Published','Draft','Unpublished'));
 	    $this->set('categories', $this->Posts->Categories->find('list')->toArray());
 		$this->set('languages',$languages);
	    $this->set('post', $post);
	}

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $post = $this->Posts->get($id);
	    if ($this->Posts->delete($post)) {
	        $this->Flash->success(__('The post with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}
	public function duplicateall() {
		$this->autoRender = false;
		$posts = $this->Posts->find('list', ['valueField' => 'title'])->where(['category_id' => 10, 'website_id' => 10,  ['status' => 'Published']])->toArray();

		foreach($posts as $country) {

			$data = array_merge($post, [
				'website_id' => $site['website_id'],
				'locale' => $site['locale'],
				'status' => 'Published',
				'duplicate' => true
			]);


			$pageData = $this->Posts->newEntity($data);

			if($this->Posts->save($pageData)) {
				$this->Flash->success(__('Post have been created.'));;
			} else{
		        $this->Flash->error(__('Post is not created.'));
			}

		}
	}
	//if post is tagged then make it available in that country and duplicate the content
	public function duplicate(){
		$this->autoRender = false;

		$posts = $this->Posts->find('list', ['valueField' => 'title'])->where(['category_id' => 10, 'website_id' => 10,  ['status' => 'Published']])->toArray();

		//get list of sites
		$website = Configure::read('Website');

		foreach($posts as $country) {

			$data = array_merge($post, [
				'website_id' => $site['website_id'],
				'locale' => $site['locale'],
				'status' => 'Published',
				'duplicate' => true
			]);


			$pageData = $this->Posts->newEntity($data);

			if($this->Posts->save($pageData)) {
				$this->Flash->success(__('Post have been created.'));;
			} else{
		        $this->Flash->error(__('Post is not created.'));
			}

		}

        return $this->redirect(['action' => 'edit', $post_id]);
	}



}