<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Log\Log;

class AssetsController extends AppController
{
	public $helpers = [
		'Form' => ['templates' => 'admin-form'],
		'Paginator' => ['templates' => 'paginator-template']
	];

	public $paginate = [
		'order' => ['created' => 'DESC'],
		'limit' => 42
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->websiteSettingsAdmin();
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

	public function index() {
        $crumbs = $this->request->session()->read('Breadcrumbs');
        $this->request->session()->write('Breadcrumbs', array_splice($crumbs, 0, 2) + ['Assets' => '/admin/assets']);

		$query = $tag_id = null;
	    if(isset($this->request->query['tag_id']) && $this->request->query['tag_id']) {
			$tag_id = $this->request->query['tag_id'];
		    $query = $this->Assets->find()
		    	->matching('Tags', function ($q){
					return $q->where(['Tags.id' => $this->request->query['tag_id']]);
				}
			);
		}

	    $this->set('tags', $this->Assets->Tags->find('list')->order(['name' => 'ASC'])->toArray());
		$this->set('tag_id', $tag_id);
		$this->set('assets', $this->paginate($query));
	}

	public function filter() {
	    $this->viewBuilder()->layout('ajax');

		$query = $tag_id = null;
	    if(isset($this->request->query['tag_id']) && !empty($this->request->query['tag_id'])) {
			$tag_id = $this->request->query['tag_id'];
		    $query = $this->Assets->find()
		    	->matching('Tags', function ($q){
					return $q->where(['Tags.id' => $this->request->query['tag_id']]);
				}
			);
		}

		$this->paginate['limit'] = 12;

	    $this->set('tags', $this->Assets->Tags->find('list')->order(['name' => 'ASC'])->toArray());
		$this->set('tag_id', $tag_id);
		$this->set('assets', $this->paginate($query));
	}

    public function upload() {
	    $this->autoRender = false;
	    //Log::write('error', $this->request->data);
		$asset = $this->Assets->newEntity($this->request->data());
        if ($this->request->is(['post', 'json'])) {
            if ($this->Assets->save($asset)) {
	            $data = [
		            'success' => true,
		            'message' => __('Your asset has been saved.')
	            ];
                $this->Flash->success($data['message']);
            } else {
 	            $data = [
		            'success' => false,
		            'message' => __('Unable to upload asset.')
	            ];
                $this->Flash->error($data['message']);
            }
        } //else {
        	//return $this->redirect(['action' => 'filter']);

        $this->redirect($this->request->session()->read('Redirect'));

       // }
       // echo json_encode($data);
	    //$tags = $this->Assets->Tags->find('list')->order(['name' => 'ASC']);
 		//$this->set(compact('asset', 'tags'));
    }

    public function add() {
		$asset = $this->Assets->newEntity($this->request->data());
        if ($this->request->is('post')) {
            if ($this->Assets->save($asset)) {
                $this->Flash->success(__('Your asset has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your asset.'));
        }
	    $tags = $this->Assets->Tags->find('list')->order(['name' => 'ASC']);
 		$this->set(compact('asset', 'tags'));
    }

    public function edit($id = null) {
	    if(!$id) {
		    throw new NotFoundException(__('Invalid Asset'));
	    }
	    $asset = $this->Assets->find()
	    	->where(['Assets.id' => $id])
	    	->contain(['Tags'])
	    	->first();

	    if($this->request->is(['post','put'])) {
	        $this->Assets->patchEntity($asset, array($this->request->data()));
	        if ($this->Assets->save($asset)) {
	            $this->Flash->success(__('Your Asset has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your asset.'));
	    }

	    $data = $asset->toArray();
	    $selected_tags = Hash::combine($data['tags'], '{n}.id', '{n}.name');

	    $tags = $this->Assets->Tags->find('list')->order(['name' => 'ASC'])->toArray();
	    $available_tags = Hash::diff($this->Assets->Tags->find('list')->order(['name' => 'ASC'])->toArray(), $selected_tags);

 		$this->set(compact('asset', 'available_tags', 'selected_tags'));
    }

	public function delete($id) {
	    $this->request->allowMethod(['get', 'delete']);
	    $asset = $this->Assets->get($id);
	    if ($this->Assets->delete($asset)) {
	        $this->Flash->success(__('Asset deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}