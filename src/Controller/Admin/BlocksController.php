<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Core\Configure;
use App\Controller\WebsitesController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Utility\Hash;

class BlocksController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->websiteSettingsAdmin();
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

	public function add($page_id = null){
	    if(!$page_id) {
		    throw new NotFoundException(__('Invalid Page'));
	    }
	    if(!isset($this->request->data['block_type_id'])) {
		    print_r($this->request);
		    die();
		    return $this->redirect(['controller' => 'pages', 'action' => 'edit', $page_id]);
	    }
	    $this->request->session()->write('Redirect', '/admin/blocks/add/'.$page_id);

		$type = $this->Blocks->BlockTypes->findById($this->request->data['block_type_id'])->first();

		$block = $this->Blocks->newEntity($this->request->data());//pr($block);
		if($this->request->is('post') && isset($this->request->data['block'])) {
			if($this->Blocks->save($block)) {
				$this->Flash->success(__('{0} block has been saved', $type->name));
                return $this->redirect(['controller' => 'pages', 'action' => 'edit', $page_id]);
			}
			$this->Flash->error(__('Block could be saved, please try again.'));
		}

		if($type->form == 'posts')
			$this->set('categories', TableRegistry::get('Categories')->find('list')->toArray());

		if($type->form == 'submenu')
			$this->set('pages', $this->Blocks->Pages->find('list')->where(['parent_id IS NULL'])->toArray());

		if($type->form == 'link')
			$this->set('slugs', $this->Blocks->Pages->find('treeList', [
				'keyPath' => 'slug',
				'valuePath' => 'title',
				'spacer' => ' '
			])->toArray());

		// ensure active checkbox is ticked by default
		$block->active = true;

		$this->set('regions', $this->request->session()->read('Website.regions.'.$this->request->data['template']));
		$this->set('block', $block);
		$this->set('element', $type->form);
		$this->render($type->form);
	}

	public function edit($id = null) {

		//pr($this->request->data);die;
		if(!$id) {
			throw new NotFoundException(__('record not found'));
		}
	    $this->request->session()->write('Redirect', '/admin/blocks/edit/'.$id);

		$block = $this->Blocks->find()
			->where(['Blocks.id' => $id])
			->contain(['BlockTypes', 'Pages'])
			->first();
		if($this->request->is(['post', 'put'])) {
			$this->Blocks->patchEntity($block, $this->request->data());
			if($this->Blocks->save($block)) {
				$this->Flash->success(__('{0} block has been updated', $block->block_type->name));
				return $this->redirect(['controller' => 'pages', 'action' => 'edit', $block->page_id]);
			}
			$this->Flash->error(__('Block could be updated, please try again.'));
		}

		if($block->data)
		{
			$data = $block->data;
			// if  block type is gellery then ensure ordering
			if(isset($data['gallery'])) {
				$data['gallery'] = Hash::sort($data['gallery'], '{n}.order', 'asc');
			}
			$this->request->data['block'] = $data;
		}

		if($block->block_type->form == 'posts')
			$this->set('categories', TableRegistry::get('Categories')->find('list')->toArray());

		if($block->block_type->form == 'submenu')
			$this->set('pages', $this->Blocks->Pages->find('list')->where(['parent_id IS NULL'])->toArray());

		$this->set('regions', $this->request->session()->read('Website.regions.'.$block->page->template));
		$this->set('block', $block);
		$this->set('element', $block->block_type->form);
		$this->render($block->block_type->form);
	}

	public function delete($id=null){
		$this->request->allowMethod(['get', 'delete']);
		$block = $this->Blocks->get($id);
		if($this->Blocks->delete($block)) {
			$this->Flash->success(__('Record has been deleted'));
			$this->redirect(['controller' => 'pages', 'action' => 'edit', $block->page_id]);
		}
		$this->redirect(['controller' => 'pages', 'action' =>'index']);

	}

}