<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\Event\Event;

class TagsController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
         $this->websiteSettingsAdmin();
   }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

	public function index() {
        $crumbs = $this->request->session()->read('Breadcrumbs');
        $this->request->session()->write('Breadcrumbs.Tags', '/admin/tags');
		$this->set('tags', $this->Tags->find('all'));
	}

    public function add() {
		$tag = $this->Tags->newEntity($this->request->data());
        if ($this->request->is('post')) {
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__('Your tag has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your tag.'));
        }
 		$this->set('tag', $tag);
    }

    public function edit($id = null) {
	    if(!$id) {
		    throw new NotFoundException(__('Invalid Tag'));
	    }
	    $tag = $this->Tags->get($id);
	    if($this->request->is(['post','put'])) {
	        $this->Tags->patchEntity($tag, $this->request->data());
	        if ($this->Tags->save($tag)) {
	            $this->Flash->success(__('Your Tag has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your tag.'));
	    }
 		$this->set('tag', $tag);
    }

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $tag = $this->Tags->get($id);
	    if ($this->Tags->delete($tag)) {
	        $this->Flash->success(__('The tag with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}