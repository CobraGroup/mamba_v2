<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;


class PagesController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
		if($this->Auth->user('role') != 'Admin') {
			$this->Auth->config('authorize', ['Controller']);
        	$this->Auth->allow();
        	$this->Auth->deny(['duplicate']);
	    }
        $this->websiteSettingsAdmin();
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

	public function index() {
        $crumbs = $this->request->session()->read('Breadcrumbs');

        $website_id = $this->request->session()->read('Website.id');

		$settings = TableRegistry::get('Settings')->findByWebsiteId($website_id)->first();
        if(empty($settings)) {
	        $this->Flash->settings(__('Home page is not set.'), ['escape' => false]);
        }

        $this->request->session()->write('Breadcrumbs', array_splice($crumbs, 0, 2) + ['Pages' => '/admin/pages']);
        $pages = $this->Pages->find('threaded')
        	->select(['id', 'title', 'parent_id'])
        	->where(['website_id' => $website_id])
        	->order(['rank' => 'ASC']);

		//$menu = $this->Pages->menu($website_id);pr($menu);

		$this->set('pages', $pages);
	}

/**
 * Ordering implementd for 2 levels only
 */
	public function rank() {
    	$this->autoRender = false;
        if ($this->request->is('post')) {
	        $data = json_decode($this->request->data['rank'], true);
	        $newData = array();
	        $rank = 0;
	        foreach ($data as $i => $item) {
		        $parent_id = $item['id'];
		        array_push($newData, [
		        	'id' => $parent_id,
				    'parent_id' => null,
		        	'rank' => $rank++
		        ]);
		        if(isset($item['children'])) {
			        foreach($item['children'] as $j => $child) {
						array_push($newData, [
				        	'id' => $child['id'],
				        	'parent_id' => $parent_id,
				        	'rank' => $rank++
				        ]);
			        }
		        }
	        }

	        if($newData) {
		        $entities = $this->Pages->find('all')
		        	->select(['id', 'parent_id', 'rank'])
		        	->where(['website_id' => $this->request->session()->read('Website.id')]);

		        $this->Pages->patchEntities($entities, $newData);
	        	foreach($entities as $entity) {
		        	$this->Pages->save($entity);
	        	}
	        	$this->Flash->success(__('Page order updated.'));
			}
	    }
	    return $this->redirect(['action' => 'index']);
	}

    public function add() {

		$page = $this->Pages->newEntity($this->request->data());

        if ($this->request->is('post')) {
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('Your page has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your page.'));
        }
 	    $this->set('parents', $this->Pages->find('treeList')->where(['website_id' => $this->request->session()->read('Website.id')]));
 	    $this->set('website_id', $this->request->session()->read('Website.id'));
 	    $this->set('status', array('Published','Draft','Trash'));
 		$this->set('templates', $this->request->session()->read('Website.templates'));
		$this->set('page', $page);
    }

    public function edit($id = null) {
	    if(!$id) {
		    throw new NotFoundException(__('Invalid Page'));
	    }
	    $page = $this->Pages->get($id);

	    if($this->request->is(['post','put'])) {
	        $this->Pages->patchEntity($page, $this->request->data());
	        if ($this->Pages->save($page)) {
	            $this->Flash->success(__('Your Page has been updated.'));
	            #return $this->redirect(['action' => 'index']);
	        } else {
	        	$this->Flash->error(__('Unable to update your page.'));
	        }
	    }
		$regions_blocks = $this->Pages->Blocks
			->find('list', ['groupField' => 'region', 'keyField' => 'id', 'valueField' => 'data'])
			->where(['page_id' => $id])
			->order(['rank' => 'ASC'])
			->toArray();

		$parents = $this->Pages
			->find('treeList', ['spacer' => '- '])
			->where(['website_id' => $this->request->session()->read('Website.id')])
			->toArray();

 	    $this->set('status', array('Published','Draft','Trash'));
		$this->set('templates', $this->request->session()->read('Website.templates'));
		$this->set('regions', $this->request->session()->read('Website.regions.'.$page->template));
		$this->set('block_types', TableRegistry::get('BlockTypes')->find('list')->order(['name' => 'ASC'])->toArray());
		$this->set('languages', TableRegistry::get('Websites')->find('languages'));
	    $this->set(compact('page', 'regions_blocks', 'parents', 'posts'));
    }

	public function delete($id) {
	    $this->request->allowMethod(['get', 'delete']);
	    $page = $this->Pages->get($id);
	    if ($this->Pages->delete($page)) {
	        $this->Flash->success(__('"{0}" has been deleted.', h($page->title)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

	public function duplicate() {
		$this->autoRender = false;

		$page_id = $this->request->data['page_id'];

		$page = $this->Pages
			->findById($page_id)
			->select(['website_id', 'locale', 'type', 'title', 'slug', 'meta_title', 'meta_description', 'other_meta_tags', 'template', 'rank', 'status'])
			->first()
			->toArray();

		$blocks = $this->Pages->Blocks
			->findByPageId($page_id)
			->select(['block_type_id', 'data', 'region', 'style', 'active'])
			->all()
			->toArray();

		$blocks_data = null;
		foreach($blocks as $block) {
			$blocks_data[] = [
				'block_type_id' => $block->block_type_id,
				'data' => $block->data,
				'region' => $block->region,
				'style' => $block->style,
				'active' => $block->active,
				'duplicate' => true
			];
		}

		foreach($this->request->data['site'] as $site) {
			if($site['name'] == false) {
				continue;
			}
			$data = array_merge($page, [
				'website_id' => $site['website_id'],
				'locale' => $site['locale'],
				'status' => 'Draft',
				'duplicate' => true,
				'blocks' => $blocks_data
			]);
			$pageData = $this->Pages->newEntity($data, [
				'associated' => ['Blocks']
			]);
			$this->Pages->save($pageData);
		}
        $this->Flash->success(__('Pages have been created.'));
        return $this->redirect(['action' => 'edit', $page_id]);
	}

}