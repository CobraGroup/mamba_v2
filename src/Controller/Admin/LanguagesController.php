<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

class LanguagesController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
		if($this->Auth->user('role') != 'Admin') {
			$this->Auth->config('authorize', ['Controller']);
	    }
    }
    
    public function beforeRender(Event $event) {
    	//if(in_array($this->request->action, ['index', 'edit', 'add'])) {
    	if(!$this->request->session()->check('Breadcrumbs.Languages')) {
    		$this->request->session()->write("Breadcrumbs.Languages", "/admin/languages");
    	}
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }
    
    public function index() {
        $this->set('languages', $this->Languages->find('all')->order(['active' => 'DESC', 'name' => 'ASC']));
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid language'));
	    }
	    $language = $this->Languages->get($id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->Languages->patchEntity($language, $this->request->data);
	        if ($this->Languages->save($language)) {
	            $this->Flash->success(__('Your language has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your language.'));
	    }
	    $this->set('language', $language);
	}

    public function add() {
        $language = $this->Languages->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Languages->save($language)) {
                $this->Flash->success(__('The language has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the language.'));
        }
        $this->set('language', $language);
    }

/*
	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	
	    $language = $this->Languages->get($id);
	    if ($this->Languages->delete($language)) {
	        $this->Flash->success(__('The language with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}
*/

}