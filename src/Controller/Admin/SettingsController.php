<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\Event\Event;


class SettingsController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
		if($this->Auth->user('role') != 'Admin') {
			$this->Auth->config('authorize', ['Controller']);
	    }
        $this->websiteSettingsAdmin();
    }

    public function beforeRender(Event $event) {
	    $crumbs = $this->request->session()->read('Breadcrumbs');
        $this->set('breadcrumbs', array_splice($crumbs, 0, 2));
    }

    public function index() {
		$website = $this->request->session()->read('Website');

	    $settings = $this->Settings->findById($website['id'])->first();

	    if($this->request->is(['post','put'])) {
		    if($settings) {
			   $this->Settings->patchEntity($settings, $this->request->data());
		    } else {
		       $settings = $this->Settings->newEntity($this->request->data());
		    }
	        if ($this->Settings->save($settings)) {
	            $this->Flash->success(__('Settings have been updated.'));
	        } else {
	        	$this->Flash->error(__('Unable to update website settings.'));
	        }
	        #pr($settings->errors());
	    }

	    $pages = $this->Settings->Pages->find('treeList', ['spacer' => '-'])
	    	->where(['website_id' => $website['id']])
	    	->order(['rank' => 'ASC']);

	    $this->set('languages', $website['languages']);
	    $this->set('website_id', $website['id']);
	    $this->set(compact('settings', 'pages'));
    }

    public function news() {
		$this->index();
    }

    public function blog() {
		$this->index();
    }

}