<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\ORM\Entity;

class WebsitesController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

/*
    public function initialize() {

	}
*/

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->request->session()->delete('Website');
        $this->request->session()->delete('Breadcrumbs.Pages');

		if($this->Auth->user('role') != 'Admin') {
			$this->Auth->config('authorize', ['Controller']);
        	$this->Auth->allow(['view', 'country']);
	    }
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

    public function country($id = null, $slug = 'en') {
    	$this->autoRender = false;
    	if(!is_numeric($id)) {
	        throw new NotFoundException(__('Website not found'));
    	}
    	$data = $this->Websites->find()
		    	->where(['Websites.id' => $id])
		    	->contain(['Languages','Companies'])
		    	->first();

		if($data) {
			$website = $data->toArray();
			$languages = Hash::combine($website['languages'], '{n}.locale', '{n}.name');
			if(count($website['languages']) > 1) {
				$website['languages'] = Hash::extract($website['languages'], sprintf('{n}[slug=%s]', $slug));
			}
			$language = current($website['languages']);
			$templates = json_decode($website['company']['templates'],true);
			$this->request->session()->write('Website', [
				'id' => $website['id'],
				'company_id' => $website['company_id'],
				'name' => $website['name'],
				'theme' => $website['company']['theme'],
				'datasource' => $website['company']['datasource'],
				'locale' => $language['locale'],
				'templates' => Hash::combine($templates, '{s}.template', '{s}.name'),
				'regions' => Hash::combine($templates, '{s}.template', '{s}.regions'),
				'languages' => $languages
			]);
			#pr($this->request->session()->read('Website'));die;
			return $this->redirect(['controller' => 'pages', 'action' => 'index']);
		}
        $this->Flash->error(__('Website not found.'));
		$this->redirect(['controller' => 'companies', 'action' => 'index']);
	}

    public function index() {
    	$this->request->session()->write('Breadcrumbs', [
    		'Home' => '/admin/companies',
    		'Admin' => '/admin/websites'
    	]);
    }

    public function view($company_id) {

        $company = $this->Websites->Companies->get($company_id);
    	$this->request->session()->write("Breadcrumbs.{$company->name}", "/admin/websites/view/{$company_id}");

        $websites = $this->Websites->find()
        	->where(['company_id' => $company_id])
        	->contain(['Countries', 'Languages'])
        	->order(['country' => 'ASC']);
        if($this->Auth->user('role') != 'Admin') {
	    	$website_ids = $this->Websites->UsersWebsites
	    		->findByUserId($this->Auth->user('id'))
	    		->combine('id', 'website_id')
	    		->toArray();

	    	$websites = $websites->where(['Websites.id IN' => $website_ids]);
        }
        $this->set(compact('websites', 'company'));
    }

    public function add($company_id = null) {
    	if(!$company_id) {
	        throw new NotFoundException(__('Invalid website'));
    	}
        $website = $this->Websites->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Websites->save($website)) {
                $this->Flash->success(__('Your website has been saved.'));
                return $this->redirect(['action' => 'view', $website->company_id]);
            }
            $this->Flash->error(__('Unable to add website.'));
        }
	    $languages = $this->Websites->Languages->find('list')->where(['Languages.active' => 1])
	    	->order(['name' => 'ASC']);
	    $countries = $this->Websites->Countries->find('list', ['keyField' => 'id', 'valueField' => 'country'])
	    	->where(['active' => true])
	    	->order(['country' => 'ASC']);

	    $this->set('countries', $countries->toArray());
	    $this->set('languages', $languages->toArray());
 	    $this->set('status', array('Published','Draft','Trash'));
		$this->set('themes', Configure::read('Themes'));
 		$this->set('company_id', $company_id);
 		$this->set('website', $website);
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid website'));
	    }
	    $website = $this->Websites->find()
	    	->where(['id' => $id])
	    	->contain(['Languages', 'Domains'])
	    	->first();

	    if ($this->request->is(['post', 'put'])) {
	        $this->Websites->patchEntity($website, $this->request->data);
	        if ($this->Websites->save($website)) {
	            $this->Flash->success(__('Website settings have been updated.'));
	            return $this->redirect(['action' => 'view', $website->company_id]);
	        }
	        $this->Flash->error(__('Unable to update your website.'));
	    }
	    $languages = $this->Websites->Languages->find('list')->where(['Languages.active' => 1])
	    	->order(['name' => 'ASC']);
	    $countries = $this->Websites->Countries->find('list', ['keyField' => 'id', 'valueField' => 'country'])
	    	->where(['active' => true])
	    	->order(['country' => 'ASC']);

	    #$domains =  $this->Websites->Domains->find('list')->where(['website_id' =>$website->id]);
	    #pr($domains->toArray());

	    $this->set('domains', $website->domains);
	    $this->set('countries', $countries->toArray());
	    $this->set('languages', $languages->toArray());
	    $this->set('status', array('Published','Draft','Trash'));
		$this->set('themes', Configure::read('Themes'));
	    $this->set('website', $website);
	}

	public function delete($id) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid website'));
	    }
	    $this->request->allowMethod(['post', 'delete']);
	    $website = $this->Websites->get($id);
	    if ($this->Websites->delete($website)) {
	        $this->Flash->success(__('The website with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}