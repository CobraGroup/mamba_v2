<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\Event\Event;

class CategoriesController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

	public function index() {
        $crumbs = $this->request->session()->read('Breadcrumbs');
        $this->request->session()->write('Breadcrumbs.Categories', '/admin/categories');
		$this->set('categories', $this->Categories->find('all'));
	}

    public function add() {
		$category = $this->Categories->newEntity($this->request->data());
        if ($this->request->is('post')) {
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('Your category has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your category.'));
        }
 		$this->set('category', $category);
    }

    public function edit($id = null) {
	    if(!$id) {
		    throw new NotFoundException(__('Invalid Category'));
	    }
	    $category = $this->Categories->get($id);
	    if($this->request->is(['post','put'])) {
	        $this->Categories->patchEntity($category, $this->request->data());
	        if ($this->Categories->save($category)) {
	            $this->Flash->success(__('Your Category has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your category.'));
	    }
 		$this->set('category', $category);
    }

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $category = $this->Categories->get($id);
	    if ($this->Categories->delete($category)) {
	        $this->Flash->success(__('The category with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}