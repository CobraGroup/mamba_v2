<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

class DomainsController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
     	if(!$this->request->session()->check('Breadcrumbs.Website Properties')) {
    		$this->request->session()->write("Breadcrumbs.Website Properties", "/admin/websites/edit/");
    	}
    }

    public function index() {
    	$this->request->session()->write('Breadcrumbs', array('Home' => '/admin/domains'));
        $this->set('domains', $this->Domains->find('all'));
    }

    public function view($website_id = null) {
	    if (!$website_id) {
	        throw new NotFoundException(__('Invalid website'));
	    }
        $domains = $this->Domains->find('all')->where('website_id', $website_id);
        $this->set(compact('domains'));
    }

    public function add($website_id = null) {
    	if(!$website_id) {
	    	throw new NotFoundException(__('Invalid website'));
    	}
        $domain = $this->Domains->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Domains->save($domain)) {
                $this->Flash->success(__('Your domain has been saved.'));
                return $this->redirect(array('controller' => 'websites', 'action' => 'edit', $website_id));
            }
            $this->Flash->error(__('Unable to add your domain.'));
        }
        $this->request->session()->write("Breadcrumbs.Website Properties", "/admin/websites/edit/{$website_id}");
 		$this->set('domain', $domain);
 		$this->set('website_id', $website_id);
    }

	public function edit($website_id = null) {
	    if (!$website_id) {
	        throw new NotFoundException(__('Invalid domain'));
	    }
	    $domain = $this->Domains->get($website_id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->Domains->patchEntity($domain, $this->request->data);
	        if ($this->Domains->save($domain)) {
	            $this->Flash->success(__('Your domain has been updated.'));
                return $this->redirect(array('controller' => 'websites', 'action' => 'edit', $domain->website_id));
	        }
	        $this->Flash->error(__('Unable to update your domain.'));
	    }
	    $this->request->session()->write("Breadcrumbs.Website Properties", "/admin/websites/edit/{$domain->website_id}");
	    $this->set('domain', $domain);
	}

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
		$domain = $this->Domains->get($id);
		if($this->Domains->delete($domain)) {
	        $this->Flash->success(__('Domain {0} has been deleted.', h($domain->name)));
            return $this->redirect(['controller' => 'websites', 'action' => 'edit', $domain->website_id]);
		}
	    $this->Flash->error(__('The domain with id: {0} has been deleted.', h($domain->id)));
		$this->redirect(['controller' => 'websites', 'action' => 'edit', $domain->website_id]);
	}

}