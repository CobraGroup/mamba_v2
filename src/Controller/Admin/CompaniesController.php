<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Core\Configure;

class CompaniesController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
		if($this->Auth->user('role') != 'Admin') {
			$this->Auth->config('authorize', ['Controller']);
        	$this->Auth->allow(['index']);
	    }
    	$this->request->session()->delete('Website');
    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

    public function index() {
    	$this->request->session()->write('Breadcrumbs', array('Home' => '/admin/companies'));
        $this->set('companies', $this->Companies->find('all'));
    }

    public function add() {
        $company = $this->Companies->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('Your company has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add  company.'));
        }
 		$this->set('company', $company);
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid company'));
	    }
	    $company = $this->Companies->find()->where(['id' => $id])->first();
	    if ($this->request->is(['post', 'put'])) {
	        $this->Companies->patchEntity($company, $this->request->data);
	        if ($this->Companies->save($company)) {
	            $this->Flash->success(__('Company settings have been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your company.'));
	    }
	    $this->set('company', $company);
	}

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $company = $this->Companies->get($id);
	    if ($this->Companies->delete($company)) {
	        $this->Flash->success(__('The company with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}