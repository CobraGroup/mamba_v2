<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class NewsController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form'],
		'Paginator' => ['templates' => 'paginator-template']
	];

	public $paginate = [
		'conditions' => ['status' => 'Published'],
		'order' => ['created' => 'DESC'],
		'limit' => 100
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->websiteSettingsAdmin();

    }

    public function beforeRender(Event $event) {
        $this->set('breadcrumbs', $this->request->session()->read('Breadcrumbs'));
    }

    public function index() {
        $crumbs = $this->request->session()->read('Breadcrumbs');
        $this->request->session()->write('Breadcrumbs', array_splice($crumbs, 0, 2) + ['News' => '/admin/news']);
        $this->set('articles', $this->paginate());
    }

    public function draft() {
        $this->paginate = ['conditions' => ['status' => 'Draft']];
        $this->set('articles', $this->paginate());
    }

    public function unpublished() {
        $this->paginate = ['conditions' => ['status' => 'Unpublished']];
        $this->set('articles', $this->paginate());
    }

    public function add() {
        $news = $this->News->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->News->save($news)) {
                $this->Flash->success(__('Your news has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your news.'));
        }
 	    $this->set('status', ['Published','Draft', 'Unpublished']);
 		$this->set('news', $news);
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid news'));
	    }
	    $news = $this->News->get($id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->News->patchEntity($news, $this->request->data);
	        if ($this->News->save($news)) {
	            $this->Flash->success(__('Your news has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your news.'));
	    }

		$languages = TableRegistry::get('Websites')->find('languages');

		foreach($languages as $language)
		{
			$exists[] = $this->News
						->findByTitle($news['title'])
						->select(['locale', 'title', 'website_id'])
						->first()->toArray();
		}


		$this->set('exists',$exists);
 	    $this->set('status', ['Published','Draft', 'Unpublished']);
	    $this->set('news', $news);
 		$this->set('languages',$languages);
	}

	public function delete($id) {
	    $this->request->allowMethod(['get', 'delete']);
	    $news = $this->News->get($id);
	    if ($this->News->delete($news)) {
	        $this->Flash->success(__('The news with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

	public function duplicate(){
		$this->autoRender = false;

		$news_id = $this->request->data['news_id'];

		$news = $this->News
				->findById($news_id)
				->select(['website_id', 'locale', 'title', 'slug', 'body', 'image', 'thumb', 'slug_title', 'excerpt', 'meta_title', 'meta_description', 'other_meta_tags', 'status'])
				->first()
				->toArray();

		foreach($this->request->data['site'] as $site) {
			if($site['name'] == false){
				continue;
			}

			$data = array_merge($news, [
				'website_id' => $site['website_id'],
				'locale' => $site['locale'],
				'status' => 'Draft',
				'duplicate' => true
			]);

			$pageData = $this->News->newEntity($data);

			if($this->News->save($pageData)) {
				$this->Flash->success(__('News have been created.'));;
			} else {
		        $this->Flash->error(__('News is not created.'));
			}

		}

		return $this->redirect(['action' => 'edit', $news_id]);
	}
}