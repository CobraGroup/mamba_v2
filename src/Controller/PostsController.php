<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class PostsController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'view']);
    }

    public function index() {
		$posts = $this->Posts->find('published');
        $this->set('posts', $posts);

    }

    public function view($category = null) {
    	$website = Configure::read('Website');
        if (!isset($this->request->params['slug'])) {
            throw new NotFoundException(__('Invalid post'));
        }

		if(!$this->Posts->find()
			->where(['Posts.slug' => DS.$category.DS.$this->request->params['slug'], 'Posts.status' => 'Published'])
			->contain(['Categories'])
			->first()
			->toArray()) {
			throw new NotFoundException('Sorry.. Post not found..!');
		} else 	{
			$post = $this->Posts->find()
			->where(['Posts.slug' => DS.$category.DS.$this->request->params['slug'], 'Posts.status' => 'Published'])
			->contain(['Categories'])
			->first()
			->toArray();
		}

		$this->set('website', $website);
        $this->set('post', $post);
        $this->render('post-with-left-submenu');
    }



}