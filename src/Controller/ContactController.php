<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use App\Form\ContactForm;

class ContactController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Security');
		$this->loadComponent('RequestHandler');
	}
   
    public function beforeFilter(Event $event)
    {
		parent::beforeFilter($event);
		$this->Security->config('unlockedActions', ['index']);
		$this->Auth->allow(['index']);
    }

    public function index()
    {
        $contact = new ContactForm();
        
        if ($this->request->is('json'))
        {
            $contact->theme = $this->viewBuilder()->theme();
            
            if ($contact->execute($this->request->data))
            {
                $data['success'] = true;
            }
            else
            {
				$data['success'] = false;
                $data['errors'] = $contact->errors();
            }
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

}