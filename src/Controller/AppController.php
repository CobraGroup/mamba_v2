<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Utility\Hash;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
		parent::initialize();
		$this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
		$this->loadComponent('Csrf');
		$this->loadComponent('Cookie');
		$this->loadComponent('Auth', [
			'loginAction' => '/login', //['controller' => 'Users', 'action' => 'login', 'prefix' => false],
			'loginRedirect' => '/admin/companies',
			'logoutRedirect' => ['controller' => 'Users', 'action' => 'login', 'prefix' => false],
			'authError' => 'Did you really think you are allowed to see that?',
			//'authorize' => ['Controller']
		]);
    }

	public function beforeFilter(Event $event)
	{
		if(isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin')
		{
			$this->viewBuilder()->layout('admin');

			if(!$this->request->session()->check('Breadcrumbs'))
				return $this->redirect('/admin/companies');
		}
		elseif(in_array($this->request->action, ['login', 'logout']))
		{
			$this->viewBuilder()->layout('login');
		}
		else
		{
			$this->websiteSettings();
			//$this->setLanguage();
		}

		$this->Auth->allow(['display']);
		$role = $this->Auth->user('role');
		$is_admin = 'Admin' == $role ? true : false;

	    $this->set(compact('role', 'is_admin'));
	}

    public function isAuthorized($user)
    {
	    if(env('HTTP_HOST') == LOGIN_URL)
		    return false;

        if (empty($this->request->params['prefix']))
            return true;

        if ($this->request->params['prefix'] === 'Admin')
        {
            $this->set('is_admin', true);
            return (bool)($user['role'] === 'Admin');
        }

        return false;
    }

/**
 * Session Webiste, set in /admin/websites/country
 */
    public function websiteSettingsAdmin() {
        if(!$this->request->session()->check('Website')) {//die;
	    	return $this->redirect('/admin/companies');
        }
		$website = $this->request->session()->read('Website');
		Configure::write('Website', $website); //pr(Configure::read('Website'));
		// connect to database
		//$conn = ConnectionManager::get($website['datasource']);
		//TableRegistry::get($this->request->controller)->connection($conn);
		I18n::locale($website['locale']);
    }

    protected function websiteSettings()
    {
    	$domain_name =  env('HTTP_HOST');

		if(Configure::read('debug'))
			Cache::delete($domain_name, 'domains');

		if(!($data = Cache::read($domain_name, 'domains')))
		{
			$domain = TableRegistry::get('Domains')
				->findByName($domain_name)
				->first();

		    $website = TableRegistry::get('Websites')
		    	->find()
		    	->where(['Websites.id' => $domain->website_id])
		    	->contain(['Languages', 'Companies'])
		    	->first()
		    	->toArray();

			$data = [
				'id' => $website['id'],
				'name' => $website['name'],
				'domain' => $domain->name,
				'theme' => $website['company']['theme'],
				//'templates' => json_decode($website['company']['templates']),
				'datasource' => $website['company']['datasource'],
				'analytics' => $website['analytics'],
				'languages' => $website['languages']
			];

			// must write to cache for SettingsTable to fetch datasource
			Cache::write($domain_name, $data, 'domains');

		    $settings = TableRegistry::get('Settings')
		    	->findByWebsiteId($domain->website_id)
		    	->first();

			$default = [
				'home_page_id' => null,
				'site_title' => null,
				'logo' => null,
				'news_posts_per_page' => 10
			];
			if($settings) {
			    $data = array_merge($data, array(
					'home_page_id' => $settings->home_page_id,
					'locale' => $settings->default_language,
					'site_title' => $settings->site_title,
					'logo' => $settings->site_logo,
					'news_posts_per_page' => $settings->news_posts_per_page,
					'image' => $settings->image,
					'most_read_limit' => $settings->most_read_limit,
					'most_read_order' => $settings->most_read_order
			    ));
		    } else {
			    $data = array_merge($data, $default);
		    }

			$categories = TableRegistry::get('Categories')
				->find('list', ['valueField'=> 'slug'])
				->toArray();

		    $menu = null;//TableRegistry::get('Pages')->menu($domain->website_id);pr($menu);die;

			$data = array_merge($data, [
				'categories' => $categories,
				'menu' => $menu
			]);

		    // overwrite cache with settings data
			Cache::write($domain_name, $data, 'domains');
		}

		// side-wide access to data
		Configure::write('Website', $data);

		// set theme
    	$this->viewBuilder()->theme($data['theme']);

		// set language
    	I18n::locale($data['locale']);

		// set vars for views
	    $this->set([
	    	'languages' => $data['languages'],
	    	'logo' => $data['logo'],
	    	'site_title' => $data['site_title']
	    ]);
    }

    protected function setLanguage() {
        $lang = null;
        if(isset($this->request->params['lang'])) {
        	$language = current(Hash::extract($this->activeLanguages(), '{n}[slug='.$this->request->params['lang'].']'));
        	if($this->request->params['lang'] == $language['slug']) {
	        	I18n::locale($language['locale']);
        	} else {
	        	$this->redirect('/');
        	}
	        #$this->request->seesion()->write('lang', $language['slug']);
			$lang = DS.$language['slug'];
        }
	    $this->set('lang', $lang);
    }

    protected function activeLanguages() {
    	#Cache::delete('languages', 'domains');
		if(($data = Cache::read('languages', 'domains')) === false) {
		    $languages = TableRegistry::get('Languages')->find('all')->where(['active' => true]);
		    $data = array();
		    foreach($languages as $language) {
			    $data[] = array(
			    	'name' => $language->name,
			    	'country' => $language->country,
			    	'locale' => $language->locale,
			    	'slug' => $language->slug,
			    );
		    }
		    Cache::write('languages', $data, 'domains');
		}
		return $data;
    }

}
