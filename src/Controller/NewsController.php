<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class NewsController extends AppController {

	public $helpers = [
		//'Paginator' => ['templates' => 'paginator-templates.php']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'view']);
    }

    public function index() {
    	$website = Configure::read('Website');
		if(!isset($website['most_read_limit'])) {
			$website['most_read_limit'] = 5;
		}
		$this->paginate = [
			'conditions' => ['status' => 'Published'],
			'order' => ['created' => 'DESC'],
			'limit' => $website['news_posts_per_page']
		];

		$most_read = $this->News
			->find('popular')
			->order(['read_count' => $website['most_read_order']])
			->limit($website['most_read_limit'])
			->toArray();

		$breadcrumbs = ['<a href="/">Home</a>', 'News'];
		
		if($website["locale"]=="en") $this->set('page_title', 'Latest News');

		$this->set('website', $website);
		$this->set('breadcrumbs', $breadcrumbs);
		$this->set('news', $this->paginate());
		$this->set('most_read', $most_read);
		$this->set('banner', $website['image']);
    }

    public function view() {
        if (!isset($this->request->params['slug'])) {
            throw new NotFoundException(__('Invalid news'));
        }
		if(strpos($_SERVER['SERVER_NAME'], '.ca')) {
			$preslug = '/news-and-blog';
		} else {
			$preslug = '/news';
		}

        $news = $this->News
        	->findBySlugAndStatus($preslug.'/'.$this->request->params['slug'], 'Published')
        	->first();
        if($website["locale"]=="en") $this->set('page_title', $news->title);

    	$website = Configure::read('Website');
		$this->set('website', $website);
		//echo "<pre>"; var_dump($website['most_read_order']); echo "</pre>";
		if(!isset($website['most_read_limit'])) {
			$website['most_read_limit'] = 4;
		}
/*
 		$most_read = $this->News
			->find('popular', ['count' => $website['most_read_order'], 'limit' => $website['most_read_limit']])
			->toArray();
*/
		$most_read = $this->News
			->find('popular')
			->order(['read_count' => $website['most_read_order']])
			->limit($website['most_read_limit'])
			->toArray();
		$this->set(compact('news', 'most_read'));
    }

}