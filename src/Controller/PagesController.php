<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingViewException;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Utility\Xml;

use Cake\Cache\Cache;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'view']);
    }

	/**
	 * Front page
	 */
    public function index() {
    	$website = Configure::read('Website');

    	if(!$website['home_page_id']) {
	    	throw new NotFoundException('Home page not set!!');
    	}
		$page = $this->Pages
			->findByIdAndWebsiteId($website['home_page_id'], $website['id'])
			->first();
		//public $layout = 'Appco';
		//$this->viewBuilder()->layout('Appco');
		
		$data = $this->blocks($page->id);
		$menu = $this->Pages->menu($website['id']);
		$breadcrumbs = $this->Pages->breadcrumbs;
		$body_class = $page->style;
		$this->set('categories', TableRegistry::get('Categories')->find('list')->toArray());
		$this->request->session()->write('Breadcrumbs', $this->Pages->breadcrumbs);

		if($website['id']==1){
			$news_text = file_get_contents("http://staging.cms.cobragroup.com/cobra-group-news/feed/");
			$news_xml = Xml::build($news_text);
			$count_news = 0;
			foreach($news_xml->channel->item as $news_item){
				$companynews[$count_news] = $news_item;
				$count_news++;
				if($count_news==4) break;
			}
			$cnblog_text = file_get_contents("http://www.chrisniarchos.org/feed/");
			$cnblog_xml = Xml::build($cnblog_text);
			$count_cnblog = 0;
			foreach($cnblog_xml->channel->item as $cnblog_item){
				$cnblogposts[$count_cnblog] = $cnblog_item;
				$count_cnblog++;
				if($count_cnblog==1) break;
			}
		}
		
		if($website["locale"]=="en") $this->set('page_title', "Face to Face Field Marketing | Direct Sales");

		$this->set(compact('website', 'page', 'data', 'menu', 'body_class', 'companynews', 'cnblogposts'));
		
        $this->render($page->template);

    }

/**
 * Other Pages
 */
    public function view() {
    	$website = Configure::read('Website');
		$this->set('website', $website);

		$parent_id = null;
		$parent_slug = "";

		$uri = $this->request->here;
		if(substr($uri,0,1)=="/") $uri = substr($uri,1);
		if(substr($uri,-1)=="/") $uri = substr($uri,0,-1);
		$uri_parts = explode("/", $uri);
		if(count($uri_parts)>1) $uri_parent = $uri_parts[0];
/*
		print_r($uri_parent);
		die();
*/

		$page = $this->Pages
			->findBySlugAndWebsiteId($this->request->params['slug'], $website['id'])
			->first();

/*
		if($page->parent_id != null){
			$parent_page = $this->Pages
				->findById($page->parent_id)
				->first();
			if($parent_page->parent_id==null){
				$parent_id = $parent_page->id;
				$parent_slug = $parent_page->slug;
			}else{
				$grandparent_page = $this->Pages
					->findById($parent_page->parent_id)
					->first();
				$parent_id = $grandparent_page->parent_id;
				$parent_slug = $grandparent_page->slug;
			}
		}
*/
/*
		print_r($page->parent_id);
		die();
*/

		if(!$page || $uri_parent!=$parent_slug){
			throw new NotFoundException('Sorry, page not found.');
		}elseif(isset($page->title)){
			$this->set('page_title', $page->title);
		}

		$this->set('menu', $this->Pages->menu($website['id']));
		$this->set('breadcrumbs', $this->Pages->breadcrumbs);
		$this->set('page', $page);
		$this->set('body_class', $page->style);
		$this->set('data', $this->blocks($page->id));

		if($page->id !== 10) {
			$this->set('shareddata', $this->sharedblocks(10));
		}

		$this->render($page->template);
    }

/**
 * Get Blocks
 */
	protected function sharedblocks($page_id = null){
		return $this->Pages->Blocks
			->find('list', ['groupField' => 'region', 'keyField' => 'id', 'valueField' => 'data'])
			->where(['page_id' => $page_id, 'active' => true, 'block_type_id' => 12])
			->order(['rank' => 'ASC'])
			->toArray();
	}

/**
 * Get Blocks
 */
	protected function blocks($page_id = null){
		return $this->Pages->Blocks
			->find('list', ['groupField' => 'region', 'keyField' => 'id', 'valueField' => 'data'])
			->where(['page_id' => $page_id, 'active' => true])
			->order(['rank' => 'ASC'])
			->toArray();
	}

/**
 * Display static views
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		$this->set(compact('page', 'subpage'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}



}
