<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\Mailer\Email;

class ContactForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema
        	->addField('name', 'string')
            ->addField('email', ['type' => 'string'])
            ->addField('body', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator
        	->notEmpty('name','Please enter your name')
        	->notEmpty('message', 'Please enter your message')
            ->add('email', 'format', [
                'rule' => 'email',
                'message' => 'Please enter a valid email address',
            ]);
    }

    protected function _execute(array $data)
    {
		$email = new Email();
		$sent = $email  //->profile('default')
			->theme($this->theme)
			->template('contact')
			->emailFormat('text')
			->to('info@thelifewallcompany.com')
			->from($data['email'], $data['name'])
			->subject('Contact enquiry')
			->viewVars(compact('data'))
			->send();
			
        return true;
    }
    
}