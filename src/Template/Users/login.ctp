<div class="col-lg-6 col-lg-offset-3 login">
	<div class="panel">
		<div class="panel-body">
	<?= $this->Flash->render('auth') ?>
	<?= $this->Form->create('Users') ?>
	    <fieldset>
	        <legend><?= __('Please enter your username and password') ?></legend>
	        <?= $this->Form->input('username') ?>
	        <?= $this->Form->input('password') ?>
	    </fieldset>
	<?= $this->Form->button(__('Login'), ['class' => 'btn btn-primary']); ?>
	<?= $this->Form->end() ?>
		</div>
	</div>
</div>
<?php $this->start('script-validate'); ?>

	<?= $this->Html->script('jquery.validate.min') ?>
	<script>
	$().ready(function(){

		$('.login form').validate({
			rules: {
				username: "required",
				password: "required"
			},
			messages: {
				username: "Please enter your username",
				password: "Please enter your password"
			},
			submitHandler: function(form) {
				$.ajax({
					url: '/login',
					method: 'POST',
					dataType: 'json',
					cache: false,
					beforeSend: function(xhr) {
						xhr.setRequestHeader('X-CSRF-Token', '<?= $this->request->param('_csrfToken'); ?>');
					},

				});
				return false;
			}
		});

	});
	</script>

<?php $this->end(); ?>