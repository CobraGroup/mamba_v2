<?php 
	if(isset($crumb)) {
		$breadcrumbs = array_merge($breadcrumbs, $crumb);
	}

	foreach($breadcrumbs as $title => $url): 
?>
	<li><?= $this->Html->link($title, $url) ?></li>
<?php endforeach ?>
