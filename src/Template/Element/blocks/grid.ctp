
<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<?php foreach($data['gallery'] as $key => $title):?>
		    <div class="col-md-4">
				<?= $this->Html->image($title['image'], ['alt' => $title['alternative_text'], 'class' => 'img-responsive']) ?>
		    </div>
			<?php endforeach; ?>
		</div>

		<br />

		<?= $this->Html->link('Edit Gallery', ['controller' => 'blocks', 'action' => 'edit', $id], ['class' => 'btn btn-primary btn-xs']) ?>

	</div>
</div>
