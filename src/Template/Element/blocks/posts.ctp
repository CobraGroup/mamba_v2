<div class="panel panel-default">
	<div class="panel-body">

		<?php if($data['title']) echo $this->Html->tag($data['tag'], $data['title']) ?>
		<div class="row">
			<?php foreach($data['posts'] as $post): ?>
			<div class="col-lg-4">
				<div class="thumbnail">
					<?php if($post->thumb) echo $this->Html->image($post->thumb) ?>
					<div class="caption">
						<?= $this->Html->link($post->title, ['controller' => 'Posts', 'action' => 'edit', $post->id]); ?>
					</div>
				</div>
			</div>		
			<?php endforeach; ?>
		</div>

		<?= $this->Html->link(__('Edit Posts Block'),
				['controller' => 'blocks', 'action' => 'edit', $id],
				['escape' => false, 'class' => 'btn btn-primary btn-xs']
			)
		?>

	</div>
</div>
