<div class="panel panel-default">
	<div class="panel-body">

		<div class="submenu">
			<?php if(isset($data['submenu'][0]['title'])) : ?><h2><?= $data['submenu'][0]['title'] ?></h2>  <?php endif; ?>
			<ul>
			<?php foreach($data['submenu'] as $i => $title): ?>
			<?php if(isset($title['title']) && $title['title'] !== $data['submenu'][0]['title']) : ?><li><?= $title['title'] ?></li>  <?php endif; ?>

			<?php if(isset($data['submenu']['sub'])) :?>
				<?php foreach($data['submenu']['sub'] as $sub): ?>
					<?php if(is_array($sub)) : ?>
							<?php if(isset($title['id'])) : ?>
								<ul class="subsubnav" style="padding-left:25px;">
								<?php foreach($sub as $k  => $sub): ?>
									<?php if($title['id'] === $sub['parent_id']) :?>

										<li><?= $sub['title'] ?></li>
									<?php endif; ?>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						<?php endif; ?>

				<?php endforeach; ?>
			<?php endif; ?>

			<?php endforeach; ?>
			</ul>
		</div>

		<?= $this->Html->link('Edit Submenu', ['controller' => 'blocks', 'action' => 'edit', $id], ['class' => 'btn btn-primary btn-xs']) ?>

	</div>
</div>