<div class="panel panel-default">
	<div class="panel-body">
		
		<?= $this->Html->tag($data['tag'], $data['title']) ?>		
		<p>
			<?= $this->Html->link(__('Edit Header'),
					['controller' => 'blocks', 'action' => 'edit', $id],
					['escape' => false, 'class' => 'btn btn-primary btn-xs']
				)
			?>
		</p>
	</div>
</div>