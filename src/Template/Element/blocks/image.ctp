<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-body">
			
			<p><?= $this->Html->link(
					$this->Html->image($data['image'], ['class' => 'img-responsive'] ),
					['controller' => 'blocks', 'action' => 'edit', $id],
					['escape' => false]
				)
			?></p>
			
			<?php if($data['title']): ?>
				<?= $data['title'] ?> <span class="badge"><?= $data['title_tag'] ?></span>
			<?php endif ?>
			
			<?= $data['caption'] ?>
		
		</div>
	</div>
</div>