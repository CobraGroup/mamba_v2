<div class="panel panel-default">
	<div class="panel-body">

		<ul class="<?= $data['slider_unique_key'] ?>">
			<?php foreach($data['slider'] as $key => $slide):?>
		    <li>
				<?= $this->Html->image($slide['image']) ?>
		    </li>
			<?php endforeach; ?>
		</ul>

		<br />
		
		<?= $this->Html->link('Edit Slider', ['controller' => 'blocks', 'action' => 'edit', $id], ['class' => 'btn btn-primary btn-xs']) ?>

	</div>
</div>


<?php $this->start('script-footer') ?>
<?=$this->Html->script('jquery.bxslider.min') ?>
<script>
$(document).ready(function(){
 	$('.<?= $data['slider_unique_key'] ?>').bxSlider({
		mode: '<?= $data['mode'] ?>',
		auto: true,
		speed: <?= $data['speed'] ?>,
		pager: false,
		controls: false,
		responsive: true
 	});
});
</script>
<?php $this->end() ?>
