<div class="panel panel-default">
	<div class="panel-body">
		
		<div class="col-md-4">
			
			<?php if($data['image']) echo $this->Html->image($data['image'], ['class' => 'img-responsive']) ?>
			
		</div>
		<div class="col-md-8">
			
			<<?= $data['tag'] ?>>
				<?= $data['name'] ?> <span style="font-weight: lighter; color: grey;"><?= $data['position'] ?></span>
			</<?= $data['tag'] ?>>
			
			<p><?= $data['caption'] ?></p>
			
			<p><?= $this->Html->link(__('Edit'), ['controller' => 'blocks', 'action' => 'edit', $id], ['class' => 'btn btn-primary btn-xs']) ?></p>
			
		</div>
			
	</div>
</div>
