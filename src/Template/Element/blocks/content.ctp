<div class="panel panel-default">
	<div class="panel-body">

		<?php if($data['image']) echo $this->Html->image($data['image'], ['class' => 'img-responsive']) ?>		
		
		
		<?php if($data['title']): ?>
			<<?= $data['tag'] ?>>
				<?= $data['title'] ?> <span style="font-weight: lighter; color: grey"><?= $data['subtitle'] ?></span>
			</<?= $data['tag'] ?>>
		<?php endif ?>

		
		<?= $data['content'] ?>
		
		<p>
			<?= $this->Html->link(__('Edit Content'),
					['controller' => 'blocks', 'action' => 'edit', $id],
					['escape' => false, 'class' => 'btn btn-primary btn-xs']
				)
			?>
		</p>
	</div>
</div>