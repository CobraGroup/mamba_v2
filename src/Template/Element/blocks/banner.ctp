<div class="panel panel-default">
	<?= $this->Html->link(
			$this->Html->image($data['image'], ['class' => 'img-responsive'] ),
			['controller' => 'blocks', 'action' => 'edit', $id],
			['escape' => false]
		)
	?>

</div>