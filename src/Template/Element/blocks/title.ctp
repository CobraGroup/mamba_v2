<div class="panel panel-default">
	<div class="panel-body">
		
		<?php if($data['title']): ?>
			<<?= $data['tag'] ?>>
				<?= $data['title'] ?> <span style="font-weight: lighter; color: grey"><?= $data['subtitle'] ?></span>
			</<?= $data['tag'] ?>>
		<?php endif ?>
		
		<p>
			<?= $this->Html->link(__('Edit Header'),
					['controller' => 'blocks', 'action' => 'edit', $id],
					['escape' => false, 'class' => 'btn btn-primary btn-xs']
				)
			?>
		</p>
	</div>
</div>