<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			
			<div class="col-lg-12">
				<h3>
					<?php if($data['order'] == 'recent'): ?>
						Recent News 
					<?php else: ?>
						Most Read New  
					<?php endif ?>
					<span class="label label-info">Limit <?= $data['limit'] ?></span>
				</h3>
			</div>		

			<?php foreach($data['list'] as $article): ?>
			<div class="col-lg-4">
				<div class="thumbnail">
					<?php if($article->thumb) echo $this->Html->image($article->thumb) ?>
					<div class="caption">
						<?= $this->Html->link($article->title, ['controller' => 'news', 'action' => 'edit', $article->id]); ?>
					</div>
				</div>
			</div>		
			<?php endforeach; ?>
		</div>

		<?= $this->Html->link(__('Edit News Block'),
				['controller' => 'blocks', 'action' => 'edit', $id],
				['escape' => false, 'class' => 'btn btn-primary btn-xs']
			)
		?>

	</div>
</div>