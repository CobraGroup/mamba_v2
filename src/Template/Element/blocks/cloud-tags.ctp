<div class="panel panel-default">
	<div class="panel-body">
		
		<p style="margin: 10px 0 20px;">
			<span style="padding: 10px; border: 1px solid grey; margin-right: 20px;"><?= $data['title'] ?></span>
			<strong>Link</strong>: <?= $data['link'] ? $data['link'] : '/'.$data['slug'] ?>
		</p>

		<?= $this->Html->link('Edit Link', ['controller' => 'blocks', 'action' => 'edit', $id], ['class' => 'btn btn-primary btn-xs']) ?>

	</div>
</div>