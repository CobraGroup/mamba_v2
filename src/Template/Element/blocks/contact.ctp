<div class="panel panel-default">
	<div class="panel-body">

		<?php if($data['title']) echo $this->Html->tag($data['tag'], $data['title']) ?>
		<?php if($data['text']) echo $this->Html->tag('p', $data['text']) ?>
				
		<?= $this->Html->link(__('Edit Contact Block'),
				['controller' => 'blocks', 'action' => 'edit', $id],
				['escape' => false, 'class' => 'btn btn-primary btn-xs']
			)
		?>

	</div>
</div>
