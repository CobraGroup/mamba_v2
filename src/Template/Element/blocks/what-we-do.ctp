
<?php foreach($news as $id => $title):?>
<li>
	<?php
		echo $this->Html->link($title['title'],
			['controller' => 'news', 'action' => 'edit', $title['id']],
			['escape' => false]
		);
	?>
    <div class="thumb">
		<?php
			echo $this->Html->link(
				$this->Html->image($title['image'], ['alt' => $title['captions'], 'class' => 'img-responsive'] ),
				['controller' => 'news', 'action' => 'edit',  $title['id']],
				['escape' => false]
			);
		?>
    </div>
    <div class="description">
        <div class="text">
			<?php
				echo $this->Html->link($this->Text->truncate($title['body'], 60, ['exact' => true, 'html' => false]),
					['controller' => 'news', 'action' => 'edit', $title['id']],
					['escape' => false]
				);

			?>
        </div>
    </div>
</li>
<?php endforeach; ?>
