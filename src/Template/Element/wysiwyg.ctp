<?php $this->append('script-footer') ?>
<script src="/tinymce/js/tinymce/tinymce.jquery.min.js"></script>
<script>
	tinymce.init({
		selector:'#body, #block-content, #excerpt',
		menubar : false,
		plugins: "searchreplace visualblocks,image, link, code, jbimages",
		toolbar: "undo redo | styleselect | bold italic underline | link | alignleft aligncenter alignright | bullist numlist outdent indent | removeformat | code | link image jbimages",
		schema: "html5",
		extended_valid_elements:"@[itemscope|itemtype|itemid|itemprop|content],div,span,time[datetime]"

	});

</script>
<?php $this->end() ?>