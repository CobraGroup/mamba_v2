	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">

			<a class="navbar-brand" href="/admin/companies">Cobra CMS</a>
			
			<div class="">
				<ul class="nav navbar-nav">
				<?php if($this->request->session()->check('Website')): ?>
					<li<?php if($this->request->controller == 'Pages'): ?> class="active"<?php endif; ?>>
						<a href="/admin/pages"><span class="glyphicon glyphicon-file"></span> <span class="hidden-xs">Pages</span></a>
					</li>
					<li<?php if(in_array($this->request->controller, array('News'))): ?> class="active"<?php endif; ?>>
						<a href="/admin/news"><span class="glyphicon glyphicon-pushpin"></span> <span class="hidden-xs">News</span></a>
					</li>
					<li<?php if(in_array($this->request->controller, array('Posts', 'Categories'))): ?> class="active"<?php endif; ?>>
						<a href="/admin/posts"><span class="glyphicon glyphicon-paperclip"></span> <span class="hidden-xs">Posts</span></a>
					</li>
					<li<?php if(in_array($this->request->controller, array('Assets'))): ?> class="active"<?php endif; ?>>
						<a href="/admin/assets"><span class="glyphicon glyphicon-picture"></span> <span class="hidden-xs">Assets</span></a>
					</li>
				<?php endif ?>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="/"><span class="glyphicon glyphicon-home"></span> <span class="sr-only">Home</span></a>
					</li>
					<li>
						<a href="/logout"><span class="glyphicon glyphicon-off"></span> <span class="sr-only">Logout</span></a>
					</li>
				</ul>

			</div>

		</div>
	</nav>
