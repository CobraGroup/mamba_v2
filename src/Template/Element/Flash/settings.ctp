<div class="alert alert-warning fade in" role="alert">
	<?= h($message) ?>
	<?= $this->Html->link(__('Update Settings'), ['controller' => 'Settings'], ['class' => 'btn btn-warning btn-xs pull-right']) ?>
</div>