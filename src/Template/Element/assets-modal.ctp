<div class="modal fade assets" tabindex="-1" role="dialog" aria-labelledby="Assets" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

		</div>
	</div>
</div>

<?php $this->append('script-footer') ?>
<script type="text/javascript">
	$().ready(function() {
		$('.image-assets').click(function(){
			$('.thumb').removeClass('thumb-active');
			$('.image').addClass('image-active');
			$('.assets').modal({
				backdrop: 'static',
				remote: '/admin/assets/filter'
			});
		});
		$('.thumb-assets').click(function(){
			$('.image').removeClass('image-active');
			$('.thumb').addClass('thumb-active');
			$('.assets').modal({
				backdrop: 'static',
				remote: '/admin/assets/filter'
			});
		});
	});
</script>
<?php $this->end() ?>