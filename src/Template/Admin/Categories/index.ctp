<?php 
	$this->start('header-title');
		echo __('Post Categories');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<div class="row">
	<div class="col-md-12">
	<p><?= $this->Html->link(
			'<span class="glyphicon glyphicon-plus"></span> ' . __('Add Category'), 
			['action' => 'add'], 
			['class' => 'btn btn-primary btn-xs', 'escape' => false]
	) ?></p>
	</div>
</div>

	<?php foreach ($categories as $category): ?>
		<?= $this->Html->link($category->name, ['action' => 'edit', $category->id], ['class' => 'btn btn-success']) ?>
	<?php endforeach; ?>

