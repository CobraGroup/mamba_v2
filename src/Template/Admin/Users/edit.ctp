<?php
	$this->start('header-title');
		echo __('Edit User');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Edit', array('class' => 'active'));
	$this->end();

	$this->start('header-right');
	$this->end();	
?>

<div class="row">

	<?= $this->Form->create($user, ['id' => 'users']) ?>
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Account Details') ?></div>
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('username') ?>
			</div>
		</div>
	
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Website Access Control') ?></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-5">
						<strong><?= __('Denied') ?></strong>
						<?= $this->Form->input('available_sites', [
								'label' => false,
								'multiple' => 'multiple',
								'options' => $available_sites
							]) 
						?>
					</div>
					<div class="col-lg-1" id="buttons">
						<p><a href="#" id="add" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a></p>
						<p><a href="#" id="remove" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-chevron-left"></span></a></p>
					</div>
					<div class="col-lg-5">
						<strong><?= __('Allowed') ?></strong>
						<?= $this->Form->input('websites._ids', [
								'label' => false,
								'multiple' => 'multiple',
								'options' => $selected_sites
							]) 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Save details') ?></div>
			<div class="panel-body">
				<?= $this->Form->input('role') ?>
				<?= $this->Form->input('blocked') ?>
				
				<?= $this->Form->button(
					'<span class="glyphicon glyphicon-save"></span> '.__('Save'),
					['escape' => false]
				) ?>
				<?= $this->Form->postLink(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					$url = '/admin/users/delete/'. $user->id,

					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				);
				?>
			</div>
		</div>
		<?= $this->Form->end() ?>
		
		<?= $this->Form->create($user) ?>
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Change Password') ?></div>
			<div class="panel-body">
				<?= $this->Form->input('password') ?>
				<?= $this->Form->button(
					'<span class="glyphicon glyphicon-save"></span> '.__('Update'),
					['class' => 'btn btn-danger', 'escape' => false]
				) ?>
			</div>
		</div>
		<?= $this->Form->end() ?>
	</div>
	

</div>


<?php $this->append('script') ?>
<script type="text/javascript">
	$().ready(function() {
        $('#add').click(function() {  
            return !$('#available-sites option:selected').remove().appendTo('#websites-ids');  
        });  
        $('#remove').click(function() {  
            return !$('#websites-ids option:selected').remove().appendTo('#available-sites');  
        });
        $('button').click(function(){
	        $('#websites-ids option').prop('selected',true);
	        $('#users').submit();
	        return true;
        });
        
        $('#available-sites, #websites-ids').css('height', '300px');
        $('#buttons').css('margin-top', '130px');
    });
</script>
<?php $this->end() ?>
