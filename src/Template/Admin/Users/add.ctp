<?php
	$this->start('header-title');
		echo __('Add User');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Add', array('class' => 'active'));
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->create($user) ?>
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('username') ?>
				<?= $this->Form->input('password') ?>
				<?= $this->Form->input('role') ?>
				<?= $this->Form->input('blocked') ?>
				<?= $this->Form->button(__('Save')) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>
