<?php
	$this->start('header-title');
		echo __('Users');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<p>
	<?= $this->Html->link('Add User', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?>
	<?= $this->Html->link('Roles', ['action' => 'roles'], ['class' => 'btn btn-xs btn-success']) ?>
</p>

<div class="panel panel-default">
	<table class="table table-striped">
		<thead>
			<?= $this->Html->tableHeaders(['Name', ['Username' => ['class' => 'hidden-xs']], 'Role', 'Last login']) ?>
		</thead>
		<tbody>
	    <?php foreach ($users as $user): ?>

	    <tr>
	        <td>
	            <?= $this->Html->link($user->name, ['action' => 'edit', $user->id]) ?><br />
	        </td>
	        <td class="hidden-xs">
	            <?= $user->username ?>
	        </td>
	        <td>
	            <span class="label label-<?= $user->blocked ? 'danger' : 'success' ?>"><?= $user->role ?></span>
	        </td>
	        <td>
	            <span class="label label-default"><?= $user->last_login ? $this->Time->timeAgoInWords($user->last_login) : 'Never' ?></span>
	        </td>
	    </tr>
	    <?php endforeach; ?>
		</tbody>
	</table>
</div>

<?php
	echo $this->Paginator->numbers();
?>
