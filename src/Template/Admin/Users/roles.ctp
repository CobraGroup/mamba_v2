<?php
	$this->start('header-title');
		echo __('Roles');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Roles', array('class' => 'active'));
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<div class="row">
	<div class="col-md-9">
		<div class="panel panel-default">
			<table class="table table-striped">
				<thead>
					<?= $this->Html->tableHeaders([['Role' => ['width' => '110']], 'Description']) ?>
				</thead>
				<tbody>
					<?= $this->Html->tableCells([
						['Admin', 'Create and delete pages, content, images, media files. Create and remove users, full editing and tagging rights.'],
						['Global editor', 'Upload and remove content and images. Approve pending country content submitted by country content writer, full editing and tagging rights.'],
						['Country editor', 'Create and upload for approval content and images.<br />
							Access to a specified country only.<br />
							Disabled option to view list of other countries.<br />
							Disabled option to tag / un-tag page for another country.'],
					]) ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">Key</div>
			<div class="panel-body">
				<h4><span class="label label-success">User Active</span></h4>
				<h4><span class="label label-danger">User Blocked</span></h4>
				<h4><span class="label label-default">Last Login</span></h4>
			</div>
		</div>
	</div>
</div>