<?php 
	$this->start('header-title');
		echo __('Media Tags');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<div class="row">
	<div class="col-md-12">
	<p>
		<?= $this->Html->link(
			'<span class="glyphicon glyphicon-plus"></span> ' . __('Add Tag'), 
			['action' => 'add'], 
			['class' => 'btn btn-primary btn-xs', 'escape' => false]
		) ?>
	</p>
	</div>
</div>

	<?php foreach ($tags as $tag): ?>
		<?= $this->Html->link($tag->name, ['action' => 'edit', $tag->id], ['class' => 'btn btn-success']) ?>
	<?php endforeach; ?>

