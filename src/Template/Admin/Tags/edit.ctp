<?php 
	$this->start('header-title');
		echo __('Media Manager');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Edit Asset', array('class' => 'active'));
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<?= $this->Form->create($tag) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>