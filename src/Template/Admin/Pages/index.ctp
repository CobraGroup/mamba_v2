<?php
	$this->start('header-title');
		echo $this->request->session()->read('Website.name');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end();

	$this->start('header-right');
	$this->end();

	$this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', ['block' => true]);
	$this->Html->script('//code.jquery.com/ui/1.10.4/jquery-ui.min.js', ['block' => true]);
	$this->Html->script('admin/jquery.nestable', ['block' => true, 'defer' => true]);
	$this->Html->css('admin/nestable', ['block' => true, 'defer' => true]);
?>

<div class="row">
	<div class="col-lg-9">
		<p><?= $this->Html->link('Add Page', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>

		<div class="cf nestable-lists">
		    <div class="dd" id="nestable">
		        <ol class="dd-list">
			    	<?php foreach ($pages as $page): ?>
		            <li class="dd-item dd3-item" data-id="<?= $page->id ?>">
		                <div class="dd-handle dd3-handle">Drag</div>
			            <div class="dd3-content"><?= $this->Html->link($page->title, ['action' => 'edit', $page->id]) ?></div>
			            <?php if($page->children): ?>
			            <ol class="dd-list">
					    	<?php foreach ($page->children as $child): ?>

				            <li class="dd-item dd3-item" data-id="<?= $child->id ?>">
				                <div class="dd-handle dd3-handle">Drag</div>
					            <div class="dd3-content"><?= $this->Html->link($child->title, ['action' => 'edit', $child->id]) ?></div>
				            </li>
				            <?php //pr($child); ?>
					    	<?php if(isset($child->children)) : ?>
							    <ol class="dd-list">
							    <?php foreach ($child->children as $subchild): ?>
							    <?php // pr($subchild); ?>
							            <li class="dd-item dd3-item" data-id="<?= $subchild->id ?>">
							                <div class="dd-handle dd3-handle">Drag</div>
								            <div class="dd3-content"><?= $this->Html->link($subchild->title, ['action' => 'edit', $subchild->id]) ?></div>
							            </li>
					         <?php endforeach; ?>
							    </ol>
					    <?php	endif; ?>
							<?php endforeach; ?>
			            </ol>
				        <?php endif; ?>
		            </li>
					<?php endforeach; ?>
		        </ol>
		    </div>
		</div>

		<?= $this->Form->create(null, ['action' => 'rank']) ?>
		<?= $this->Form->hidden('rank', ['id' => 'nestable-output']) ?>
		<p><?= $this->Form->button(
				'<span class="glyphicon glyphicon-save"></span> '.__('Save Page Order'),
				['class' => 'btn btn-primary'], ['escape' => false]
			)
		?></p>
		<?= $this->Form->end() ?>
	</div>

	<?php if(isset($is_admin)): ?>
	<div class="col-lg-3">

		<h3 style="color: white"><?= __('Settings') ?></h3>
		<ul class="list-group">
			<li class="list-group-item"><?= $this->Html->link(__('Site'), ['controller' => 'Settings', 'action' => 'index']) ?></li>
			<li class="list-group-item"><?= $this->Html->link(__('News'), ['controller' => 'Settings', 'action' => 'news']) ?></li>
			<!-- <li class="list-group-item"><?= $this->Html->link(__('Blog'), ['controller' => 'Settings', 'action' => 'blog']) ?></li> -->
		</ul>
	</div>
	<?php endif ?>

</div>


<script>
$(document).ready(function(){

    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));

        } else {
            output.val('JSON browser support required.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    updateOutput($('#nestable').data('output', $('#nestable-output')));

});
</script>
