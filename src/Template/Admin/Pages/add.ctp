<?php
	$this->start('header-title');
		echo $this->request->session()->read('Website.name');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Add Page'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<?= $this->Form->create($page) ?>
<?= $this->Form->hidden('website_id', ['value' => $website_id]) ?>
<div class="row">

	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="page-block">
					<?= $this->Form->input('title') ?>
					<?= $this->Form->input('slug') ?>
					<?= $this->Form->input('style', ['label' => 'Page Style']) ?>
					<?= $this->Form->input('template') ?>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">SEO Details</div>
			<div class="panel-body">
				<?= $this->Form->input('meta_title', ['rows' => '2']) ?>
				<?= $this->Form->input('meta_description', ['rows' => '2']) ?>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?php if($page->title != 'Home') echo $this->Form->input('parent_id') ?>
				<?= $this->Form->input('status', ['options' => array_combine($status,$status)]) ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-pencil"></span> '.__('Save'), ['escape' => false]) ?>
			</div>
		</div>
	</div>

</div>
<?= $this->Form->end() ?>
