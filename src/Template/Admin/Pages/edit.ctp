<?php
	$this->start('header-title');
		echo $this->request->session()->read('Website.name');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Edit Page'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();

	foreach ($regions_blocks as $region => $blocks) {
		$this->append($region);
			foreach ($blocks as $id => $data) {
				echo $this->element('blocks/'.$data['element'], ['id' => $id, 'data' => $data]);
			}
		$this->end();
	}
?>

<?= $this->Form->create($page) ?>
<div class="row">
	<div class="col-lg-8">

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="page-block">
					<?= $this->Form->input('title') ?>
					<?= $this->Form->input('slug') ?>
					<?= $this->Form->input('style', ['label' => 'Page Style']) ?>
				</div>
			</div>
		</div>

		<div class="blocks">
		<?php 
			foreach ($regions as $region => $header) {
				echo $this->Html->tag('h3', $header);
				echo $this->fetch($region);
			}
		?>			
		</div>

	</div>
	<div class="col-lg-4">

		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Publish') ?></div>
			<div class="panel-body">
				<?= $this->Form->input('parent_id', ['empty' => 'No Parent']) ?>
				<?= $this->Form->input('template', ['options' => $templates]) ?>
				<?= $this->Form->input('status', ['options' => array_combine($status,$status)]) ?>
				<?= $this->Form->input('show_in_menu') ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Update'), ['escape' => false]) ?>
				
				<?= $this->Html->link(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					['action' => 'delete', $page->id],
					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				); ?>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('meta_title', ['label' => 'SEO Title', 'rows' => '2']) ?>
				<?= $this->Form->input('meta_description', ['label' => 'SEO Description', 'rows' => '2']) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->create(null, ['url' => '/admin/blocks/add/'.$page->id.'/', 'id' => 'addBlock']) ?>
				<?= $this->Form->hidden('page_id', ['value' => $page->id]) ?>
				<?= $this->Form->hidden('template', ['value' => $page->template]) ?>
				<?= $this->Form->hidden('action', ['value' => 'Add']) ?>
				<?= $this->Form->input('block_type_id', ['options' => $block_types]) ?>
				<?= $this->Form->button('Add Block', ['class' => 'btn btn-warning']) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
		
		<?php if(isset($is_admin) && $languages): ?>
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Duplicate Page') ?></div>
			<div class="panel-body">
				<?= $this->Form->create(null, ['action' => 'duplicate']) ?>
				<?= $this->Form->hidden('page_id', ['value' => $page->id]) ?>
				<?php 
					$i = 0;
					foreach($languages as $language) {
						$i++;
						echo $this->Form->hidden("site.{$i}.website_id", ['value' => $language['website_id']]);
						echo $this->Form->hidden("site.{$i}.locale", ['value' => $language['locale']]);
						echo $this->Form->input("site.{$i}.name", ['type' => 'checkbox', 'label' => $language['name']]);
					}
				?>
				<?= $this->Form->button(__('Create Pages'), ['class' => 'btn btn-success']) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
		<?php endif ?>

	</div>
</div>

<script style="text/javascript">
$(document).ready(function(){
/*
	$('#template').change(function(){
		console.log($(this).val());
		$('#addBlock input[name=template]').val($(this).val());
	});
*/
});
</script>
