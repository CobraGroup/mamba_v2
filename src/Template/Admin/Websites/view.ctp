<?php 
	$this->start('header-title');
		echo $company->name . ' ' . __('Websites');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<?php if(isset($is_admin)): ?>
<div class="row">
	<div class="col-md-12">
	<p><?= $this->Html->link(
			'<span class="glyphicon glyphicon-plus"></span> ' . __('Add Country'), 
			['action' => 'add', $company->id], 
			['class' => 'btn btn-info btn-xs', 'escape' => false]
	) ?></p>
	</div>
</div>
<?php endif ?>

<?php foreach($websites as $website): ?>
<?php $this->append($website->country_id > 1 ? 'countries' : 'international') ?>
	<div class="btn-group">
		<button class="btn btn-<?= $website->live ? 'success' : 'default' ?> btn-lg dropdown-toggle" type="button" data-toggle="dropdown">
			<?= $website->country->country ?> <span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<?php foreach($website->languages as $language): ?>
			<li>
				<?= $this->Html->link(
					'<span class="glyphicon glyphicon-'.$language->slug.'"></span> ' . __($language->name),
					//$language->slug ? '/admin/languages/switcher/'.$language->slug : '/admin/pages',
					'/admin/websites/country/'.$website->id . ($language->slug ? DS.$language->slug : null),
					['escape' => false]
				) ?>
			</li>
			<?php endforeach ?>
			
			<?php if(isset($is_admin)): ?>
			<li class="divider"></li>
			<li>
				<?= $this->Html->link(
					'<span class="glyphicon glyphicon-cog"></span> ' . __('Website Properties'), 
					['action' => 'edit', $website->id],
					['escape' => false]
				) ?>
			</li>
			<?php endif ?>
			
		</ul>
	</div>
<?php $this->end() ?>
<?php endforeach ?>


	<?= $this->fetch('international') ?>
	<?= $this->fetch('countries') ?>
