<?php 
	$this->start('header-title');
		echo __('New Website Properties');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Add', array('class' => 'active'));
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<?= $this->Form->create($website) ?>
<?= $this->Form->hidden('company_id', ['value' => $company_id]) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('country_id') ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Languages</div>
			<div class="panel-body">
				<?= $this->Form->input('languages._ids', [
						'label' => false,
						'multiple' => 'checkbox',
						'options' => $languages,
					]) 
				?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('live') ?>
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Attributes</div>
			<div class="panel-body">
				<?= $this->Form->input('analytics', ['label' => 'Google Analytics']) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>