<?php 
	$this->start('header-title');
		echo __('Admin');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<div class="row">

	<div class="col-md-3">
		<div class="well">
			<h2><span class="glyphicon glyphicon-user"></span> Users</h2>
			<?= $this->Html->link('View all', ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-primary btn-block']); ?>
			<?= $this->Html->link('Add User', ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-primary btn-block']); ?>
			<?= $this->Html->link('Roles List', ['controller' => 'Users', 'action' => 'roles'], ['class' => 'btn btn-primary btn-block']); ?>
		</div>
	</div>

<!--
	<div class="col-md-3">
		<div class="well">
			<h2><span class="glyphicon glyphicon-picture"></span> Media</h2>
			<?= $this->Html->link('Asset Manager', ['controller' => 'Assets', 'action' => 'index'], ['class' => 'btn btn-primary btn-block']); ?>
			<?= $this->Html->link('Tags', ['controller' => 'Tags', 'action' => 'index'], ['class' => 'btn btn-primary btn-block']); ?>
		</div>
	</div>
-->

	<div class="col-md-3">
		<div class="well">
			<h2><span class="glyphicon glyphicon-globe"></span> Languages</h2>
			<?= $this->Html->link('List All', '/admin/languages', ['class' => 'btn btn-primary btn-block']); ?>
			<?= $this->Html->link('Add Language', '/admin/languages/add', ['class' => 'btn btn-primary btn-block']); ?>
		</div>
	</div>

</div>