<?php 
	$this->start('header-title');
		//echo __('{0} Website Properties', [$website->name]);
		echo $website->name;
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Website Properties', array('class' => 'active'));
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<?= $this->Form->create($website) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('country_id') ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Languages</div>
			<div class="panel-body">
				<?= $this->Form->input('languages._ids', [
						'label' => false,
						'multiple' => 'checkbox',
						'options' => $languages,
					]) 
				?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Domains</div>
			<div class="panel-body">
				<?= $this->Html->link('Add Domain', '/admin/domains/add/'.$website->id, ['class' => 'btn btn-xs btn-primary']) ?>
			</div>
				<ul class="list-group">
					<?php foreach($domains as $domain): ?>
					<li class="list-group-item"><?= $this->Html->link($domain->name, '/admin/domains/edit/'.$domain->id) ?></li>
					<?php endforeach ?>
				</ul>
		</div>
	</div>
	
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('live') ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> ' . __('Update'), ['escape' => false]) ?>
				<?= $this->Html->link(__('Cancel'), ['action' => 'view', $website->company_id], ['class' => 'btn btn-default']) ?>
				<?= $this->Html->link(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					['action' => 'delete', $website->id],
					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				); ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Attributes</div>
			<div class="panel-body">
				<?= $this->Form->input('analytics', ['label' => 'Google Analytics']) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>