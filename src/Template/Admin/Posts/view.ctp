<?php
	$this->start('header-title');
		echo $category->name;
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Category'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<p>
	<?= $this->Html->link('Add Post', ['action' => 'add', $category->id], ['class' => 'btn btn-xs btn-primary']) ?>
</p>


    <ul class="list-group">
    	<?php foreach ($posts as $post): ?>
        <li>
            <?= $this->Html->link($post->title, ['action' => 'edit', $post->id]) ?>
        </li>
        <td>
            <?= $this->Html->link($post->subtitle, ['action' => 'edit', $post->id]) ?>
        </td>
		<?php endforeach; ?>
    </ul>

    <p style="color: #fff"><em>Dev note: Implement drag & drop ordering</em></p>
