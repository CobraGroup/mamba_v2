<?php
	$this->start('header-title');
		echo __('Posts');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Published'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<p>
	<?= $this->Html->link('Add Post', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?>
	<?= $this->Html->link('Published', ['controller' => 'Categories', 'action' => 'index'], ['class' => 'btn btn-xs btn-success']) ?>
	<?= $this->Html->link('Draft', ['action' => 'draft'], ['class' => 'btn btn-xs btn-danger']) ?>
	<?= $this->Html->link('Unpublished', ['action' => 'unpublished'], ['class' => 'btn btn-xs btn-danger']) ?>
</p>

<div class="panel panel-default">
	<table class="table table-striped">
		<thead>
			<?= $this->Html->tableHeaders(['Title', 'Category', 'Status', 'Created']) ?>
		</thead>
	    <tbody>
	    <?php foreach ($posts as $post): ?>

	    	<?= $this->Html->tableCells([
		    		$this->Html->link($post->title, ['action' => 'edit', $post->id]),
		    		$this->Html->link($post->category->name, ['action' => 'view', $post->category->id]),
		    		$this->Html->link($post->status, ['action' => 'edit', $post->id]),
		    		$post->created->format('d.m.Y'),
		    		$this->Form->postLink(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					['action' => 'delete', $post->id],
					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
		    	])
		    ?>
	    <?php endforeach; ?>
	    </tbody>
	</table>
</div>

<nav>
	<ul class="pagination">
		<?= $this->Paginator->first(); ?>
		<?= $this->Paginator->numbers(); ?>
		<?= $this->Paginator->last(); ?>
	</ul>
</nav>
