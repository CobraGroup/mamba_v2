<?php
	$this->start('header-title');
		echo __('Unpublished Posts');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Unpublished'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<p>
	<?= $this->Html->link('Add Post', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?>
	<?= $this->Html->link('Published', ['action' => 'index'], ['class' => 'btn btn-xs btn-danger']) ?>
	<?= $this->Html->link('Draft', ['action' => 'draft'], ['class' => 'btn btn-xs btn-danger']) ?>
	<?= $this->Html->link('Unpublished', ['action' => 'unpublished'], ['class' => 'btn btn-xs btn-success']) ?>
</p>

<div class="panel panel-default">
	<table class="table table-striped">
		<thead>
			<?= $this->Html->tableHeaders(['Title', 'Category', 'Status','Created']) ?>
		</thead>
	    <tbody>
	    <?php foreach ($posts as $post): ?>
	    <tr>
	        <td>
	            <?= $this->Html->link($post->title, ['action' => 'edit', $post->id]) ?>
	        </td>
	        <td>
	            <?= $this->Html->link($post->subtitle, ['action' => 'edit', $post->id]) ?>
	        </td>
	        <td>
	        	<?= $this->Html->link($post->status, ['action' => 'edit', $post->id]) ?>
	        </td>
	        <td>
	            <?= $this->Html->link($post->category->name, ['action' => 'view', $post->category->id]) ?>
	        </td>
	        <td>
	            <?= $post->created->format('d.m.Y') ?>
	        </td>
	    </tr>
	    <?php endforeach; ?>
	    </tbody>
	</table>
</div>

<nav>
	<ul class="pagination">
		<?= $this->Paginator->first(); ?>
		<?= $this->Paginator->numbers(); ?>
		<?= $this->Paginator->last(); ?>
	</ul>
</nav>