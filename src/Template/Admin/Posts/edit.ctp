<?php
	$this->start('header-title');
		echo $post->title;
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Edit Post'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>


<?= $this->Form->create($post) ?>
<?= $this->Form->hidden('image') ?>
<?= $this->Form->hidden('thumb') ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('title') ?>
				<?= $this->Form->input('subtitle') ?>
				<?= $this->Form->input('slug_title', ['label' => 'Slug']) ?>
				<?= $this->Form->input('slug_override') ?>
				<?= $this->Form->input('excerpt', ['label' => 'Excerpt']) ?>
				<?= $this->Form->input('body', ['rows' => '3']) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<p class="image"><?php if($post->image) echo $this->Html->image($post->image, ['class' => 'img-responsive']) ?></p>
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-picture"></span> '.__('Select Image'),
								'#',
								['data-toggle' => 'modal', 'data-target' => '', 'class' => 'btn btn-default image-assets', 'escape' => false]
							)
						?>
						<a href="#" class="btn remove" data-target="image"<?php if(!$post->image): ?> style="display:none"<?php endif ?>>
							<span class="glyphicon glyphicon-trash text-danger"></span>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<p class="thumb"><?php if($post->thumb) echo $this->Html->image($post->thumb, ['class' => 'img-responsive']) ?></p>
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-picture"></span> '.__('Select Thumbnail'),
								'#',
								['data-toggle' => 'modal', 'data-target' => '', 'class' => 'btn btn-default thumb-assets', 'escape' => false]
							)
						?>
						<a href="#" class="btn remove" data-target="thumb"<?php if(!$post->thumb): ?> style="display:none"<?php endif ?>>
							<span class="glyphicon glyphicon-trash text-danger"></span>
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('category_id') ?>
				<?= $this->Form->input('status', array('options' => array_combine($status,$status))) ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Update'), ['escape' => false]) ?>

			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">SEO</div>
			<div class="panel-body">
				<?= $this->Form->input('meta_title') ?>
				<?= $this->Form->input('meta_description') ?>
			</div>
		</div>
<?= $this->Form->end() ?>
		<?php if(isset($is_admin)): ?>
		<div class="panel panel-default">
			<div class="panel-heading">Duplicate</div>
			<div class="panel-body">
				<?= $this->Form->create(null, ['action' => 'duplicate']) ?>
				<?= $this->Form->hidden('post_id', ['value' => $post->id]) ?>
				<?php
					$i = 0;
					foreach($languages as $language) {

						$checked = false;
						if(isset($exists[$i]['website_id'])) {
							if($language['website_id'] == $exists[$i]['website_id']) {
								$checked = true;
							}
						}

						echo $this->Form->hidden("site.{$i}.website_id", ['value' => $language['website_id']]);
						echo $this->Form->hidden("site.{$i}.locale", ['value' => $language['locale']]);
						echo $this->Form->input("site.{$i}.name", ['type' => 'checkbox', 'checked' => $checked, 'label' => $language['name']]);
						$i++;
					}
				?>
				<?= $this->Form->button(__('Create Posts'), ['class' => 'btn btn-success']) ?>
				<?= $this->Form->end() ?>


			</div>
		</div>
		<?php endif; ?>
		<div class="panel panel-default">
			<div class="panel-heading">Delete post</div>
			<div class="panel-body"><?= $this->Form->postLink(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					['action' => 'delete', $post->id],
					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				); ?></div>
		</div>

	</div>
</div>



<?= $this->element('assets-modal') ?>
<?= $this->element('wysiwyg') ?>

<script>
$(document).ready(function() {
	$('.remove').click(function(){
		var target = $(this).data('target');
		$('p.'+target).html('');
		$('input[name='+target+']').val('');
		$(this).remove();
		return false;
	});
});
</script>