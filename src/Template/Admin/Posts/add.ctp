<?php
	$this->start('header-title');
		echo __('New Post');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Add Post'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<?= $this->Form->create($post) ?>
<?= $this->Form->hidden('image');?>
<?= $this->Form->hidden('thumb');?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('title') ?>
				<?= $this->Form->input('subtitle') ?>
				<?= $this->Form->input('slug_title', ['label' => 'Slug']) ?>
				<?= $this->Form->input('slug_override') ?>
				<?= $this->Form->input('excerpt', ['label' => 'Excerpt']) ?>
				<?= $this->Form->input('body', ['rows' => '3']) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<p class="image"><?php if($post->image) echo $this->Html->image($post->image, ['class' => 'img-responsive']) ?></p>
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-picture"></span> '.__('Select Image'),
								'#',
								['data-toggle' => 'modal', 'data-target' => '', 'class' => 'btn btn-default image-assets', 'escape' => false]
							)
						?>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<p class="thumb"><?php if($post->thumb) echo $this->Html->image($post->thumb, ['class' => 'img-responsive']) ?></p>
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-picture"></span> '.__('Select Thumbnail'),
								'#',
								['data-toggle' => 'modal', 'data-target' => '', 'class' => 'btn btn-default thumb-assets', 'escape' => false]
							)
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('category_id', ['default' => $category_id]) ?>
				<?= $this->Form->input('status', array('options' => array_combine($status,$status))) ?>
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">SEO</div>
			<div class="panel-body">
				<?= $this->Form->input('meta_title', ['rows' => '2']) ?>
				<?= $this->Form->input('meta_description', ['rows' => '2']) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>


<?= $this->element('assets-modal') ?>
<?= $this->element('wysiwyg') ?>