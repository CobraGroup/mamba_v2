<?php if(!empty($posts)): ?>

	<?php foreach($posts as $id => $title): ?>
		<?= $this->Form->input('posts', [
				'type'=> 'checkbox',
				'name' => 'block[posts][]',
				'label' => $title,
				'value' => $id,
				'checked' => isset($selected) ? in_array($id, $selected) : false,
				'hiddenField' => false
			])
		?>
	<?php endforeach; ?>
	

<?php else: ?>

	<div class="alert alert-warning">
		No Posts found for selected category.
		<?= $this->Html->link(__('Add Post'), ['controller' => 'Posts', 'action' => 'add'], ['class' => 'btn btn-warning btn-xs pull-right']) ?>
	</p>
	
<?php endif; ?>
