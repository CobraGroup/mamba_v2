<?php 
	$this->start('header-title');
		echo __('Edit Company');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', $company->name, ['class' => 'active']);
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<?= $this->Form->create($company) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('theme') ?>
				<?= $this->Form->input('screenshot') ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('rank', ['label' => 'Order']) ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Update'), ['escape' => false]) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>