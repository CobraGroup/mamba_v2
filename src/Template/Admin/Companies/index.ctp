<?php 
	$this->start('header-title');
		echo __('Content Manager');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<?php if(isset($is_admin)): ?>
<div class="row">
	<div class="col-md-12">
	<p><?= $this->Html->link(
			'<span class="glyphicon glyphicon-plus"></span> ' . __('Add Company'), 
			['action' => 'add'], 
			['class' => 'btn btn-info btn-xs', 'escape' => false]
	) ?></p>
	</div>
</div>
<?php endif ?>

<div class="row">
	<?php foreach ($companies as $company): ?>
	<div class="col-sm-6 col-md-3">
		<div class="thumbnail">
			<?= $this->Html->link(
					$this->Html->image($company->screenshot, ['alt' => $company->name]),
					['controller' => 'websites', 'action' => 'view', $company->id],
					['escape' => false]
			) ?>
			<div class="caption">
				<h3>
					<?= $this->Html->link($company->name, ['controller' => 'websites', 'action' => 'view', $company->id]) ?>
					
					<?php if(isset($is_admin)): ?>
					<div class="pull-right">
					<?= $this->Html->link(
							'<span class="glyphicon glyphicon-cog"></span>', 
							['action' => 'edit', $company->id], 
							['class' => 'btn', 'escape' => false]
						)
					?>
					</div>
					<?php endif ?>
				</h3>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>

