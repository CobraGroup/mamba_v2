<?php
	$this->start('header-title');
		echo __('Cobra Group');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Add Domain', array('class' => 'active'));
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<h1>Add Domain</h1>

<?= $this->Form->create($domain) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->hidden('website_id', ['value' => $website_id]) ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>
