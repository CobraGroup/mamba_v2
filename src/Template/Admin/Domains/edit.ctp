<?php
	$this->start('header-title');
		echo __('Cobra Group');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Edit Domain', array('class' => 'active'));
	$this->end();

	$this->start('header-right');
	$this->end();
?>

<?= $this->Form->create($domain) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->hidden('website_id', ['value' => $domain->website_id]) ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->button(__('Save')) ?>
				<?php
				 echo $this->Form->postLink(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					$url = '/admin/domains/delete/'. $domain->id,

					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				);
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>
