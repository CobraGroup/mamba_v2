<?php
	$this->start('header-title');
		echo __('Languages');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<p><?= $this->Html->link('Add Language', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>

<div class="panel panel-default">
	<table class="table table-striped">
		<thead>
			<?= $this->Html->tableHeaders(['Language', 'Country', ['Locale' => ['class' => 'hidden-xs']], ['slug' => ['class' => 'hidden-xs']]]) ?>
		</thead>
		<tbody>
	    <?php foreach ($languages as $language): ?>
	    <tr>
	        <td><?= $this->Html->link($language->name, ['action' => 'edit', $language->id]) ?></td>
	        <td class="text-<?= $language->active ? 'success' : 'danger' ?>"><?= $language->country ?></td>
	        <td class="hidden-xs"><?= $language->locale ?></td>
	        <td class="hidden-xs"><?= $language->slug ?></td>
<!--
	        <td>
	            <?= $this->Html->link(
	            	'<span class="glyphicon glyphicon-pencil"></span>', 
	            	['action' => 'edit', $language->id], 
	            	['escape' => false]
	            ) ?>
	            <?= $this->Form->postLink(
	                '<span class="glyphicon glyphicon-trash text-danger"></span>',
	                ['action' => 'delete', $language->id],
	                ['confirm' => 'Are you sure?', 'escape' => false])
	            ?>
	        </td>
-->
	    </tr>
	    <?php endforeach; ?>
		</tbody>
	</table>
</div>

