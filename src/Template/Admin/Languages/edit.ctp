<?php
	$this->start('header-title');
		echo __('Edit Language');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Edit', array('class' => 'active'));
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<div class="row">

	<?= $this->Form->create($language) ?>
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('country') ?>
				<?= $this->Form->input('locale') ?>
				<?= $this->Form->input('slug') ?>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('active') ?>
				<?= $this->Form->button(
					'<span class="glyphicon glyphicon-save"></span> '.__('Save'), 
					['escape' => false]
				) ?>
				<?php /* $this->Form->postLink(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					['action' => 'delete', $language->id],
					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				)*/ ?>
			</div>
		</div>
	</div>
	<?= $this->Form->end() ?>
	
</div>
