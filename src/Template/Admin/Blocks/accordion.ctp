<?php
	$this->start('header-title');
		echo __('Content Block');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs', [
			'crumb' => ['Edit Page' => '/admin/pages/edit/'.$block->page_id]
		]);
		echo $this->Html->tag('li', 'Block', array('class' => 'active'));
	$this->end();

	$tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'strong'];
?>

<?= $this->Form->create($block) ?>
<?= $this->Form->hidden('page_id') ?>
<?= $this->Form->hidden('template') ?>
<?= $this->Form->hidden('block_type_id') ?>
<?= $this->Form->hidden('block.element', ['value' => 'accordion']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				
				<div class="row">
					<div class="col-lg-9">
						<?= $this->Form->input('block.title') ?>
					</div>
					<div class="col-lg-3">
					<?= $this->Form->input('block.tag', ['options' => array_combine($tags,$tags)]) ?>
					</div>
				</div>
				
				<?= $this->Form->input('block.content', ['type' => 'textarea']) ?>

			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('region', ['options' => $regions]) ?>
				<?= $this->Form->input('rank', ['label' => 'Order']) ?>
				<?= $this->Form->input('block.style', ['label' => 'CSS styling']) ?>
				<?= $this->Form->input('active') ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $block->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>


<?= $this->element('wysiwyg') ?>

