<?php
	$this->start('header-title');
		echo __('Posts Block');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs', [
			'crumb' => ['Edit Page' => '/admin/pages/edit/'.$block->page_id]
		]);
		echo $this->Html->tag('li', 'Block', array('class' => 'active'));
	$this->end();

	$range = range(1,10);
	
	$tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'strong'];
?>

<?= $this->Form->create($block) ?>
<?= $this->Form->hidden('page_id') ?>
<?= $this->Form->hidden('template') ?>
<?= $this->Form->hidden('block_type_id') ?>
<?= $this->Form->hidden('block.element', ['value' => 'contact']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<?= $this->Form->input('block.title') ?>
					</div>
					<div class="col-lg-3">
						<?= $this->Form->input('block.tag', ['options' => array_combine($tags,$tags)]) ?>
					</div>
				</div>
				<?= $this->Form->input('block.text', ['type' => 'textarea']) ?>
				<div id="post"></div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('region', ['options' => $regions]) ?>
				<? //$this->Form->input('block.style', ['label' => 'CSS styling']) ?>
				<? //$this->Form->input('rank', ['label' => 'Order']); ?>
				<?= $this->Form->input('active') ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>

				<?php if($this->request->action == 'edit'): ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $block->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>


<?= $this->start('script-footer') ?>
<script type="text/javascript">
	$(document).ready(function(){

		<?php if($block->action != 'Add'): ?>
			$('#post').load('/admin/posts/filter/' + <?= $this->request->data['block']['category_id'] ?> + '/' + '<?= $block->id ?>');
		<?php endif ?>

		$('#block-category-id').change(function(){
			var category_id = $(this).val();
			var categories = '/admin/posts/filter/' + category_id;

			<?php if($block->action != 'Add'): ?>
				categories += '/' + '<?= $block->id ?>'
			<?php endif ?>
			//console.log(categories);
			$('#post').load(categories);
		});

	})
</script>
<?php $this->end(); ?>