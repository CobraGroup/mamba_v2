<?php
	$this->start('header-title');
		echo __('Links Block');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs', [
			'crumb' => ['Edit Page' => '/admin/pages/edit/'.$block->page_id]
		]);
		echo $this->Html->tag('li', 'Block', array('class' => 'active'));
	$this->end();
	
?>

<?= $this->Form->create($block) ?>
<?= $this->Form->hidden('page_id'); ?>
<?= $this->Form->hidden('template'); ?>
<?= $this->Form->hidden('block_type_id'); ?>
<?= $this->Form->hidden('block.element', ['value' => 'link']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				
				<?= $this->Form->input('block.section_title') ?>
				
				<h3>Links</h3>
				<hr>
				<div class="add-links-list">
					
					<?php
					// print_r($block["data"]);
					foreach($block["data"]["title"] as $key=>$title){
						if($title){
					?>
					<div>
						
						<div class="delete-add-link-block">delete this link</div>
						<?= $this->Form->input('block.title[]', ['label' => 'Title', 'value' => $title]);?>
						<div class="row">
							<div class="col-lg-5">
								<?= $this->Form->input('block.slug[]', ['label' => 'Internal Link', 'value' => $block["data"]["slug"][$key]]);?>
							</div>
							<div class="col-lg-2" style="text-align: center; margin-top: 30px; color: grey">
								- OR -
							</div>
							<div class="col-lg-5">
								<?= $this->Form->input('block.link[]', ['label' => 'External link', 'value' => $block["data"]["link"][$key]]);?>
								<small style="color: grey"><em>Leave this field blank to use internal link.</em></small>
							</div>
						</div>
						
						<hr>
					
					</div>
					<?php
						}
					}
					?>
					<div class="add-a-link">
						
						<div class="delete-add-link-block">delete this link</div>
						<?= $this->Form->input('block.title[]', ['label' => 'Title', 'value' => ""]);?>
						<div class="row">
							<div class="col-lg-5">
								<?= $this->Form->input('block.slug[]', ['label' => 'Internal Link', 'value' => ""]);?>
							</div>
							<div class="col-lg-2" style="text-align: center; margin-top: 30px; color: grey">
								- OR -
							</div>
							<div class="col-lg-5">
								<?= $this->Form->input('block.link[]', ['label' => 'External link', 'value' => ""]);?>
								<small style="color: grey"><em>Leave this field blank to use internal link.</em></small>
							</div>
						</div>
						
						<hr>
					
					</div>
				
				</div>
				
			</div>
		</div>
		<button type="button" class="btn btn-md add-a-link-button">Add a Link</button>
		<?= $this->start('script-footer') ?>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".add-a-link-button").on("click", function(){
					$(".add-a-link").clone().removeClass("add-a-link").appendTo(".add-links-list");
				});
				$(".delete-add-link-block").on("click", function(){
					$(this).parent().remove();
				});
			});
		</script>
		<?php $this->end(); ?>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('region', ['options' => $regions]); ?>
				<?= $this->Form->input('rank', ['label' => 'Order']); ?>
				<?= $this->Form->input('block.style', ['label' => 'CSS styling']); ?>
				<?= $this->Form->input('active'); ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $block->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>
