<?php
	$this->start('header-title');
		echo __('Submenu Block');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs', [
			'crumb' => ['Edit Page' => '/admin/pages/edit/'.$block->page_id]
		]);
		echo $this->Html->tag('li', 'Block', array('class' => 'active'));
	$this->end();
	
?>

<?= $this->Form->create($block) ?>
<?= $this->Form->hidden('page_id'); ?>
<?= $this->Form->hidden('template'); ?>
<?= $this->Form->hidden('block_type_id'); ?>
<?= $this->Form->hidden('block.element', ['value' => 'cloud-tags']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				
				<?= $this->Form->input('block.title');?>
				
				<div class="row">
					<div class="col-lg-5">
						<?= $this->Form->input('block.slug', ['label' => 'Choose a page']);?>
					</div>
					<div class="col-lg-2" style="text-align: center; margin-top: 30px; color: grey">
						- OR -
					</div>
					<div class="col-lg-5">
						<?= $this->Form->input('block.link', ['label' => 'Enter a link']);?>
						<small style="color: grey"><em>Leave this field blank to use page link.</em></small>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('region', ['options' => $regions]); ?>
				<?= $this->Form->input('rank', ['label' => 'Order']); ?>
				<?= $this->Form->input('block.style', ['label' => 'CSS styling']); ?>
				<?= $this->Form->input('active'); ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $block->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>
