<?php
	$this->start('header-title');
		echo __('Image Block');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs', [
			'crumb' => ['Edit Page' => '/admin/pages/edit/'.$block->page_id]
		]);
		echo $this->Html->tag('li', 'Block', array('class' => 'active'));
	$this->end();

	$tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'strong'];
?>

<?= $this->Form->create($block) ?>
<?= $this->Form->hidden('page_id'); ?>
<?= $this->Form->hidden('template'); ?>
<?= $this->Form->hidden('block_type_id'); ?>
<?= $this->Form->hidden('block.element', ['value' => 'person']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->hidden('block.image');?>
				<p class="image">
					<?php if(isset($this->request->data['block']['image'])) echo $this->Html->image($this->request->data['block']['image'], ['class' => 'img-responsive']) ?>
				</p>
				<p><?= $this->Html->link(
						'<span class="glyphicon glyphicon-picture"></span> '.__('Select Image'),
						'#',
						['data-toggle' => 'modal', 'data-target' => '.bs-example-modal-lg', 'class' => 'btn btn-default image-assets', 'escape' => false]
					)
				?></p>
				<?= $this->Form->input('block.alternative_text'); ?>

				<hr />
				<div class="row">
					<div class="col-md-5"><?= $this->Form->input('block.name'); ?></div>
					<div class="col-md-5"><?= $this->Form->input('block.position'); ?></div>
					<div class="col-md-2"><?= $this->Form->input('block.tag', ['options' => array_combine($tags, $tags)]); ?></div>
					<div class="col-md-12"><?= $this->Form->input('block.caption'); ?></div>
				</div>
<!--
				<hr />
				<div class="row">
					<div class="col-md-6"><?= $this->Form->input('block.link'); ?></div>
					<div class="col-md-6"><?= $this->Form->input('block.link_label'); ?></div>
				</div>
-->
				<hr />
				<?= $this->Form->input('block.style', ['label' => 'CSS styling']); ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('region', ['options' => $regions]); ?>
				<?= $this->Form->input('rank', ['label' => 'Order']); ?>
				<?= $this->Form->input('active'); ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $block->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>


<div class="modal fade assets" tabindex="-1" role="dialog" aria-labelledby="Assets" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

		</div>
	</div>
</div>

<?php $this->start('script-footer') ?>
<script type="text/javascript">
	$().ready(function() {
		$('.image-assets').click(function(){
			$('.image').addClass('image-active');
			$('.assets').modal({
				backdrop: 'static',
				remote: '/admin/assets/filter'
			});
		});
	});
</script>
<?php $this->end() ?>

