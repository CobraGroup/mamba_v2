<?php
	$this->start('header-title');
		echo __('Gallery Block');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs', [
			'crumb' => ['Edit Page' => '/admin/pages/edit/'.$block->page_id]
		]);
		echo $this->Html->tag('li', 'Block', array('class' => 'active'));
	$this->end();

	$tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'strong'];
?>

<?= $this->Form->create($block) ?>
<?= $this->Form->hidden('page_id'); ?>
<?= $this->Form->hidden('template'); ?>
<?= $this->Form->hidden('block_type_id'); ?>
<?= $this->Form->hidden('block.element', ['value' => 'gallery']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-5">
						<?= $this->Form->input('block.title') ?>
					</div>
					<div class="col-lg-5">
						<?= $this->Form->input('block.sub_title') ?>
					</div>
					<div class="col-lg-2">
						<?= $this->Form->input('block.tag', ['options' => array_combine($tags,$tags)]) ?>
					</div>
				</div>

				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-plus"></span> '.__('Add Thumbnail'),
						'#',
						['data-toggle' => 'modal', 'data-target' => '.bs-example-modal-lg', 'class' => 'btn btn-default image-assets', 'escape' => false]
					)
				?>
			</div>
		</div>

		<div class="row gallery">
		<?php if(!isset($this->request->data['action'])): ?>
			<?php foreach($this->request->data['block']['gallery'] as $i => $data): ?>
			<div class="col-lg-4" id="block-<?= $i ?>">
				<div class="panel panel-default">
					<div class="panel-body">
						<?= $this->Html->image($data['image'], ['class' => 'img-responsive']) ?>
						<?= $this->Form->hidden("block.gallery.{$i}.image", ['value' => $data['image']]) ?>
						<?= $this->Form->input("block.gallery.{$i}.title", ['value' => $data['title']]) ?>
						<?= $this->Form->input("block.gallery.{$i}.caption", ['value' => $data['caption']]) ?>
						<?= $this->Form->input("block.gallery.{$i}.class", ['value' => $data['class']]) ?>
						<?= $this->Form->input("block.gallery.{$i}.link", ['value' => $data['link']]) ?>
						<?= $this->Form->input("block.gallery.{$i}.alternative_text", ['value' => $data['alternative_text']]) ?>
						<?= $this->Form->input("block.gallery.{$i}.order", ['value' => $data['order']]) ?>
						<br />
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-trash"></span> Delete',
								'#',
								['data-block' => $i, 'class' => 'text-danger delete-image', 'escape' => false]
							)
						?>
					</div>
				</div>
			</div>
			<?php endforeach ?>
		<?php endif ?>
		</div>

	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('region', ['options' => $regions]); ?>
				<?= $this->Form->input('rank', ['label' => 'Order']); ?>
				<?= $this->Form->input('block.style', ['label' => 'CSS styling']); ?>
				<?= $this->Form->input('active'); ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save'), ['escape' => false]) ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $block->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>


<div class="modal fade assets" tabindex="-1" role="dialog" aria-labelledby="Assets" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

		</div>
	</div>
</div>

<?php $this->start('script-footer') ?>
<script type="text/javascript">
	$().ready(function() {
		$('.image-assets').click(function(){
			$('.assets').modal({
				backdrop: 'static',
				remote: '/admin/assets/filter'
			});
		});
		$('.delete-image').click(function(){
			var i = $(this).data('block');
			$('#block-'+i).remove();
		});
	});
</script>
<?php $this->end() ?>

