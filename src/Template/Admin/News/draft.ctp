<?php 
	$this->start('header-title');
		echo $this->request->session()->read('Website.name');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Draft'), ['class' => 'active']);
	$this->end(); 
	
	$this->start('header-right');
	$this->end(); 
?>

<p>
	<?= $this->Html->link('Published', ['action' => 'index'], ['class' => 'btn btn-xs btn-success']) ?>
	<?= $this->Html->link('Unpublished', ['action' => 'index'], ['class' => 'btn btn-xs btn-danger']) ?>
</p>

<div class="panel panel-default">
	<table class="table table-striped">
		<thead>
			<?= $this->Html->tableHeaders(['Title', 'Created', '']) ?>
		</thead>
	    <tbody>
	    <?php foreach ($articles as $article): ?>
	    <tr>
	        <td>
	            <?= $this->Html->link($article->title, ['action' => 'edit', $article->id]) ?>
	        </td>
	        <td>
	            <?= $article->created->format('d.m.Y') ?>
	        </td>
	        <td>
	        	<span class="glyphicon glyphicon-picture text-<?= $article->image ? 'success' : 'danger' ?>"></span>
	        	<span class="glyphicon glyphicon-globe text-<?= $article->meta_title && $article->meta_description ? 'success' : 'danger' ?>"></span>
	        </td>
	    </tr>
	    <?php endforeach; ?>
	    </tbody>
	</table>
</div>

<nav>
	<ul class="pagination">
		<?= $this->Paginator->first(); ?>
		<?= $this->Paginator->numbers(); ?>
		<?= $this->Paginator->last(); ?>
	</ul>
</nav>