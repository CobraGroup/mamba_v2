<?php
	$this->start('header-title');
		echo $news->title;
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', __('Edit Article'), ['class' => 'active']);
	$this->end();

	$this->start('header-right');
	$this->end();
?>


<?= $this->Form->create($news) ?>
<?= $this->Form->hidden('image') ?>
<?= $this->Form->hidden('thumb') ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('title') ?>
				<?= $this->Form->input('slug_title') ?>
				<?= $this->Form->input('excerpt', ['label' => 'Excerpt']) ?>
				<?= $this->Form->input('body', ['rows' => '3']) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<p class="image"><?php if($news->image) echo $this->Html->image($news->image, ['class' => 'img-responsive']) ?></p>
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-picture"></span> '.__('Select Image'),
								'#',
								['data-toggle' => 'modal', 'data-target' => '', 'class' => 'btn btn-default image-assets', 'escape' => false]
							)
						?>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<p class="thumb"><?php if($news->thumb) echo $this->Html->image($news->thumb, ['class' => 'img-responsive']) ?></p>
						<?= $this->Html->link(
								'<span class="glyphicon glyphicon-picture"></span> '.__('Select Thumbnail'),
								'#',
								['data-toggle' => 'modal', 'data-target' => '', 'class' => 'btn btn-default thumb-assets', 'escape' => false]
							)
						?>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('status', array('options' => array_combine($status,$status))) ?>
				<?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Update'), ['escape' => false]) ?>
				<?= $this->Html->link(
					'<span class="glyphicon glyphicon-trash"></span> Delete',
					['action' => 'delete', $news->id],
					['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
				); ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">SEO</div>
			<div class="panel-body">
				<?= $this->Form->input('meta_title') ?>
				<?= $this->Form->input('meta_description') ?>
			</div>
		</div>

	</div>
<?= $this->Form->end() ?>
	<div class="col-lg-4">
		<?php if(isset($is_admin)): ?>
		<div class="panel panel-default">
			<div class="panel-heading">Duplicate</div>
			<div class="panel-body">
							<?= $this->Form->create(null, ['action' => 'duplicate']) ?>

				<?= $this->Form->hidden('news_id', ['value' => $news->id]) ?>
				<?php
					$i = 0;
					foreach($languages as $language) {

						$checked = false;
						if(isset($exists[$i]['website_id'])) {
							if($language['website_id'] == $exists[$i]['website_id']) {
								$checked = true;
							}
						}

						echo $this->Form->hidden("site.{$i}.website_id", ['value' => $language['website_id']]);
						echo $this->Form->hidden("site.{$i}.locale", ['value' => $language['locale']]);
						echo $this->Form->input("site.{$i}.name", ['type' => 'checkbox', 'checked' => $checked, 'label' => $language['name']]);
						$i++;
					}
				?>
				<?= $this->Form->button(__('Create News'), ['class' => 'btn btn-success']) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
		<?php endif; ?>	</div>

	</div>

<?= $this->element('assets-modal') ?>
<?= $this->element('wysiwyg') ?>
