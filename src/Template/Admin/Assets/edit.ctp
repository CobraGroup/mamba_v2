<?php 
	$this->start('header-title');
		echo __('Media Manager');
	$this->end(); 
	
	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Edit Asset', array('class' => 'active'));
	$this->end(); 
	
	$this->start('header-right');
	$this->end();
?>

<?= $this->Form->create($asset, ['type' => 'file', 'id' => 'assets']) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('filename', ['type' => 'file']) ?>
				<?= $this->Form->input('over_write', ['type' => 'checkbox']) ?>
				<?= $this->Form->input('alternative_text') ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Uploaded Image</div>
			<?= $this->Html->image($asset->path.DS.$asset->name, ['class' => 'img-responsive']) ?>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->button(__('Save')) ?>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-trash"></span> Delete',
						['action' => 'delete', $asset->id],
						['confirm' => 'Are you sure? This action cannot be undone!!', 'class' => 'btn text-danger pull-right', 'escape' => false]
					)
				?>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">Tags</div>
			<div class="panel-body">
				<div class="col-lg-5">
					<?= $this->Form->input('available_tags', [
							'label' => false,
							'multiple' => true,
							'options' => $available_tags
						]) 
					?>
				</div>
				<div class="col-lg-2">
					<p><a href="#" id="add" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a></p>
					<p><a href="#" id="remove" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-chevron-left"></span></a></p>
				</div>
				<div class="col-lg-5">
					<?= $this->Form->input('tags', [
							'label' => false,
							'multiple' => true,
							'options' => $selected_tags
						]) 
					?>
				</div>
			</div>
		</div>
				
	</div>
</div>
<?= $this->Form->end() ?>

<?php $this->Html->script('//tinymce.cachefly.net/4.1/tinymce.min.js', ['block' => true]) ?>
<script>
$().ready(function() {  
	tinymce.init({
		selector: 'textarea',
		toolbar: 'false'
	});
});
</script>

<?php $this->append('script') ?>
<script type="text/javascript">
	$().ready(function() {  
        $('#add').click(function() {  
            return !$('#available-tags option:selected').remove().appendTo('#tags-ids');  
        });  
        $('#remove').click(function() {  
            return !$('#tags-ids option:selected').remove().appendTo('#available-tags');  
        });
        $('button').click(function(){
	        $('#tags-ids option').prop('selected',true);
	        $('#assets').submit();
	        return true;
        });
    });
</script>
<?php $this->end() ?>
