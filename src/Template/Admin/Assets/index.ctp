<?php
	$this->start('header-title');
		echo __('Media Manager');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
	$this->end();

	$this->start('header-right');
		echo $this->Form->create(null, ['class' => 'form-inline', 'type' => 'get']);
		echo $this->Form->input('tag_id', ['label' => false, 'default' => $tag_id, 'empty' => 'All tags']);
		echo ' ';
		echo $this->Form->button('Filter');
		echo $this->Form->end();
	$this->end();
?>

<div class="row">
	<div class="col-md-12">
		<p>
			<?= $this->Html->link(
					'<span class="glyphicon glyphicon-plus"></span> ' . __('Add Asset'),
					['action' => 'add'],
					['class' => 'btn btn-primary btn-xs', 'escape' => false]
				) 
			?>
			<?= $this->Html->link(
					'<span class="glyphicon glyphicon-list"></span> ' . __('Tags'),
					['controller'=>'tags', 'action' => 'index'],
					['class' => 'btn btn-success btn-xs', 'escape' => false]
				)
			?>
		</p>
	</div>
</div>

<?php foreach (array_chunk($assets->toArray(),6,true) as $row): ?>
<div class="row">
	<?php foreach ($row as $asset): ?>
	<div class="col-sm-6 col-md-2">
		<div class="thumbnail">
			<?= $this->Html->link(
					$this->Html->image($asset->path.'/thumbs/'.$asset->name, ['alt' => $asset->alternative_text]),
					['action' => 'edit', $asset->id],
					['escape' => false]
			) ?>
		</div>
	</div>
	<?php endforeach; ?>
</div>
<?php endforeach; ?>


<nav>
	<ul class="pagination">
		<?= $this->Paginator->first(); ?>
		<?= $this->Paginator->numbers(); ?>
		<?= $this->Paginator->last(); ?>
	</ul>
</nav>