	<div class="modal-header" style="padding: 0; border: none;">
		<button type="button" class="close" data-dismiss="modal" style="padding: 20px 25px 0;">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		</button>
		<ul class="nav nav-tabs" role="tablist" style="padding: 15px 0 0 30px;">
			<li role="presentation" class="active"><a href="#library" aria-controls="library" role="tab" data-toggle="tab">Media Library</a></li>
			<li role="presentation"><a href="#upload" aria-controls="upload" role="tab" data-toggle="tab">Upload</a></li>
		</ul>
	</div>
	
	<div class="modal-body">
		<div class="tab-content">
			
			<div role="tabpanel" class="tab-pane active" id="library">
				<?php foreach (array_chunk($assets->toArray(),4,true) as $row): ?>
				<div class="row">
					<?php foreach ($row as $asset): ?>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail">
							<?= $this->Html->image($asset->path.'/thumbs/'.$asset->name, ['data-path' => $asset->path.DS.$asset->name]) ?>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endforeach; ?>
		
				<nav>
					<ul class="pagination">
						<?= $this->Paginator->first(); ?>
						<?= $this->Paginator->numbers(); ?>
						<?= $this->Paginator->last(); ?>
					</ul>
				</nav>
			</div>

			<div role="tabpanel" class="tab-pane" id="upload">
				<?= $this->Form->create(null, ['type' => 'file', 'action' => 'upload', 'class' => 'uploadForm']) ?>
				<?= $this->Form->input('filename', [
						'type' => 'file', 
						'class' => 'btn btn-warning', 
						'label' => false, 
						'title' => 'Select a file for upload'
					]) 
				?>
				<?php //$this->Form->input('alternative_text') ?>
				<?= $this->Form->input('tags._ids', [
						'multiple' => 'checkbox',
						'options' => $tags,
					]) 
				?>
			<?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary upload-btn']) ?>
			<?= $this->Form->end() ?>
			</div>
						
		</div>
	</div>
	
	<div class="modal-footer">
		<div class="footer-upload">
		</div>
		<div class="footer-insert">
			<div class="pull-left filter">
				<?= $this->Form->create(null, ['class' => 'form-inline', 'type' => 'get']) ?>
				<?= $this->Form->input('tag_id', ['label' => false, 'default' => $tag_id, 'empty' => 'All Tags']) ?>
				<?= $this->Form->button('Filter') ?>
				<?= $this->Form->end() ?>
			</div>
			<?= $this->Form->button(__('Insert'), ['class' => 'btn btn-primary insert']) ?>
		</div>
	</div>


<script type="text/javascript">
	$().ready(function() {
		
		$('.footer-upload').hide();
		$('.nav-tabs a').click(function(){
			if($(this).attr('href') == '#upload') {
				$('.footer-upload').show();
				$('.footer-insert').hide();
			} else {
				$('.footer-upload').hide();
				$('.footer-insert').show();
			}
		});
		
		$('.upload-btn').click(function(event){
			//event.preventDefault();
			$('.uploadForm').submit();
/*
			var data = {
				_csrfToken: $('input[name=_csrfToken]').val(),
				form: $('.uploadForm')[0]
			};
			$.ajax({
				url: '/admin/assets/upload',
				type: 'POST',
				data: new FormData($('.uploadForm')),
				contentType: false,
				processData: false,
				cache: false,
				success: function(data){
					console.log(data);
					$('.modal-content').load('/admin/assets/filter');
				}
			});

			console.log($('.uploadForm').serialize());
			
			console.log(data);
*/
						//console.log(new FormData($('.uploadForm')));

			//console.log($('#media').attr('action'));
			//console.log('upload');
			
			//$('.modal-content').load('/admin/assets/filter');
			return false;
		});
		
		
		$('.pagination a').click(function(){
			$('.modal-content').load($(this).attr('href'));
			return false;
		});
		$('.filter button').click(function(){
			$('.modal-content').load('/admin/assets/filter?tag_id=' + $('.filter #tag-id').val());
			return false;
		});
		$('.thumbnail').click(function(){
			$('.thumbnail').css('background', '#fff');
			$('.thumbnail').removeClass('selected');
			$(this).css('background', 'blue');
			$(this).addClass('selected');
			return false;
		});
		$('.insert').click(function(){

			if($('.image').hasClass('image-active')) {
				$('.image-active').html('<img src="' + $('.selected img').data('path') + '" class="img-responsive" />');
				if($('input[name="image"]').length) {
					$('input[name="image"]').val($('.selected img').data('path'));
				} else {
					$('input[name="block[image]"]').val($('.selected img').data('path'));
				}
			}

			if($('.thumb').hasClass('thumb-active')) {
				$('.thumb-active').html('<img src="' + $('.selected img').data('path') + '" class="img-responsive" />');
				if($('input[name="thumb"]')) {
					$('input[name="thumb"]').val($('.selected img').data('path'));
				} else {
					$('input[name="block[thumb]"]').val($('.selected img').data('path'));
				}
			}

			if($('div').hasClass('gallery')) {
				var i = $('.gallery').children().length + 1;
				$('.gallery').append(
					'<div class="col-lg-4">'+
						'<div class="panel panel-default">'+
							'<div class="panel-body">'+
								'<img src="' + $('.selected img').data('path') + '" class="img-responsive" />'+
								'<input class="form-control" name="block[gallery]['+i+'][image]" value="' + $('.selected img').data('path') + '" type="hidden">'+
								'<label for="link">Link</label>'+
								'<input id="link" class="form-control" name="block[gallery]['+i+'][link]" value="" type="text">'+
								'<label for="alternative_text">Alternative Text</label>'+
								'<input id="alternative_text" class="form-control" name="block[gallery]['+i+'][alternative_text]" value="" type="text">'+
								'<label for="order">Order</label>'+
								'<input class="form-control" name="block[gallery]['+i+'][order]" value="" type="text">'+
							'</div>'+
						'</div>'+
					'</div>'
				);

			}

			if($('div').hasClass('slider')) {
				var i = $('.slider').children().length;
				$('.slider').append(
					'<div class="col-lg-12">'+
						'<div class="panel panel-default">'+
							'<div class="panel-body">'+
								'<div class="row">'+
									'<div class="col-md-7">'+
										'<img src="' + $('.selected img').data('path') + '" class="img-responsive" />'+
									'</div>'+
									'<div class="col-md-5">'+
										'<input class="form-control" name="block[slider]['+i+'][image]" value="' + $('.selected img').data('path') + '" type="hidden">'+
										'<label for="title">Title</label>'+
										'<input id="title" class="form-control" name="block[slider]['+i+'][title]" value="" type="text">'+
										'<label for="caption">Caption</label>'+
										'<textarea id="caption" class="form-control" name="block[slider]['+i+'][caption]"></textarea>'+
										'<label for="order">Order</label>'+
										'<input class="form-control" name="block[slider]['+i+'][order]" value="" type="text">'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'
				);

			}

			$('.assets').modal('hide');
			return false;
		});
	});
</script>
