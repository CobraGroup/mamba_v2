<?php
	$this->start('header-title');
		echo __('Website Settings');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Settings', array('class' => 'active'));
	$this->end();	
?>

<?= $this->Form->create($settings) ?>
<?= $this->Form->hidden('website_id', ['value' => $website_id]); ?>
<div class="row">
	
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('Site Settings') ?></div>				
			<div class="panel-body">
				<?= $this->Form->input('home_page_id', ['options' => $pages]);?>				
				<?= $this->Form->input('default_language', ['options' => $languages]);?>
				<?= $this->Form->input('site_title');?>
				<?= $this->Form->input('site_logo'); ?>
			</div>
		</div>
		
		<p><?= $this->Form->button('<span class="glyphicon glyphicon-save"></span> '.__('Save Site Settings'), ['escape' => false]) ?></p>
	</div>
	
	<div class="col-lg-4">

		<div class="panel panel-default">
			<div class="panel-heading"><?= __('More Settings') ?></div>				
			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('News'), ['action' => 'news'], ['class' => 'btn btn-success']) ?></li>
				<li class="list-group-item"><?= $this->Html->link(__('Blog'), ['action' => 'blog'], ['class' => 'btn btn-success']) ?></li>
			</ul>
		</div>
	</div>
	
</div>
<?= $this->Form->end() ?>