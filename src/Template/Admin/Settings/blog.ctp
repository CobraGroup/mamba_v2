<?php
	$this->start('header-title');
		echo __('Website Settings');
	$this->end();

	$this->start('breadcrumbs');
		echo $this->element('breadcrumbs');
		echo $this->Html->tag('li', 'Settings', array('class' => 'active'));
	$this->end();	
?>

<?= $this->Form->create($settings) ?>
<?= $this->Form->hidden('website_id', ['value' => $website_id]); ?>
<div class="row">
	
	<div class="col-lg-8">
<!--
		<div class="panel panel-default">
			<div class="panel-heading">News Landing Page</div>				
			<div class="panel-body">				
				<?= $this->Form->input('news_posts_per_page'); ?>

				<?= $this->Form->hidden('image');?>
				<p class="image">
					<?php if(isset($settings->image)) echo $this->Html->image($settings->image, ['class' => 'img-responsive']) ?>
				</p>
				<?= $this->Html->link(
						'<span class="glyphicon glyphicon-picture"></span> '.__('Select Image'),
						'#',
						['data-toggle' => 'modal', 'data-target' => '.bs-example-modal-lg', 'class' => 'btn btn-default image-assets', 'escape' => false]
					)
				?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Most Read</div>				
			<div class="panel-body">				
				<?= $this->Form->input('most_read_order', ['label' => 'Order', 'options' => ['DESC' => 'Descending', 'ASC' => 'Ascending']]); ?>
				<?= $this->Form->input('most_read_limit', ['label' => 'Limit']); ?>
			</div>
		</div>
-->
	</div>
	
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading"><?= __('More Settings') ?></div>				
			<ul class="list-group">
				<li class="list-group-item"><?= $this->Html->link(__('Site'), ['action' => 'index'], ['class' => 'btn btn-success']) ?></li>
				<li class="list-group-item"><?= $this->Html->link(__('News'), ['action' => 'news'], ['class' => 'btn btn-success']) ?></li>
			</ul>
		</div>
	</div>
	
</div>
<?= $this->Form->end() ?>


<div class="modal fade assets" tabindex="-1" role="dialog" aria-labelledby="Assets" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

		</div>
	</div>
</div>

<?php $this->start('script-footer') ?>
<script type="text/javascript">
	$().ready(function() {
		$('.image-assets').click(function(){
			$('.image').addClass('image-active');
			$('.assets').modal({
				backdrop: 'static',
				remote: '/admin/assets/filter'
			});
		});
	});
</script>
<?php $this->end() ?>

