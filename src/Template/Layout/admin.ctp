<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __('Cobra Group CMS');
?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon') ?>

	<?= $this->Html->css('admin/bootstrap.min') ?>
	<?= $this->Html->css('admin/style') ?>
	<?= $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') ?>
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>
<body>

	<?= $this->element('nav/'.strtolower($this->request->session()->read('Auth.User.role'))) ?>

	<header>
		<div class="container">
			<div class="header-left">
				<h1><?= $this->fetch('header-title') ?></h1>
				<ol class="breadcrumb">
					<?= $this->fetch('breadcrumbs') ?>
				</ol>
			</div>
			<div class="header-right">
				<?= $this->fetch('header-right') ?>
			</div>
		</div>
	</header>
	<div class="container">

		<div id="content">
			<?= $this->Flash->render() ?>
			<?= $this->fetch('content') ?>
		</div>
		<footer>
		</footer>
	</div>


<?= $this->Html->script('admin/slugify') ?>
<?= $this->Html->script('bootstrap.min') ?>
<?= $this->fetch('script-footer') ?>
</body>
</html>
