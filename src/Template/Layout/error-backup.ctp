<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>
	<?php
			$this->assign('title', 'Appco Group | Error');
	?>
		<?= $this->fetch('title') ?>
	</title>

	<?= $this->Html->meta('icon') ?>
	<link href="/css/reset.css" rel="stylesheet"></link>

</head>

<body>
	<div id="wrapper">
		<div id="header">
	        <div class="logo-container">
	            <a title="Appco Group" class="logo-left" href="/" hreflang="x-default" rel="alternate">


	            </a>
				<?php if(false): ?>
		        <div class="search_bar">
			        <?= $this->Form->create(null, ['url' => '/sitemaps/search', 'type' => 'get', 'accept-charset' => 'utf-8'])?>
				    <?= $this->Form->input('search', ['div' => 'input search', 'label' => false]) ?>
				    <?= $this->Form->submit('Search') ?>
		            <?= $this->Form->end() ?>
		        </div>
				<?php endif ?>
	        </div>


		<div id="header">
			<div id="cssmenu">
				<ul>
					<?php
						function active($currect_page){
						  $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
						  $url = end($url_array);
						  if($currect_page == $url){
						      echo 'selected';
						  }
						}
					?>
					<li class="home <?= active('/');?>"><a href='/'><span>Home</span></a></li>
					<li class="menu <?= active('who-we-are');?>"><a href='/who-we-are'><span>Who we are</span></a></li>
					<li class="menu <?= active('what-we-do');?>"><a href='/what-we-do'><span>What we do</span></a></li>
					<li class="menu <?= active('giving-back');?>"><a href='/giving-back'><span>Giving back</span></a></li>
					<li class="menu <?= active('first-impressions');?>"><a href='/first-impressions'><span>Ace your interview</span></a></li>
					<li class="menu <?= active('blog/');?>"><a href='/blog/'><span>Blog</span></a></li>
					<li class="menu <?= active('faqs');?>"><a href='/faqs'><span>FAQs</span></a></li>
					<li class="menu <?= active('contact-us');?>"><a href='/contact-us'><span>Contact us</span></a></li>
					<li class="menu <?= active('apply');?>last"><a href='/apply'><span>Apply</span></a></li>
				</ul>
			</div>
        <div class="clearfix"></div>
    </div>

	    </div>

		<div id="container">
<img style="clear:both; float:left: padding:0; margin:0; margin-top:-200px;width:100%;height:100%;display:block;" alt="404 page error" src="/img/assets/404.png">


		</div>

		<div class="clearfix"></div>

		<div id="footer">
			<?= $this->element('footer'); ?>
		</div>

</body>
</html>

