<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Cobra Group CMS</title>
	
	<?= $this->Html->meta('icon') ?>

	<?= $this->Html->css('admin/bootstrap.min') ?>
	<?= $this->Html->css('admin/style') ?>
	
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>
<body>
	<div class="container">
		
		<div id="content">
			<?= $this->Flash->render() ?>
			<?= $this->fetch('content') ?>
		</div>
		<footer>
		</footer>
	</div>
</body>
</html>
