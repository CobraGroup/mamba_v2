<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html style="height:100%; line-height: 100%;">
<head>
	<?= $this->Html->charset() ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>
	<?php
			$this->assign('title', 'Appco Group | Error');
	?>
		<?= $this->fetch('title') ?>
	</title>

	<?= $this->Html->meta('icon') ?>
	<link href="/css/reset.css" rel="stylesheet"></link>

</head>

<body style="height: 100%; line-height: 100%;">

		<div id="container" style="text-align: center; width: 100%; height: 100%; line-height: 100%;">
			<a href="/" style="color:#666; font-size: 1.2em; text-decoration: none; font-weight: bold;">
				<img style="padding:0; margin:3em 0 0 0;height:300px;vertical-align: middle;" alt="404 page error" src="/img/assets/404-2016.png"><br><br>
				Click here to go to Home Page
			</a>
		</div>

		<div class="clearfix"></div>

</body>
</html>

