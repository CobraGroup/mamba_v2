<?php
use Cake\Core\Configure;

$this->layout = 'error';
?>

<h2><?= __d('cake', 'An 404 Has Occurred') ?></h2>
<p class="error">
	<strong><?= __d('cake', 'Error') ?>: </strong>

</p>
<?php
if (Configure::read('debug')):
	echo $this->element('auto_table_warning');
	echo $this->element('exception_stack_trace');
endif;
?>
