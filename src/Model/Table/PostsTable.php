<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\I18n\I18n;
use Cake\Cache\Cache;
use Cake\Core\Configure;

class PostsTable extends Table {

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return Configure::read('Website.datasource');
		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

		$this->belongsTo('Categories');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('title');

        return $validator;
    }

    public function beforeSave(Event $event, Entity $entity) {
	    if(isset($entity->duplicate)) {
		    return true;
	    }
		$category = $this->Categories->findById($entity->category_id)->first();
	    $entity->set([
	    	'locale' => I18n::locale(),
	    	'slug' => DS.$category->slug.DS.$entity->slug_title
	    ]);
	    return true;
	}

    public function beforeFind(Event $event, Query $query, $options) {

	    if(isset($entity->duplicate)) {
		    return true;
	    }
		if(!isset($options['injectLocale']) || $options['injectLocale'] !== false) {
		        $query->where(['Posts.locale' => I18n::locale()]);
		    }

	}

    public function findPublished(Query $query, array $options) {
        $query->where([
            'Posts.status' => 'Published',
            'Posts.locale' => I18n::locale()
        ])->contain(['Categories']);
        return $query;
    }

}
