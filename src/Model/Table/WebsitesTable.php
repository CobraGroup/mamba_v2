<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Query;
use Cake\Core\Configure;

class WebsitesTable extends Table {

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
                
        $this->belongsTo('Companies');
        $this->belongsTo('Countries', [
    		'className' => 'Languages',
    		'foreignKey' => 'country_id'
    	]);
        $this->belongsToMany('Languages', [
        	'className' => 'Languages',
        	'joinTable' => 'websites_languages',
        ]);
        $this->hasMany('UsersWebsites');
        $this->belongsToMany('Websites', [
        	'className' => 'Websites',
        	'joinTable' => 'users_websites',
        ]);
       $this->hasMany('Domains');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('name')
            ->notEmpty('country_id');

        return $validator;
    }

	public function findLanguages(Query $query, array $options) {
		$sites = $query->where(['company_id' => Configure::read('Website.company_id')])
			->contain(['Countries'])
			->toArray();

		$locale = Configure::read('Website.locale');
		$languages = null;
		foreach($sites as $site) {
			if($site->country->locale == $locale) {
				continue;
			}
			$languages[] = [
				'website_id' => $site->id,
				'name' => $site->country->name,
				'country' => $site->country->country,
				'locale' => $site->country->locale
			];
		}
		return $languages;
	}

}
