<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;

class BlocksTable extends Table {

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return Configure::read('Website.datasource');
		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
        //$this->addBehavior('Block');

		$this->belongsTo('BlockTypes');
		$this->belongsTo('Pages');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('page_id')
            ->notEmpty('block_type_id')
            ->notEmpty('data')
            ->notEmpty('region');

        return $validator;
    }

	public function beforeSave(Event $event, Entity $entity) {
	    if(isset($entity->duplicate)) {
		    return true;
	    }
		$data = $entity->toArray();
		if(isset($data['block']['Model']) && $data['block']['Model'] == 'Posts') {
			$data['block']['post_ids'] = $data['block']['posts'];
		}
		$entity->set([
			'data' => $data['block']
		]);
		return true;
    }

}