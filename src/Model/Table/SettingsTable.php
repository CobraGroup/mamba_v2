<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\Cache\Cache;

class SettingsTable extends Table {

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return Configure::read('Website.datasource');
		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('site_logo', 'Path to logo required.')
            ->notEmpty('news_posts_per_page', 'Enter number of posts required per page.');

        return $validator;
    }

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
                
        $this->belongsTo('Pages');
    }

}
