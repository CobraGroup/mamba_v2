<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Query;

class DomainsTable extends Table {

    public function initialize(array $config)
    {
    	//ConnectionManager::config('default');

        $this->addBehavior('Timestamp');
/*
		$this->belongsTo('Websites', [
			'className' => 'Websites',
			'foreignKey' => 'website_id',
			'conditions' => ['live' => true]
		]);
*/
    }

	public static function defaultConnectionName()
	{
		return 'default';
	}

    public function requirePresence(Validator $validator)
    {
        $validator
            ->notEmpty('name', 'Please fill this field')
			->validatePresence('name');

        return $validator;
    }

    public function findWebsite(Query $query, array $options)
    {
        $query->where([
            'Domains.name' => $options['domain']
        ]);
        return $query;
    }

}
