<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Cache\Cache;
use Cake\I18n\I18n;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Core\Configure;

class PagesTable extends Table
{

	public $breadcrumbs = null;

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return Configure::read('Website.datasource');
		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

         $this->hasMany('Blocks', [
        	'className' => 'Blocks',
        	'foreign_key' => 'page_id',
        	'dependent' => true
         ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('title')
            ->notEmpty('slug');

        return $validator;
    }

    public function beforeSave(Event $event, Entity $entity) {
	    if(isset($entity->duplicate)) {
		    return true;
	    }
	    $entity->set(['locale' => I18n::locale()]);
	    return true;
	}

    public function beforeFind(Event $event, Query $query) {
	    $query->where(['locale' => I18n::locale()]);
	}

	public function findTreeList(Query $query, array $options) {
		return $this
			->find('threaded', ['parentField' => 'parent_id', 'order' => ['rank' => 'ASC']])
			->formatResults(function ($results) use ($options) {
				$options += [
					'keyPath' => 'id',
					'valuePath' => 'title',
					'spacer' => '-'
				];
				return $results
					->listNested()
					->printer($options['valuePath'], $options['keyPath'], $options['spacer']);
			});
	}

/*
 * Menu structure, active links & breadcrumbs
 */
	public function menu($website_id = null)
	{
		if(!$website_id)
			$website_id = Configure::read('Website.id');

		$pages = $this->find('threaded', ['parentField' => 'parent_id', 'order' => ['rank' => 'ASC']])
			->select(['slug', 'title', 'id', 'parent_id'])
			->where(['website_id' => $website_id])
			->toArray();

		if($pages)
		{
			$menus = [];
			foreach($pages as $menu)
				$menus[] = $menu->toArray();

			$crumb[] = sprintf('<a href="%s">%s</a>', '/', 'Home');
			$this->breadcrumbs = $crumb;

			// set active links
			foreach($menus as $i => $parent)
			{
				if('/'.$parent['slug'].'/' == env('REQUEST_URI'))
				{
					$crumb[] = $parent['title'];
					$this->breadcrumbs = $crumb;

					$menus[$i]['style'] = true;
					return $menus;
				}
				foreach($parent['children'] as $j => $child)
				{
					if('/'.$parent['slug'].'/'.$child['slug'].'/' == env('REQUEST_URI'))
					{
						$crumb[] = sprintf('<a href="/%s">%s</a>', $menus[$i]['slug'], $menus[$i]['title']);
						$crumb[] = $child['title'];
						$this->breadcrumbs = $crumb;

						$menus[$i]['style'] = true;
						$menus[$i]['children'][$j]['style'] = true;
						return $menus;
					}
					
					foreach($child['children'] as $k => $grandchild)
					{
						if('/'.$parent['slug'].'/'.$grandchild['slug'].'/' == env('REQUEST_URI'))
						{
							$crumb[] = sprintf('<a href="/%s">%s</a>', $menus[$i]['slug'], $menus[$i]['title']);
							$crumb[] = sprintf('<a href="/%s">%s</a>', $menus[$i]['slug'].'/'.$parent['children'][$j]['slug'], $parent['children'][$j]['title']);
							$crumb[] = $grandchild['title'];
							$this->breadcrumbs = $crumb;
	
							$menus[$i]['style'] = true;
							$menus[$i]['children'][$k]['style'] = true;
							return $menus;
						}
					}
				}
			}

			return $menus;
		}

		return null;
	}

}
