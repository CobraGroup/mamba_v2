<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class LanguagesTable extends Table {

    public function initialize(array $config) {
        #$this->addBehavior('Timestamp');

        $this->belongsToMany('Websites', [
        	'className' => 'Websites',
        	'joinTable' => 'websites_languages',
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('name')
            ->notEmpty('country')
            ->notEmpty('locale');
          //  ->notEmpty('slug');

        return $validator;
    }

}
