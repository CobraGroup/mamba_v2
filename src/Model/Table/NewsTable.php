<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\I18n\I18n;
use Cake\Cache\Cache;
use Cake\Core\Configure;

class NewsTable extends Table {

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return 'default';
			//return Configure::read('Website.datasource');

		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('title')
            ->notEmpty('slug');
           // ->notEmpty('body');

        return $validator;
    }

    public function beforeSave(Event $event, Entity $entity) {
	    if(isset($entity->duplicate)) {
		    return true;
	    }

		if(I18n::locale() == 'en_CA') {
			$preslug = '/news-and-blog/';
		} else {
			$preslug = '/news/';
		}

	    $entity->set([
	    	'locale' => I18n::locale(),
	    	'slug' => $preslug.$entity->slug_title
	    ]);
	    return true;
	}

    public function beforeFind(Event $event, Query $query) {
	    if(isset($entity->duplicate)) {
		    return true;
	    }
	    $website = Configure::read('Website');
	    if($website["id"]!=18) $query->where(['locale' => I18n::locale()]);
	}

    public function findBlock(Query $query, array $options) {
        $query->where([
            'status' => 'Published',
            'locale' => I18n::locale()
        ])->limit(2);
        return $query;
    }

    public function findLocaleNews(Query $query, array $options) {
        $query->where([
            'status' => 'Published',
            'locale' => $options['locale']
        ])
    	->select(['id', 'locale', 'title', 'created', 'excerpt', 'thumb', 'slug'])
		->order(['created' => 'DESC'])
		->limit(1);
        return $query;
    }

    public function findPopular(Query $query, array $options) {
 		$query->where([
 			'status' => 'Published',
 			'locale' => I18n::locale(),
            'created >=' => date("Y-m-d", mktime(0,0,0,1,1,date("Y")-1))

 		]);
        return $query;
    }

    public function findSlug(Query $query, array $options) {
        $query->where([
            'News.status' => true,
            'News.locale' => I18n::locale(),
            'News.slug' => $options['slug']
        ]);
        return $query;
    }

}
