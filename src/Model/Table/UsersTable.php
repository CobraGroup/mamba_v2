<?php

namespace App\Model\Table;

use Cake\Auth\DigestAuthenticate;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Cache\Cache;
use Cake\Core\Configure;

class UsersTable extends Table {

	public static function defaultConnectionName() {
		return 'default';
	}

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->belongsToMany('Websites', [
        	'className' => 'Websites',
        	'joinTable' => 'users_websites',
        ]);
	}

    public function validationDefault(Validator $validator) {
        return $validator
            ->notEmpty('name', 'A name is required')
            ->notEmpty('username', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', ['Admin', 'Editor']],
                'message' => 'Please enter a valid role'
            ]);
    }

    public function beforeSave(Event $event) {
        $entity = $event->data['entity'];

        // Make a password for digest auth.
        $entity->digest_hash = DigestAuthenticate::password(
            $entity->username,
            $entity->plain_password,
            env('SERVER_NAME')
        );
        return true;
    }
}