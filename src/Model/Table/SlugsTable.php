<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Query;
use Cake\Cache\Cache;
use Cake\I18n\I18n;
use Cake\Event\Event;
use Cake\ORM\Entity;

class SlugsTable extends Table {

	public static function defaultConnectionName() {
		$data = Cache::read(env('HTTP_HOST'), 'domains');
		return $data['datasource'];
	}

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');            
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('name');

        return $validator;
    }
    
    public function beforeSave(Event $event, Entity $entity) {
	    $entity->set(['model' => 'Pages', 'locale' => I18n::locale()]);
	    return true;
	}

    public function beforeFind(Event $event, Query $query) {
	    $query->where(['model' => 'Pages', 'locale' => I18n::locale()]);
	}

}
