<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Query;
use Cake\Cache\Cache;
use Cake\Core\Configure;

class ContactsTable extends Table {

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return Configure::read('Website.datasource');
		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}


    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
    }


    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('phone')
            ->notEmpty('name')
            ->notEmpty('email')
            ->notEmpty('message');

        return $validator;
    }



}
