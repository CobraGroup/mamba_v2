<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Cache\Cache;
use Cake\Core\Configure;

class AssetsTagsTable extends Table {

	public static function defaultConnectionName() {
		if(Configure::check('Website')) {
			return Configure::read('Website.datasource');
		} else {
			$data = Cache::read(env('HTTP_HOST'), 'domains');
			return $data['datasource'];
		}
	}
    
}