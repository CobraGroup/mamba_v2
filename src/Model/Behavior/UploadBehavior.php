<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Inflector;

class UploadBehavior extends Behavior {

	protected $_table;
	protected $folder;
	protected $thumbs;

	public function __construct(Table $table, array $config = []) {
		parent::__construct($table, $config);
		$this->_table = $table;
	}

	public function beforeSave(Event $event, Entity $entity) {
		$path = $this->path();
		if(empty($entity->filename['name']) && $entity->name) {
			return true;
		}
		if (is_uploaded_file($entity->filename['tmp_name'])) {
			switch($entity->filename['type']) {
				case 'image/jpeg':
				case 'image/png':
				case 'image/gif':
					$name = $this->image($entity->filename, $entity->over_write);
					break;
			}
			
			if($name) {
				$entity->set([
		    		'name' => $name,
		    		'original_name' => $entity->filename['name'],
		    		'path' => $path,
		    		'type' => $entity->filename['type'],
		    	]);
				unset($entity->filename);
				return true;
	    	}
		}
		return false;
	}
	
	public function beforeDelete(Event $event, Entity $entity) {
		// ?? check if image is used by other posts before permantent delete
	}
	
	public function afterDelete(Event $event, Entity $entity) {
		unlink(WWW_ROOT.$entity->path.DS.$entity->name);
		return true;
	}
	
	protected function path() {
		$path = 'uploads/'.date('Y/m');
		
    	$folder = new Folder(WWW_ROOT.$path, true, 0777);
    	$this->folder = $folder->path;

    	$thumbs = new Folder($folder->path.'/thumbs', true, 0777);
    	$this->thumbs = $thumbs->path;
    	
    	return DS.$path;
	}
	
	protected function image($file, $over_write) {
	    $filename = strtolower(Inflector::camelize($file['name']));
	    $filetype = $file['type'];
		// uploaded file info
    	$file = new File($file['tmp_name']);
	 	// copy uploaded file to destination 
    	if($file->copy($this->folder.DS.$filename, $over_write)) {
	    	$this->generateThumb($filename, $filetype);
    	} else {
    		$filename = null;
    	}
		$file->delete();
		return $filename;
	}
	
	protected function generateThumb($filename, $filetype) {
		list($width, $height) = getimagesize($this->folder.DS.$filename);
		$dim = $this->dimension('resizeCrop', $width, $height, 250, 200);
					
		$thumb = imagecreatetruecolor($dim['new_width'], $dim['new_height']);
		
		switch($filetype) {
			case 'image/jpeg':
				$source = imagecreatefromjpeg($this->folder.DS.$filename);
				break;
			case 'image/png':
				$source = imagecreatefrompng($this->folder.DS.$filename);
				break;
			case 'image/gif':
				$source = imagecreatefromgif($this->folder.DS.$filename);
				break;
			default :
				$source = null;
		}
		
		if(!$source) {
			// invalidate	
		}
		
		imagecopyresampled($thumb, $source, 0, 0 , $dim['x'], $dim['y'], $dim['new_width'], $dim['new_height'], $dim['width'], $dim['height']);

		switch($filetype) {
			case 'image/jpeg':
				imagejpeg($thumb, $this->thumbs.DS.$filename, 80);
				break;
			case 'image/png':
				imagepng($thumb, $this->thumbs.DS.$filename, 9);
				break;
			case 'image/gif':
				imagegif($thumb, $this->thumbs.DS.$filename);
				break;
			default :
				//return false;
		}
		imagedestroy($thumb);
		imagedestroy($source);
	}
	
	protected function dimension($type, $oldWidth, $oldHeight, $newWidth, $newHeight) {
		switch ($type){
		
			case 'resize':
				# Maintains the aspect ration of the image and makes sure that it fits
				# within the maxW(newWidth) and maxH(newHeight) (thus some side will be smaller)
				$widthScale = 2;
				$heightScale = 2;

				// Check to see that we are not over resizing, otherwise, set the new scale
				if($newWidth) {
					if($newWidth > $oldWidth) $newWidth = $oldWidth;
					$widthScale = 	$newWidth / $oldWidth;
				}
				if($newHeight) {
					if($newHeight > $oldHeight) $newHeight = $oldHeight;
					$heightScale = $newHeight / $oldHeight;
				}
				//debug("W: $widthScale  H: $heightScale<br>");
				if($widthScale < $heightScale) {
					$maxWidth = $newWidth;
					$maxHeight = false;
				} elseif ($widthScale > $heightScale ) {
					$maxHeight = $newHeight;
					$maxWidth = false;
				} else {
					$maxHeight = $newHeight;
					$maxWidth = $newWidth;
				}

				if($maxWidth > $maxHeight){
					$applyWidth = $maxWidth;
					$applyHeight = ($oldHeight*$applyWidth)/$oldWidth;
				} elseif ($maxHeight > $maxWidth) {
					$applyHeight = $maxHeight;
					$applyWidth = ($applyHeight*$oldWidth)/$oldHeight;
				} else {
					$applyWidth = $maxWidth;
					$applyHeight = $maxHeight;
				}
				$startX = 0;
				$startY = 0;
				break;
				
			case 'resizeCrop':
				// Check to see that we are not over resizing, otherwise, set the new scale
				// -- resize to max, then crop to center
				if($newWidth > $oldWidth) $newWidth = $oldWidth;
					$ratioX = $newWidth / $oldWidth;

				if($newHeight > $oldHeight) $newHeight = $oldHeight;
					$ratioY = $newHeight / $oldHeight;									

				if ($ratioX < $ratioY) {
					$startX = round(($oldWidth - ($newWidth / $ratioY))/2);
					$startY = 0;
					$oldWidth = round($newWidth / $ratioY);
					$oldHeight = $oldHeight;
				} else {
					$startX = 0;
					$startY = round(($oldHeight - ($newHeight / $ratioX))/2);
					$oldWidth = $oldWidth;
					$oldHeight = round($newHeight / $ratioX);
				}
				$applyWidth = $newWidth;
				$applyHeight = $newHeight;
				break;

			case 'crop':
				// -- a straight centered crop
				$startY = ($oldHeight - $newHeight)/2;
				$startX = ($oldWidth - $newWidth)/2;
				$oldHeight = $newHeight;
				$applyHeight = $newHeight;
				$oldWidth = $newWidth;
				$applyWidth = $newWidth;
				break;
		}
		return array(
			'x' => $startX,
			'y' => $startY,
			'new_width' => $applyWidth,
			'new_height' => $applyHeight,
			'width' => $oldWidth,
			'height' => $oldHeight
		);
	}
	
}