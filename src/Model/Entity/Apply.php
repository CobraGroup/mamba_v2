<?php

namespace App\Model\Entity;


use Cake\ORM\Entity;

class Apply extends Entity {


    protected $_accessible = [
        'email'            => true,
        'first_name'       => true,
        'covering_letter'  => true,
        'phone'            => true,

    ];

}
