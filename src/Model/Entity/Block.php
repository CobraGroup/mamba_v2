<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class Block extends Entity {

    protected function _getData() {

        // is_array required for saving data
        if(is_array($this->_properties['data'])) {
        	return serialize($this->_properties['data']);
        }
	    else {
	        // if ID does not exist, this is a new record therefore do not attempt to unserialize
	        if(!isset($this->_properties['id'])) {
		        return $this->_properties['data'];
	        }

	        $data = unserialize($this->_properties['data']);

	        if(array_key_exists('Model', $data) && in_array('posts', $data)) {


				if($data['category_id'] == 10) {
			        $data['posts'] = TableRegistry::get('Posts')
			        	->find('all')
			        	->applyOptions(['injectLocale'=>false])
			        	->select(['id', 'title','subtitle', 'excerpt', 'body', 'thumb', 'slug', 'slug_override'])
			        	->where(['Posts.category_id' => 10])
			        	->where(['Posts.id IN' => $data['post_ids']])
						->order(['title' => 'ASC'])
			        	->toArray();

				} else {
			        $data['posts'] = TableRegistry::get('Posts')
			        	->find('all')
						->applyOptions(['injectLocale'=>true])
			        	->select(['id', 'title','subtitle', 'excerpt', 'body', 'thumb', 'slug', 'slug_override'])
			        	->where(['Posts.id IN' => $data['post_ids']])
						->order(['rank' => 'ASC', 'title' => 'ASC'])
			        	->toArray();
 				}

	        }


	        if(array_key_exists('Model', $data) && in_array('News', $data)) {
		        $website = Configure::read('Website');
		        if($website["id"]==18){
			        $data['list'] = array();
			        $english_countries = ["en_GB","en_AU","en_US","en_CA","en_NZ","en_IN"];
			        foreach($english_countries as $english_country_locale){
				        $options = [
				        	'callbacks' => false,
				        	'locale' => $english_country_locale
				        ];
						$local_news = TableRegistry::get('News')->find('localeNews', $options)->toArray();
				        if(count($local_news)) $data['list'] = array_merge($data['list'], $local_news);
			        }
		        }else{
			        if($data['order'] == 'recent') {
				        $order = ['created' => 'DESC'];
			        } else {
				        $order = ['read_count' => 'DESC'];
			        }
					$data['list'] = TableRegistry::get('News')
						->find('all')
			        	->select(['id', 'title', 'created', 'excerpt', 'thumb', 'slug'])
			        	->where(['status' => 'Published'])
						->order($order)
						->limit($data['limit'])
			        	->toArray();
		        }
	        }
/*

			if(in_array('submenu', $data)) {
				$id = $data['page_id'];
				$data['submenu'] = TableRegistry::get('Pages')
					->find('list', ['idField' => 'slug', 'valueField' => 'title'])
					->where(['OR' => ['id' => $id, 'parent_id' => $id]])
					->order(['rank' => 'ASC'])
					->toArray();
			}
*/


			if(in_array('submenu', $data)) {
				$id = $data['page_id'];
				$data['submenu'] = TableRegistry::get('Pages')
					->find('all')
					->select(['id', 'slug', 'title'])
					->where(['OR' => ['id' => $id, 'parent_id' => $id]])
		        	->where(['status' => 'Published'])
					->order(['rank' => 'ASC'])
					->toArray();
				$data['submenu'] = json_decode(json_encode($data['submenu']),TRUE);

				foreach($data['submenu'] as $id => $record) {
					$submenu_sub = TableRegistry::get('Pages')
									->find('all')
									->select(['id', 'parent_id', 'slug', 'title'])
									->where(['OR' => ['parent_id' => $record['id']]])
									->where(['status' => 'Published'])
									->order(['rank' => 'ASC'])
									->toArray();
					if(($id>0) && count($submenu_sub)) $data['submenu']['sub'][$record["id"]] = json_decode(json_encode($submenu_sub),TRUE);
/*
					$data['submenu']['sub'][$record['id']]= json_decode(json_encode($data['submenu']['sub']),TRUE);
*/
				}

			}

	        return $data;
        }
        //return $this->_properties['data'];
    }

}
