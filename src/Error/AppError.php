<?php

namespace App\Error;

use Cake\Error\BaseErrorHandler;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Exception\ExceptionRenderer;

class AppError extends BaseErrorHandler {
    public function _displayError($error, $debug) {
        return 'There has been an error!';
    }
    public function _displayException($exception) {
        return 'There has been an exception!';
    }
    // Other methods.

    public function handleFatalError($code, $description, $file, $line)
    {
        return 'A fatal error has happened';
    }
    public function isSecured() {
        return 'Security is activated';
    }
}
