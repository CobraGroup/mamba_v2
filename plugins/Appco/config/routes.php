<?php

use Cake\Routing\Router;
use Cake\Cache\Cache;

Router::scope('/', function($routes) {

	//$routes->redirect('/our-people/senior-executives', ['controller' => 'Pages', 'action' => 'view', 'slug' => 'senior-executives'], ['status' => 302]);

	if($site = Cache::read(env('HTTP_HOST'), 'domains')) {

		$categories = Cache::read(env('HTTP_HOST'), 'domains');
		if(isset($site['categories'])) {
				$haystack = array('what-we-do','about-us');
			foreach($site['categories'] as $category) {
				if(!in_array($category, $haystack)) {
					$routes->connect(DS.$category.DS.':slug', ['controller' => 'posts', 'action' => 'view', $category]);
				}
			}
		}
	}

});

