<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php

		$country =  ($website['locale'] == 'en_GB' ? 'UK': 'group');
		if(isset($page)) {
			if(isset($page->meta_title) && $page->meta_title !== ''){
				$this->assign('title', $page->meta_title);
			} else if(isset($page->title)){
				$this->assign('title', $page->title);
			} else {
				$this->assign('title', 'Appco Group'.ucwords(strtolower(str_replace('/',' | ',str_replace('-',' ', $this->request->here)))));
			}
		}

		if(isset($news)){
			 if($this->request->here === '/news') {
				$this->assign('title', 'Latest News | Appco '.$country);
			} else if(isset($news->meta_title) && $news->meta_title !== ''){
				$this->assign('title', $news->meta_title);
			} else {
				$this->assign('title', $news->title . ' | Appco '.$country);
			}
		}
	?>

	<title><?= $this->fetch('title') ?></title>
	<?= $this->Html->meta('icon') ?>


		<?php

		if(isset($page->meta_description)){
			$this->assign('description', $page->meta_description);
		} else if(isset($news->meta_description)){
			$this->assign('description', $news->meta_description);
		} else if($this->request->here == '/news'){
			$this->assign('description', 'Read the latest company news from Appco Group '.$country.' & keep up to date with the latest developments from the field marketing industry.');
		} else {
			$this->assign('description', 'Appco Group');
		}

	?>


	<meta name="title" content="<?= $this->fetch('title') ?>">
	<meta name="description" content="<?= $this->fetch('description') ?>">

	<meta name="p:domain_verify" content="b75ea90b87e805da036b52d32e6d2d46"/>


	<?php if($website['locale'] =='en_GB'): ?>
<script type="text/javascript">
	setTimeout(function(){var a=document.createElement("script");
	var b=document.getElementsByTagName("script")[0];
	a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0040/6574.js?"+Math.floor(new Date().getTime()/3600000);
	a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
	<?php


		  function getAddress()
		 {
		    /*** check for https ***/
			$protocol ='http';

			if ( isset( $_SERVER["HTTPS"] ) && strtolower( $_SERVER["HTTPS"] ) == "on" ) {
			    $protocol .= "s";
			}

		    /*** return the full address ***/
		    return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		 }


		switch (getAddress()) {
			case 'http://www.appcogroup.co.uk/':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/about-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/about-us" />';	break;
			case  'http://www.appcogroup.co.uk/achievements':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/achievements" />';	break;
			case  'http://www.appcogroup.co.uk/appco-worldwide':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/appco-worldwide" />';	break;
			case  'http://www.appcogroup.co.uk/blog':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/blog" />';	break;
			case  'http://www.appcogroup.co.uk/board-of-directors':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors" />';	break;
			case  'http://www.appcogroup.co.uk/board-of-directors/chris-niarchos':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors/chris-niarchos" />';	break;
			case  'http://www.appcogroup.co.uk/board-of-directors/mike-blane':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors/mike-blane" />';	break;
			case  'http://www.appcogroup.co.uk/board-of-directors/paul-sanderson':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors/paul-sanderson" />';	break;
			case  'http://www.appcogroup.co.uk/clients':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/clients" />';	break;
			case  'http://www.appcogroup.co.uk/codes-of-conduct-and-practice':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/codes-of-conduct-and-practice" />';	break;
			case  'http://www.appcogroup.co.uk/complete-fundraising-solutions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/complete-fundraising-solutions" />';	break;
			case  'http://www.appcogroup.co.uk/contact-centre':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/contact-centre" />';	break;
			case  'http://www.appcogroup.co.uk/contact-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/contact-us" />';	break;
			case  'http://www.appcogroup.co.uk/default.asp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/default.aspx':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/Default.aspx':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/default.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/default.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/energy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/energy" />';	break;
			case  'http://www.appcogroup.co.uk/engagement-process-for-field-representatives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/engagement-process-for-field-representatives" />';	break;
			case  'http://www.appcogroup.co.uk/face-to-face-customer-acquisition':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/face-to-face-customer-acquisition" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/complete-fundraising-solutions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/complete-fundraising-solutions" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/face-to-face-customer-acquisition':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/face-to-face-customer-acquisition" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/field-marketing-opportunities':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/field-marketing-opportunities" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing-opportunities':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing-opportunities" />';	break;
			case  'http://www.appcogroup.co.uk/field-representatives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-representatives" />';	break;
			case  'http://www.appcogroup.co.uk/frequently-asked-questions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/frequently-asked-questions" />';	break;
			case  'http://www.appcogroup.co.uk/fundraising':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/fundraising" />';	break;
			case  'http://www.appcogroup.co.uk/home.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/home.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/home.php':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/home-efficiency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/home-efficiency" />';	break;
			case  'http://www.appcogroup.co.uk/how-your-earnings-work':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/how-your-earnings-work" />';	break;
			case  'http://www.appcogroup.co.uk/index.asp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/index.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/index.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/main.asp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/main.aspx':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/main.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/main.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/main.php':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'http://www.appcogroup.co.uk/monthly-security-bond-returns':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/monthly-security-bond-returns" />';	break;
			case  'http://www.appcogroup.co.uk/mythbusters':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/mythbusters" />';	break;
			case  'http://www.appcogroup.co.uk/Mythbusters':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/mythbusters" />';	break;
			case  'http://www.appcogroup.co.uk/news':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news" />';	break;
			case  'http://www.appcogroup.co.uk/news/2013-aussie-racing-cars-super-series':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/2013-aussie-racing-cars-super-series" />';	break;
			case  'http://www.appcogroup.co.uk/news/25th-anniversary-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/25th-anniversary-convention" />';	break;
			case  'http://www.appcogroup.co.uk/news/81-000-uk-homes-receive-healthy-food-boxes-in-2014':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/81-000-uk-homes-receive-healthy-food-boxes-in-2014" />';	break;
			case  'http://www.appcogroup.co.uk/news/a-great-year-for-appco-group-brasil':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/a-great-year-for-appco-group-brasil" />';	break;
			case  'http://www.appcogroup.co.uk/news/ags-celebrate-thriving-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ags-celebrate-thriving-partnership" />';	break;
			case  'http://www.appcogroup.co.uk/news/ags-global-fundraising-services-helps-raise-650-000-at-ejaf-event':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ags-global-fundraising-services-helps-raise-650-000-at-ejaf-event" />';	break;
			case  'http://www.appcogroup.co.uk/news/ags-shows-support-for-pets-in-need':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ags-shows-support-for-pets-in-need" />';	break;
			case  'http://www.appcogroup.co.uk/news/ali-mir-awarded-vice-presidency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ali-mir-awarded-vice-presidency" />';	break;
			case  'http://www.appcogroup.co.uk/news/american-cook-out':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/american-cook-out" />';	break;
			case  'http://www.appcogroup.co.uk/news/anthony-tarquini-receives-vice-presidency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/anthony-tarquini-receives-vice-presidency" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-achieves-record-adoptions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-achieves-record-adoptions" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-ahead-of-uk-government-s-bid-to-boost-british-food':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-ahead-of-uk-government-s-bid-to-boost-british-food" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-brasil-dramatically-increases-revenue-for-unicef':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-brasil-dramatically-increases-revenue-for-unicef" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-client-hello-fresh-scoop-gold-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-client-hello-fresh-scoop-gold-award" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-espana-receives-praise-for-great-professionalism-from-medicos-del-mundo':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-espana-receives-praise-for-great-professionalism-from-medicos-del-mundo" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-about-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-about-us" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-australia-wins-innovation-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-australia-wins-innovation-award" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-backs-coast-to-coast-charity-cycle':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-backs-coast-to-coast-charity-cycle" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-celebrates-top-performers-at-gala-awards-night':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-celebrates-top-performers-at-gala-awards-night" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-convention-2012':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-convention-2012" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-heads-to-south-korea':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-heads-to-south-korea" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-helping-save-lives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-helping-save-lives" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-increases-donor-confidence':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-increases-donor-confidence" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-india-thriving':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-india-thriving" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-in-numbers':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-in-numbers" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-ireland-help-raise-millions-for-hospital-refurbishment':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-ireland-help-raise-millions-for-hospital-refurbishment" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-ireland-sponsors-a-bursary-for-certificate-in-fundraising-qualification':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-ireland-sponsors-a-bursary-for-certificate-in-fundraising-qualification" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-joins-fight-against-poverty':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-joins-fight-against-poverty" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-philippines-volunteers-to-help-typhoon-haiyan-victims':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-philippines-volunteers-to-help-typhoon-haiyan-victims" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-polska-scoops-sales-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-polska-scoops-sales-award" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-promotes-child-sponsorship':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-promotes-child-sponsorship" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-prosales-shine':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-prosales-shine" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-shows-real-heart':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-shows-real-heart" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-singapore-wins-top-performing-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-singapore-wins-top-performing-award" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-support-rebrands-to-reflect-broader-range-of-services':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-support-rebrands-to-reflect-broader-range-of-services" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-thanked-for-great-work':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-thanked-for-great-work" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-group-us-goes-over-the-edge':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-us-goes-over-the-edge" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-help-fight-against-female-mutilation':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-help-fight-against-female-mutilation" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-helping-uk-to-become-greener':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helping-uk-to-become-greener" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-helps-fulfill-young-man-s-dream-to-walk-again':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-fulfill-young-man-s-dream-to-walk-again" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-helps-nbcf-break-record-with-10-000-donors':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-nbcf-break-record-with-10-000-donors" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-helps-raise-13-000-at-battersea-dogs-cats-home-ball':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-raise-13-000-at-battersea-dogs-cats-home-ball" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-helps-zane-conroy-take-first-steps-in-five-years':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-zane-conroy-take-first-steps-in-five-years" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-is-truly-part-of-our-team-says-charity-bmdp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-is-truly-part-of-our-team-says-charity-bmdp" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-it-refresh-to-provide-smarter-service-for-clients':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-it-refresh-to-provide-smarter-service-for-clients" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-kicks-off-lottery':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-kicks-off-lottery" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-malaysia-helps-raise-us-36-million-to-support-natural-conservation':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-malaysia-helps-raise-us-36-million-to-support-natural-conservation" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-managing-director-does-the-als-ice-bucket-challenge':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-managing-director-does-the-als-ice-bucket-challenge" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-meets-animals-in-need':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-meets-animals-in-need" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-netherlands-ranks-in-inc-5000-europe-list':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-netherlands-ranks-in-inc-5000-europe-list" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-partnership-thrives-in-the-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-partnership-thrives-in-the-us" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-partner-tackles-tour-de-france-for-marie-curie-':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-partner-tackles-tour-de-france-for-marie-curie-" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-recognises-top-performers-at-flagship-event':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-recognises-top-performers-at-flagship-event" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-rekindles-relationship-with-the-royal-british-legion-on-wwi-centenary':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-rekindles-relationship-with-the-royal-british-legion-on-wwi-centenary" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-return-to-visit-sos-families':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-return-to-visit-sos-families" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-saves-homeowners-millions-on-their-energy-bills':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-saves-homeowners-millions-on-their-energy-bills" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-sets-up-home-in-krakow':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-sets-up-home-in-krakow" />';	break;
			case  'http://www.appcogroup.co.uk/news/appcos-sporting-ambitions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appcos-sporting-ambitions" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-thailand-signs-record-number-of-unicef-donors':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-thailand-signs-record-number-of-unicef-donors" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-trailwalkers':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-trailwalkers" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-uk-helps-government-boost-energy-efficient-homes':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-helps-government-boost-energy-efficient-homes" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-uk-is-crucial-to-vso-s-dramatic-revenue-growth':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-is-crucial-to-vso-s-dramatic-revenue-growth" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-uk-launches-quality-control-centre':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-launches-quality-control-centre" />';	break;
			case  'http://www.appcogroup.co.uk/news/appco-uk-promotes-renewable-energy-growth':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-promotes-renewable-energy-growth" />';	break;
			case  'http://www.appcogroup.co.uk/news/auckland-hosts-2012-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/auckland-hosts-2012-convention" />';	break;
			case  'http://www.appcogroup.co.uk/news/australians-give-generously-to-support':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/australians-give-generously-to-support" />';	break;
			case  'http://www.appcogroup.co.uk/news/award-winning-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/award-winning-partnership" />';	break;
			case  'http://www.appcogroup.co.uk/news/brazil-delegates-are-flying-high':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/brazil-delegates-are-flying-high" />';	break;
			case  'http://www.appcogroup.co.uk/news/celebrating-25-years-of-success':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-25-years-of-success" />';	break;
			case  'http://www.appcogroup.co.uk/news/celebrating-a-successful-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-a-successful-partnership" />';	break;
			case  'http://www.appcogroup.co.uk/news/celebrating-breakthrough-research':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-breakthrough-research" />';	break;
			case  'http://www.appcogroup.co.uk/news/celebrating-sos-anniversary':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-sos-anniversary" />';	break;
			case  'http://www.appcogroup.co.uk/news/charity-celebrates-exciting-milestone':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/charity-celebrates-exciting-milestone" />';	break;
			case  'http://www.appcogroup.co.uk/news/charity-cycle-ends-with-smiles-and-cheers-at-bondi':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/charity-cycle-ends-with-smiles-and-cheers-at-bondi" />';	break;
			case  'http://www.appcogroup.co.uk/news/charity-seeks-further-expansion':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/charity-seeks-further-expansion" />';	break;
			case  'http://www.appcogroup.co.uk/news/christmas-raffle-in-aid-of-child-s-i-foundation':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/christmas-raffle-in-aid-of-child-s-i-foundation" />';	break;
			case  'http://www.appcogroup.co.uk/news/conquering-kili':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/conquering-kili" />';	break;
			case  'http://www.appcogroup.co.uk/news/crs-hopes-for-chinese-cracker':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/crs-hopes-for-chinese-cracker" />';	break;
			case  'http://www.appcogroup.co.uk/news/declaring-freedom-from-poverty':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/declaring-freedom-from-poverty" />';	break;
			case  'http://www.appcogroup.co.uk/news/dream-it-believe-it-achieve-it':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/dream-it-believe-it-achieve-it" />';	break;
			case  'http://www.appcogroup.co.uk/news/exciting-new-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/exciting-new-partnership" />';	break;
			case  'http://www.appcogroup.co.uk/news/expansion-continues-across-the-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-continues-across-the-us" />';	break;
			case  'http://www.appcogroup.co.uk/news/expansion-continues-stateside':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-continues-stateside" />';	break;
			case  'http://www.appcogroup.co.uk/news/expansion-in-full-swing-across-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-in-full-swing-across-us" />';	break;
			case  'http://www.appcogroup.co.uk/news/expansion-plans-for-wales':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-plans-for-wales" />';	break;
			case  'http://www.appcogroup.co.uk/news/face-to-face-fundraiser-hailed-a-hero-for-preventing-house-fire':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/face-to-face-fundraiser-hailed-a-hero-for-preventing-house-fire" />';	break;
			case  'http://www.appcogroup.co.uk/news/face-to-face-fundraisers-make-massive-difference-to-erskine-s-veterans':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/face-to-face-fundraisers-make-massive-difference-to-erskine-s-veterans" />';	break;
			case  'http://www.appcogroup.co.uk/news/fight-continues-against-heart-disease':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fight-continues-against-heart-disease" />';	break;
			case  'http://www.appcogroup.co.uk/news/fighting-for-harrison':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fighting-for-harrison" />';	break;
			case  'http://www.appcogroup.co.uk/news/france-is-newest-location-in-appco-group-network':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/france-is-newest-location-in-appco-group-network" />';	break;
			case  'http://www.appcogroup.co.uk/news/fundraiser-to-gain-first-hand-experience-in-kenya-':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fundraiser-to-gain-first-hand-experience-in-kenya-" />';	break;
			case  'http://www.appcogroup.co.uk/news/fw1-continues-sponsorship':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fw1-continues-sponsorship" />';	break;
			case  'http://www.appcogroup.co.uk/news/fw1-shines-at-sunshine-coast':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fw1-shines-at-sunshine-coast" />';	break;
			case  'http://www.appcogroup.co.uk/news/fw1-super-series-races-into-sydney':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fw1-super-series-races-into-sydney" />';	break;
			case  'http://www.appcogroup.co.uk/news/generosity-pays-out':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/generosity-pays-out" />';	break;
			case  'http://www.appcogroup.co.uk/news/gold-sponsor-for-ifc-2013':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/gold-sponsor-for-ifc-2013" />';	break;
			case  'http://www.appcogroup.co.uk/news/growing-up-with-cancer-project':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/growing-up-with-cancer-project" />';	break;
			case  'http://www.appcogroup.co.uk/news/head-of-uk-pfra-supports-face-to-face-fundraising':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/head-of-uk-pfra-supports-face-to-face-fundraising" />';	break;
			case  'http://www.appcogroup.co.uk/news/helping-new-zealands-homeless':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/helping-new-zealands-homeless" />';	break;
			case  'http://www.appcogroup.co.uk/news/home-delivery-kick-starts-2014-with-record-breaking-success':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/home-delivery-kick-starts-2014-with-record-breaking-success" />';	break;
			case  'http://www.appcogroup.co.uk/news/how-to-be-a-great-student-in-the-face-to-face-marketing-industry':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/how-to-be-a-great-student-in-the-face-to-face-marketing-industry" />';	break;
			case  'http://www.appcogroup.co.uk/news/inspiring-trip-for-appco-australia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/inspiring-trip-for-appco-australia" />';	break;
			case  'http://www.appcogroup.co.uk/news/inspiring-trip-for-fundraisers':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/inspiring-trip-for-fundraisers" />';	break;
			case  'http://www.appcogroup.co.uk/news/international-womens-day':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/international-womens-day" />';	break;
			case  'http://www.appcogroup.co.uk/news/investing-in-vision':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/investing-in-vision" />';	break;
			case  'http://www.appcogroup.co.uk/news/leading-the-way-in-new-technology':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/leading-the-way-in-new-technology" />';	break;
			case  'http://www.appcogroup.co.uk/news/lunch-with-the-prime-minister':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/lunch-with-the-prime-minister" />';	break;
			case  'http://www.appcogroup.co.uk/news/making-a-difference-this-christmas':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/making-a-difference-this-christmas" />';	break;
			case  'http://www.appcogroup.co.uk/news/monthly-giving-highlights-why-face-to-face-fundraising-works-for-charities':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/monthly-giving-highlights-why-face-to-face-fundraising-works-for-charities" />';	break;
			case  'http://www.appcogroup.co.uk/news/more-than-14m-cut-from-uk-homeowners-energy-bills-in-2014':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/more-than-14m-cut-from-uk-homeowners-energy-bills-in-2014" />';	break;
			case  'http://www.appcogroup.co.uk/news/must-see-ted-talk-busts-myth-that-charities-can-save-the-world-on-a-shoestring':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/must-see-ted-talk-busts-myth-that-charities-can-save-the-world-on-a-shoestring" />';	break;
			case  'http://www.appcogroup.co.uk/news/national-epilepsy-day':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/national-epilepsy-day" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-accommodation-for-young-aussies':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-accommodation-for-young-aussies" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-campaign-continues-saving-lives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-campaign-continues-saving-lives" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-campaign-launched-in-singapore':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-campaign-launched-in-singapore" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-campaign-launches-in-australia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-campaign-launches-in-australia" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-mclaren-gt-unveiled-at-world-s-premier-auto-event':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-mclaren-gt-unveiled-at-world-s-premier-auto-event" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-partnership-for-cobra':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-partnership-for-cobra" />';	break;
			case  'http://www.appcogroup.co.uk/news/new-partnership-in-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-partnership-in-us" />';	break;
			case  'http://www.appcogroup.co.uk/news/north-american-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/north-american-convention" />';	break;
			case  'http://www.appcogroup.co.uk/news/observer-names-appco-client-riverford-retailer-of-the-year':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/observer-names-appco-client-riverford-retailer-of-the-year" />';	break;
			case  'http://www.appcogroup.co.uk/news/owners-meeting-returns-to-the-uk':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/owners-meeting-returns-to-the-uk" />';	break;
			case  'http://www.appcogroup.co.uk/news/paralympian-praises-appco-group':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/paralympian-praises-appco-group" />';	break;
			case  'http://www.appcogroup.co.uk/news/partnership-expands-across-australia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/partnership-expands-across-australia" />';	break;
			case  'http://www.appcogroup.co.uk/news/partnership-helps-wishes-come-true':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/partnership-helps-wishes-come-true" />';	break;
			case  'http://www.appcogroup.co.uk/news/planet-wouldn-t-be-the-same-without-appco-says-charity':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/planet-wouldn-t-be-the-same-without-appco-says-charity" />';	break;
			case  'http://www.appcogroup.co.uk/news/poland-delivers-exceptional-results':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/poland-delivers-exceptional-results" />';	break;
			case  'http://www.appcogroup.co.uk/news/poland-joins-unicef-365-programme':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/poland-joins-unicef-365-programme" />';	break;
			case  'http://www.appcogroup.co.uk/news/poland-s-mid-year-meeting-a-success':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/poland-s-mid-year-meeting-a-success" />';	break;
			case  'http://www.appcogroup.co.uk/news/project-mali':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/project-mali" />';	break;
			case  'http://www.appcogroup.co.uk/news/queensland-hosts-fw1-super-series':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/queensland-hosts-fw1-super-series" />';	break;
			case  'http://www.appcogroup.co.uk/news/regional-conventions-in-spain':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/regional-conventions-in-spain" />';	break;
			case  'http://www.appcogroup.co.uk/news/rio-de-janeiro-opens-its-doors-for-business':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/rio-de-janeiro-opens-its-doors-for-business" />';	break;
			case  'http://www.appcogroup.co.uk/news/royal-garden-party-honours-appco-client-the-british-red-cross':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/royal-garden-party-honours-appco-client-the-british-red-cross" />';	break;
			case  'http://www.appcogroup.co.uk/news/rspca-visit-gives-fundraisers-insight-to-the-business':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/rspca-visit-gives-fundraisers-insight-to-the-business" />';	break;
			case  'http://www.appcogroup.co.uk/news/runners-show-real-heart':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/runners-show-real-heart" />';	break;
			case  'http://www.appcogroup.co.uk/news/safety-on-australias-beaches':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/safety-on-australias-beaches" />';	break;
			case  'http://www.appcogroup.co.uk/news/saving-australia-s-turtles':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/saving-australia-s-turtles" />';	break;
			case  'http://www.appcogroup.co.uk/news/saving-poland-s-wildlife':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/saving-poland-s-wildlife" />';	break;
			case  'http://www.appcogroup.co.uk/news/second-award-for-appco-malaysia-from-telecommunications-client':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/second-award-for-appco-malaysia-from-telecommunications-client" />';	break;
			case  'http://www.appcogroup.co.uk/news/sgi-launches-with-wwf-russia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sgi-launches-with-wwf-russia" />';	break;
			case  'http://www.appcogroup.co.uk/news/sos-donations-help-prevent-poverty':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sos-donations-help-prevent-poverty" />';	break;
			case  'http://www.appcogroup.co.uk/news/spain-delivers-excellent-results':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/spain-delivers-excellent-results" />';	break;
			case  'http://www.appcogroup.co.uk/news/spain-host-their-annual-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/spain-host-their-annual-convention" />';	break;
			case  'http://www.appcogroup.co.uk/news/sponsorship-for-inspiring-athlete':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sponsorship-for-inspiring-athlete" />';	break;
			case  'http://www.appcogroup.co.uk/news/sporting-success-down-under':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sporting-success-down-under" />';	break;
			case  'http://www.appcogroup.co.uk/news/success-for-appco-group-nederland':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/success-for-appco-group-nederland" />';	break;
			case  'http://www.appcogroup.co.uk/news/successful-energy-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/successful-energy-partnership" />';	break;
			case  'http://www.appcogroup.co.uk/news/successful-global-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/successful-global-partnership" />';	break;
			case  'http://www.appcogroup.co.uk/news/successful-year-in-sweden':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/successful-year-in-sweden" />';	break;
			case  'http://www.appcogroup.co.uk/news/support-for-fundraising-congress':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/support-for-fundraising-congress" />';	break;
			case  'http://www.appcogroup.co.uk/news/support-launches-in-sweden':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/support-launches-in-sweden" />';	break;
			case  'http://www.appcogroup.co.uk/news/sweden-hosts-annual-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sweden-hosts-annual-convention" />';	break;
			case  'http://www.appcogroup.co.uk/news/the-difference-between-leadership-and-management':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-difference-between-leadership-and-management" />';	break;
			case  'http://www.appcogroup.co.uk/news/the-forgotten-crisis':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-forgotten-crisis" />';	break;
			case  'http://www.appcogroup.co.uk/news/the-ifc-heads-to-holland':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-ifc-heads-to-holland" />';	break;
			case  'http://www.appcogroup.co.uk/news/the-true-value-of-charity-fundraising-by-dan-pallotta':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-true-value-of-charity-fundraising-by-dan-pallotta" />';	break;
			case  'http://www.appcogroup.co.uk/news/touching-video-from-zane-conroy-thanks-appco-for-support':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/touching-video-from-zane-conroy-thanks-appco-for-support" />';	break;
			case  'http://www.appcogroup.co.uk/news/trekkers-meet-with-the-taoiseach':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/trekkers-meet-with-the-taoiseach" />';	break;
			case  'http://www.appcogroup.co.uk/news/usa-celebrates-dual-success-with-high-achievers-and-new-offices':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/usa-celebrates-dual-success-with-high-achievers-and-new-offices" />';	break;
			case  'http://www.appcogroup.co.uk/news/vice-presidency-for-howie-seymour':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/vice-presidency-for-howie-seymour" />';	break;
			case  'http://www.appcogroup.co.uk/news/why-is-leadership-important-to-me':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/why-is-leadership-important-to-me" />';	break;
			case  'http://www.appcogroup.co.uk/news/wong-chee-wai-is-appco-asia-s-newest-vice-president':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/wong-chee-wai-is-appco-asia-s-newest-vice-president" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise" />';	break;
			case  'http://www.appcogroup.co.uk/privacy-policy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/privacy-policy" />';	break;
			case  'http://www.appcogroup.co.uk/product-and-services-coaching':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/product-and-services-coaching" />';	break;
			case  'http://www.appcogroup.co.uk/product-development':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/product-development" />';	break;
			case  'http://www.appcogroup.co.uk/recognised-qualifications':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/recognised-qualifications" />';	break;
			case  'http://www.appcogroup.co.uk/solar':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/solar" />';	break;
			case  'http://www.appcogroup.co.uk/termination-of-trading-agreement--returning-security-bonds':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/termination-of-trading-agreement--returning-security-bonds" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/alan-telford':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/alan-telford" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/ali-mir':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/ali-mir" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/anthony-tarquini':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/anthony-tarquini" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/darren-dunn':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/darren-dunn" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/howie-seymour':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/howie-seymour" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/james-greaves':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/james-greaves" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/mars-cowley-smyth':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/mars-cowley-smyth" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/michael-scully':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/michael-scully" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/olivier-blomme':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/olivier-blomme" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/paul-burkett':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/paul-burkett" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/saiprakash-kuckian':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/saiprakash-kuckian" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/shane-ward':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/shane-ward" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/simon-murphy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/simon-murphy" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/spencer-galbally':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/spencer-galbally" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/steve-sapsford':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/steve-sapsford" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/stewart-hartley':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/stewart-hartley" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/taras-koochin':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/taras-koochin" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/tony-fernandez':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/tony-fernandez" />';	break;
			case  'http://www.appcogroup.co.uk/vice-presidents/wong-chee-wai':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/wong-chee-wai" />';	break;
			case  'http://www.appcogroup.co.uk/weekly-process':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/weekly-process" />';	break;
			case  'http://www.appcogroup.co.uk/what-to-expect-in-your-first-week':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/what-to-expect-in-your-first-week" />';	break;
			case  'http://www.appcogroup.co.uk/your-field-representative-business-and-status':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/your-field-representative-business-and-status" />';	break;
			case  'https://www.appcogroup.co.uk/':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/about-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/about-us" />';	break;
			case  'https://www.appcogroup.co.uk/achievements':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/achievements" />';	break;
			case  'https://www.appcogroup.co.uk/appco-worldwide':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/appco-worldwide" />';	break;
			case  'https://www.appcogroup.co.uk/blog':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/blog" />';	break;
			case  'https://www.appcogroup.co.uk/board-of-directors':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors" />';	break;
			case  'https://www.appcogroup.co.uk/board-of-directors/chris-niarchos':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors/chris-niarchos" />';	break;
			case  'https://www.appcogroup.co.uk/board-of-directors/mike-blane':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors/mike-blane" />';	break;
			case  'https://www.appcogroup.co.uk/board-of-directors/paul-sanderson':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/board-of-directors/paul-sanderson" />';	break;
			case  'https://www.appcogroup.co.uk/clients':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/clients" />';	break;
			case  'https://www.appcogroup.co.uk/codes-of-conduct-and-practice':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/codes-of-conduct-and-practice" />';	break;
			case  'https://www.appcogroup.co.uk/complete-fundraising-solutions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/complete-fundraising-solutions" />';	break;
			case  'https://www.appcogroup.co.uk/contact-centre':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/contact-centre" />';	break;
			case  'https://www.appcogroup.co.uk/contact-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/contact-us" />';	break;
			case  'https://www.appcogroup.co.uk/default.asp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/default.aspx':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/Default.aspx':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/default.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/default.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/energy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/energy" />';	break;
			case  'https://www.appcogroup.co.uk/engagement-process-for-field-representatives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/engagement-process-for-field-representatives" />';	break;
			case  'https://www.appcogroup.co.uk/face-to-face-customer-acquisition':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/face-to-face-customer-acquisition" />';	break;
			case  'https://www.appcogroup.co.uk/field-marketing':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing" />';	break;
			case  'https://www.appcogroup.co.uk/field-marketing/complete-fundraising-solutions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/complete-fundraising-solutions" />';	break;
			case  'https://www.appcogroup.co.uk/field-marketing/face-to-face-customer-acquisition':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/face-to-face-customer-acquisition" />';	break;
			case  'https://www.appcogroup.co.uk/field-marketing/field-marketing-opportunities':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/field-marketing-opportunities" />';	break;
			case  'https://www.appcogroup.co.uk/field-marketing-opportunities':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing-opportunities" />';	break;
			case  'https://www.appcogroup.co.uk/field-representatives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-representatives" />';	break;
			case  'https://www.appcogroup.co.uk/frequently-asked-questions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/frequently-asked-questions" />';	break;
			case  'https://www.appcogroup.co.uk/fundraising':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/fundraising" />';	break;
			case  'https://www.appcogroup.co.uk/home.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/home.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/home.php':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/home-efficiency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/home-efficiency" />';	break;
			case  'https://www.appcogroup.co.uk/how-your-earnings-work':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/how-your-earnings-work" />';	break;
			case  'https://www.appcogroup.co.uk/index.asp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/index.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/index.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/main.asp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/main.aspx':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/main.htm':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/main.html':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/main.php':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
			case  'https://www.appcogroup.co.uk/monthly-security-bond-returns':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/monthly-security-bond-returns" />';	break;
			case  'https://www.appcogroup.co.uk/mythbusters':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/mythbusters" />';	break;
			case  'https://www.appcogroup.co.uk/news':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news" />';	break;
			case  'https://www.appcogroup.co.uk/news/2013-aussie-racing-cars-super-series':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/2013-aussie-racing-cars-super-series" />';	break;
			case  'https://www.appcogroup.co.uk/news/25th-anniversary-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/25th-anniversary-convention" />';	break;
			case  'https://www.appcogroup.co.uk/news/81-000-uk-homes-receive-healthy-food-boxes-in-2014':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/81-000-uk-homes-receive-healthy-food-boxes-in-2014" />';	break;
			case  'https://www.appcogroup.co.uk/news/a-great-year-for-appco-group-brasil':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/a-great-year-for-appco-group-brasil" />';	break;
			case  'https://www.appcogroup.co.uk/news/ags-celebrate-thriving-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ags-celebrate-thriving-partnership" />';	break;
			case  'https://www.appcogroup.co.uk/news/ags-global-fundraising-services-helps-raise-650-000-at-ejaf-event':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ags-global-fundraising-services-helps-raise-650-000-at-ejaf-event" />';	break;
			case  'https://www.appcogroup.co.uk/news/ags-shows-support-for-pets-in-need':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ags-shows-support-for-pets-in-need" />';	break;
			case  'https://www.appcogroup.co.uk/news/ali-mir-awarded-vice-presidency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/ali-mir-awarded-vice-presidency" />';	break;
			case  'https://www.appcogroup.co.uk/news/american-cook-out':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/american-cook-out" />';	break;
			case  'https://www.appcogroup.co.uk/news/anthony-tarquini-receives-vice-presidency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/anthony-tarquini-receives-vice-presidency" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-achieves-record-adoptions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-achieves-record-adoptions" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-ahead-of-uk-government-s-bid-to-boost-british-food':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-ahead-of-uk-government-s-bid-to-boost-british-food" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-brasil-dramatically-increases-revenue-for-unicef':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-brasil-dramatically-increases-revenue-for-unicef" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-client-hello-fresh-scoop-gold-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-client-hello-fresh-scoop-gold-award" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-espana-receives-praise-for-great-professionalism-from-medicos-del-mundo':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-espana-receives-praise-for-great-professionalism-from-medicos-del-mundo" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-about-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-about-us" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-australia-wins-innovation-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-australia-wins-innovation-award" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-backs-coast-to-coast-charity-cycle':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-backs-coast-to-coast-charity-cycle" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-celebrates-top-performers-at-gala-awards-night':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-celebrates-top-performers-at-gala-awards-night" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-convention-2012':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-convention-2012" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-heads-to-south-korea':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-heads-to-south-korea" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-helping-save-lives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-helping-save-lives" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-increases-donor-confidence':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-increases-donor-confidence" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-india-thriving':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-india-thriving" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-in-numbers':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-in-numbers" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-ireland-help-raise-millions-for-hospital-refurbishment':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-ireland-help-raise-millions-for-hospital-refurbishment" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-ireland-sponsors-a-bursary-for-certificate-in-fundraising-qualification':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-ireland-sponsors-a-bursary-for-certificate-in-fundraising-qualification" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-joins-fight-against-poverty':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-joins-fight-against-poverty" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-philippines-volunteers-to-help-typhoon-haiyan-victims':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-philippines-volunteers-to-help-typhoon-haiyan-victims" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-polska-scoops-sales-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-polska-scoops-sales-award" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-promotes-child-sponsorship':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-promotes-child-sponsorship" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-prosales-shine':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-prosales-shine" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-shows-real-heart':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-shows-real-heart" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-singapore-wins-top-performing-award':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-singapore-wins-top-performing-award" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-support-rebrands-to-reflect-broader-range-of-services':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-support-rebrands-to-reflect-broader-range-of-services" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-thanked-for-great-work':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-thanked-for-great-work" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-group-us-goes-over-the-edge':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-group-us-goes-over-the-edge" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-help-fight-against-female-mutilation':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-help-fight-against-female-mutilation" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-helping-uk-to-become-greener':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helping-uk-to-become-greener" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-helps-fulfill-young-man-s-dream-to-walk-again':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-fulfill-young-man-s-dream-to-walk-again" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-helps-nbcf-break-record-with-10-000-donors':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-nbcf-break-record-with-10-000-donors" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-helps-raise-13-000-at-battersea-dogs-cats-home-ball':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-raise-13-000-at-battersea-dogs-cats-home-ball" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-helps-zane-conroy-take-first-steps-in-five-years':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-helps-zane-conroy-take-first-steps-in-five-years" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-is-truly-part-of-our-team-says-charity-bmdp':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-is-truly-part-of-our-team-says-charity-bmdp" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-it-refresh-to-provide-smarter-service-for-clients':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-it-refresh-to-provide-smarter-service-for-clients" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-kicks-off-lottery':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-kicks-off-lottery" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-malaysia-helps-raise-us-36-million-to-support-natural-conservation':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-malaysia-helps-raise-us-36-million-to-support-natural-conservation" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-managing-director-does-the-als-ice-bucket-challenge':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-managing-director-does-the-als-ice-bucket-challenge" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-meets-animals-in-need':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-meets-animals-in-need" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-netherlands-ranks-in-inc-5000-europe-list':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-netherlands-ranks-in-inc-5000-europe-list" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-partnership-thrives-in-the-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-partnership-thrives-in-the-us" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-partner-tackles-tour-de-france-for-marie-curie-':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-partner-tackles-tour-de-france-for-marie-curie-" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-recognises-top-performers-at-flagship-event':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-recognises-top-performers-at-flagship-event" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-rekindles-relationship-with-the-royal-british-legion-on-wwi-centenary':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-rekindles-relationship-with-the-royal-british-legion-on-wwi-centenary" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-return-to-visit-sos-families':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-return-to-visit-sos-families" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-saves-homeowners-millions-on-their-energy-bills':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-saves-homeowners-millions-on-their-energy-bills" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-sets-up-home-in-krakow':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-sets-up-home-in-krakow" />';	break;
			case  'https://www.appcogroup.co.uk/news/appcos-sporting-ambitions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appcos-sporting-ambitions" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-thailand-signs-record-number-of-unicef-donors':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-thailand-signs-record-number-of-unicef-donors" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-trailwalkers':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-trailwalkers" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-uk-helps-government-boost-energy-efficient-homes':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-helps-government-boost-energy-efficient-homes" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-uk-is-crucial-to-vso-s-dramatic-revenue-growth':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-is-crucial-to-vso-s-dramatic-revenue-growth" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-uk-launches-quality-control-centre':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-launches-quality-control-centre" />';	break;
			case  'https://www.appcogroup.co.uk/news/appco-uk-promotes-renewable-energy-growth':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/appco-uk-promotes-renewable-energy-growth" />';	break;
			case  'https://www.appcogroup.co.uk/news/auckland-hosts-2012-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/auckland-hosts-2012-convention" />';	break;
			case  'https://www.appcogroup.co.uk/news/australians-give-generously-to-support':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/australians-give-generously-to-support" />';	break;
			case  'https://www.appcogroup.co.uk/news/award-winning-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/award-winning-partnership" />';	break;
			case  'https://www.appcogroup.co.uk/news/brazil-delegates-are-flying-high':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/brazil-delegates-are-flying-high" />';	break;
			case  'https://www.appcogroup.co.uk/news/celebrating-25-years-of-success':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-25-years-of-success" />';	break;
			case  'https://www.appcogroup.co.uk/news/celebrating-a-successful-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-a-successful-partnership" />';	break;
			case  'https://www.appcogroup.co.uk/news/celebrating-breakthrough-research':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-breakthrough-research" />';	break;
			case  'https://www.appcogroup.co.uk/news/celebrating-sos-anniversary':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/celebrating-sos-anniversary" />';	break;
			case  'https://www.appcogroup.co.uk/news/charity-celebrates-exciting-milestone':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/charity-celebrates-exciting-milestone" />';	break;
			case  'https://www.appcogroup.co.uk/news/charity-cycle-ends-with-smiles-and-cheers-at-bondi':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/charity-cycle-ends-with-smiles-and-cheers-at-bondi" />';	break;
			case  'https://www.appcogroup.co.uk/news/charity-seeks-further-expansion':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/charity-seeks-further-expansion" />';	break;
			case  'https://www.appcogroup.co.uk/news/christmas-raffle-in-aid-of-child-s-i-foundation':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/christmas-raffle-in-aid-of-child-s-i-foundation" />';	break;
			case  'https://www.appcogroup.co.uk/news/conquering-kili':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/conquering-kili" />';	break;
			case  'https://www.appcogroup.co.uk/news/crs-hopes-for-chinese-cracker':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/crs-hopes-for-chinese-cracker" />';	break;
			case  'https://www.appcogroup.co.uk/news/declaring-freedom-from-poverty':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/declaring-freedom-from-poverty" />';	break;
			case  'https://www.appcogroup.co.uk/news/dream-it-believe-it-achieve-it':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/dream-it-believe-it-achieve-it" />';	break;
			case  'https://www.appcogroup.co.uk/news/exciting-new-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/exciting-new-partnership" />';	break;
			case  'https://www.appcogroup.co.uk/news/expansion-continues-across-the-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-continues-across-the-us" />';	break;
			case  'https://www.appcogroup.co.uk/news/expansion-continues-stateside':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-continues-stateside" />';	break;
			case  'https://www.appcogroup.co.uk/news/expansion-in-full-swing-across-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-in-full-swing-across-us" />';	break;
			case  'https://www.appcogroup.co.uk/news/expansion-plans-for-wales':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/expansion-plans-for-wales" />';	break;
			case  'https://www.appcogroup.co.uk/news/face-to-face-fundraiser-hailed-a-hero-for-preventing-house-fire':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/face-to-face-fundraiser-hailed-a-hero-for-preventing-house-fire" />';	break;
			case  'https://www.appcogroup.co.uk/news/face-to-face-fundraisers-make-massive-difference-to-erskine-s-veterans':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/face-to-face-fundraisers-make-massive-difference-to-erskine-s-veterans" />';	break;
			case  'https://www.appcogroup.co.uk/news/fight-continues-against-heart-disease':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fight-continues-against-heart-disease" />';	break;
			case  'https://www.appcogroup.co.uk/news/fighting-for-harrison':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fighting-for-harrison" />';	break;
			case  'https://www.appcogroup.co.uk/news/france-is-newest-location-in-appco-group-network':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/france-is-newest-location-in-appco-group-network" />';	break;
			case  'https://www.appcogroup.co.uk/news/fundraiser-to-gain-first-hand-experience-in-kenya-':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fundraiser-to-gain-first-hand-experience-in-kenya-" />';	break;
			case  'https://www.appcogroup.co.uk/news/fw1-continues-sponsorship':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fw1-continues-sponsorship" />';	break;
			case  'https://www.appcogroup.co.uk/news/fw1-shines-at-sunshine-coast':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fw1-shines-at-sunshine-coast" />';	break;
			case  'https://www.appcogroup.co.uk/news/fw1-super-series-races-into-sydney':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/fw1-super-series-races-into-sydney" />';	break;
			case  'https://www.appcogroup.co.uk/news/generosity-pays-out':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/generosity-pays-out" />';	break;
			case  'https://www.appcogroup.co.uk/news/gold-sponsor-for-ifc-2013':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/gold-sponsor-for-ifc-2013" />';	break;
			case  'https://www.appcogroup.co.uk/news/growing-up-with-cancer-project':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/growing-up-with-cancer-project" />';	break;
			case  'https://www.appcogroup.co.uk/news/head-of-uk-pfra-supports-face-to-face-fundraising':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/head-of-uk-pfra-supports-face-to-face-fundraising" />';	break;
			case  'https://www.appcogroup.co.uk/news/helping-new-zealands-homeless':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/helping-new-zealands-homeless" />';	break;
			case  'https://www.appcogroup.co.uk/news/home-delivery-kick-starts-2014-with-record-breaking-success':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/home-delivery-kick-starts-2014-with-record-breaking-success" />';	break;
			case  'https://www.appcogroup.co.uk/news/how-to-be-a-great-student-in-the-face-to-face-marketing-industry':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/how-to-be-a-great-student-in-the-face-to-face-marketing-industry" />';	break;
			case  'https://www.appcogroup.co.uk/news/inspiring-trip-for-appco-australia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/inspiring-trip-for-appco-australia" />';	break;
			case  'https://www.appcogroup.co.uk/news/inspiring-trip-for-fundraisers':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/inspiring-trip-for-fundraisers" />';	break;
			case  'https://www.appcogroup.co.uk/news/international-womens-day':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/international-womens-day" />';	break;
			case  'https://www.appcogroup.co.uk/news/investing-in-vision':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/investing-in-vision" />';	break;
			case  'https://www.appcogroup.co.uk/news/leading-the-way-in-new-technology':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/leading-the-way-in-new-technology" />';	break;
			case  'https://www.appcogroup.co.uk/news/lunch-with-the-prime-minister':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/lunch-with-the-prime-minister" />';	break;
			case  'https://www.appcogroup.co.uk/news/making-a-difference-this-christmas':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/making-a-difference-this-christmas" />';	break;
			case  'https://www.appcogroup.co.uk/news/monthly-giving-highlights-why-face-to-face-fundraising-works-for-charities':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/monthly-giving-highlights-why-face-to-face-fundraising-works-for-charities" />';	break;
			case  'https://www.appcogroup.co.uk/news/more-than-14m-cut-from-uk-homeowners-energy-bills-in-2014':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/more-than-14m-cut-from-uk-homeowners-energy-bills-in-2014" />';	break;
			case  'https://www.appcogroup.co.uk/news/must-see-ted-talk-busts-myth-that-charities-can-save-the-world-on-a-shoestring':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/must-see-ted-talk-busts-myth-that-charities-can-save-the-world-on-a-shoestring" />';	break;
			case  'https://www.appcogroup.co.uk/news/national-epilepsy-day':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/national-epilepsy-day" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-accommodation-for-young-aussies':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-accommodation-for-young-aussies" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-campaign-continues-saving-lives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-campaign-continues-saving-lives" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-campaign-launched-in-singapore':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-campaign-launched-in-singapore" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-campaign-launches-in-australia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-campaign-launches-in-australia" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-mclaren-gt-unveiled-at-world-s-premier-auto-event':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-mclaren-gt-unveiled-at-world-s-premier-auto-event" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-partnership-for-cobra':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-partnership-for-cobra" />';	break;
			case  'https://www.appcogroup.co.uk/news/new-partnership-in-us':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/new-partnership-in-us" />';	break;
			case  'https://www.appcogroup.co.uk/news/north-american-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/north-american-convention" />';	break;
			case  'https://www.appcogroup.co.uk/news/observer-names-appco-client-riverford-retailer-of-the-year':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/observer-names-appco-client-riverford-retailer-of-the-year" />';	break;
			case  'https://www.appcogroup.co.uk/news/owners-meeting-returns-to-the-uk':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/owners-meeting-returns-to-the-uk" />';	break;
			case  'https://www.appcogroup.co.uk/news/paralympian-praises-appco-group':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/paralympian-praises-appco-group" />';	break;
			case  'https://www.appcogroup.co.uk/news/partnership-expands-across-australia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/partnership-expands-across-australia" />';	break;
			case  'https://www.appcogroup.co.uk/news/partnership-helps-wishes-come-true':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/partnership-helps-wishes-come-true" />';	break;
			case  'https://www.appcogroup.co.uk/news/planet-wouldn-t-be-the-same-without-appco-says-charity':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/planet-wouldn-t-be-the-same-without-appco-says-charity" />';	break;
			case  'https://www.appcogroup.co.uk/news/poland-delivers-exceptional-results':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/poland-delivers-exceptional-results" />';	break;
			case  'https://www.appcogroup.co.uk/news/poland-joins-unicef-365-programme':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/poland-joins-unicef-365-programme" />';	break;
			case  'https://www.appcogroup.co.uk/news/poland-s-mid-year-meeting-a-success':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/poland-s-mid-year-meeting-a-success" />';	break;
			case  'https://www.appcogroup.co.uk/news/project-mali':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/project-mali" />';	break;
			case  'https://www.appcogroup.co.uk/news/queensland-hosts-fw1-super-series':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/queensland-hosts-fw1-super-series" />';	break;
			case  'https://www.appcogroup.co.uk/news/regional-conventions-in-spain':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/regional-conventions-in-spain" />';	break;
			case  'https://www.appcogroup.co.uk/news/rio-de-janeiro-opens-its-doors-for-business':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/rio-de-janeiro-opens-its-doors-for-business" />';	break;
			case  'https://www.appcogroup.co.uk/news/royal-garden-party-honours-appco-client-the-british-red-cross':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/royal-garden-party-honours-appco-client-the-british-red-cross" />';	break;
			case  'https://www.appcogroup.co.uk/news/rspca-visit-gives-fundraisers-insight-to-the-business':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/rspca-visit-gives-fundraisers-insight-to-the-business" />';	break;
			case  'https://www.appcogroup.co.uk/news/runners-show-real-heart':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/runners-show-real-heart" />';	break;
			case  'https://www.appcogroup.co.uk/news/safety-on-australias-beaches':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/safety-on-australias-beaches" />';	break;
			case  'https://www.appcogroup.co.uk/news/saving-australia-s-turtles':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/saving-australia-s-turtles" />';	break;
			case  'https://www.appcogroup.co.uk/news/saving-poland-s-wildlife':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/saving-poland-s-wildlife" />';	break;
			case  'https://www.appcogroup.co.uk/news/second-award-for-appco-malaysia-from-telecommunications-client':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/second-award-for-appco-malaysia-from-telecommunications-client" />';	break;
			case  'https://www.appcogroup.co.uk/news/sgi-launches-with-wwf-russia':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sgi-launches-with-wwf-russia" />';	break;
			case  'https://www.appcogroup.co.uk/news/sos-donations-help-prevent-poverty':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sos-donations-help-prevent-poverty" />';	break;
			case  'https://www.appcogroup.co.uk/news/spain-delivers-excellent-results':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/spain-delivers-excellent-results" />';	break;
			case  'https://www.appcogroup.co.uk/news/spain-host-their-annual-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/spain-host-their-annual-convention" />';	break;
			case  'https://www.appcogroup.co.uk/news/sponsorship-for-inspiring-athlete':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sponsorship-for-inspiring-athlete" />';	break;
			case  'https://www.appcogroup.co.uk/news/sporting-success-down-under':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sporting-success-down-under" />';	break;
			case  'https://www.appcogroup.co.uk/news/success-for-appco-group-nederland':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/success-for-appco-group-nederland" />';	break;
			case  'https://www.appcogroup.co.uk/news/successful-energy-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/successful-energy-partnership" />';	break;
			case  'https://www.appcogroup.co.uk/news/successful-global-partnership':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/successful-global-partnership" />';	break;
			case  'https://www.appcogroup.co.uk/news/successful-year-in-sweden':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/successful-year-in-sweden" />';	break;
			case  'https://www.appcogroup.co.uk/news/support-for-fundraising-congress':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/support-for-fundraising-congress" />';	break;
			case  'https://www.appcogroup.co.uk/news/support-launches-in-sweden':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/support-launches-in-sweden" />';	break;
			case  'https://www.appcogroup.co.uk/news/sweden-hosts-annual-convention':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/sweden-hosts-annual-convention" />';	break;
			case  'https://www.appcogroup.co.uk/news/the-difference-between-leadership-and-management':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-difference-between-leadership-and-management" />';	break;
			case  'https://www.appcogroup.co.uk/news/the-forgotten-crisis':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-forgotten-crisis" />';	break;
			case  'https://www.appcogroup.co.uk/news/the-ifc-heads-to-holland':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-ifc-heads-to-holland" />';	break;
			case  'https://www.appcogroup.co.uk/news/the-true-value-of-charity-fundraising-by-dan-pallotta':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/the-true-value-of-charity-fundraising-by-dan-pallotta" />';	break;
			case  'https://www.appcogroup.co.uk/news/touching-video-from-zane-conroy-thanks-appco-for-support':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/touching-video-from-zane-conroy-thanks-appco-for-support" />';	break;
			case  'https://www.appcogroup.co.uk/news/trekkers-meet-with-the-taoiseach':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/trekkers-meet-with-the-taoiseach" />';	break;
			case  'https://www.appcogroup.co.uk/news/usa-celebrates-dual-success-with-high-achievers-and-new-offices':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/usa-celebrates-dual-success-with-high-achievers-and-new-offices" />';	break;
			case  'https://www.appcogroup.co.uk/news/vice-presidency-for-howie-seymour':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/vice-presidency-for-howie-seymour" />';	break;
			case  'https://www.appcogroup.co.uk/news/why-is-leadership-important-to-me':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/why-is-leadership-important-to-me" />';	break;
			case  'https://www.appcogroup.co.uk/news/wong-chee-wai-is-appco-asia-s-newest-vice-president':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/news/wong-chee-wai-is-appco-asia-s-newest-vice-president" />';	break;
			case  'https://www.appcogroup.co.uk/our-expertise':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise" />';	break;
			case  'https://www.appcogroup.co.uk/privacy-policy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/privacy-policy" />';	break;
			case  'https://www.appcogroup.co.uk/product-and-services-coaching':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/product-and-services-coaching" />';	break;
			case  'https://www.appcogroup.co.uk/product-development':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/product-development" />';	break;
			case  'https://www.appcogroup.co.uk/recognised-qualifications':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/recognised-qualifications" />';	break;
			case  'https://www.appcogroup.co.uk/solar':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/solar" />';	break;
			case  'https://www.appcogroup.co.uk/termination-of-trading-agreement--returning-security-bonds':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/termination-of-trading-agreement--returning-security-bonds" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/alan-telford':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/alan-telford" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/ali-mir':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/ali-mir" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/anthony-tarquini':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/anthony-tarquini" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/darren-dunn':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/darren-dunn" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/howie-seymour':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/howie-seymour" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/james-greaves':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/james-greaves" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/mars-cowley-smyth':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/mars-cowley-smyth" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/michael-scully':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/michael-scully" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/olivier-blomme':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/olivier-blomme" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/paul-burkett':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/paul-burkett" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/saiprakash-kuckian':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/saiprakash-kuckian" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/shane-ward':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/shane-ward" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/simon-murphy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/simon-murphy" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/spencer-galbally':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/spencer-galbally" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/steve-sapsford':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/steve-sapsford" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/stewart-hartley':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/stewart-hartley" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/taras-koochin':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/taras-koochin" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/tony-fernandez':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/tony-fernandez" />';	break;
			case  'https://www.appcogroup.co.uk/vice-presidents/wong-chee-wai':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/vice-presidents/wong-chee-wai" />';	break;
			case  'https://www.appcogroup.co.uk/weekly-process':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/weekly-process" />';	break;
			case  'https://www.appcogroup.co.uk/what-to-expect-in-your-first-week':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/what-to-expect-in-your-first-week" />';	break;
			case  'https://www.appcogroup.co.uk/your-field-representative-business-and-status':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/your-field-representative-business-and-status" />';	break;
			case  'http://www.appcogroup.co.uk/about-us/meeting-the-highest-standards-of-quality':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/about-us/meeting-the-highest-standards-of-quality" />';	break;
			case  'http://www.appcogroup.co.uk/about-us/achievements':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/about-us/achievements" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/door-to-door-sales-and-services':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/door-to-door-sales-and-services" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/field-representatives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/field-representatives" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/your-field-representative-business-and-status':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/your-field-representative-business-and-status" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/engagement-process-for-field-representatives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/engagement-process-for-field-representatives" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/what-to-expect-in-your-first-week':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/what-to-expect-in-your-first-week" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/product-and-services-coaching':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/product-and-services-coaching" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/weekly-process':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/weekly-process" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/how-your-earnings-work':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/how-your-earnings-work" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/monthly-security-bond-returns':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/monthly-security-bond-returns" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/termination-of-trading-agreement--returning-security-bonds':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/termination-of-trading-agreement--returning-security-bonds" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/codes-of-conduct-and-practice':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/codes-of-conduct-and-practice" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/recognised-qualifications':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/recognised-qualifications" />';	break;
			case  'http://www.appcogroup.co.uk/field-marketing/frequently-asked-questions':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/field-marketing/frequently-asked-questions" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/fundraising':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/fundraising" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/energy':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/energy" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/home-delivery':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/home-delivery" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/home-efficiency':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/home-efficiency" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/solar':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/solar" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/product-development':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/product-development" />';	break;
			case  'http://www.appcogroup.co.uk/our-expertise/contact-centre':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-expertise/contact-centre" />';	break;
			case  'http://www.appcogroup.co.uk/our-people/senior-executives':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-people/senior-executives" />';	break;
			case  'http://www.appcogroup.co.uk/our-people/vice-presidents':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/our-people/vice-presidents" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/perveen-virdee':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/perveen-virdee" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/robert-gibbs':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/robert-gibbs" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/nancy-buxton':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/nancy-buxton" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/graham-bunce':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/graham-bunce" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/dan-brisk':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/dan-brisk" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/ben-oliver':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/ben-oliver" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/lorna-ryan':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/lorna-ryan" />';	break;
			case  'http://www.appcogroup.co.uk/senior-executives/gary-ward':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/senior-executives/gary-ward" />';	break;
			case  'http://www.appcogroup.co.uk/index.php':	echo '<link rel="canonical" href="http://www.appcogroup.co.uk/" />';	break;
		} ?>
	<?php endif; ?>

	<?= $this->Html->css(['reset', '/css/appco/css/all', 'cookiecuttr','custom'], ['defer' => true]) ?>


	<?php
		echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');


	?>
	<?php


		if($this->request->here == '/clients' || $this->request->here == '/klienter' || $this->request->here == '/clientes') {
			echo $this->Html->script('//code.jquery.com/ui/1.10.4/jquery-ui.min.js',['defer' => true]);
			echo $this->Html->script('jquery.bxslider.min',['defer' => true]);
			echo $this->Html->css('jquery.bxslider',['defer' => true]);
		}
		echo $this->Html->script(array('jquery.cookie','jquery.cookiecuttr', 'appco'),['defer' => true]);

	?>
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>

	<?php if(isset($website['analytics'])) :
			echo "<script>";
			echo $website['analytics'];
			echo "</script>";
		endif;
	?>


       <?php

        if($website['locale'] == 'es') {

		?>
			<script type="text/javascript">
				$(document).ready(function() {
					//call cookie pocily
					$.cookieCuttr({
						cookieCutter: true,
						cookieAnalytics: false,
						cookiePolicyLink: '/politica-de-cookies',
					    cookieDeclineButton: false,
					    cookieMessage: 'Esta página web utiliza cookies,  <a href="{{cookiePolicyLink}}" title="click here">haga clic aquí para leer más sobre ellas</a>. Para usar la pagina web correctamente por favor acepte las cookies.',
					    cookieAcceptButtonText: ' Aceptar cookies',
					    cookiePolicyPageMessage: 'You can change the message here',
					    cookieResetButton: false
					});
				});
			</script>

	  <?php
        }else {
	   ?>

			<script type="text/javascript">
				$(document).ready(function() {
					//call cookie pocily
					$.cookieCuttr({
						cookieCutter: true,
						cookieAnalytics: false,
						cookiePolicyLink: '/footer/privacy-policy',
					    cookieDeclineButton: false,
					    cookieMessage: 'We use cookies on this website, you can read about them <a href="{{cookiePolicyLink}}" title="click here"> here</a>. To use the website as intended please...',
					    cookiePolicyPageMessage: 'You can change the message here',
					    cookieResetButton: false
					});
				});
			</script>
		<?php
	        }
        ?>

 	<?php if(strpos($_SERVER['SERVER_NAME'], 'appcogroupusa.com')) : ?>
	<style type="text/css">
		@media screen and (min-width: 900px) {
			 /* one item */
			nav li:first-child::nth-last-child(1) {
			    width: 100%;
			}

			/* two items */
			nav li:first-child::nth-last-child(2),
			nav li:first-child::nth-last-child(2) ~ li {
			    width: 50%;
			}

			/* three items */
			nav li:first-child::nth-last-child(3),
			nav li:first-child::nth-last-child(3) ~ li {
			    width: 33.3333%;
			}

			/* four items */
			nav li:first-child::nth-last-child(4),
			nav li:first-child::nth-last-child(4) ~ li {
			    width: 25%;
			}


			/* two items */
			nav li:first-child::nth-last-child(5),
			nav li:first-child::nth-last-child(5) ~ li {
			    width: 20%;
			}

			/* three items */
			nav li:first-child::nth-last-child(6),
			nav li:first-child::nth-last-child(6) ~ li {
			    width: 16.6667%;
			}

			/* four items */
			nav li:first-child::nth-last-child(7),
			nav li:first-child::nth-last-child(7) ~ li {
			    width: 14.2857%;
			}

			nav li:first-child::nth-last-child(8),
			nav li:first-child::nth-last-child(8) ~ li {
			    width: 12.5%;
			}

			nav li:first-child::nth-last-child(9),
			nav li:first-child::nth-last-child(9) ~ li {
			    width: 11.11%;
			}
			nav li:first-child::nth-last-child(10),
			nav li:first-child::nth-last-child(10) ~ li {
			    width: 10%;
			}

			.nav ul {
				display: table;
			}

			.nav li {
				display: table-cell;
				float: none;
				 text-align: center;

			}

		}
		@media screen and (max-width: 900px) {
			.nav ul {
				display: block;
			}

			nav li:first-child::nth-last-child(8),
			nav li:first-child::nth-last-child(8) ~ li {
			    width: 100%;

			}

			.nav li {
				width: 100%;
				display: block;
				float: left;

			}
		}
	</style>

		<?php endif; ?>



</head>
	<?php $here = DS.current(explode('/', substr($this->request->here, 1)));   ?>
	<?php if($here =='/testimonials' )
					$body_class ='testimony';
		 ?>


<body class="<?= $body_class ?> <?=  str_replace('/', ' ', $this->request->here)  ?>">


	<div <?php  echo $here=='/contact-us'?'itemscope itemtype="http://schema.org/LocalBusiness"':'' ?> id="wrapper">
		<div id="header">
	        <div class="logo-container">
	            <a title="Appco Group" class="logo-left" href="/" hreflang="x-default" rel="alternate">
		            <?php
		            if(substr($logo, 0, 5)!="/img/") $logo = "/img/".$logo;
		            ?>
	                <span style="display:inline-block; width: 240px; height: 70px; background: url(<?php echo $logo ?>) no-repeat left top;">&nbsp;</span>
	            </a>

	            <div class="social_icons">
					<?php $hide = array('sv', 'it', 'es', 'en_US'); ?>
			            <!-- Go to www.addthis.com/dashboard to customize your tools -->
						<script async="async" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a2dce77bf1c151" type="text/javascript"></script>

		            	<ul>

			            	<li></li>

			            	<?php if(strpos($_SERVER['SERVER_NAME'], 'appcogroupusa.com')) { ?>
			            	<li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/fb.png', ['alt' => 'Appco UK Facebook', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.facebook.com/appcogroup',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
			            	</li>
				            <li>
				            		<?= $this->Html->link(
				            				$this->Html->image('appco/tw.png', ['alt' => 'Appco UK Twitter', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://twitter.com/appcogroup',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
				            </li>
			            	<li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/you-tube-appco.jpg', ['alt' => 'Appco UK YouTube', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'http://www.youtube.com/user/appcogrouptv',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
			            	</li>
			            	<?php }?>

			            	<?php if(strpos($_SERVER['SERVER_NAME'], 'appcogroup.es')) { ?>
			            			<li>
									<?= $this->Html->link(
				            				$this->Html->image('appco/fb-es.png', ['alt' => 'Appco Spain Facebook', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.facebook.com/Appco-Group-Espa%C3%B1a-1438293629785516/',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
									</li>
									<li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/tw.png', ['alt' => 'Appco UK Twitter', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://twitter.com/appco_es',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
									</li>
			            	<?php }?>



			            	<?php  if(!in_array($website['locale'], $hide)): ?>
			            		<li>
			            		<?php if($website['locale'] == 'en_CA') : ?>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/fb.png', ['alt' => 'Appco UK Facebook', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.facebook.com/pages/Appco-Group-Canada/241975842603099?ref=hl',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>

			            		<?php elseif($website['locale'] == 'en_IN') : ?>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/fb.png', ['alt' => 'Appco UK Facebook', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.facebook.com/Appco-India-422348737968509/',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
			            		<?php else: ?>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/fb.png', ['alt' => 'Appco UK Facebook', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.facebook.com/appcogroup',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>

			            		<?php endif; ?>
			            		</li>
			            		<li>
			            			<?php if($website['locale'] == 'en_CA') : ?>
				            			<?= $this->Html->link(
					            				$this->Html->image('appco/in.png', ['alt' => 'Appco Canada Linkedin', 'class' => 'eiycgmdoqkquuellrbth']),
					            				'https://www.linkedin.com/company/appco-group?trk=tyah&trkInfo=tas%3AAppco%2Cidx%3A1-1-1',
					            				['target' => '_blank', 'escape' => false]
					            			)
					            		?>
										<?php elseif($website['locale'] == 'en_GB') : ?>
										<?= $this->Html->link(
					            				$this->Html->image('appco/in.png', ['alt' => 'Appco UK Linkedin', 'class' => 'eiycgmdoqkquuellrbth']),
					            				'https://www.linkedin.com/company/appco-group?trk=tyah&trkInfo=tas%3AAppco%2Cidx%3A1-1-1',
					            				['target' => '_blank', 'escape' => false]
					            			)
					            		?>
										<?php elseif($website['locale'] == 'en_IN') : ?>
										<?= $this->Html->link(
					            				$this->Html->image('appco/in.png', ['alt' => 'Appco India Linkedin', 'class' => 'eiycgmdoqkquuellrbth']),
					            				'https://www.linkedin.com/company/appco-group-india',
					            				['target' => '_blank', 'escape' => false]
					            			)
					            		?>
				            			<?php else : ?>

				            			<?= $this->Html->link(
					            				$this->Html->image('appco/in.png', ['alt' => 'Appco UK Linkedin', 'class' => 'eiycgmdoqkquuellrbth']),
					            				'https://www.linkedin.com/company/appco-group?trk=tyah&trkInfo=tas%3AAppco%2Cidx%3A1-1-1',
					            				['target' => '_blank', 'escape' => false]
					            			)
					            		?>
				            		<?php endif; ?>
			            		</li>
			            		<li>
			            			<?php if($website['locale'] == 'en_GB') : ?>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/tw.png', ['alt' => 'Appco UK Twitter', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://twitter.com/appcogroup',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>

				            		<?php else: ?>
				            		<?= $this->Html->link(
				            				$this->Html->image('appco/tw.png', ['alt' => 'Appco UK Twitter', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://twitter.com/appcogroup',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
				            		<?php endif; ?>

			            		</li>


			            		<?php if($website['locale'] !== 'en_AU' && $website['locale'] !== 'en_CA') : ?>
			            		<li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/google-plus-appco.jpg', ['alt' => 'Appco UK Google+', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://plus.google.com/+appcogroup/posts',
				            				['target' => '_blank', 'escape' => false, 'rel' => 'publisher']
				            			)
				            		?>
			            		</li>
			            		<?php endif; ?>
			            		<?php if($website['locale'] == 'en_CA') : ?>
			            		<li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/instagram-icon.jpeg', ['alt' => 'Appco Canada Instagram', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.instagram.com/appcogroupcanada/',
				            				['target' => '_blank', 'escape' => false, 'rel' => 'publisher']
				            			)
				            		?>
			            		</li>

			            		<?php endif; ?>
			            		<?php if($website['locale'] !== 'en_CA') : ?>
			            			<?php if($website['locale'] == 'es') : ?>

			            			<?php else: ?>
			            			<li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/you-tube-appco.jpg', ['alt' => 'Appco UK YouTube', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'http://www.youtube.com/user/appcogrouptv',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
			            		</li>
			            			<?php endif; ?>
			            			<li><?= $this->Html->link(
				            				$this->Html->image('appco/pinterest.png', ['alt' => 'Appco UK Pinterest', 'class' => 'eiycgmdoqkquuellrbth']),
				            				'https://www.pinterest.com/appcouk/',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?></li>

								<?php endif; ?>
		            		<?php endif; ?>

    	            	</ul>
	            </div>
				<?php if(false): ?>
		        <div class="search_bar">
			        <?= $this->Form->create(null, ['url' => '/sitemaps/search', 'type' => 'get', 'accept-charset' => 'utf-8'])?>
				    <?= $this->Form->input('search', ['div' => 'input search', 'label' => false]) ?>
				    <?= $this->Form->submit('Search') ?>
		            <?= $this->Form->end() ?>
		        </div>
				<?php endif ?>
	        </div>

			<?php

				if(strpos($_SERVER['SERVER_NAME'], '.ca')) {
					$nav = [
						'/about-us' => 'About us',
						'/what-we-do' => 'What we do',
						'/board-of-directors' => 'Our people',
						'/news-and-blog' => 'News & Blog',
						'/appco-worldwide' => 'Appco worldwide',
						'/contact-us' => 'Contact us',
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.au')) {
					$nav = [
						'/about-us' => 'About us',
						'/what-we-do' => 'What we do',
						'/our-people/board-of-directors' => 'Our people',
						'/news' => 'News',
						'/appco-worldwide' => 'Appco worldwide',
						'/clients' => 'Clients',
						'/contact-us' => 'Contact us',
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.uk')) {

					$nav = [
						'/about-us' => 'About us',
						'/field-marketing' => 'Field marketing',
						'/our-expertise' => 'Our expertise',
						'/our-people/board-of-directors' => 'Our people',
						'/news' => 'News',
						'/appco-worldwide' => 'Appco worldwide',
						'/blog/' => 'Blog',
						'/clients' => 'Clients',
						'/contact-us' => 'Contact us',
						'/appco-uk-frequently-asked-questions' => 'FAQs'
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.in')) {

					$nav = [
						'/about-us' => 'About us',
						'/what-we-do' => 'What we do',
						'/our-people/vice-presidents' => 'Our people',
						'/news' => 'News',
						'/appco-worldwide' => 'Appco worldwide',
						'/clients' => 'Clients',
						'/contact-us' => 'Contact us',
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.nz')) {
					$nav = [
						'/about-us' => 'About us',
						'/what-we-do' => 'What we do',
						'/our-people/board-of-directors' => 'Our people',
						'/news' => 'News',
						'/appco-worldwide' => 'Appco worldwide',

						'/clients' => 'Clients',
						'/contact-us' => 'Contact us',
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.se')) {
					$nav = [
						'/om-oss' => 'Om oss',
						'/vad-vi-gor' => 'Vad vi gör',
						'/ledning' => 'Vi som arbetar här',
						'/nyheter' => 'Nyheter',
						'/appco-varlden-over' => 'Appco världen över',

						'/klienter' => 'Klienter',
						'/kontakta-oss' => 'Kontakta oss',
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.it')) {
					$nav = [
						'/chi-siamo' => 'Chi siamo',
						'/di-cosa-ci-occupiamo' => 'Di cosa ci occupiamo',
						'/notizie' => 'Notizie',
						'/appco-nel-mondo' => 'Appco nel mondo',
						'/dove-siamo' => 'Dove siamo',
						'/lavora-con-noi' => 'Lavora con noi'
					];
				} elseif(strpos($_SERVER['SERVER_NAME'], '.com')) {

					if(strpos($_SERVER['SERVER_NAME'], 'appcogroupusa.com')) {
						$nav = [
							'/about-us' => 'About us',
							'/face-to-face-marketing' => 'Face-to-face marketing',
							'/our-expertise' => 'Our expertise',
							'/news' => 'News',
							'/appco-worldwide' => 'Appco worldwide',
							'/contact-us' => 'Contact us',
							'/clients' => 'Clients',

						];

					} else {
						$nav = [
							'/about-us' => 'About us',
							'/field-marketing' => 'Field marketing',
							'/our-people' => 'Our people',
							'/news' => 'News',
							'/appco-worldwide' => 'Appco worldwide',
							'/blog-usa' => 'Blog',
							'/clients' => 'Clients',
							'/testimonials' => 'Testimonials',
						];
					}

				} elseif(strpos($_SERVER['SERVER_NAME'], '.es')){

					$nav = [
						'/sobre-appco' => 'Sobre Appco',
						'/que-hacemos' => 'Qué hacemos',
						'/quienes-somos' => 'Quiénes somos',
						'/noticias' => 'Noticias',
						'/el-blog' => 'El blog',
						'/appco-en-el-mundo' => 'Appco en el mundo',
						'/clientes' => 'Clientes',
						'/contacto' => 'Contáctenos',
						'/preguntas-frecuentes' => 'Preguntas frecuentes'
					];

				}
				else {
					$nav = [
/*
						'/about-us' => 'About us',
						'/field-marketing' => 'Field marketing',
						'/our-expertise' => 'Our expertise',
						'/board-of-directors' => 'Our people',
						'/news' => 'News',
						'/appco-worldwide' => 'Appco worldwide',
						'/blog' => 'Blog',
						'/clients' => 'Clients',
						'/contact-us' => 'Contact us'
*/
						'/about-us' => 'About us',
						'/what-we-do' => 'What we do',
						'/vice-presidents' => 'Our people',
						'/news' => 'News',
						'/appco-worldwide' => 'Appco worldwide',
						'/blog' => 'Blog',
						'/clients' => 'Clients',
						//'/testimonials' => 'Testimonials',
						'/contact-us' => 'Contact us',
					];
				}

			?>

		        <nav class="mobile-clearfix nav">
		         <div class="mobile-nav"><!-- <a href="/field-marketing" class="static-menu">Field marketing</a><a href="/news" class="static-menu">| News</a> --><a href="#" id="pull" class="static-menu"></a></div>
				<ul class="mobile-clearfix">
					<li class="home"><?= $this->Html->link(__('Home'), '/', ['class' => $here == '/' ? 'active' : null]) ?></li>
					<?php
						foreach($nav as $slug => $title):
							$attributes = [];
							if($slug == $here)
								$attributes = ['class' => 'active'];
					?>
					<li><?= $this->Html->link(__($title), $slug, $attributes) ?></li>
					<?php endforeach ?>
				</ul>
			</nav>
	        <div class="clearfix"></div>

	    </div>


		<?= $this->Flash->render() ?>

		<?= $this->fetch('content') ?>


		<div class="clearfix"></div>
		<div id="footer">
		    <div class="outter">
	            <div>
	                <div class="left">
	                    <div class="logos">
	                        <ul>
	                            <li><?= $this->Html->link(
		                            $this->Html->image('appco/appco_logo_footer.png', ['alt' => 'Appco group - Field Marketing  Agency', 'itemprop' => 'image' ]),
		                             'http://www.appcogroup.com/',
			            				['target' => '_blank', 'escape' => false]
			            			)?></li>
			            			<?php if($website['locale'] !== 'es'): ?>
	                            <li><?=  $this->Html->link(
		                            $this->Html->image('appco/besomethingmore.png', ['alt' => 'Be something more with Appco']),
			            				'https://www.linkedin.com/company/be-somethingmore',
			            				['target' => '_blank', 'escape' => false]
			            			) ?>
			            		</li>
			            			<?php endif; ?>
	                        </ul>
	                    </div>
	                </div>
	                <div class="right">
		                <?php if(false): ?>
	                    <ul class="logos">
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/responsible-trust.png', ['alt' => 'Responsible Gambling Trusts Donor - Appco']),
			            				'http://www.responsiblegamblingtrust.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li>
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/the-lotteries-council.png', ['alt' => 'The Lotteries Council']),
			            				'http://www.lotteriescouncil.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li>
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/pfra.png', ['alt' => 'PFRA Approved']),
			            				'http://www.pfra.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li>
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/institute-of-fundraising.png', ['alt' => 'Instiitute of Fundraisers']),
			            				'http://www.institute-of-fundraising.org.uk/find-a-supplier/appco-group-support/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li>
	                    </ul>
	                    <?php endif ?>
	                   <?php if($website['locale'] == 'es'): ?>
		                    <style type="text/css">
			                    #footer .right .logos li {
			                    	margin-left:0 !important;
								}
								@media (max-width: 768px) {
									.logos {
										margin-top: 20px;
									}
								}
								@media (max-width: 360px) {
									.logos {
										margin-top: 20px;
									}
								}
		                    </style>
							 <ul class="logos">
	                   			<li style="margin: 0;">
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/aefr.png', ['alt' => 'Asociación Española de Fundraising']),
			            				'http://www.aefundraising.org/quienes/agencias.html',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
	                   			<li style="margin: 0;">
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/apd.png', ['alt' => 'APD']),
			            				'http://www.apd.es/asociados',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
                                <li style="margin: 0;">
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/bccs.png', ['alt' => 'BCCS']),
			            				'http://www.britishchamberspain.com/appco-group-espana.html',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
                                <li style="margin: 0;">
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/corresponsables.png', ['alt' => 'corresponsables']),
			            				'http://www.corresponsables.com/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
                                <li style="margin: 0;">
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/mkt.png', ['alt' => 'MKT']),
			            				'http://www.asociacionmkt.es/socios/appco-group-espana/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
                                <li style="margin: 0;">
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/pimec.png', ['alt' => 'Pimec']),
			            				'http://www.pimec.org/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
                    		</ul>
		                 <?php endif; ?>
	                     <?php if($website['locale'] == 'en_GB'): ?>
							 <ul class="logos">
	                   			<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/appco-dsa-membr_huge.png', ['alt' => 'DSA Member']),
			            				'http://www.dma.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>
	                            <li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/the-lotteries-council.png', ['alt' => 'The Lotteries Council']),
			            				'http://www.lotteriescouncil.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
								</li>
                                <li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/pfra.png', ['alt' => 'PFRA Approved']),
			            				'http://www.pfra.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
			            		</li>
	                            <li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/institute-of-fundraising.png', ['alt' => 'Instiitute of Fundraisers']),
				            				'http://www.institute-of-fundraising.org.uk/',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
			            		</li>
                    		</ul>
		                 <?php endif; ?>
	                    <?php if($website['locale'] == 'en_AU'):?>
	                    <ul class="logos">
							<li>

		                  <?php  echo $this->Html->link(
			            				$this->Html->image('/uploads/2015/02/fiaau.jpg', ['alt' => 'Fia']),
			            				'http://www.fia.org.au/index.php',
			            				['target' => '_blank', 'escape' => false]
			            			); ?>

							</li>
	                    </ul>
			            <?php		endif;
	                    ?>
	                    <?php if($website['locale'] == 'en_NZ'): ?>
	                    <ul class="logos">
							<li><?php

		            			echo $this->Html->link(
			            				$this->Html->image('/img/appco/auckland-chamber-of-commerce.png', ['alt' => 'Auckland Chamber of commerce']),
			            				'http://www.aucklandchamber.co.nz/',
			            				['target' => '_blank', 'escape' => false]
			            			);
								?>
							</li>
		            		<li>
							<?php	echo $this->Html->link(
			            				$this->Html->image('/img/appco/FINZ-Logo.png', ['alt' => 'FINZ-Logo']),
			            				'http://finz.org.nz/',
			            				['target' => '_blank', 'escape' => false]
			            			); ?>
		            		</li>
							<li><?php

		            			echo $this->Html->link(
			            				$this->Html->image('/img/appco/PFRA-Logo.png', ['alt' => 'PFRA Approved']),
			            				'http://pfra.org.nz/',
			            				['target' => '_blank', 'escape' => false]
			            			);
								?>
							</li>
	                    </ul>

			            	<?php endif; ?>
	                     <?php if(strpos($_SERVER['SERVER_NAME'], 'appcogroupusa.com')): ?>
						 	<ul class="logos" style="padding-top: 20px;">
								<li><?php

			            			echo $this->Html->link(
				            				$this->Html->image('/img/appco/dmfa-logo_huge.png', ['alt' => 'DMFA']),
				            				'https://www.dmfa.org/',
				            				['target' => '_blank', 'escape' => false]
				            			);
									?>
								</li>
							</ul>
			            	<?php endif; ?>
	                </div>
	            </div>
		    </div>
		    <div class="grey">
		        <div class="outter">
		            <div class="copyright">
			        	<div class="footer-company-logo">
				        	<?= $this->Html->image('appco/cobra-group-company.png', ['alt' => 'Cobra Group Company']) ?>
			            			<?php if($website['locale'] == 'es'): ?>
	                            <div class="besomething" style="margin-left:15px;float:right;"><?=  $this->Html->link(
		                            $this->Html->image('appco/besomethingmore.png', ['alt' => 'Be something more with Appco']),
			            				'https://www.linkedin.com/company/be-somethingmore',
			            				['target' => '_blank', 'escape' => false]
			            			) ?>
			            		</div>
			            			<?php endif; ?>
			        	</div>
			        	<div class="footer-company-links">
							<ul>
				                <?php if($website['locale'] ==  'sv'): ?>

									<li><?= $this->Html->link(__('Om oss'), '/om-oss') ?></li>
									<li><?= $this->Html->link(__('Vad vi gör'), '/vad-vi-gor') ?></li>
									<li><?= $this->Html->link(__('Nyheter'), '/nyheter') ?></li>
									<li><?= $this->Html->link(__('Kontakter'), '/kontakta-oss') ?></li>
									<li><?= $this->Html->link(__('Sekretess Policy'), '/sekretess-policy') ?></li>
				                <?php elseif($website['locale'] ==  'it'): ?>

									<li><?= $this->Html->link(__('Chi siamo'), '/chi-siamo') ?></li>
									<li><?= $this->Html->link(__('Di cosa ci occupiamo'), '/di-cosa-ci-occupiamo') ?></li>
									<li><?= $this->Html->link(__('Dove siamo'), '/dove-siamo') ?></li>
									<li><?= $this->Html->link(__('Politica sulla riservatezza'), '/politica-sulla-riservatezza') ?></li>
								 <?php elseif($website['locale'] ==  'es'): ?>
									<li><?= $this->Html->link(__('Sobre Appco'), '/sobre-appco') ?></li>
									<li><?= $this->Html->link(__('Qué hacemos'), '/que-hacemos') ?></li>
									<li><?= $this->Html->link(__('Appco en el mundo'), '/appco-en-el-mundo') ?></li>
									<li><?= $this->Html->link(__('Clientes'), '/clientes') ?></li>
								<?php elseif($website['locale'] ==  'en_GB'): ?>
									<li><?= $this->Html->link(__('Appco worldwide'), '/appco-worldwide') ?></li>
	                                <li><?= $this->Html->link(__('News'), '/news') ?></li>
									<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>
	                                <li><?= $this->Html->link(__('Sitemap'), '/sitemap-gb.html') ?></li>
								<?php elseif($website['locale'] ==  'en_IN'): ?>
									<li><?= $this->Html->link(__('About us'), '/about-us') ?></li>
									<li><?= $this->Html->link(__('What we do'), '/what-we-do') ?></li>
									<li><?= $this->Html->link(__('News'), '/news') ?></li>
									<li><?= $this->Html->link(__('Contact us'), '/contact-us') ?></li>
									<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>
	                               	<li><?= $this->Html->link(__('Sitemap'), '/sitemap-in.html') ?></li>
	                            <?php elseif(strpos($_SERVER['SERVER_NAME'], '.appca')): ?>
									<li><?= $this->Html->link(__('About us'), '/about-us') ?></li>
									<li><?= $this->Html->link(__('News'), '/news') ?></li>
									<li><?= $this->Html->link(__('Contact us'), '/contact-us') ?></li>
									<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>
								<?php elseif(strpos($_SERVER['SERVER_NAME'], 'appcogroup.com')): ?>
									<li><?= $this->Html->link(__('About us'), '/about-us') ?></li>

									<li><?= $this->Html->link(__('News'), '/news') ?></li>
									<li><?= $this->Html->link(__('Contact us'), '/appco-worldwide') ?></li>
									<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>
								<?php else: ?>

									<li><?= $this->Html->link(__('About us'), '/about-us') ?></li>
									<li><?= $this->Html->link(__('What we do'), '/what-we-do') ?></li>
									<li><?= $this->Html->link(__('News'), '/news') ?></li>
									<li><?= $this->Html->link(__('Contact us'), '/contact-us') ?></li>
									<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>




								<?php endif; ?>
			                </ul>
			        	</div>
		            </div>
		            <div class="rights">
					   <?php if($website['locale'] == 'sv') : ?>
					        <p>Appco Group © alla rättigheter reserverade 2016 </p>
					   <?php elseif($website['locale'] == 'it') : ?>
					        <p>Appco Group © tutti i diritti riservati 2016</p>
					   <?php else : ?>
					    <p>Appco Group &copy; all rights reserved 2016 </p>
					   <?php endif; ?>


		            </div>
		        </div>
		     </div>
		</div>
	</div>


  <?php if($website['locale'] == 'en_GB') { ?>

	<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NL735P"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NL735P');</script>
	<!-- End Google Tag Manager -->
<?php }?>

</body>
</html>