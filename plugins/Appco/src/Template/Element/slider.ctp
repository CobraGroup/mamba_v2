<div class="<?= $block['style'] ?>">

	<ul class="<?= $block['slider_unique_key'] ?>">
		<?php foreach($block['slider'] as $key => $slide):?>
	    <li>
	    <?php if(isset($slide['link'])): ?>
			<?= $this->Html->image($slide['image'], [
				$slide['title'],
				'url' => $slide['link']
				]); ?>
		<?php	else: ?>
					<?= $this->Html->image($slide['image']) ?>
		<?php endif; ?>
	    </li>
		<?php endforeach; ?>
	</ul>

</div>


<?php $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', ['block' => true]) ?>
<?php $this->Html->script('jquery.bxslider.min', ['block' => true]) ?>

<script>
$(document).ready(function(){
 	$('.<?= $block['slider_unique_key'] ?>').bxSlider({
		mode: '<?= $block['mode'] ?>',
		auto: true,
		speed: <?= $block['speed'] ?>,
		pager: false,
		controls: false
 	});
});
</script>