<ul class="base-row">
	<?php foreach($block as $id => $title):?>
     <li class="base-item" style="height: 272px;">
        <div class="inner">
            <div class="thumb">
            	<?= $this->Html->image($title['image'], ['alt' => $title['captions'], 'class' => 'img-responsive'] )?>
            </div>
            <div class="description">
                <div class="text">
                    <?= $this->Html->tag('h3', $title['title']) ?>
                    <p><?= $this->Html->link(__('View all'), $title['slug']) ?></p>
					<p>&nbsp;</p>
                </div>
            </div>
        </div>
    </li>
	<?php endforeach; ?>
</ul>