<?php
	$display = ' style="display:none;"';
	if($block['title'] == 'All regions') {
		$display = null;
		$block['title'] = str_replace(' ', '', $block['title']);
	}
?>

<table id="<?= $block['title'] ?>"<?= $display ?>>
	<?php
		$posts = array_chunk($block['posts'],2);
		foreach($posts as $post):
	?>
	<tr>
		<?php foreach($post as $country): ?>
		<td data-country="<?= $country->title ?>">
			<h4><?= $country->title ?></h4>
			<?= $country->body ?>
		</td>
		<?php endforeach; ?>
	</tr>
	<?php endforeach; ?>
</table>

