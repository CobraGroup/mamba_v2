<div class="<?= $block['style'] ?>">
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>

	<?php foreach($block['posts'] as $post): ?>
	<div class="person" style="clear: both">
	    <div class="image" style="width:170px;float: left;margin: 10px 0">
			<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?>
	    </div>
	    <div class="description" style="width:560px;float: right;">
	        <h2><?= $post->title ?></h2>
	        <?php if(!empty($post->subtitle)) :?>
	        <h3><?= $post->subtitle ?></h3>
	        <?php endif; ?>

			<p><?= $post->excerpt ?></p>
			<?= $this->Html->link(__('Read more'), $post->slug, ['class' => 'arrow-read']) ?>
	    </div>
	</div>
	<?php endforeach; ?>
</div>