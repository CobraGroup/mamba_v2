<?php if($website['locale'] == 'en_AU'):

					$list = array(
									'your-independent-contractor-business-and-status' => 'Your Independent Contractor business and status',
									'how-independent-contractor-commission-or-fee-earnings-work' =>'How Independent Contractor commission or fee earnings work',
									'product-and-service-training' => 'Product and service training',
									'weekly-process' => 'Weekly process',
									'termination-of-trading-agreement--returning-security-bonds'=>'Termination of Trading Agreement & returning Security Bonds',
									'what-to-expect-in-your-first-week' =>'What to expect in your first week',
									'frequently-asked-questions' => 'Frequently asked questions',
									'laws-and-codes-of-conduct' =>'Laws and codes of conduct'
								);
					$urls = array(
									'/your-independent-contractor-business-and-status',
									'/how-independent-contractor-commission-or-fee-earnings-work',
									'/product-and-service-training',
									'/weekly-process',
									'/termination-of-trading-agreement--returning-security-bonds',
									'/what-to-expect-in-your-first-week',
									'/frequently-asked-questions' ,
									'/laws-and-codes-of-conduct',
									'/independent-contractors'

								);

		endif;
 ?>



<h2><?= $this->Html->link($block['submenu'][0]['title'], $block['submenu'][0]['slug']) ?><a href="#" id="pulldown"></a></h2>
 <?php if($this->request->here == '/appco-uk-frequently-asked-questions' || $this->request->here =='/preguntas-frecuentes') :  ?>
<?php if($website['locale'] == 'en_GB'):

					$list = array(
									'Contacting us',
									'Face-to-face sales and fundraising',
									'The Appco Group Model',
									'Employees vs self-employed contractors',
									'Myths & misconceptions'
								);
					$urls = array(
									'/appco-uk-frequently-asked-questions#1',
									'/appco-uk-frequently-asked-questions#2',
									'/appco-uk-frequently-asked-questions#3',
									'/appco-uk-frequently-asked-questions#4',
									'/appco-uk-frequently-asked-questions#5'
								);

	elseif($website['locale'] == 'es'):

					$list = array(
									'Contáctanos',
									'Captación de fondos y ventas face-to-face',
									'El modelo de negocio de appco group',
									'Empleados vs. Profesionales autónomos'
								);
					$urls = array(
									'/preguntas-frecuentes#1',
									'/preguntas-frecuentes#2',
									'/preguntas-frecuentes#3',
									'/preguntas-frecuentes#4'
								);
		endif;
 ?>


<ul>
	<?php
		$i = 0;
		foreach($list as $title):
	?>
		<li class="anchorLink <?php echo ($this->request->here == '/'.$urls[$i]?'active': ''); ?>"><?= $this->Html->link($title,$urls[$i]) ?></li>
	<?php
		$i++;
		endforeach;
		?>
</ul>
<?php endif; ?>

<ul>

	<?php foreach($block['submenu'] as $id =>  $title): ?>
		<?php if(isset($title['slug']) && ($block['submenu'][0]['slug'] !==$title['slug'])) : ?>
			<li class="<?php echo ($this->request->here == '/'.$title['slug']?'active': ''); ?>"><?= $this->Html->link($title['title'], $block['submenu'][0]['slug'].'/'.$title['slug']) ?></li>
		<?php endif; ?>
		<?php
			if(isset($title['id']) && isset($block['submenu']['sub'][$title['id']])) { ?>
				<ul class="submenu show">

			<?php	foreach($block['submenu']['sub'][$title['id']] as $i => $v): ?>

			<?php


					if($title['id'] == $v['parent_id']) { ?>
						<li class="<?php echo ($this->request->here == '/'.$v['slug']?'active': ''); ?>"><?= $this->Html->link($v['title'], $block['submenu'][0]['slug'].'/'.$v['slug']) ?></li>

			<?php	}
				 ?>

		<?php endforeach; ?>
				</ul>
		<?php } ?>

	<?php endforeach; ?>

</ul>

<?php if($website['locale'] !== '466en_AU'): ?>
 <?php else : ?>
	<h2><?= $this->Html->link(current($block['submenu']), key($block['submenu'])) ?><a href="#" id="pulldown">Submenu</a></h2>
	<ul>
		<?php foreach(array_slice($block['submenu'],1) as $slug =>  $title): ?>

			<li class="<?php echo ($this->request->here == '/'.$slug?'active': ''); ?>"><?= $this->Html->link($title, $slug) ?></li>
		<?php endforeach; ?>
		<?php if($website['locale'] == 'en_AU' && in_array($this->request->here, $urls, true)): ?>
			<ul class="submenu <?php echo in_array($this->request->here, $urls, true)?' show':' hide'; ?>">
			<?php foreach($list as $slug =>  $title): ?>
				<li><?= $this->Html->link($title, $slug) ?></li>
			<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</ul>
<?php endif; ?>
