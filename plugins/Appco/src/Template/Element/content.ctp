<?php
	if(isset($block['title']) && !empty($block['title']))
		$this->assign('title', $block['title']);
?>

<div class="<?= $block['style'] ?>">

	<?php
		if(isset($block['title']) && !empty($block['title']))
			echo $this->Html->tag($block['tag'], $block['title'])
	?>

	<div>
		<?= $block['content'] ?>
	</div>

</div>