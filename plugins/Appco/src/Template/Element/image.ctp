<<?= $block['tag'] ?>  class="<?= $block['tag_style'] ?>">

	<?= $this->Html->link(
			$this->Html->image($block['image'], ['alt' => $block['alternative_text']]),
			$block['link'],
			array('class' => 'no-link-style', 'escape' => false)
		)
	?>

	<?php if($block['title']) echo $this->Html->tag($block['title_tag'], $block['title']) ?>

	<?php if($block['caption']) echo $this->Html->tag('p', $block['caption']) ?>

	<?php if($block['link']) echo $this->Html->link($block['link_label'], $block['link'], array('class' => 'link-style', 'escape' => false)) ?>

</<?= $block['tag'] ?>>
