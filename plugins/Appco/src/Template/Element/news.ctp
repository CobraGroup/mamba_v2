
<div class="<?= $block['style']?$block['style']:''; ?>">
    <div class="header">
		<h2><?=__('Latest news') ?></h2>
		<hr>
	</div>
		<ul>
			<?php foreach($block['list'] as $article): ?>
			<li>
		        <h3><?= $article->title ?></h3>
		        <h4><?= $article->created->format(DATE_FMT)  ?></h4>
		        <?= $article->excerpt ?>
		        <div>
		            <?= $this->Html->link(__('Read full story'), $article->slug, ['class' => 'arrow-read']) ?>
		        </div>
			</li>
			<?php endforeach ?>
		</ul>

</div>

