<div class="<?= $block['style'] ?>">
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>

	<hr />
	<ul>
		<?php foreach($block['posts'] as $post): ?>
		<li>
		    <div class="thumb">
				<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?>
		    </div>
		    <div class="description">
		        <div class="text">
			        <?= $this->Html->tag('h3', $post->title) ?>
			        <?php if(!empty($post->subtitle)) echo  $this->Html->tag('h3', $post->subtitle) ?>
					<p style="padding: 0;margin: 0;"><?= $post->excerpt ?></p>
					<?= $this->Html->link(__('Read more'), $post->slug_override ? $post->slug_override : $post->slug, ['class' => 'arrow-read']) ?>
		        </div>
		    </div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>