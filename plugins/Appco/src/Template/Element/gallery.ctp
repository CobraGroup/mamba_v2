<?php //pr($block); ?>
<ul class="list-inline">
	<?php foreach($block['gallery'] as $gallery):?>
     <li>
     	<?php
	     	$image = $this->Html->image($gallery['image'], ['alt' => $gallery['alternative_text']]);

	     	if($gallery['link']) {
		     	echo $this->Html->link($image, $gallery['link'], ['escape' => false]);
	     	} else {
		     	echo $image;
	     	}
	     ?>
    </li>
	<?php endforeach; ?>
</ul>