<ul>
<?php foreach($block as $id => $title):?>
            <li>
                <div class="thumb sddd"><?= $this->Html->image($title['image'], ['alt' => $title['captions'], 'class' => 'img-responsive'] ) ?>
                </div>
                <div class="description">
                    <div class="text">
                        <h3><?= $title['title'] ?></h3>

	                        <?= $this->Text->truncate($title['excerpt']?$title['excerpt']:$title['body'], 35, ['exact' => true, 'html' => false]) ?><br>
						<a href="<?=  $title['slug'] ?>">Read more</a>
                    </div>
                </div>
            </li>
<?php endforeach; ?>
</ul>