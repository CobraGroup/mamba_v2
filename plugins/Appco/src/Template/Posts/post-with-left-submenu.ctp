<div id="container">
    <div id="banner">
        <div class="left">
			<?= $this->Html->image($post['image'], ['alt' => $post['title'], 'class' => 'img-responsive'] ); ?>
        </div>
    <div class="right">
        <img alt="Appco map banner" src="/uploads/2015/05/map.jpg">
    </div>
    <div class="clearfix"></div>
</div>
	<?php

	// This function will take $_SERVER['REQUEST_URI'] and build a breadcrumb based on the user's current path
	function breadcrumbs($separator = ' &raquo; ', $home = 'Home') {
	    // This gets the REQUEST_URI (/path/to/file.php), splits the string (using '/') into an array, and then filters out any empty values
	    $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

	    // This will build our "base URL" ... Also accounts for HTTPS :)
	    $base = $_SERVER['HTTP_HOST'] . '/';
			//echo "<pre>"; var_dump($base);  echo "</pre>";
	    // Initialize a temporary array with our breadcrumbs. (starting with our home page, which I'm assuming will be the base URL)
	    $breadcrumbs = array("<a href=\"/\">$home</a>");

	    // Find out the index for the last value in our path array
	    $last = end(array_keys($path));

	    // Build the rest of the breadcrumbs
	    foreach ($path as $x => $crumb) {
	       // echo "<pre>"; var_dump($crumb);  echo "</pre>";
	        // Our "title" is the text that will be displayed (strip out .php and turn '_' into a space)
	        $title = ucwords(str_replace(array('.php', '-'), array('', ' '), $crumb));

	        // If we are not on the last index, then display an <a> tag
	        if ($x != $last) {
	            $breadcrumbs[] = "<a href=\"/$crumb\">$title</a>";
	        }

	        // Otherwise, just display the title (minus)
	        else {
		        $breadcrumbs[] = $title;
	        }

	    }

	    // Build our temporary array (pieces of bread) into one big string :)
	    return implode($separator, $breadcrumbs);
	}

	?>

	<p class="breadcrumb"><?= breadcrumbs() ?></p>
<div class="row layout3" id="content">
    <div class="column menu">


	    <?php if(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_IN'): ?>
			<h2><a href="/our-people">Our people</a></h2>
			<ul>
				<li><a href="/our-people/senior-executives">Senior Executives</a></li>
			</ul>
		<?php  elseif(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_CA'):?>
			<ul>
				<h2><a href="/our-people">Our people</a></h2>
				<li><a href="/board-of-directors">Board of Directors</a></li>

				<li><a href="/our-people/country-heads-and-sales-managers">Country Heads and Sales Managers</a></li>
				<li  class="active"><a href="/our-people/senior-executives">Senior Executives</a></li>
			</ul>
		<?php elseif(current($this->request->params['pass']) == 'country-heads-and-sales-managers' && $website['locale'] !== 'en'): ?>
			<ul>
				<h2><a href="/our-people">Our people</a></h2>
				<li><a href="/board-of-directors">Board of Directors</a></li>

				<li class="active"><a href="/our-people/country-heads-and-sales-managers">Country Heads and Sales Managers</a></li>
				<li><a href="/our-people/senior-executives">Senior Executives</a></li>
			</ul>
<?php elseif(in_array($this->request->params['pass'][0], array('appco-group-holdings-bv', 'appco-group-country-heads', 'country-heads-and-sales-managers') ) && $website['locale'] == 'en'): ?>

<h2><a href="/our-people">Our people</a><a id="pulldown" href="#"></a></h2>
<ul>

	<?php //echo "<pre>"; var_dump($website['locale']); echo "</pre>"; ?>
	<li class="<?=  current($this->request->params['pass']) == 'appco-group-board-of-directors' ? 'active': ' '; ?>"><a href="/our-people/appco-group-board-of-directors">Appco Group Board of Directors</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-senior-executives' ? 'active': ' '; ?>"><a href="/our-people/appco-group-senior-executives">Appco Group Senior Executives</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-vice-presidents' ? 'active': ' '; ?>"><a href="/our-people/appco-group-vice-presidents">Appco Group Vice Presidents</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-country-heads' ? 'active': ' '; ?>"><a href="/our-people/appco-group-country-heads">Appco Group Country Heads</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-holdings-bv' ? 'active': ' '; ?>"><a href="/our-people/appco-group-holdings-bv">Appco Group Holdings BV</a></li>


</ul>
		<?php elseif(current($this->request->params['pass']) == 'board-of-directors' && $website['locale'] == 'en_AU'): ?>
			<ul>

				<h2><a href="/our-people">Our people</a></h2>
				<li class="active"><a href="/board-of-directors">Board of Directors</a></li>
				<li><a href="/our-people/senior-executives">Senior Executives</a></li>

			</ul>
		<?php elseif(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_AU'): ?>
			<ul>

				<h2><a href="/our-people">Our people</a></h2>
				<li><a href="/board-of-directors">Board of Directors</a></li>
				<li class="active"><a href="/our-people/senior-executives">Senior Executives</a></li>

			</ul>
			<?php elseif(in_array($this->request->params['pass'][0], array('senior-executives', 'board-of-directors', 'vice-presidents') ) && $website['locale'] == 'en_GB'): ?>
			<ul>

				<h2><a href="/our-people">Our people</a></h2>
				<li class="<?=  current($this->request->params['pass']) == 'board-of-directors' ? 'active': ' '; ?>"><a href="/board-of-directors">Board of Directors</a></li>
				<li class="<?=  current($this->request->params['pass']) == 'senior-executives' ? 'active': ' '; ?>"><a href="/our-people/senior-executives">Senior Executives</a></li>
				<li class="<?=  current($this->request->params['pass']) == 'vice-presidents' ? 'active': ' '; ?>"><a href="/our-people/vice-presidents">Vice Presidents</a></li>
			</ul>

			<?php elseif(in_array($this->request->params['pass'][0], array('senior-executives', 'board-of-directors', 'vice-presidents') ) && $website['locale'] == 'en'): ?>
<h2><a href="/our-people">Our people</a><a id="pulldown" href="#"></a></h2>
<ul>

	<?php //echo "<pre>"; var_dump($website['locale']); echo "</pre>"; ?>
	<li class="<?=  current($this->request->params['pass']) == 'appco-group-board-of-directors' ? 'active': ' '; ?>"><a href="/our-people/appco-group-board-of-directors">Appco Group Board of Directors</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-senior-executives' ? 'active': ' '; ?>"><a href="/our-people/appco-group-senior-executives">Appco Group Senior Executives</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-vice-presidents' ? 'active': ' '; ?>"><a href="/our-people/appco-group-vice-presidents">Appco Group Vice Presidents</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-country-heads' ? 'active': ' '; ?>"><a href="/our-people/appco-group-country-heads">Appco Group Country Heads</a></li>

	<li class="<?=  current($this->request->params['pass']) == 'appco-group-holdings-bv' ? 'active': ' '; ?>"><a href="/our-people/appco-group-holdings-bv">Appco Group Holdings BV</a></li>


</ul>

		<?php elseif(current($this->request->params['pass']) == 'ledning' && $website['locale'] == 'sv'): ?>
			<ul>
				<h2><a href="/our-people">Our people</a></h2>
				<li class="active"><a href="/board-of-directors">Board of Directors</a></li>
				<li><a href="/our-people/senior-executives">Senior Executives</a></li>
			</ul>
			<ul>


				<li class="active"><a href="/ledning">Ledning</a></li>
				<li class=""><a href="/personal">Personal</a></li>
				<li class=""><a href="/fltrepresentanter">Fältrepresentanter</a></li>
						<ul class="submenu show">
							<li class=""><a href="/affrsmjligheter-och-status-som-fltrepresentant">Affärsmöjligheter och status som fältrepresentant</a></li>
							<li class=""><a href="/produkt-och-sljutbildning">Produkt-och säljutbildning</a></li>
							<li class=""><a href="/hur-provisionsbaserad-ln-fungerar">Hur provisionsbaserad lön fungerar</a></li>
							<li class=""><a href="/vanliga-frgor-och-svar">Vanliga frågor och svar</a></li>
							<li class=""><a href="/vad-du-kan-frvnta-dig-under-din-frsta-vecka">Vad du kan förvänta dig under din första vecka</a></li>
							<li class=""><a href="/vad-du-kan-frvnta-dig-vecka-fyra">Vad du kan förvänta dig vecka fyra</a></li>
							<li class=""><a href="/tillbaka-till-dig-som-fltrepresentant">Tillbaka till dig som fältrepresentant</a></li>
						</ul>
			</ul>
		<?php elseif(current($this->request->params['pass']) == 'board-of-directors' && $website['locale'] == 'en_NZ'): ?>
			<ul>
				<h2><a href="/our-people">Our people</a></h2>
				<li class="active"><a href="/board-of-directors">Board of Directors</a></li>
				<li><a href="/our-people/senior-executives">Senior Executives</a></li>
			</ul>
		<?php elseif(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_NZ'): ?>
			<ul>
				<h2><a href="/our-people">Our people</a></h2>
				<li><a href="/board-of-directors">Board of Directors</a></li>
				<li class="active"><a href="/our-people/senior-executives">Senior Executives</a></li>
			</ul>
		<?php endif; ?>

    </div>
    <div class="column content">
        <h1><?= $post['title'] ?></h1>
        <div class="keyplayer">
	        <div class="image" style="width: 170px; float: left;">
				<?= $this->Html->image($post['thumb'], ['alt' => $post['title']]) ?>
	        </div>
            <div class="description">
                <h2><?= $post['subtitle'] ?></h2>
				<p><?= $post['body'] ?></p>
                <div class="go-back-container">
                    <?= $this->Html->link(__('Back to ').$post['category']['name'], $this->request->referer(), ['class' => '']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</div>