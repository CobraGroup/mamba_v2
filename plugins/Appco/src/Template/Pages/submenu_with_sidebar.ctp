<?php //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">
    <div id="banner">
		<?php echo $this->fetch('banner');  ?>
	</div>

    <div class="center">
        <div class="clearfix"></div>
	</div>


    <div class="row layout2" id="content">

        <div class="column menu">
			<?= $this->element('submenu') ?>
		</div>

		<div class="column content">
			<?= $this->fetch('content') ?>
		</div>

		<div class="column right">
			<?= $this->fetch('sidebar') ?>
		</div>

	</div>
	<div class="clearfix"></div>


</div>
