<?php #pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">

    <div id="banner">
		<?php echo $this->fetch('banner');  ?>
	    <div class="clearfix"></div>
	</div>

    <div class="row" id="content">
		<div class="column content">
			<?= $this->fetch('content') ?>

		</div>
	</div>

</div>
