	<?php

	// This function will take $_SERVER['REQUEST_URI'] and build a breadcrumb based on the user's current path
	function breadcrumbs($separator = ' &raquo; ', $home = 'Home') {
	    // This gets the REQUEST_URI (/path/to/file.php), splits the string (using '/') into an array, and then filters out any empty values
	    $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

	    // This will build our "base URL" ... Also accounts for HTTPS :)
	    $base = $_SERVER['HTTP_HOST'] . '/';
			//echo "<pre>"; var_dump($base);  echo "</pre>";
	    // Initialize a temporary array with our breadcrumbs. (starting with our home page, which I'm assuming will be the base URL)
	    $breadcrumbs = array("<a href=\"/\">$home</a>");

	    // Find out the index for the last value in our path array
	    $last = end(array_keys($path));

	    // Build the rest of the breadcrumbs
	    foreach ($path as $x => $crumb) {
	       // echo "<pre>"; var_dump($crumb);  echo "</pre>";
	        // Our "title" is the text that will be displayed (strip out .php and turn '_' into a space)
	        $title = ucwords(str_replace(array('.php', '-'), array('', ' '), $crumb));

	        // If we are not on the last index, then display an <a> tag
	        if ($x != $last) {
	            $breadcrumbs[] = "<a href=\"/$crumb\">$title</a>";
	        }

	        // Otherwise, just display the title (minus)
	        else {
		        $breadcrumbs[] = $title;
	        }

	    }

	    // Build our temporary array (pieces of bread) into one big string :)
	    return implode($separator, $breadcrumbs);
	}

	?>

<div id="container">
    <div id="banner">
<?php if($website['locale'] == 'es'): ?>
		<img src="<?= $banner ?>" />

		<?php else: ?>
	    <div class="left">
			<img src="<?= $banner ?>" />
    	</div>
    	<div class="right">
		    	<img src="/uploads/2014/11/static-map-banner.jpg" alt="Appco worldwide map">
	    </div>
<?php endif; ?>
	</div>

    <div class="row layout4" id="content">

	<p class="breadcrumb"><?= breadcrumbs() ?></p>

        <div class="column content">
	        <h1><?= __('News'); ?></h1>
			<?php foreach ($news as $article): ?>
            <div class="article">
				<div class="image">
                	<img class="news-image" alt="<?= $article->title ?>" src="<?= $article->thumb?>">
            	</div>
            	<?php $article->slug = preg_replace('/\bnews\b/', __('news'), $article->slug); ?>
				<div class="description">
                	<h2><?= $this->Html->link($article->title, $article->slug) ?></h2>
                	<h4><?= $article->created->format(DATE_FMT) ?></h4>

                	<p><?php echo $this->Text->truncate(strip_tags($article->excerpt), 110,array('exact' => false)); ?></p>
	                <div><?= $this->Html->link(__('Read more'), $article->slug, ['class' => 'arrow-read']) ?></div>
            	</div>
			</div>
			<?php endforeach ?>
		</div>
		<div class="column right">
		    <div class="most_read">
			    <div class="header">
				    <h2><?= __('Most read') ?></h2>
				    <hr />
			    </div>
				<ul>
					<?php foreach ($most_read as $article): ?>
					<li>
						<h3><?= $this->Html->link($article->title, $article->slug) ?></h3>
						<h4><?= $article->created->format(DATE_FMT) ?></h4>
						<p><?= $article->excerpt ?></p>
					</li>
					<?php endforeach ?>
				</ul>
	     	</div>
		</div>
	</div>
</div>
<!--[if gte IE 8]>
<style>
ul.pagination {
	clear:both;
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
}

.pagination li {
	display: inline;
	float: left;
	padding: 5px 10px;
}
.pagination li a {
    display: block;
}
</style>
<![endif]-->

<?php if($this->Paginator->numbers()) :?>
<nav>
	<ul class="pagination">
		<?= $this->Paginator->first(); ?>
		<?= $this->Paginator->numbers(); ?>
		<?= $this->Paginator->last(); ?>
	</ul>
</nav>
<?php endif; ?>
