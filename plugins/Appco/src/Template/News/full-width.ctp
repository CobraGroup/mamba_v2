<?php

	$this->start('banner');
		foreach($blocks['banner'] as $block) {
			echo $this->Html->image($block['image'], ['alt' => $block['captions']]);
		}
	$this->end();

	$this->start('news-list');
		if(isset($news)) {
			echo $this->element('news-list', ['block' => $news]);
		}
	$this->end();

	$this->start('popular-news-section');
		if(isset($popular_news)) {
			echo $this->element('popular-news-section', ['block' => $popular_news]);
		}
	$this->end();

?>

<div id="container" >
    <div id="banner">

		<?php echo $this->fetch('banner');  ?>

	    <div class="right">
	        <img alt="Appco map banner" src="/uploads/2014/11/static-map-banner.jpg" />
	    </div>
	    <div class="clearfix">
		</div>
    </div>
    <div class="center">
        <div class="clearfix"></div>
	</div>
	<div class="row layout4" id="content">
		    <div class="column content">
				<?= $this->fetch('news-list') ?>

				<div class="clearfix"></div>
	        </div>
	        <div class="column right">
	            <div class="most_read">
	                         <div class="header">
							 <h3><?=__('Most read') ?></h3>
							 <hr>
	 						</div>
	 						<?= $this->fetch('popular-news-section') ?>

	            </div>
	        </div>
	    <div class="clearfix"></div>
	</div>
</div>




