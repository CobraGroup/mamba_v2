<ul>
<?php foreach($block as $id => $title):?>
	<li>
        <h3><?= $title['title'] ?></h3>
        <h4><?= $title['Published'] ?></h4>
        <p><?= $this->Text->truncate($title['excerpt']?$title['excerpt']:$title['body'], 160, ['exact' => true, 'html' => false])?></p>
        <div>
            <a class="arrow-read" href="<?= $title['slug']?>"><?=__('Read full story') ?></a>
        </div>
    </li>
<?php endforeach; ?>
</ul>