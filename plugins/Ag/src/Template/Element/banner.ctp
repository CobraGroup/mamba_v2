
    <div class="row-fluid <?= $block['style'] ?>">
        <div class="span12">
            <div class="bg-image">
                <?= $this->Html->image($block['image'], ['alt' => strip_tags($block['content'])]) ?>
            </div>
        </div>
    </div>

