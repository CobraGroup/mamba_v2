<div class="<?= $block['style'] ?>">
<?php
if(substr_count($block['style'], "appco-ww-countries")){
?>
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>
	<ul>
		<?php foreach($block['posts'] as $post): ?>
		<li>
			<?php
			if($post->thumb){
			?>
		    <div class="thumb">
				<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?>
		    </div>
		    <?php
		    }
		    ?>
		    <div class="description">
		        <div class="text">
			        <?= $this->Html->tag('h3', $post->title) ?>
			        <?php if(!empty($post->subtitle)) echo  $this->Html->tag('h3', $post->subtitle) ?>
			        <?php if(!empty($post->excerpt)) echo  $this->Html->tag('p', $post->excerpt) ?>
			        <?php if(!empty($post->body) && $block['style']!="expertise") echo  $this->Html->tag('div', $post->body) ?>	
		        </div>
		    </div>
		</li>
		<?php endforeach; ?>
	</ul>
<?php
}elseif(substr_count($block['style'], "contact-us-items")){
?>
	<hr>
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>
	<ul>
		<?php foreach($block['posts'] as $post): ?>
		<li>
			<?php
			if($post->thumb){
			?>
		    <div class="thumb">
				<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?>
		    </div>
		    <?php
		    }
		    ?>
		    <div class="description">
		        <div class="text">
			        <?= $this->Html->tag('h4', $post->title) ?>
			        <?php if(!empty($post->subtitle)) echo  $this->Html->tag('h3', $post->subtitle) ?>
			        <?php if(!empty($post->excerpt)) echo  $this->Html->tag('p', $post->excerpt) ?>
			        <?php if(!empty($post->body)) echo  $this->Html->tag('div', $post->body) ?>	
		        </div>
		    </div>
		</li>
		<?php endforeach; ?>
	</ul>
<?php
}elseif(substr_count($block['style'], "people")){
	echo '<!-- '.$website['locale'].' -->';
?>
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>
	<ul>
		<?php foreach($block['posts'] as $post): ?>
		<li>
			<?php
			$post_slug = $post->slug_override ? $post->slug_override : $post->slug;
			
			if($website['locale']=="en_US"){
				$slug_parts = explode("/", $post_slug);
				$post_slug = $slug_parts[count($slug_parts)-1];
				if($post->thumb){
				?>
			    <div class="thumb">
					<a href="#<?php echo $post_slug ?>" class="our-people-item" data-toggle="modal" data-target="#<?php echo $post_slug ?>"><?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?></a>
			    </div>
			    <?php
			    }
			    ?>
			    <div class="description">
			        <div class="text">
				        <h2><a href="#<?php echo $post_slug ?>" class="our-people-item" data-toggle="modal" data-target="#<?php echo $post_slug ?>"><?php echo $post->title; ?></a></h2>
				        <?php if(!empty($post->subtitle)) echo  $this->Html->tag('h3', $post->subtitle) ?>
				        <?php if(!empty($post->excerpt)) echo  $this->Html->tag('p', $post->excerpt) ?>
				        <a href="#<?php echo $post_slug ?>" class="our-people-item underlined" data-toggle="modal" data-target="#<?php echo $post_slug ?>">Read more</a>
			        </div>
			    </div>
				<!-- Lightbox/Modal pop-up for <?php echo $post->title; ?> -->
				<div class="modal fade" id="<?php echo $post_slug; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $post_slug; ?>-info">
				  <div class="modal-dialog our-people" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="<?php echo $post_slug; ?>-info"><?php echo $post->title; ?></h4>
				        <h5><?php echo $post->subtitle ?></h5>
				      </div>
				      <div class="modal-body">
				      	<p class="text-center">
					      	<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title, 'class' => 'img-thumbnail img-rounded'] ) ?>
					   	</p>
				        <?php echo $post->body ?>
				      </div>
				    </div>
				  </div>
				</div>
			<?php
			}else{
				if($post->thumb){
				?>
			    <div class="thumb">
					<a href="<?= $post_slug ?>" class=""><?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?></a>
			    </div>
			    <?php
			    }
			    ?>
			    <div class="description">
			        <div class="text">
				        <h2><a href="<?= $post_slug ?>"><?php echo $post->title; ?></a></h2>
				        <?php if(!empty($post->subtitle)) echo  $this->Html->tag('h3', $post->subtitle) ?>
				        <?php if(!empty($post->excerpt)) echo  $this->Html->tag('p', $post->excerpt) ?>
				        <a href="<?= $post_slug ?>" class="underlined">Read more</a>
			        </div>
			    </div>
			<?php
			}
			?>
		</li>
		<?php endforeach; ?>
	</ul>
<?php
}else{
	// expertise and others
?>
	<hr>
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>
	<ul>
		<?php foreach($block['posts'] as $post): ?>
		<li>
			<a href="<?= $post->slug_override ? $post->slug_override : $post->slug ?>" class="">
				<?php
				if($post->thumb){
				?>
			    <div class="thumb">
					<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?>
			    </div>
			    <?php
			    }
			    ?>
			    <div class="description">
			        <div class="text">
				        <?= $this->Html->tag('h2', $post->title) ?>
				        <?php if(!empty($post->subtitle)) echo  $this->Html->tag('h3', $post->subtitle) ?>
				        <?php if(!empty($post->excerpt)) echo  $this->Html->tag('p', strip_tags($post->excerpt)) ?>
				        <?php if(!empty($post->body) && $block['style']!="expertise") echo  $this->Html->tag('div', $post->body) ?>	
			        </div>
			    </div>
		    </a>
		</li>
		<?php endforeach; ?>
	</ul>
<?php
}
?>
</div>