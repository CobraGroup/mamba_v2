
<?php if($block['style'] =='modular'): ?>
	<div class="<?= $block['style']?$block['style']:''; ?>">
	<?php  $article = $block['list']; //echo "<pre>"; var_dump($article); echo "</pre>"; die;?>

		<div class="social-ln">
		<?php
		for($i=0;$i<=3;$i++){
			?>
			<div class="social-ln-item social-ln-item-<?php echo $i ?>">
				<div class="social-ln-image">
					<a href="<?= $article[$i]->slug ?>"><img alt="<?= $article[$i]->title ?>" src="<?= $article[$i]->thumb?>" /></a>
				</div>
				<div class="social-ln-text">
					<?php
					if($i==3){
						?>
					<div class="social-ln-block">
						<div class="social-ln-title"><a href="<?= $article[$i]->slug ?>"><h3><?= mb_strimwidth($article[$i]->title, 0, 68, "..."); ?></h3></a></div>
						<div class="social-ln-excerpt">
							<?= mb_strimwidth(strip_tags($article[$i]->excerpt), 0, 150, "..."); ?>
						</div>
					</div>
						<?php
					}else{
						?>
					<div class="social-ln-title"><a href="<?= $article[$i]->slug ?>"><h3><?= mb_strimwidth($article[$i]->title, 0, 68, "..."); ?></h3></a></div>
						<?php
					}
					?>
					<div class="social-ln-date">
						<?php if($website["locale"] =='en_GB')
							 			$country = "UK";
							 else if($website["locale"] =='es')
							 		$country = "Spain";
							 else if($website["locale"] =='en_AU')
							 		$country = "Australia";
							 else if($website["locale"] =='en_US')
							 		$country = "USA";
							 else if($website["locale"] =='pl')
							 		$country = "Poland";
							 else if($website["locale"] =='it')
							 		$country = "Italia";
							 else 	$country = "worldwide";
						?>

						<?php if(isset($article[$i])): ?>
						&#128336; &nbsp;<?= $article[$i]->created->format("d/m/Y")  ?> | <span>Appco Group <?php echo $country ?></span>
						<?php endif; ?>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php
		}
		?>
		</div>
	</div>
<?php else: ?>
	<div class="<?= $block['style']?$block['style']:''; ?>">
		<ul class="base-row">
			<?php foreach($block['list'] as $article): ?>
			<a href="<?= $article->slug ?>">
				<li class="base-item">
					<div class="base-item-wrap">
				        <h3><?= mb_strimwidth($article->title, 0, 60, "..."); ?>&nbsp;|&nbsp;<span class="base-item-date"><?= $article->created->format("d/m/Y")  ?></span>&nbsp;|&nbsp;</h3>

				        <p><?= strip_tags($article->excerpt) ?></p>
					</div>
				</li>
			</a>
			<?php endforeach ?>
		</ul>

	</div>
<?php endif; ?>

