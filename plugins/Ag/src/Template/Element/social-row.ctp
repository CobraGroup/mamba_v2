<h1><?= __('News'); ?></h1>
 	<ul>
	 	<?php var_dump($block); ?>
<?php foreach ($block as $id => $article): ?>
	<li>
            <li>
				<div class="image">
                	<img width="244" height="150" alt="<?= $article->title ?>" src="<?= $article->image?>">
            	</div>
				<div class="description">
                	<h2><a href="<?= $article->slug ?>"><?= $article->title ?></a></h2>
                	<h4><?= $article->created ?></h4>
					<p><?= $this->Text->truncate($article->body, 115, ['exact' => true, 'html' => false]) ?></p>
	                <div>
	                	<a class="more arrow-read" href="<?= $article->slug ?>">Read more</a>
	               </div>
            	</div>
			</li>
	</li>
<?php endforeach; ?>
</ul>
