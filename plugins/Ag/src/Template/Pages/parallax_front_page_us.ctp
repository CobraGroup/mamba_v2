<?php  //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">

    <div id="banner">
	    <div class="row-fluid <?= $block['style'] ?>">
	        <div class="span12">
	            <div class="bg-image">
		            <?php foreach($data['banner'] as $gallery):?>
	                	<?= $this->Html->image($gallery['image'], ['alt' => 'Appco group USA'])?>
	                <?php endforeach; ?>
		            <div class="imageInfo">
			            <div class="imageTitle">APPCO GROUP USA</div>
			            <div class="imageCaption">FIELD MARKETING AGENCY</div>
		            </div>
	            </div>
	        </div>
	    </div>
		<div class="clearfix"></div>
	</div>

    <div class="layout1" id="content">
		<div class="modal-body first-row ">
		  <div class="col-md-6 text-center row-left-intro" >
		    <p class="first-row-mobile"><a href="/about-us">Appco Group USA is</a></p>
		    <div class="intro-square">
				<h1><a href="/about-us">Appco Group <span style="font-size:120%;">USA is</span></a></h1>
		    </div>
		  </div>
		  <div class="col-md-6 text-center parallax-right-column">

			<div id="textCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		        <li data-target="#textCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#textCarousel" data-slide-to="1"></li>

		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
<p>A face-to-face marketing agency that provides high-quality customer and donor acquisition services and direct sales solutions.</p><p>Every day, our professional field representatives across the US speak to thousands of people on behalf of our clients, securing thousands of customers and donors for them every year.</p><p>The Appco USA team is based in our New York head office, and includes experts in fundraising, energy, home efficiency and home delivery, as well as sales support, business development, account management and communications.</p></div>
<div class="item"><p>Our team continually strives to ensure our clients receive the highest level of service and support, while helping to promote their brands and acquire new customers and donors.</p><p>Our drive and enthusiasm in what we do &#8211; here and in 28 other locations across the globe &#8211; has allowed Appco Group to become one of the world&#8217;s most successful sales and marketing companies.</p>

		        </div>
		      </div>
		    </div>

		  </div>
		</div>
		<div class="clearfix"></div>

		<!---  social --->
		<div class="modal-body social-row ">
			<h2 class="social-header">LATEST NEW<span>S</span></h2>
			<div class="social-row-wrapper">
				<div class="col-md-6 row-two-left-column">
					<?= $this->fetch('social-row') ?>
				</div>
				<div class="col-md-6 row-two-right-column">
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fappcousa%2F&tabs=timeline&width=468&height=468&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="468" height="468" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
		<div class="layout1 row second-row">
			<div class="row-two-left-column">
				<p class="second-row-mobile"><a href="/field-marketing">WHAT WE DO</a></p>
				<div class="intro-square"><h2><a href="/field-marketing">What <span>we do</span></a></h2></div>
			</div>
			<div class="row-two-right-column">
			    <ul class="base-row">
				    <?= $this->fetch('second-row') ?>
			    </ul>
			</div>

		</div>


</div>


