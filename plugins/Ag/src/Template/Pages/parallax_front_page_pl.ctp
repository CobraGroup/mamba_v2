<?php  //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">

    <div id="banner">
	    <div class="row-fluid <?= $block['style'] ?>">
	        <div class="span12">
	            <div class="bg-image">
		            <?php foreach($data['banner'] as $gallery):?>
	                	<?= $this->Html->image($gallery['image'], ['alt' => 'Appco Group Poland'])?>
	                <?php endforeach; ?>
		            <div class="imageInfo">
			            <div class="imageTitle">Appco Group Polska</div>
			            <div class="imageCaption">Marketing terenowy</div>
		            </div>
	            </div>
	        </div>
	    </div>
		<div class="clearfix"></div>
	</div>

    <div class="layout1" id="content">
		<div class="modal-body first-row ">
		  <div class="col-md-6 text-center row-left-intro" >
		    <p class="first-row-mobile"><a href="/about-us">Appco Group Poland</a></p>
		    <div class="intro-square">
				<h1><a href="/about-us">Appco Group <span style="font-size:32px; display: block; line-height: 40px; padding:0px;">Polska jest...</span></a></h1>
		    </div>
		  </div>
		  <div class="col-md-6 text-center parallax-right-column">

			<div id="textCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
					<p>liderem na rynku marketingu face-to-face. Każdego dnia rozmawiamy z ponad milionem ludzi na całym świecie. Dzięki temu nawiązujemy relacje i zapewniamy naszym klientom docelowych odbiorców ich produktów lub usług.</p>
					<p>Wierzymy, że odpowiedzialnie prowadzona sprzedaż metodą face-to-face buduje doskonałe relacje z odbiorcami, przynosi korzyści klientom  i stanowi wparcie dla małych firm i przedsiębiorców.</p>
					<p>Posiadając 25-letnie doświadczenie wiemy, że najbardziej opłacalną metodą pozyskiwania klientów i darczyńców jest odpowiedzialne prowadzenie sprzedaży i marketingu metodą face-to-face.</p>
		        </div>
		      </div>
		    </div>

		  </div>
		</div>
		<div class="clearfix"></div>

		<!---  social --->
		<div class="modal-body social-row ">
			<h2 class="social-header" style="font-size: 550%;">AKTUALNOŚC<span>I</span></h2>
			<div class="social-row-wrapper">
				<div class="col-md-12 row-two-left-column appco-pl">
					<?= $this->fetch('social-row') ?>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
		<div class="layout1 row second-row">
			<div class="row-two-left-column">
				<p class="second-row-mobile"><a href="/field-marketing">CO ROBIMY</a></p>
				<div class="intro-square"><h2><a href="/field-marketing">Co <span>robimy</span></a></h2></div>
			</div>
			<div class="row-two-right-column">
			    <ul class="base-row">
				    <?= $this->fetch('second-row') ?>
			    </ul>
			</div>

		</div>


</div>


