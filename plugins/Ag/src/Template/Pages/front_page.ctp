<?php #pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">
	<div class="container-wrapper">
	    <div id="banner">
			<?php echo $this->fetch('banner');  ?>
		    <div class="clearfix"></div>
		</div>

	    <div class="row layout1" id="content">
	        <div class="column left">
		        <div>
					<?= $this->fetch('left-column') ?>
				</div>
			</div>

			<div class="column center">
					<?= $this->fetch('middle-column') ?>
			</div>
			<div class="column right">
					<?= $this->fetch('right-column') ?>

			</div>
		</div>
		<div class="clearfix"></div>
	<?php
		$ignore_list = [ 'en_AU', 'en_NZ', 'it'];
		if(!in_array($website['locale'], $ignore_list)):

	?>
		<div class="block what_we_do">

		    <h2><?=__('What we do') ?></h2>
		    <ul class="base-row">
				<?= $this->fetch('content-footer') ?>
		    </ul>
		</div>
	<?php endif; ?>
	</div>
</div>
