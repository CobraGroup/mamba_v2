<?php  //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">

    <div id="banner">
	    <div class="row-fluid <?= $block['style'] ?>">
	        <div class="span12">
	            <div class="bg-image">
		            <?php foreach($data['banner'] as $gallery):?>
	                	<?= $this->Html->image($gallery['image'], ['alt' => 'Appco group USA'])?>
	                <?php endforeach; ?>
		            <div class="imageInfo">
			            <div class="imageTitle">APPCO GROUP ITALIA</div>
			            <div class="imageCaption">FIELD MARKETING AGENCY</div>
		            </div>
	            </div>
	        </div>
	    </div>
		<div class="clearfix"></div>
	</div>

    <div class="layout1" id="content">
		<div class="modal-body first-row ">
		  <div class="col-md-6 text-center row-left-intro" >
		    <p class="first-row-mobile"><a href="/about-us">Appco Group ITALIA is</a></p>
		    <div class="intro-square">
				<h1><a href="/about-us">Appco Group <span style="font-size:48px; display: block; line-height: 48px; padding:0px;">ITALIA is</span></a></h1>
		    </div>
		  </div>
		  <div class="col-md-6 text-center parallax-right-column">

			<div id="textCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		        <li data-target="#textCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#textCarousel" data-slide-to="1"></li>
		        <li data-target="#textCarousel" data-slide-to="2"></li>
		        <li data-target="#textCarousel" data-slide-to="3"></li>
		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
<p><strong>L&rsquo;industria leader nel marketing&nbsp;diretto di qualit&agrave;.&nbsp;</strong><br />Ogni giorno, in tutto il mondo, parliamo con pi&ugrave; di un milione di persone e questo ci permette di creare connessioni con i mercati target dei nostri clienti, ottenendo cos&igrave; migliaia di nuovi client per gli stessi ogni anno.</p></div>
<div class="item"><p>$4 miliardi e il fatturato annuo che realizza Appco Group per suoi pricipali clienti</p></div>
<div class="item"><p>$1 miliardo e I'importo raccolto, tra il 1997 e il 2013, da Appco Group Support per le associazioni benefiche che promuove</p></div>
<div class="item"><p>$40 milioni in donazioni e il totale raccolto a livello mondiale, ogni mese, da Appco Group Support per le associazioni di beneficenza che sostiene</p></div>

		        </div>
		      </div>
		    </div>

		  </div>
		</div>
		<div class="clearfix"></div>

		<!---  social --->
		<div class="modal-body social-row ">
			<h2 class="social-header">LATEST NEW<span>S</span></h2>
			<div class="social-row-wrapper">
				<div class="col-md-6 row-two-left-column">
					<?= $this->fetch('social-row') ?>
				</div>
				<div class="col-md-6 row-two-right-column">
					<a height="469" data-chrome="nofooter noborders" class="twitter-timeline" href="https://twitter.com/appcogroup" data-widget-id="688026263815098369">Tweets by @appcogroup</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
		<div class="layout1 row second-row">
			<div class="row-two-left-column">
				<p class="second-row-mobile"><a href="/field-marketing">WHAT WE DO</a></p>
				<div class="intro-square"><h2><a href="/field-marketing">What <span>we do</span></a></h2></div>
			</div>
			<div class="row-two-right-column">
			    <ul class="base-row">
				    <?= $this->fetch('second-row') ?>
			    </ul>
			</div>

		</div>


</div>


