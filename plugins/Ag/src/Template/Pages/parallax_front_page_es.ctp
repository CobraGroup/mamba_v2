<?php  //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">

    <div id="banner">
	    <div class="row-fluid <?= $block['style'] ?>">
	        <div class="span12">
	            <div class="bg-image">
		            <?php foreach($data['banner'] as $gallery):?>
	                	<?= $this->Html->image($gallery['image'], ['alt' => 'Appco group UK'])?>
	                <?php endforeach; ?>
		            <div class="imageInfo">
			            <div class="imageTitle">APPCO GROUP</div>
			            <div class="imageCaption">AGENCIA DE MARKETING DIRECTO</div>
		            </div>
	            </div>
	        </div>
	    </div>
		<div class="clearfix"></div>
	</div>

    <div class="layout1" id="content">
		<div class="modal-body first-row ">
		  <div class="col-md-6 text-center row-left-intro" >
		    <p class="first-row-mobile"><a href="/sobre-appco">APPCO GROUP ESPAÑA ES...</a></p>
		    <div class="intro-square">
				<h1><a href="/sobre-appco">APPCO GROUP <span style="font-size:64%;">ESPAÑA ES</span></a></h1>
		    </div>
		  </div>
		  <div class="col-md-6 text-center parallax-right-column">

			<div id="textCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		        <li data-target="#textCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#textCarousel" data-slide-to="1"></li>
		        <li data-target="#textCarousel" data-slide-to="2"></li>
		        <li data-target="#textCarousel" data-slide-to="3"></li>
		        <li data-target="#textCarousel" data-slide-to="4"></li>
		        <li data-target="#textCarousel" data-slide-to="5"></li>
		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
					<p>| Una agencia de marketing directo con casi 30 a&ntilde;os de experiencia mundial en adquisici&oacute;n de clientes y de donantes regulares, aportando valor a nuestros partners. Junto con los embajadores de marca de nuestra red de franquicias independientes, ayudamos a nuestros partners a conectar, de manera profesional, con sus mercados y a adquirir miles de nuevos clientes y donantes de calidad cada a&ntilde;o. Esto es lo que llamamos el Human Commercial&#x2122;.</p>
		        </div>
		        <div class="item">
					<p>| Nuestro Human Commercial&#x2122; es mucho m&aacute;s que solo una estrategia de marketing directo. Se trata de un m&eacute;todo profesional e interactivo, un experiencia cara a cara con el p&uacute;blico objetivo de nuestros partners, conectando con ellos a trav&eacute;s de los profesionales m&aacute;s preparados. Y funciona tanto para nuestros partners como para los consumidores finales.</p>
		        </div>
		        <div class="item">
					<p>| La clave de nuestro &eacute;xito es la interiorizaci&oacute;n de las campa&ntilde;as comerciales que desarrollamos: nos aseguramos de que los embajadores de marca conocen con profundidad el producto, servicio o causa que van a representar. Esto garantiza resultados a largo plazo para nuestros partners. Esta ha sido la misi&oacute;n de Appco Group en el mundo durante m&aacute;s de 25 a&ntilde;os, generar relaciones consistentes y duraderas entre nuestros partners y sus consumidores o donantes.</p>
		        </div>
		        <div class="item">
					<p>| Conectar con cientos de personas al d&iacute;a en nuestro pa&iacute;s, a trav&eacute;s del sistema de Human Commercial&#x2122; de Appco Group, incrementa el reconocimiento social de nuestros partners. Los equipos comerciales de emprendedores independientes, persiguen la excelencia en ventas; aplicando cada d&iacute;a los m&aacute;s altos est&aacute;ndares de calidad. El entusiasmo y la pasi&oacute;n con la que afrontamos nuestro d&iacute;a a d&iacute;a ha hecho posible que Appco Group sea la agencia de marketing directo m&aacute;s potente del mundo.</p>
		        </div>
		        <div class="item">
					<p>| Desde nuestra oficina en Barcelona, Appco Group Espa&ntilde;a tiene varios equipos dedicados a los diferentes sectores en los que trabajamos &#x2013;desde campa&ntilde;as para partners energ&eacute;ticos, pasando por acciones de fundraising, y comercializaci&oacute;n de una diversa gama de productos-. Los Campaign Managers desarrollan los procesos, la gesti&oacute;n de las cuentas y la comunicaci&oacute;n. Expertos del producto que proporcionan el mejor servicio posible a nuestros partners y socios comerciales, solidificando nuestra experiencia a base de adquirir nuevos conocimientos cada d&iacute;a.     </p>
		        </div>
		        <div class="item">
					<p>| Estamos orgullosos de nuestra experiencia y desarrollo profesional en la industria del marketing directo, nuestro servicio al cliente y conocimiento de un sector que nos apasiona. Nuestro modelo de negocio se basa en la rentabilidad, es decir, nuestros clientes solo pagan por los resultados que obtenemos para ellos. </p><p>&Eacute;chale un vistazo a nuestra p&aacute;gina web y no dudes en ponerte en contacto con nosotros.</p>
		        </div>

		      </div>
		    </div>

		  </div>
		</div>
		<div class="clearfix"></div>

		<!---  social --->
		<div class="modal-body social-row ">
			<h2 class="social-header">SÍGUENOS LA PISTA</h2>
			<div class="social-row-wrapper">
				<div class="col-md-6 row-two-left-column">
					<?= $this->fetch('social-row') ?>
				</div>
				<div class="col-md-6 row-two-right-column">
					<a height="469" class="twitter-timeline" data-chrome="nofooter noborders" href="https://twitter.com/Appco_es">Tweets by @Appco_es</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
		<div class="layout1 row second-row">
			<div class="row-two-left-column">
				<p class="second-row-mobile"><a href="/red-comercial">QUE HACEMOS</a></p>
				<div class="intro-square"><h2><a href="/red-comercial">QUÉ <span>hacemos</span></a></h2></div>
			</div>
			<div class="row-two-right-column">
			    <ul class="base-row">
				    <?= $this->fetch('second-row') ?>
			    </ul>
			</div>

		</div>


</div>


