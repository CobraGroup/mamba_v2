<?php

// This function will take $_SERVER['REQUEST_URI'] and build a breadcrumb based on the user's current path
function breadcrumbs($separator = '<span class="divider"> / </span>', $home = 'Home', $locale = 'en_GB') {
    global $website;
    // This gets the REQUEST_URI (/path/to/file.php), splits the string (using '/') into an array, and then filters out any empty values
    
    $uri = $_SERVER['REQUEST_URI'];
    if($locale == 'en_GB'){
	    $uri = str_replace("senior-executives","about-us/our-people",$uri);
    }
    
    $path = array_filter(explode('/', parse_url($uri, PHP_URL_PATH)));

    // This will build our "base URL" ... Also accounts for HTTPS :)
    $base = $_SERVER['HTTP_HOST'] . '/';
		//echo "<pre>"; var_dump($base);  echo "</pre>";
    // Initialize a temporary array with our breadcrumbs. (starting with our home page, which I'm assuming will be the base URL)
    $breadcrumbs = array("<li><a href=\"/\"  itemprop=\"/\"><span itemprop=\"Home\">$home</span></a></li>");

    // Find out the index for the last value in our path array
    $last = end(array_keys($path));

    // Build the rest of the breadcrumbs
    foreach ($path as $x => $crumb) {
       // echo "<pre>"; var_dump($crumb);  echo "</pre>";
        // Our "title" is the text that will be displayed (strip out .php and turn '_' into a space)
        $title = ucwords(str_replace(array('.php', '-'), array('', ' '), $crumb));

	    if($locale == 'en_GB' && $crumb == 'our-people'){
		    $crumb = 'about-us/our-people';
		}
        // If we are not on the last index, then display an <a> tag
        if ($x != $last) {
            $breadcrumbs[] = "<li><a href=\"/$crumb\" itemprop=\"/$crumb\"><span itemprop=\"title\">$title</span></a></li>";
        }

        // Otherwise, just display the title (minus)
        else {
	        $breadcrumbs[] = $title;
        }

    }

    // Build our temporary array (pieces of bread) into one big string :)
    return implode($separator, $breadcrumbs);
}

?>
<div class="banner">
<div class="row-fluid ">
        <div class="span12">
            <div class="bg-image">
				<?= $this->Html->image($post['image'], ['alt' => $post['title']] ); ?>
            </div>
        </div>
    </div>

</div>
<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" data-role="header">
	<div class="widecrumb"><ul class="breadcrumb "><?= breadcrumbs() ?></ul></div>
</div>

<div id="container" class="col-md-12">
 	<div class="container-wrapper">

		<div class="row layout3" id="content">
		    <div class="column menu">

			    <?php if(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_IN'): ?>
					<h2><a href="/our-people">Our people</a></h2>
					<ul>
						<li><a href="/our-people/senior-executives">Senior Executives</a></li>
					</ul>
				<?php  elseif(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_CA'):?>
					<ul>
						<h2><a href="/our-people">Our people</a></h2>
						<li><a href="/board-of-directors">Board of Directors</a></li>

						<li><a href="/our-people/country-heads-and-sales-managers">Country Heads and Sales Managers</a></li>
						<li  class="active"><a href="/our-people/senior-executives">Senior Executives</a></li>
					</ul>
				<?php elseif(current($this->request->params['pass']) == 'country-heads-and-sales-managers'): ?>
					<ul>
						<h2><a href="/our-people">Our people</a></h2>
						<li><a href="/board-of-directors">Board of Directors</a></li>

						<li class="active"><a href="/our-people/country-heads-and-sales-managers">Country Heads and Sales Managers</a></li>
						<li><a href="/our-people/senior-executives">Senior Executives</a></li>
					</ul>

				<?php elseif(current($this->request->params['pass']) == 'board-of-directors' && $website['locale'] == 'en_AU'): ?>
					<ul>

						<h2><a href="/our-people">Our people</a></h2>
						<li class="active"><a href="/board-of-directors">Board of Directors</a></li>
						<li><a href="/our-people/senior-executives">Senior Executives</a></li>
						<li><a href="/our-people/independent-contractors">Independent contractors</a></li>
					</ul>
				<?php elseif(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_AU'): ?>
					<ul>

						<h2><a href="/our-people">Our people</a></h2>
						<li><a href="/board-of-directors">Board of Directors</a></li>
						<li class="active"><a href="/our-people/senior-executives">Senior Executives</a></li>
						<li><a href="/our-people/independent-contractors">Independent contractors</a></li>
					</ul>
					<?php elseif(in_array($this->request->params['pass'][0], array('senior-executives', 'board-of-directors', 'vice-presidents') ) && $website['locale'] == 'en_GB'): ?>
					<ul class="row-fluid hideTray">
						<li class="deeper  hasSubmenu">
							<a href="/about-us/industry-expertise">Industry expertise</a>
							<ul class="submenu" data-submenu_id="180" style="display: none;">			
								<li class=""><a href="/about-us/fundraising">Fundraising</a></li>
								<li class=""><a href="/about-us/energy">Energy</a></li>
								<li class=""><a href="/about-us/home-delivery">Home delivery</a></li>
								<li class=""><a href="/about-us/home-efficiency">Home efficiency</a></li>
								<li class=""><a href="/about-us/solar">Solar</a></li>
								<li class=""><a href="/about-us/product-development">Product development</a></li>
								<li class=""><a href="/about-us/contact-centre">Contact centre</a></li>
							</ul>
						</li>
						<li class="deeper active"><a href="/about-us/our-people">Our people</a>						</li>
						<li class="deeper "><a href="/about-us/our-clients">Our clients</a>						</li>
						<li class="deeper "><a href="/about-us/appco-worldwide">Appco worldwide</a>						</li>
						<li class="deeper "><a href="/about-us/achievements">Achievements</a>						</li>
						<li class="deeper "><a href="/about-us/mythbusters">Mythbusters</a>						</li>
					</ul>

				<?php elseif(current($this->request->params['pass']) == 'ledning' && $website['locale'] == 'sv'): ?>
					<ul>
						<h2><a href="/our-people">Our people</a></h2>
						<li class="active"><a href="/board-of-directors">Board of Directors</a></li>
						<li><a href="/our-people/senior-executives">Senior Executives</a></li>
					</ul>
					<ul>


						<li class="active"><a href="/ledning">Ledning</a></li>
						<li class=""><a href="/personal">Personal</a></li>
						<li class=""><a href="/fltrepresentanter">Fältrepresentanter</a></li>
								<ul class="submenu show">
									<li class=""><a href="/affrsmjligheter-och-status-som-fltrepresentant">Affärsmöjligheter och status som fältrepresentant</a></li>
									<li class=""><a href="/produkt-och-sljutbildning">Produkt-och säljutbildning</a></li>
									<li class=""><a href="/hur-provisionsbaserad-ln-fungerar">Hur provisionsbaserad lön fungerar</a></li>
									<li class=""><a href="/vanliga-frgor-och-svar">Vanliga frågor och svar</a></li>
									<li class=""><a href="/vad-du-kan-frvnta-dig-under-din-frsta-vecka">Vad du kan förvänta dig under din första vecka</a></li>
									<li class=""><a href="/vad-du-kan-frvnta-dig-vecka-fyra">Vad du kan förvänta dig vecka fyra</a></li>
									<li class=""><a href="/tillbaka-till-dig-som-fltrepresentant">Tillbaka till dig som fältrepresentant</a></li>
								</ul>
					</ul>
				<?php elseif(current($this->request->params['pass']) == 'board-of-directors' && $website['locale'] == 'en_NZ'): ?>
					<ul>
						<h2><a href="/our-people">Our people</a></h2>
						<li class="active"><a href="/board-of-directors">Board of Directors</a></li>
						<li><a href="/our-people/senior-executives">Senior Executives</a></li>
					</ul>
				<?php elseif(current($this->request->params['pass']) == 'senior-executives' && $website['locale'] == 'en_NZ'): ?>
					<ul>
						<h2><a href="/our-people">Our people</a></h2>
						<li><a href="/board-of-directors">Board of Directors</a></li>
						<li class="active"><a href="/our-people/senior-executives">Senior Executives</a></li>
					</ul>
				<?php endif; ?>

		    </div>
		    <div class="column content">
		        <h1><?= $post['title'] ?></h1>
		        <div class="keyplayer">
			        <div class="image" style="width: 170px; float: left;">
						<?= $this->Html->image($post['thumb'], ['alt' => $post['title']]) ?>
			        </div>
		            <div class="description">
		                <h2><?= $post['subtitle'] ?></h2>
						<p><?= $post['body'] ?></p>
						<?php
						if($website['locale'] != 'en_GB'){
						?>
		                <div class="go-back-container">
		                    <?= $this->Html->link(__('Back to ').$post['category']['name'], $post['category']['slug'], ['class' => '']) ?>
		                </div>
		                <?php
		                }
		                ?>
		            </div>
		        </div>
		    </div>
		    <div class="clearfix"></div>
		</div>
 	</div>
</div>