<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Cobra Group">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?php echo (isset($page_title)) ? $page_title.' | ' : '' ?>Appco Group</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<?php
	$cssfiles_to_load = [
		"/css/appco2016/bootstrap-custom/bootstrap.min.css",
		"/css/appco2016/bootstrap-custom/ie10-viewport-bug-workaround.css",
		"/css/appco2016/style.css"
	];
?>
	<style>
<?php
	foreach($cssfiles_to_load as $cssfile){
		$css_content = file_get_contents(".".$cssfile);
		print $css_content."
";
	}
?>
	</style>

<?php
if(strpos($_SERVER['SERVER_NAME'],'staging')!==false){
	echo '
	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">';
}
?>
</head>

<body>

<?php echo $this->fetch('content') ?>

<div class="footer">
	<div class="brand-colours">
	    <span class="red">&nbsp;</span><span class="blue">&nbsp;</span><span class="brown">&nbsp;</span><span class="purple">&nbsp;</span><span class="orange">&nbsp;</span><span class="red">&nbsp;</span>
	</div>
</div>
<div class="footer footer-top">
	<div class="container">
		<div class="row">
			<div class="col col-md-4 col-xs-12 col-footer-logo">
				<a href="http://www.cobragroup.com/" target="_blank"><img src="/img/appco2016/cobra-group-logo.png"></a>
			</div>
			<div class="col col-md-8 hidden-xs hidden-sm text-right">
				<a href="/appco-worldwide">Appco worldwide</a> | <a href="/news">News</a> | <a href="/privacy-policy">Privacy policy</a> | <a href="#">Sitemap</a>
			</div>
		</div>
	</div>
</div>
<div class="footer footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col col-md-4 col-xs-12 col-footer-copy">
				Appco Group &copy; all rights reserved <?php echo date("Y") ?> 
			</div>
			<div class="col col-md-8 col-xs-12 col-footer-social">
				<span class="follow-title">FOLLOW US:</span>
				<p class="follow-buttons addthis_horizontal_follow_toolbox">
					<a href="http://www.facebook.com/appcogroup" target="_blank" class="addthis_button_facebook_follow" addthis:userid="appcogroup"><img src="/img/appco2016/buttons/facebook.png"></a>
					<a href="http://twitter.com/appcogroup" target="_blank" class="addthis_button_twitter_follow" addthis:userid="appcogroup"><img src="/img/appco2016/buttons/twitter.png"></a>
					<a href="http://www.linkedin.com/company/appco-group" target="_blank" class="addthis_button_linkedin_follow" addthis:userid="appco-group" addthis:usertype="company"><img src="/img/appco2016/buttons/linkedin.png"></a>
					<a href="https://plus.google.com/+appcogroup" target="_blank" class="addthis_button_google_follow" addthis:userid="+appcogroup"><img src="/img/appco2016/buttons/gplus.png"></a>
					<a href="http://www.pinterest.com/appcouk" target="_blank" class="addthis_button_pinterest_follow" addthis:userid="appcouk"><img src="/img/appco2016/buttons/pinterest.png"></a>
					<a href="http://www.youtube.com/user/appcogrouptv?sub_confirmation=1" target="_blank" class="addthis_button_youtube_follow" addthis:userid="appcogrouptv"><img src="/img/appco2016/buttons/youtube.png"></a>
				</p>
			</div>
		</div>
	</div>
</div>
<?php
	$jsfiles_to_load = [
		"/js/appco2016/jquery/jquery-2.2.4.min.js",
		"/js/appco2016/jquery/jquery.mobile.custom.min.js",
		"/js/appco2016/bootstrap-custom/bootstrap.min.js",
		"/js/appco2016/bootstrap-custom/ie10-viewport-bug-workaround.js",
		"/js/appco2016/scripts.js"
	];
?>
	<script>
<?php
	foreach($jsfiles_to_load as $jsfile){
		$jsfile = file_get_contents(".".$jsfile);
		print $jsfile."
";
	}
?>
	</script>
</body>
</html>
