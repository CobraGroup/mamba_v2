<div class="posts <?php echo $block['style']; ?>">

<?php
if($block['title']) echo $this->Html->tag($block['tag'], $block['title']);
?>

	<div class="row">
<?php 
foreach($block['posts'] as $post){
	
	$post->slug = $post->slug_override ? $post->slug_override : $post->slug;
	$slug_parts = explode("/", $post->slug);
	$post_slug = $slug_parts[count($slug_parts)-1];
	
	$bootstrap_col_class = "col-md-4 col-xs-12";
	$link_to_post_lightbox = false;
	$show_details = true;
	
	if(substr_count($block['style'], "our-people")){
		$bootstrap_col_class = "col-md-3 col-sm-4 col-xs-6";
		$link_to_post_lightbox = true;
		$show_details = false;
	}
?>
		<div class="post-item col <?php echo $bootstrap_col_class ?>">
<?php if($link_to_post_lightbox)					echo '<div class="our-people-item" data-toggle="modal" data-target="#'.$post_slug.'">'; ?>
<?php if(!empty($post->thumb)) 						echo '<div class="post-thumb">'.$this->Html->image($post->thumb, ['alt' => $post->title, 'class' => 'img-responsive']).'</div>'; ?>
<?php if(!empty($post->title))						echo $this->Html->tag('h3', $post->title) ?>
<?php if(!empty($post->subtitle))					echo $this->Html->tag('h4', $post->subtitle) ?>
<?php if(!empty($post->excerpt) && $show_details)	echo $this->Html->tag('p', $post->excerpt) ?>
<?php if(!empty($post->body) && $show_details)		echo $this->Html->tag('div', $post->body) ?>
<?php if($link_to_post_lightbox)					echo '</div>'; ?>
<?php
if($link_to_post_lightbox){
?>
			<!-- Lightbox/Modal pop-up for <?php echo $post->title; ?> -->
			<div class="modal fade" id="<?php echo $post_slug; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $post_slug; ?>-info">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="<?php echo $post_slug; ?>-info"><?php echo $post->title; ?><span><?php echo $post->subtitle ?></span></h4>
			      </div>
			      <div class="modal-body">
			      	<div class="text-center">
				      	<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title, 'class' => 'img-thumbnail img-rounded'] ) ?>
				   	</div>
			        <?php echo $post->body ?>
			      </div>
			    </div>
			  </div>
			</div>
<?php
}
?>
		</div>
<?php
}
?>
	</div>

</div>	
