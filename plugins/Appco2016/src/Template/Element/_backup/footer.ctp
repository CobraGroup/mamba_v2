		<div id="footer" data-role="footer">
		    <div class="outter">
	            <div>
	                <div class="left">
	                    <div class="logos">
	                        <ul>
	                            <li><?= $this->Html->link(
		                            $this->Html->image('appco/appco_logo_footer.png', ['alt' => 'Appco group - Field Marketing  Agency', 'itemprop' => 'image' ]),
		                             'http://www.appcogroup.com/',
			            				['target' => '_blank', 'escape' => false]
			            			)?></li>
	                            <li><?=  $this->Html->link(
		                            $this->Html->image('appco/besomethingmore.png', ['alt' => 'Be something more with Appco']),
			            				'https://www.linkedin.com/company/be-somethingmore',
			            				['target' => '_blank', 'escape' => false]
			            			) ?></li>
	                        </ul>
	                    </div>
	                </div>
	                <div class="right">
		                <?php if(false): ?>
	                    <ul class="logos">
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/responsible-trust.png', ['alt' => 'Responsible Gambling Trusts Donor - Appco']),
			            				'http://www.responsiblegamblingtrust.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li><span class="seperator"></span>
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/the-lotteries-council.png', ['alt' => 'The Lotteries Council']),
			            				'http://www.lotteriescouncil.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li><span class="seperator"></span>
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/pfra.png', ['alt' => 'PFRA Approved']),
			            				'http://www.pfra.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li>
		            		<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/institute-of-fundraising.png', ['alt' => 'Instiitute of Fundraisers']),
			            				'http://www.institute-of-fundraising.org.uk/find-a-supplier/appco-group-support/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
		            		</li>
	                    </ul>
	                    <?php endif ?>
	                    <?php if($website['locale'] == 'es'): ?>
							 <ul class="logos">
	                   			<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/New-proud-to-be-member.png', ['alt' => 'Proud to be member of British chamber']),
			            				'http://www.britishchamberspain.com/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>

                    		</ul>
		                 <?php endif; ?>
	                     <?php if($website['locale'] == 'en_GB'): ?>
							 <ul class="logos">
	                   			<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/appco-dsa.png', ['alt' => 'DSA Member']),
			            				'http://www.dsa.org.uk/dsa-membership/associates-of-the-dsa/appco-group-uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>

	                   			<li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/appco-dsa-membr_huge.png', ['alt' => 'DMA Member']),
			            				'http://www.dma.org.uk/company/appco-group-uk',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
                                </li>

	                            <li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/the-lotteries-council.png', ['alt' => 'The Lotteries Council']),
			            				'http://www.lotteriescouncil.org.uk/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
								</li>
                                <li>
		            			<?= $this->Html->link(
			            				$this->Html->image('appco/pfra.png', ['alt' => 'PFRA Approved']),
			            				'http://www.pfra.org.uk/membership/pfra-members/',
			            				['target' => '_blank', 'escape' => false]
			            			)
			            		?>
			            		</li>
	                            <li>
			            			<?= $this->Html->link(
				            				$this->Html->image('appco/institute-of-fundraising.png', ['alt' => 'Institute of Fundraisers']),
				            				'http://www.institute-of-fundraising.org.uk/consultants-and-suppliers/find-a-supplier/ags-global-fundraising-services/',
				            				['target' => '_blank', 'escape' => false]
				            			)
				            		?>
			            		</li>
                    		</ul>
		                 <?php endif; ?>
	                    <?php if($website['locale'] == 'en_AU'):?>
	                    <ul class="logos">
							<li>

		                  <?php  echo $this->Html->link(
			            				$this->Html->image('/uploads/2015/02/fiaau.jpg', ['alt' => 'Fia']),
			            				'http://www.fia.org.au/index.php',
			            				['target' => '_blank', 'escape' => false]
			            			); ?>

							</li>
	                    </ul>
			            <?php		endif;
	                    ?>
	                    <?php if($website['locale'] == 'en_NZ'): ?>
	                    <ul class="logos">
							<li><?php

		            			echo $this->Html->link(
			            				$this->Html->image('/img/appco/auckland-chamber-of-commerce.png', ['alt' => 'Auckland Chamber of commerce']),
			            				'http://www.aucklandchamber.co.nz/',
			            				['target' => '_blank', 'escape' => false]
			            			);
?>
							</li>
		            		<li>
							<?php	echo $this->Html->link(
			            				$this->Html->image('/img/appco/FINZ-Logo.png', ['alt' => 'FINZ-Logo']),
			            				'http://finz.org.nz/',
			            				['target' => '_blank', 'escape' => false]
			            			); ?>
		            		</li>
							<li><?php

		            			echo $this->Html->link(
			            				$this->Html->image('/img/appco/PFRA-Logo.png', ['alt' => 'PFRA Approved']),
			            				'http://pfra.org.nz/',
			            				['target' => '_blank', 'escape' => false]
			            			);
?>
							</li>
	                    </ul>

			            	<?php endif; ?>
	                </div>
	            </div>
		    </div>
		    <div class="brand-colours">
			    <span class="red">&nbsp;</span>
			    <span class="blue">&nbsp;</span>
			    <span class="brown">&nbsp;</span>
			    <span class="purple">&nbsp;</span>
			    <span class="orange">&nbsp;</span>
			    <span class="red">&nbsp;</span>
			</div>
		    <div class="grey">
		        <div class="outter">

		            <div class="left">
			        	<div class="cobra-company">
				        	<?=  $this->Html->link(
					        	$this->Html->image('appco/ag/cobra-group-company-cropped.png', ['alt' => 'Cobra Group Company']),
					        	'http://www.cobragroup.com/',
			            				['target' => '_blank', 'escape' => false]
			            		);
			            	 ?>
			        	</div>
		            </div>
					<div class="right">
		                <ul>
			                <?php if($website['locale'] ==  'sv'): ?>

								<li><?= $this->Html->link(__('Om oss'), '/om-oss') ?></li>
								<li><?= $this->Html->link(__('Vad vi gör'), '/vad-vi-gor') ?></li>
								<li><?= $this->Html->link(__('Nyheter'), '/nyheter') ?></li>
								<li><?= $this->Html->link(__('Kontakter'), '/kontakta-oss') ?></li>
								<li><?= $this->Html->link(__('Sekretess Policy'), '/sekretess-policy') ?></li>
			                <?php elseif($website['locale'] ==  'it'): ?>

								<li><?= $this->Html->link(__('Chi siamo'), '/chi-siamo') ?></li>
								<li><?= $this->Html->link(__('Di cosa ci occupiamo'), '/di-cosa-ci-occupiamo') ?></li>
								<li><?= $this->Html->link(__('Dove siamo'), '/dove-siamo') ?></li>
								<li><?= $this->Html->link(__('Politica sulla riservatezza'), '/politica-sulla-riservatezza') ?></li>
							 <?php elseif($website['locale'] ==  'es'): ?>
								<li><?= $this->Html->link(__('Sobre Appco'), '/sobre-appco') ?></li>
								<li><?= $this->Html->link(__('Qué hacemos'), '/que-hacemos') ?></li>
								<li><?= $this->Html->link(__('Noticias'), '/noticias') ?></li>
								<li><?= $this->Html->link(__('Appco en el mundo'), '/appco-en-el-mundo') ?></li>
								<li><?= $this->Html->link(__('Clientes'), '/clientes') ?></li>
								<li><?= $this->Html->link(__('Contáctenos'), '/contacto') ?></li>
							<?php elseif($website['locale'] ==  'en_GB'): ?>
								<li><?= $this->Html->link(__('Appco worldwide'), '/about-us/appco-worldwide') ?></li><li class="seperator">&nbsp;|&nbsp;</li>
                                <li><?= $this->Html->link(__('News'), '/news') ?></li><li class="seperator">&nbsp;|&nbsp;</li>
								<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li><li class="seperator">&nbsp;|&nbsp;</li>
                                 <li><?= $this->Html->link(__('Sitemap'), '/sitemap-gb.html') ?></li>
							<?php elseif($website['locale'] ==  'en_US'): ?>
								<li><?= $this->Html->link(__('Appco worldwide'), '/appco-worldwide') ?></li><li class="seperator">&nbsp;|&nbsp;</li>
                                <li><?= $this->Html->link(__('News'), '/news') ?></li><li class="seperator">&nbsp;|&nbsp;</li>
								<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li><li class="seperator">&nbsp;|&nbsp;</li>
                                <li><?= $this->Html->link(__('Sitemap'), '/sitemap-gb.html') ?></li>
							<?php elseif($website['locale'] ==  'en_IN'): ?>
								<li><?= $this->Html->link(__('About us'), '/about-us') ?></li>
								<li><?= $this->Html->link(__('What we do'), '/what-we-do') ?></li>
								<li><?= $this->Html->link(__('News'), '/news') ?></li>
								<li><?= $this->Html->link(__('Contact us'), '/contact-us') ?></li>
								<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>
                               	<li><?= $this->Html->link(__('Sitemap'), '/sitemap-in.html') ?></li>
							<?php else: ?>

								<li><?= $this->Html->link(__('About us'), '/about-us') ?></li>
								<li><?= $this->Html->link(__('What we do'), '/what-we-do') ?></li>
								<li><?= $this->Html->link(__('News'), '/news') ?></li>
								<li><?= $this->Html->link(__('Contact us'), '/contact-us') ?></li>
								<li><?= $this->Html->link(__('Privacy policy'), '/privacy-policy') ?></li>




							<?php endif; ?>
		                </ul>
					</div>
		        </div>
		            <div class="rights">
			            <div class="bottom-links-wrapper">
					   <?php if($website['locale'] == 'sv') : ?>
					        <p>Appco Group © alla rättigheter reserverade 2016 </p>
					   <?php elseif($website['locale'] == 'it') : ?>
					        <p>Appco Group © tutti i diritti riservati 2016</p>
					   <?php else : ?>
						   	<div class="bottom-links-left">
						    	<p>Appco Group &copy; all rights reserved 2016 </p>
			            	</div>
		            		<div class="bottom-links-right">

			            		<!-- AddThis Follow BEGIN -->
			            		<div class="bottom-links-right-title">
									<h3 style="color:#fff;">FOLLOW US: </h3>
			            		</div>
			            		<div class="bottom-links-right-links">
								<p class="addthis_horizontal_follow_toolbox">
									<a class="addthis_button_facebook_follow" addthis:userid="appcogroup"><img src="/img/ag/buttons/facebook.png"></a>

									<a class="addthis_button_twitter_follow" addthis:userid="appcogroup"><img src="/img/ag/buttons/twitter.png"></a>

									<a class="addthis_button_linkedin_follow" addthis:userid="appco-group" addthis:usertype="company"><img src="/img/ag/buttons/linkedin.png"></a>

									<a class="addthis_button_google_follow" addthis:userid="+appcogroup"><img src="/img/ag/buttons/gplus.png"></a>
								<?php if($website['locale'] == 'en_GB'): ?>
									<a class="addthis_button_pinterest_follow" addthis:userid="appcouk"><img src="/img/ag/buttons/pinterest.png"></a>
								<?php endif; ?>
									<a class="addthis_button_youtube_follow" addthis:userid="appcogrouptv"><img src="/img/ag/buttons/youtube.png"></a>


								</p>
			            		</div>

		            		</div>
					   <?php endif; ?>

					   </div>

		            </div>

		     </div>
		</div>