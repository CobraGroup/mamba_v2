<?php
	if(isset($block['title']) && !empty($block['title']))
		$this->assign('title', $block['title']);
if(substr_count($block['style'], "add-top-hr")){
	?>
<hr>
	<?php
}
?>

<div class="<?= $block['style'] ?>">
	<div class="row-left-intro">
		<div class="intro-square">
		<?php
			if(isset($block['title']) && !empty($block['title']))
				echo $this->Html->tag($block['tag'], $block['title'])
		?>
		</div>
	</div>
	<div>
		<?= $block['content'] ?>
	</div>

</div>