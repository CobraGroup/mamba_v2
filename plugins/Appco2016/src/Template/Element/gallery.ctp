<?php //pr($block); ?>

<div class="gallery">
	
	<div class="row">
		
<?php 
foreach($block['gallery'] as $gallery){

	if(!isset($gallery['class']))	$gallery['class'] = '';
	if(!isset($gallery['title']))	$gallery['title'] = '';
	if(!isset($gallery['caption']))	$gallery['caption'] = '';
?>
		<div class="col col-md-3">
<?php
$image = $this->Html->image($gallery['image'], ['alt' => $gallery['alternative_text'], 'class' => 'img-thumbnail']);

if($gallery['link']) {
	echo $this->Html->link($image, $gallery['link'], ['escape' => false ]);
}else{
	echo $image;
}

if(isset($gallery['caption'])) {
?>
<a href="<?= $gallery['link']?>">
	<div class="imageInfo">
		<div class="imageTitle"><?= $gallery['title'] ?></div>
		<div class="imageCaption"><?= $gallery['caption']?></div>
	</div>
</a>
<?php 
}
?>
		</div>
<?php
}
?>
		
	</div>
	
</div>