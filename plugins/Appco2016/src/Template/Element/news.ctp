<?php
$home_news = $block['list'];

$news_countries = [
	"en_IN" => [
		"title" => "Appco India",
		"url"	=> "http://www.appcogroup.in"
	],
	"en_GB" => [
		"title" => "Appco UK",
		"url"	=> "http://www.appcogroup.co.uk"
	],
	"en_US" => [
		"title" => "Appco USA",
		"url"	=> "http://www.appcogroupusa.com"
	],
	"en_CA" => [
		"title" => "Appco Canada",
		"url"	=> "http://www.appcogroup.ca"
	],
	"en_AU" => [
		"title" => "Appco Australia",
		"url"	=> "http://www.appcogroup.com.au"
	],
	"en_NZ" => [
		"title" => "Appco New Zealand",
		"url"	=> "http://www.appcogroup.co.nz"
	]
];
?>
<div class="home-latest-news-title home-about-appco-odd hidden-xs hiddden-sm">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
				<h4>LATEST NEW<span>S</span></h4>
			</div>
		</div>
	</div>
</div>
<div class="home-latest-news-title-mobile home-about-appco-odd hidden-md hidden-lg">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
				<h4>LATEST <span>NEWS</span></h4>
			</div>
		</div>
	</div>
</div>
<div class="home-latest-news home-about-appco-odd">
	<div class="container">
		<div class="row row-eq-height">
			<?php
			$news_count = 0;
			foreach($home_news as $news_id => $news_detail){
				$news_count++;
				?>
			<div class="col col-md-4 col-xs-8 col-xs-offset-2">
				<a href="<?php echo $news_countries[$news_detail->locale]["url"]; ?><?php echo $news_detail->slug ?>" target="_blank">
					<div class="news-image"><img src="<?php echo $news_detail->thumb ?>" class="img-responsive"></div>
					<div class="news-title"><h4><?php echo $news_countries[$news_detail->locale]["title"]; ?>, <?php echo $news_detail->created->format("d/m/Y") ?></h4><h3><?php echo $news_detail->title; ?></h3></div>
				</a>
			</div>
				<?php
				if(bcmod($news_count,3)==0){
					?>
		</div>
		<div class="row row-eq-height">
					<?php
				}
			}
			?>
		</div>
	</div>
</div>