<ul class="list-group">
	<li class="list-group-item title"><a href="/<?php echo $block['submenu'][0]['slug'] ?>"><?php echo $block['submenu'][0]['title'] ?></a></li>
<?php foreach($block['submenu'] as $id =>  $title): ?>
<?php 
		$showsubmenu = false;
		foreach($block['submenu']['sub'][$title['id']] as $i => $v){
			if($this->request->here == '/'.$block['submenu'][0]['slug'].'/'.$v['slug'].'/') $showsubmenu = true;
		}
		if(isset($title['slug']) && ($block['submenu'][0]['slug'] !== $title['slug'])) : 
?>
	<li class="list-group-item<?php if($this->request->here == '/'.$block['submenu'][0]['slug'].'/'.$title['slug'].'/' || $showsubmenu=="true"){ echo ' active'; $showsubmenu = true;} ?>">
		<a href="/<?php echo $block['submenu'][0]['slug'].'/'.$title['slug'] ?>"><?php echo $title['title'] ?>
<?php
			if(isset($title['id']) && isset($block['submenu']['sub'][$title['id']]) && count($block['submenu']['sub'][$title['id']])) { 
?><span class="badge"><?php echo ($showsubmenu) ? "-" : "+" ?></span><?php
			}
?></a>
<?php
		if(isset($title['id']) && isset($block['submenu']['sub'][$title['id']]) && count($block['submenu']['sub'][$title['id']])) {
			if($showsubmenu){
?>
		<ul>
<?php
		foreach($block['submenu']['sub'][$title['id']] as $i => $v):
			if($title['id'] == $v['parent_id']){ 
?>
			<li<?php echo ($this->request->here == '/'.$block['submenu'][0]['slug'].'/'.$v['slug'].'/' ? ' class="active"': ''); ?>><?= $this->Html->link($v['title'], $block['submenu'][0]['slug'].'/'.$v['slug']) ?></li>
<?php	
			}
		endforeach;
?>
		</ul>
<?php 
			}
		}
?>
	</li>
<?php endif; ?>
<?php endforeach; ?>
</ul>
