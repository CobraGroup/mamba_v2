<div class="banner">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
<?php echo ($block['image']) ? $this->Html->image($block['image'], ['alt' => strip_tags($block['content']), 'class' => 'img-responsive']) : $this->Html->image($website['image'], ['alt' => 'Appco Group', 'class' => 'img-responsive']); ?>
			</div>
		</div>
	</div>
</div>
