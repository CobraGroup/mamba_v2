<?php
$here = $this->request->here;

if(substr($here,0,1)=="/") $here = substr($here, 1);
if(substr($here,-1,1)=="/") $here = substr($here, 0, -1);

$mainnav = [
	"appco-groups-expertise" => "Our expertise",
	"our-story" => "Our story",
	"news" => "News",
	"appco-worldwide" => "Contact us"
];
?>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
			</button>
			<a class="navbar-brand" href="/"><img src="/img/appco2016/appco-logo.png"></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
<?php
			foreach($mainnav as $slug => $title){
				?>
				<li<?php echo (substr($here, 0, strlen($slug)) == $slug && $title!='Contact Us') ? ' class="active"' : '' ?>><a href="/<?php echo $slug ?>"><?php echo $title ?></a></li>
<?php
			}
			?>
			</ul>
		</div>
	</div>
</nav>