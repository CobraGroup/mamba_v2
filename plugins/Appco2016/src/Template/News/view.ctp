<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}

echo $this->element("banner");
echo $this->element("menu");
echo $this->element("breadcrumbs");
?>

<div class="page-with-right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-9 content">

				<h1><?= $news->title ?></h1>
				
				<div class="news-details">
					<div class="row">

						<div class="news-image pull-right"><?= $this->Html->image($news->thumb, ['alt' => $news->title, 'class' => 'img-thumbnail']) ?></div>
<?php
						$news_body = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $news->body);
						$news_body = preg_replace('/<p>/', '<p class="lead">', $news_body, 1);
						echo $news_body;
						
						$t = preg_replace('/\bnews\b/', __('news'), 'news');
						echo $this->Html->link(__('Back to News'), $t);
?>

					</div>
				</div>

			</div>
			<div class="col col-md-3 sidebar">

				<h3>Top <?php echo count($most_read) ?> most read</h3>
				<ul class="list-unstyled">
					<?php foreach ($most_read as $article): ?>
					<li class="news-summary">
						<h2><?php echo $this->Html->link($article->title, $article->slug); ?></h2>
						<p class="date"><?php echo $article->created->format("d/m/Y"); ?></p>
					</li>
					<?php endforeach ?>
				</ul>

			</div>
		</div>
	</div>
</div>
