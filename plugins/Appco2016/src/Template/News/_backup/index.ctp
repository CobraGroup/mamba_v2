<?php

// This function will take $_SERVER['REQUEST_URI'] and build a breadcrumb based on the user's current path
function breadcrumbs($separator = '<span class="divider"> / </span>', $home = 'Home') {
    // This gets the REQUEST_URI (/path/to/file.php), splits the string (using '/') into an array, and then filters out any empty values
    $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

    // This will build our "base URL" ... Also accounts for HTTPS :)
    $base = $_SERVER['HTTP_HOST'] . '/';
		//echo "<pre>"; var_dump($base);  echo "</pre>";
    // Initialize a temporary array with our breadcrumbs. (starting with our home page, which I'm assuming will be the base URL)
    $breadcrumbs = array("<li><a href=\"/\"  itemprop=\"/\"><span itemprop=\"Home\">$home</span></a></li>");

    // Find out the index for the last value in our path array
    $last = end(array_keys($path));

    // Build the rest of the breadcrumbs
    foreach ($path as $x => $crumb) {
       // echo "<pre>"; var_dump($crumb);  echo "</pre>";
        // Our "title" is the text that will be displayed (strip out .php and turn '_' into a space)
        $title = ucwords(str_replace(array('.php', '-'), array('', ' '), $crumb));

        // If we are not on the last index, then display an <a> tag
        if ($x != $last) {
            $breadcrumbs[] = "<li><a href=\"/$crumb\" itemprop=\"/$crumb\"><span itemprop=\"title\">$title</span></a></li>";
        }

        // Otherwise, just display the title (minus)
        else {
	        $breadcrumbs[] = $title;
        }

    }

    // Build our temporary array (pieces of bread) into one big string :)
    return implode($separator, $breadcrumbs);
}

?>

<div class="banner">
    <div class="row-fluid">
        <div class="span12">
            <div class="bg-image">
	            <?php if(!empty($website['image'])) : ?>
                <?= $this->Html->image($website['image'], ['alt' => 'Appco news']) ?>
                <?php else:  ?>
                <?= $this->Html->image('/uploads/2016/06/news-appco-group-uk.jpg', ['alt' => 'Appco news']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>
<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" data-role="header">
	<div class="widecrumb"><ul class="breadcrumb "><?= breadcrumbs() ?></ul></div>
</div>

<div id="container" data-role="main" >
	<div class="container-wrapper news">
	    <div class="row layout4" id="content">

	      <div class="col-md-8 content">

			  	<div class="row-left-intro">
				  	<div class="intro-square">
						<h1><?= __('News'); ?></h1>
					</div>
				</div>

		        <?php foreach ($news as $article): ?>
		        <div class="article">
					<div class="image">

	                	<a href="<?= $article->slug ?>"><img class="news-image img-responsive img-center" alt="<?= $article->title ?>" src="<?= $article->thumb?>"></a>
	            	</div>
	            	<div class="description">
	                	<h2><?= $this->Html->link($article->title, $article->slug) ?><br><span class="date"><?= $article->created->format("d/m/Y") ?></span></h2>
	                	<p><?= mb_strimwidth(strip_tags($article->excerpt), 0, 168, "..."); ?></p>
		                <div class="read-more"><?= $this->Html->link(__('Read more'), $article->slug, ['class' => 'arrow-read']) ?></div>
	            	</div>
	            </div>
		        <?php endforeach ?>
			<?php if($this->Paginator->numbers()) :?>
			<nav>
				<ul class="pagination pgntn-page-pagination">
					<?= $this->Paginator->first("«"); ?>
					<?= $this->Paginator->numbers(); ?>
					<?= $this->Paginator->last("»"); ?>
				</ul>
			</nav>
			<?php endif; ?>

		  </div>
	      <div class="col-md-4 menu most_read">
			    <div class="header">
				    <h2>Top 5 most read</h2>
			    </div>
				<ul>
					<?php foreach ($most_read as $article): ?>
					<li>
						<h3><?= $this->Html->link($article->title, $article->slug) ?></h3>
						<h4><?= $article->created->format("d/m/Y") ?></h4>
					</li>
					<?php endforeach ?>
				</ul>
	      </div>

		</div>
	</div>
</div>
<!--[if gte IE 8]>
<style>
ul.pagination {
	clear:both;
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
}

.pagination li {
	display: inline;
	float: left;
	padding: 5px 10px;
}
.pagination li a {
    display: block;
}
</style>
<![endif]-->


