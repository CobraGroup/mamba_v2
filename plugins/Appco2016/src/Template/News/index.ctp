<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}

echo $this->element("banner");
echo $this->element("menu");
echo $this->element("breadcrumbs");
?>

<div class="page-with-right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-9 content">

				<h1><?php echo __('News'); ?></h1>
				
				<div class="news-list">
<?php foreach ($news as $article){ ?>
					<div class="row">
						<div class="col col-md-4 news-image"><a href="<?php echo $article->slug ?>"><img class="img-responsive" alt="<?php echo $article->title ?>" src="<?php echo $article->thumb?>"></a></div>
						<div class="col col-md-8 news-summary">
		                	<h2><?php echo $this->Html->link($article->title, $article->slug) ?></h2>
		                	<p class="date"><?php echo $article->created->format("d/m/Y") ?></p>
		                	<p class="excerpt"><?php echo mb_strimwidth(strip_tags($article->excerpt), 0, 168, "..."); ?></p>
			                <p class="read-more"><?php echo $this->Html->link(__('Read more'), $article->slug) ?></p>
						</div>
					</div>
<?php } ?>
				</div>
<?php if($this->Paginator->numbers()){?>
				<nav>
					<ul class="pagination">
						<?php echo $this->Paginator->first("«"); ?>
						<?php echo $this->Paginator->numbers(); ?>
						<?php echo $this->Paginator->last("»"); ?>
					</ul>
				</nav>
<?php } ?>

			</div>
			<div class="col col-md-3 sidebar">

				<h3>Top <?php echo count($most_read) ?> most read</h3>
				<ul class="list-unstyled">
					<?php foreach ($most_read as $article): ?>
					<li class="news-summary">
						<h2><?php echo $this->Html->link($article->title, $article->slug); ?></h2>
						<p class="date"><?php echo $article->created->format("d/m/Y"); ?></p>
					</li>
					<?php endforeach ?>
				</ul>

			</div>
		</div>
	</div>
</div>

