<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}
?>
<?php echo $this->fetch('banner') ?>

<?php
echo $this->element("menu");
?>

<?php
echo $this->element("breadcrumbs");
?>

<div class="page-with-left-sidebar">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12 content">

<?php 
echo $this->fetch('content');
?>

			</div>
		</div>
	</div>
</div>
