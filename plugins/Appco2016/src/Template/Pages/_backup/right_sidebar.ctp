
<?php //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">
	 <div class="container-wrapper">
	    <div id="banner">
			<?php echo $this->fetch('banner');  ?>
		    <div class="clearfix"></div>
		</div>

	    <div class="row layout4" id="content">
		<?php

		// This function will take $_SERVER['REQUEST_URI'] and build a breadcrumb based on the user's current path
		function breadcrumbs($separator = ' &raquo; ', $home = 'Home') {
		    // This gets the REQUEST_URI (/path/to/file.php), splits the string (using '/') into an array, and then filters out any empty values
		    $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

		    // This will build our "base URL" ... Also accounts for HTTPS :)
		    $base = $_SERVER['HTTP_HOST'] . '/';
				//echo "<pre>"; var_dump($base);  echo "</pre>";
		    // Initialize a temporary array with our breadcrumbs. (starting with our home page, which I'm assuming will be the base URL)
		    $breadcrumbs = array("<a href=\"/\"  itemprop=\"/\">$home</a>");

		    // Find out the index for the last value in our path array
		    $last = end(array_keys($path));

		    // Build the rest of the breadcrumbs
		    foreach ($path as $x => $crumb) {
		       // echo "<pre>"; var_dump($crumb);  echo "</pre>";
		        // Our "title" is the text that will be displayed (strip out .php and turn '_' into a space)
		        $title = ucwords(str_replace(array('.php', '-'), array('', ' '), $crumb));

		        // If we are not on the last index, then display an <a> tag
		        if ($x != $last) {
		            $breadcrumbs[] = "<a href=\"/$crumb\" itemprop=\"/$crumb\"><span itemprop=\"title\">$title</span></a>";
		        }

		        // Otherwise, just display the title (minus)
		        else {
			        $breadcrumbs[] = $title;
		        }

		    }

		    // Build our temporary array (pieces of bread) into one big string :)
		    return implode($separator, $breadcrumbs);
		}

		?>
			<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<p class="breadcrumb"><?= breadcrumbs() ?></p>
			</div>
	        <div class="column content">
				<?= $this->fetch('content') ?>
			</div>
			<div class="column right">
				<?= $this->fetch('right-sidebar') ?>
			</div>
		</div>
	 </div>
</div>
