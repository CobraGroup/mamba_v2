<?php

	$this->start('banner');
		foreach($blocks['banner'] as $block) {
			echo $this->Html->image($block['image'], ['alt' => $block['captions']]);
		}
	$this->end();

	$this->start('left-column');
		foreach($blocks['left-column'] as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();

	$this->start('center-column');
		foreach($blocks['center-column'] as $block) {

			echo $this->element('expertise-column', ['block' => $block]);
		}
	$this->end();

	$this->start('news-section');
		foreach($blocks['news-section'] as $block) {
			echo $this->element('news-section', ['block' => $block]);
		}
	$this->end();
	$this->start('what-we-do-section');
		foreach($blocks['what-we-do-section'] as $block) {

			echo $this->element('what-we-do', ['block' => $block]);
		}
	$this->end();
?>

<div id="container">
    <div id="banner">
		<?php echo $this->fetch('banner');  ?>
	</div>

    <div class="center">
        <div class="clearfix"></div>
	</div>

    <div class="row layout1" id="content">
        <div class="column left">
			<?= $this->fetch('left-column') ?>
		</div>

		<div class="column center">
		    <div class="expertise">
		    <h2>Industry expertise</h2>
			<hr>
			<?= $this->fetch('center-column') ?>
	     	</div>
		</div>
	</div>

	<div class="column right">
        <div class="latest_news">
            <div class="header">
				<h2>Latest news</h2>
				<hr>
			</div>
			<?= $this->fetch('news-section') ?>
		</div>
	</div>
	<div class="clearfix"></div>


</div>
