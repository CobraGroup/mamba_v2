<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}

echo $this->fetch("banner");
echo $this->element("menu");
echo $this->element("breadcrumbs");
?>

<div class="page-with-left-sidebar">
	<div class="container">
		<div class="row">
			<div class="col col-sm-4 col-md-3 sidebar">

<?php 
echo $this->fetch('sidebar');
?>

			</div>
			<div class="col col-sm-8 col-md-9 content">

<?php 
echo $this->fetch('content');
?>

			</div>
		</div>
	</div>
</div>

