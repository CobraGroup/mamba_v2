<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}
?>
<div class="home-intro home-intro-map">
	<div class="container">
		<div class="row">
			<div class="col col-md-2 col-xs-6 col-logo">
				<a href="/"><img src="/img/appco2016/appco-logo.png"></a>
			</div>
			<div class="col col-md-7 hidden-xs hidden-sm col-location">
				<div class="location-arrow nav-right-button">
					<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
				</div>
				<div class="dropdown">
					<h4 class="dropdown-toggle" type="button" id="dropdownArea1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Where do you want to go?</h4>
					<ul class="dropdown-menu" aria-labelledby="dropdownArea1">
						<li><a href="http://www.appcogroup.com.au/" target="_blank">Australia</a></li>
						<li><a href="http://www.appcogroup.be/en" target="_blank">Belgium</a></li>
						<li><a href="http://www.appcogroup.com.br/" target="_blank">Brazil</a></li>
						<li><a href="http://www.appcogroup.ca/" target="_blank">Canada</a></li>
						<li><a href="http://www.appcogroup.com/news/appco-group-opens-cyprus-office" target="_blank">Cyprus</a></li>
						<li><a href="http://www.appcogroup.fr/" target="_blank">France</a></li>
						<li><a href="http://www.appcogroup.gr/" target="_blank">Greece</a></li>
						<li><a href="http://www.appcogroup.hk/" target="_blank">Hong Kong</a></li>
						<li><a href="http://www.appcogroupindia.in/" target="_blank">India</a></li>
						<li><a href="http://www.appcogroup.id/" target="_blank">Indonesia</a></li>
						<li><a href="http://www.appcogroupireland.ie/" target="_blank">Ireland</a></li>
						<li><a href="http://www.appcogroup.it/" target="_blank">Italy</a></li>
						<li><a href="http://www.appcogroup.jp/" target="_blank">Japan</a></li>
						<li><a href="http://www.appcogroup.kr/" target="_blank">Korea</a></li>
						<li><a href="http://www.appcogroup.my/" target="_blank">Malaysia</a></li>
						<li><a href="http://www.appcogroupnederland.nl/" target="_blank">Netherlands</a></li>
						<li><a href="http://www.appcogroup.co.nz/" target="_blank">New Zealand</a></li>
						<li><a href="http://www.appcogroup.ph/" target="_blank">Philippines</a></li>
						<li><a href="http://www.appcogroup.pl/" target="_blank">Poland</a></li>
						<li><a href="http://www.appcogroup.sg/" target="_blank">Singapore</a></li>
						<li><a href="http://www.appcogroup.es/" target="_blank">Spain</a></li>
						<li><a href="http://www.appcogroup.se/" target="_blank">Sweden</a></li>
						<li><a href="http://www.appcogroup.in.th/" target="_blank">Thailand</a></li>
						<li><a href="http://www.appcogroup.co.uk/" target="_blank">UK</a></li>
						<li><a href="http://www.appcogroupusa.com/" target="_blank">USA</a></li>
					</ul>
				</div>
			</div>
			<div class="col col-md-3 col-xs-6 col-menu">
				<div class="location-arrow nav-right-button">
					<span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
				</div>
				<div class="dropdown">
					<h4 class="dropdown-toggle" type="button" id="dropdownArea2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<span class="hidden-xs hidden-sm">Menu</span>
						<span class="hidden-md hidden-lg">&nbsp;</span>
					</h4>
					<ul class="dropdown-menu" aria-labelledby="dropdownArea2">
						<li><a href="/appco-groups-expertise">Our expertise</a></li>
						<li><a href="/our-story">Our story</a></li>
						<li><a href="/news">News</a></li>
						<li><a href="/appco-worldwide">Contact us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="google-map-bg"><iframe src="https://www.google.com/maps/d/embed?mid=1WD6a8qkDtqPzJIGrrTDXoSFzijI" width="100%" height="100%"></iframe></div>
	<div class="bottom-down-arrow map-fullscreen-button hidden-xs hidden-sm">
		<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> <span class="text">expand the map</span>
	</div>
</div>

<div class="home-about-appco home-about-appco-odd">
	<div class="container">
		<div class="row">
			<div class="col col-md-6 hidden-xs hidden-sm">
				<div class="col-about-appco-image">
					<img src="/img/appco2016/home/appco-group-is.jpg">
				</div>
			</div>
			<div class="col col-md-6">
				<div class="col-about-appco-text">
					<h1><div class="hidden-xs hidden-sm"><span>Welcome to</span>APPCO GROUP</div><div class="hidden-md hidden-lg">Appco Group is ...</div></h1>
					<div id="home-carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
<?php
							$count_carousel = 0;
							foreach($data["intro-carousel"] as $block){
								$count_carousel++;
?>
							<div class="item <?php echo ($count_carousel==1) ? "active" : ""; ?>">
<?php
								echo $block['content'];
?>
							</div>
<?php
							}
?>
						</div>
						<ol class="carousel-indicators">
<?php
							for($i=0; $i<$count_carousel;$i++){
?>
							<li data-target="#home-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i==0) ? "active" : ""; ?>"></li>
<?php
							}
?>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="home-appco-services-title hidden-xs hiddden-sm">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
				<h4>APPCO GROUP'S EXPERTIS<span>E</span></h4>
			</div>
		</div>
	</div>
</div>
<div class="home-appco-services-title-mobile hidden-md hidden-lg">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
				<h4>APPCO GROUP'S <span>EXPERTISE</span></h4>
			</div>
		</div>
	</div>
</div>
<div class="home-appco-services hidden-xs hidden-sm">
	<div class="home-appco-services-bg"><br></div>
	<div class="container">
		<div class="row row-eq-height">
<?php
			foreach($data["boxes"] as $block){
?>
			<div class="col col-xs-12 col-sm-6 col-md-3">
				<img src="<?php echo $block['image'] ?>">
				<h2><?php echo $block['title'] ?></h2>
				<p><?php echo strip_tags($block['content']) ?></p>
				<a href="<?php echo $block['subtitle'] ?>"><button class="btn">Read more</button></a>
			</div>
<?php
			}
?>
		</div>
	</div>
</div>

<div class="home-appco-services-mobile hidden-md hidden-lg">
	<div id="home-carousel-services" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
<?php
			$count_carousel = 0;
			foreach($data["boxes"] as $block){
				$count_carousel++;
?>
			<div class="item <?php echo ($count_carousel==1) ? "active" : ""; ?>">
				<img src="<?php echo $block['image'] ?>">
				<h2><?php echo $block['title'] ?></h2>
				<p><?php echo strip_tags($block['content']) ?></p>
				<a href="<?php echo $block['subtitle'] ?>"><button class="btn">Read more</button></a>
			</div>
<?php
			}
?>
		</div>
		<ol class="carousel-indicators">
<?php
			for($i=0; $i<$count_carousel;$i++){
?>
			<li data-target="#home-carousel-services" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i==0) ? "active" : ""; ?>"></li>
<?php
			}
?>
		</ol>
	</div>
</div>

<?php
			$count_featured = 0;
			foreach($data["featured"] as $block){
				$count_featured++;
				$image_content = '
			<div class="col col-md-6 hidden-xs hidden-sm">
				<div class="col-about-appco-image">
					<img src="'.$block['image'].'">
				</div>
			</div>				
';
				$text_content = '
			<div class="col col-md-6 col-xs-12">
				<div class="col-about-appco-text">
					<h2>'.$block['title'].'</h2>
					<p>'.strip_tags($block['content']).'</p>
					<a href="'.$block['subtitle'].'"><button class="btn">Read more</button></a>
				</div>
			</div>
';

				if(bcmod($count_featured, 2) != 0){
					?>
<div class="home-about-appco">
	<div class="container">
		<div class="row">
			<?php echo $image_content.$text_content ?>
		</div>
	</div>
</div>
					<?php
				}else{
					?>
<div class="home-about-appco home-about-appco-odd">
	<div class="container">
		<div class="row">
			<?php echo $text_content.$image_content ?>
		</div>
	</div>
</div>
					<?php
				}
			}
?>
		</div>

<?php
echo $this->fetch("news");
?>

