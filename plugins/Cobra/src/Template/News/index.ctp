<h1>Cobra <?= __('News'); ?></h1>

<ul>
<?php foreach ($news as $article): ?>
	<li>
		<?= $this->Html->link($article->title, $lang.'/news/'.$article->slug->name) ?>
	</li>
<?php endforeach; ?>
</ul>