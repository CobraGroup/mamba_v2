<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __('Cobra Group');
?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon') ?>

	<?= $this->Html->css('cobra') ?>

	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>
<body>
	<header>
		<div class="header-title">
			<span><?= $this->fetch('title') ?></span>
		</div>
		<nav>
			<ul>
				<li><?= $this->Html->link('Home', '/') ?></li>
				<li><?= $this->Html->link('News', '/news') ?></li>
			</ul>
		</nav>
		<div class="header-help">
			<?php foreach($languages as $language): ?>
			<span><a href="/<?= $language['slug'] ?>"><?= $language['country'] ?></a></span>
			<?php endforeach ?>
			<span><a href="/login">Login</a></span>
		</div>
	</header>
	<div id="container">

		<div id="content">
			<?= $this->Flash->render() ?>

			<div class="row">
				<?= $this->fetch('content') ?>
			</div>
		</div>
		<footer>
			<div id="footer">
			    <div class="outter">
			            <div>
			                <div class="left">
			                    <div class="logos">
			                        <ul>
			                            <li>
			                                <img alt="Appco group - Field Marketing  Agency" src="/img/appco/appco_logo_footer.png">
			                            </li>
			                            <li>
			                                <img alt="Be something more with Appco" src="/img/appco/besomethingmore.png">
			                            </li>
			                        </ul>
			                    </div>
			                </div>
			                <div class="right">
			                    <ul class="logos">

			                    </ul>
			                </div>
			            </div>
			    </div>
			    <div class="grey">
			        <div class="outter">
			            <div class="copyright">
				        	<div class="cobra-company">
				        		<img alt="Cobra Group Company" src="/img/cobra-group-company.png">
				        	</div>
			                <ul>
			                                        	<li><a href="/about-us" hreflang="x-default" rel="alternate">About us</a></li>
			                                                            	<li><a href="/what-we-do" hreflang="x-default" rel="alternate">What we do</a></li>
			                                                            	<li><a href="/news" hreflang="x-default" rel="alternate">News</a></li>

			                    								<li><a href="/appco-worldwide" hreflang="x-default" rel="alternate">Contact us</a></li>
										                                              <li><a href="/footer/privacy-policy" hreflang="x-default" rel="alternate">Privacy policy</a></li>

			                </ul>
			            </div>
			            <div class="rights">
				            <!-- Go to www.addthis.com/dashboard to customize your tools -->

						<script async="async" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a2dce77bf1c151" type="text/javascript"></script>


			            	<ul>
			            		<li><a rel="nofollow" target="_blank" href="https://www.linkedin.com/company/appco-group?trk=tyah&amp;trkInfo=tas%3AAppco%2Cidx%3A1-1-1"><img alt="Linked In" src="/img/social/in.png" class="dcssjsecwotblflzbmkg"></a></li>
			            		<li><a rel="nofollow" target="_blank" href="https://twitter.com/appcogroup"><img alt="Twitter" src="/img/social/tw.png" class="dcssjsecwotblflzbmkg"></a></li>
			            		<li><a rel="nofollow" target="_blank" href="https://www.facebook.com/appcogroup"><img alt="Facebook" src="/img/social/fb.png" class="dcssjsecwotblflzbmkg"></a></li>
			            	             		<li><a rel="nofollow" target="_blank" href="https://plus.google.com/+appcogroup/posts"><img alt="Google Plus" src="/img/social/google-plus-appco.jpg" class="dcssjsecwotblflzbmkg"></a></li>
			            		<li><a rel="nofollow" target="_blank" href="http://www.youtube.com/user/appcogrouptv"><img alt="Youtube" src="/img/social/you-tube-appco.jpg" class="dcssjsecwotblflzbmkg"></a></li>
			            	            	</ul>
			            	                <p>Appco Group &copy; all rights reserved 2014 </p>

			            </div>
			        </div>
			     </div>
			</div>
		</footer>
	</div>
</body>
</html>
