<?php
use Cake\Routing\Router;

Router::prefix('admin', function($routes) {
	$routes->connect('/websites', ['plugin' => 'WebsitesManager', 'controller' => 'Websites', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/websites/:action', ['plugin' => 'WebsitesManager', 'controller' => 'Websites'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/websites/:action/*', ['plugin' => 'WebsitesManager', 'controller' => 'Websites'], ['routeClass' => 'InflectedRoute']);

	#$routes->connect('/domains', ['plugin' => 'DomainsManager', 'controller' => 'Domains', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
	#$routes->connect('/domains/:action', ['plugin' => 'DomainsManager', 'controller' => 'Domains'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/domains/:action/*', ['plugin' => 'DomainsManager', 'controller' => 'Domains'], ['routeClass' => 'InflectedRoute']);

});


Router::plugin('WebsitesManager', function($routes) {
	$routes->fallbacks();
});
