<?php

namespace WebsitesManager\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WebsitesTable extends Table {

    public function initialize(array $config) {
        #$this->addBehavior('Timestamp');        
        $this->belongsToMany('Languages', [
        	'className' => 'Languages',
        	'joinTable' => 'websites_languages'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('name')
            ->notEmpty('theme');

        return $validator;
    }

}
