<?php

namespace WebsitesManager\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DomainsTable extends Table {

    public function initialize(array $config) {
        #$this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('name')
            ->notEmpty('website_id');

        return $validator;
    }

}
