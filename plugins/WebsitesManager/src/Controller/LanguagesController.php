<?php

namespace WebsitesManager\Controller;

use LanguagesController\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

class LanguagesController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'switcher']);
    }

    public function index() {
        $this->set('langs', $this->Languages->find('all'));
    }

    public function switcher() {
        if (!isset($this->request->params['lang'])) {
            throw new NotFoundException(__('Invalid article'));
        }

        $article = $this->Languages->find()
	        ->where(['slug' => $this->request->params['slug']])
	        ->first();
	        
        $this->set('article', $article);
        
       // pr(I18n::getDefault());
        //$this->Articles->test();

    }

}