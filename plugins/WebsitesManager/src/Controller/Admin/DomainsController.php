<?php

namespace WebsitesManager\Controller\Admin;

use DomainsController\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

class DomainsController extends AppController {

	public $helpers = [
		'Form' => [
			'templates' => 'bootstrap_form.php', // location: /config
		]
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'view']);
    }

    public function view($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid website'));
	    }
        $domains = $this->Domains->find('all')->where('website_id' => $id);
        $this->set(compact('domains'));
    }

    public function add() {
        $domain = $this->Domains->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Domains->save($domain)) {
                $this->Flash->success(__('Your domain has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your domain.'));
        }
 	    $this->set('status', array('Published','Draft','Trash'));
 		$this->set('domain', $domain);
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid domain'));
	    }
	    $domain = $this->Domains->get($id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->Domains->patchEntity($domain, $this->request->data);
	        if ($this->Domains->save($domain)) {
	            $this->Flash->success(__('Your domain has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your domain.'));
	    }
	    $this->set('status', array('Published','Draft','Trash'));
	    $this->set('domain', $domain);
	}

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $domain = $this->Domains->get($id);
	    if ($this->Domains->delete($domain)) {
	        $this->Flash->success(__('The domain with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}