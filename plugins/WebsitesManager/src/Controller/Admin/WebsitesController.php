<?php

namespace WebsitesManager\Controller\Admin;

use WebsitesManager\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

class WebsitesController extends AppController {

	public $helpers = [
		'Form' => [
			'templates' => 'bootstrap_form.php', // location: /config
		]
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'view']);
    }

    public function index() {
        $websites = $this->Websites->find('all');
        $this->set(compact('websites'));
    }

    public function add() {
        $website = $this->Websites->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Websites->save($website)) {
                $this->Flash->success(__('Your website has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your website.'));
        }
	    $languages = $this->Websites->Languages->find('list')->where(['Languages.active' => 1]);
	    
	    $this->set('languages', $languages->toArray());
 	    $this->set('status', array('Published','Draft','Trash'));
 		$this->set('website', $website);
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid website'));
	    }
	    $website = $this->Websites->find()
	    	->where(['id' => $id])
	    	->contain(['Languages'])
	    	->first();

	    if ($this->request->is(['post', 'put'])) {
	        $this->Websites->patchEntity($website, $this->request->data);
	        if ($this->Websites->save($website)) {
	            $this->Flash->success(__('Your website has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your website.'));
	    }
	    $languages = $this->Websites->Languages->find('list')->where(['Languages.active' => 1]);
	    
	    $this->set('languages', $languages->toArray());
	    $this->set('status', array('Published','Draft','Trash'));
	    $this->set('website', $website);

	}

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $website = $this->Websites->get($id);
	    if ($this->Websites->delete($website)) {
	        $this->Flash->success(__('The website with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}