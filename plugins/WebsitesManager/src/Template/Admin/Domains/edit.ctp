<h1>Edit Domain</h1>

<?= $this->Form->create($domain) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('website_id') ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>