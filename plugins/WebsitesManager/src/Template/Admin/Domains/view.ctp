<h1><?= __d('websites_manager', 'Domains'); ?> </h1>
<p><?= $this->Html->link('Add Domain', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-striped">
			<thead>
		    <tr>
		        <th>Name</th>
		        <th>Theme</th>
		        <th>Status</th>
		        <th>Actions</th>
		    </tr>
			</thead>
		    <tbody>
		    <?php foreach ($domains as $domain): ?>
		    <tr>
		        <td>
		            <?= $this->Html->link($domain->name, ['action' => 'edit', $domain->id]) ?>
		        </td>
		        <td>
		            <?= $domain->theme ?>
		        </td>
		        <td>
		            <?= $domain->status ?>
		        </td>
		        <td>
		            <?= $this->Form->postLink(
		                'Delete',
		                ['action' => 'delete', $domain->id],
		                ['confirm' => 'Are you sure?', 'class' => 'btn btn-xs btn-danger'])
		            ?>
		            <?= $this->Html->link('Edit', ['action' => 'edit', $domain->id], ['class' => 'btn btn-xs btn-primary']) ?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		    </tbody>
		</table>
	</div>
</div>