<h1><?= __d('websites_manager', 'Websites'); ?></h1>
<p><?= $this->Html->link('Add Website', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-striped">
			<thead>
		    <tr>
		        <th>Name</th>
		        <th>Theme</th>
		        <th>Status</th>
		        <th>Actions</th>
		    </tr>
			</thead>
		    <tbody>
		    <?php foreach ($websites as $website): ?>
		    <tr>
		        <td>
		            <?= $this->Html->link($website->name, ['action' => 'edit', $website->id]) ?>
		        </td>
		        <td>
		            <?= $website->theme ?>
		        </td>
		        <td>
		            <?= $website->live ?>
		        </td>
		        <td>
		            <?php //$this->Html->link('Domains', '/admin/domains/view/'.$website->id, ['class' => 'btn btn-xs btn-primary']) ?>
		            <?= $this->Html->link('Edit', ['action' => 'edit', $website->id], ['class' => 'btn btn-xs btn-primary']) ?>
		            <?= $this->Form->postLink(
		                'Delete',
		                ['action' => 'delete', $website->id],
		                ['confirm' => 'Are you sure?', 'class' => 'btn btn-xs btn-danger'])
		            ?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		    </tbody>
		</table>
	</div>
</div>