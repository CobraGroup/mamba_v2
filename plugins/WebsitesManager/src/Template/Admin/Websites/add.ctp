<h1>Add Website</h1>

<?= $this->Form->create($website) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('analytics') ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">Languages</div>
			<div class="panel-body">
				<?= $this->Form->input('languages._ids', [
						'label' => false,
						'multiple' => 'checkbox',
						'options' => $languages,
					]) 
				?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('theme') ?>
				<?= $this->Form->input('live') ?>
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>