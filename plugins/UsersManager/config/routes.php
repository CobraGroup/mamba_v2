<?php
use Cake\Routing\Router;

Router::connect('/login', ['plugin' => 'UsersManager', 'controller' => 'Users', 'action' => 'login']);
Router::connect('/logout', ['plugin' => 'UsersManager', 'controller' => 'Users', 'action' => 'logout']);

Router::prefix('admin', function($routes) {
	$routes->connect('/users', ['plugin' => 'UsersManager', 'controller' => 'Users', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/users/:action', ['plugin' => 'UsersManager', 'controller' => 'Users', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/users/:action/*', ['plugin' => 'UsersManager', 'controller' => 'Users', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
});

Router::plugin('UsersManager', function($routes) {
	$routes->fallbacks();
});
