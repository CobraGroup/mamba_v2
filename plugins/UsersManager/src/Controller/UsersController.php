<?php

namespace UsersManager\Controller;

use UsersManager\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['register', 'logout']);
    }

	public function login() {
		$this->log((new DefaultPasswordHasher)->hash('C0br4'));
	    if ($this->request->is('post')) {
	        $user = $this->Auth->identify();$this->log($user);
	        if ($user) {
	            $this->Auth->setUser($user);
	            return $this->redirect($this->Auth->redirectUrl());
	        } else {
	            $this->Flash->error(
	                __('Username or password is incorrect'),
	                'default',
	                [],
	                'auth'
	            );
	        }
	    }
	}

	public function register() {
	    $user = $this->Users->newEntity($this->request->data);
	    if ($this->Users->save($user)) {
	        $this->Auth->setUser($user->toArray());
	        return $this->redirect([
	            'controller' => 'Users',
	            'action' => 'home'
	        ]);
	    }
	}

	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}

}