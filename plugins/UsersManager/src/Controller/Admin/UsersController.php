<?php

namespace UsersManager\Controller\Admin;

use UsersManager\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController {

	public $helpers = [
		'Form' => ['templates' => 'admin-form']
	];

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function index() {
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id) {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid user'));
	    }
	    $user = $this->Users->get($id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->Users->patchEntity($user, $this->request->data);
	        if ($this->Users->save($user)) {
	            $this->Flash->success(__('Your user has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your user.'));
	    }
	    $this->set('roles', array('Editor' => 'Editor', 'admin' => 'Admin'));
	    $this->set('user', $user);
	}

    public function add() {
        $user = $this->Users->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
	    $this->set('roles', array('Editor' => 'Editor', 'admin' => 'Admin'));
        $this->set('user', $user);
    }

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);

	    $user = $this->Users->get($id);
	    if ($this->Users->delete($user)) {
	        $this->Flash->success(__('The user with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}