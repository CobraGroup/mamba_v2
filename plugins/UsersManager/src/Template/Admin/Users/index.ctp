<h1><?= __d('news_manager', 'News'); ?> users</h1>
<p><?= $this->Html->link('Add User', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-striped">
			<thead>
		    <tr>
		        <th>Title</th>
		        <th>Username</th>
		        <th>Role</th>
		        <th>Created</th>
		        <th>Actions</th>
		    </tr>
			</thead>
			<tbody>
		    <?php foreach ($users as $user): ?>
		    <tr>
		        <td>
		            <?= $this->Html->link($user->name, ['action' => 'edit', $user->id]) ?>
		        </td>
		        <td>
		            <?= $user->username ?>
		        </td>
		        <td>
		            <?= $user->role ?>
		        </td>
		        <td>
		            <?= $user->created->format(DATE_RFC850) ?>
		        </td>
		        <td>
		            <?= $this->Form->postLink(
		                'Delete',
		                ['action' => 'delete', $user->id],
		                ['confirm' => 'Are you sure?', 'class' => 'btn btn-xs btn-danger'])
		            ?>
		            <?= $this->Html->link('Edit', ['action' => 'edit', $user->id], ['class' => 'btn btn-xs btn-primary']) ?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>