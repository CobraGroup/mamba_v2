<h1>Edit User</h1>

<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $this->Form->create($user) ?>
				<?= $this->Form->input('name') ?>
				<?= $this->Form->input('username') ?>
				<?= $this->Form->input('role') ?>
				<?= $this->Form->input('blocked') ?>
				<?= $this->Form->button(__('Save')) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Change Password</div>
			<div class="panel-body">
				<?= $this->Form->create($user) ?>
				<?= $this->Form->input('password') ?>
				<?= $this->Form->button(__('Update')) ?>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>
