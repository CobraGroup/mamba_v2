<?php
$here = $this->request->here;

if(substr($here,0,1)=="/") $here = substr($here, 1);
if(substr($here,-1,1)=="/") $here = substr($here, 0, -1);

switch($here){
	case("directors"):
?>
<div class="directors">
	<div class="container">
		<?php 
		if($block['title']){
		?>
		<div class="row">
			<div class="col col-xs-12">
			<!-- <h1>Cobra Group <span>Board of Directors</span></h1> -->
			<?php echo $this->Html->tag($block['tag'], $block['title']); ?>
			</div>
		</div>	
		<?php
		}
		?>
		<div class="row">
			<?php
			foreach($block['posts'] as $post){
				$post->slug = $post->slug_override ? $post->slug_override : $post->slug;
				$slug_parts = explode("/", $post->slug);
				$post_slug = $slug_parts[count($slug_parts)-1];
			?>
			<div class="col col-md-3 director-item">
				<div data-toggle="modal" data-target="#<?php echo $post_slug; ?>">
					<div class="row">
						<div class="col-xs-4 col-md-12 director-photo">
							<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title] ) ?>
						</div>
						<div class="col-xs-8 col-md-12">
							<div class="director-info">
								<h2><?php echo $post->title ?></h2>
								<h3><?php echo $post->subtitle ?></h3>
								<p class="summary"><?php echo strip_tags($post->excerpt) ?></p>
								<p class="read-more">Read more</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Lightbox/Modal pop-up for <?php echo $post->title; ?> -->
			<div class="modal fade" id="<?php echo $post_slug; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $post->slug; ?>-info">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="<?php echo $post->slug; ?>-info"><?php echo $post->title; ?><span><?php echo $post->subtitle ?></span></h4>
			      </div>
			      <div class="modal-body">
			      	<div class="text-center">
				      	<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title, 'class' => 'img-thumbnail img-rounded'] ) ?>
				   	</div>
			        <?php echo $post->body ?>
			      </div>
			    </div>
			  </div>
			</div>
			<?php
			}
			?>
		</div>
		<?php
		?>
	</div>
</div>
<?php
	break;
	case("contact-us"):
	?>
<div class="contact-us">
	<div class="container">
		<div class="row row-eq-height">
			<?php
			foreach($block['posts'] as $post){
			?>
			
			<div class="col col-md-6 contact-info">

					<h2>CONTACT US <span><?php echo $post->title; ?></span></h2>
					<?php echo $post->body ?>
					<p>
						<a href="<?php echo strip_tags($post->excerpt) ?>" target="_blank"><button class="btn btn-lg btn-info">VIEW OUR MAP LOCATION</button></a>
					</p>

			</div>
			
			<?php
			}
			?>
		</div>
	</div>
</div>
	<?php
	break;
}
?>