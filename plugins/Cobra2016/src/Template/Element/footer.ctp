    <div class="footer hidden-xs hidden-sm">
    	<div class="container">
    		<div class="row row-eq-height">
    			<div class="col col-md-3 text-center col-border-right cobra-group-logo">
    				<img src="/img/cobra2016/cobra-group-logo.png">
    			</div>
    			<div class="col col-md-3 hidden-xs hidden-sm col-border-right footer-menu">
    				<ul>
    					<li><h5><a href="#">ABOUT US</a></h4></li>
    					<li><h5><a href="#">NEWS</a></h4></li>
    					<li><h5><a href="#">DIRECTORS</a></h4></li>
    					<li><h5><a href="#">CONTACT US</a></h4></li>
    				</ul>
    			</div>
    			<div class="col col-md-3 col-border-right footer-contact">
	    			<h4>CONTACT US</h4>
    				<p>
						Robert Gibbs<br>
						Director of Business Development<br>
						+44 (0) 20 7424 3700<br>
						<a href="#">rgibbs@cobragroup.com</a><br>
					</p>
    			</div>
    			<div class="col col-md-3 footer-social">
    				<h4>FOLLOW US</h4>
    				<p>
	    				<a href="#"><img src="/img/cobra2016/cobra-facebook.png"></a>
	    				<a href="#"><img src="/img/cobra2016/cobra-linkedin.png"></a>
	    				<a href="#"><img src="/img/cobra2016/cobra-twitter.png"></a>
    				</p>
    			</div>
    		</div>
    	</div>

	    <div class="footer-address hidden-xs hidden-sm">
	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-12 text-center">
	    				<p>Cobra Group International (Hong Kong) Limited: Level 27 Worldwide House, 19 Des Voeux Road, Central, Hong Kong. Registration number: HK F18858.</p>
	    			</div>
	    		</div>
	    	</div>
	    </div>
    </div>

    <div class="mobile-footer-social hidden-md hidden-lg">
    	<div class="container">
    		<div class="row">
    			<div class="col-xs-12 text-center">
    				<h4>FOLLOW US</h4> 
    				<a href="#"><img src="/img/cobra2016/cobra-facebook.png"></a>
    				<a href="#"><img src="/img/cobra2016/cobra-linkedin.png"></a>
    				<a href="#"><img src="/img/cobra2016/cobra-twitter.png"></a>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="mobile-footer-cobra-group-logo hidden-md hidden-lg">
    	<div class="container">
    		<div class="row">
    			<div class="col-xs-12 text-center">
    				<img src="/img/cobra2016/cobra-group-logo.png">
    			</div>
    		</div>
    	</div>
    </div>
