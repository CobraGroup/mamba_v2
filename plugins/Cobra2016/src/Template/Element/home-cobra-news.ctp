	<?php
	$companynews = [
		[
			"title" 	=> "What can Microsoft’s AI chatbot experiment teach young entrepreneurs?",
			"date"		=> "March 16th",
			"source"	=> "chrisniarchos.org",
			"excerpt"	=> "If you didn’t hear about Microsoft’s recent artificial intelligence (AI) chatbot misfortune as the story was unfolding, you won’t have to search hard on the web to find out about it."
		],
		[
			"title" 	=> "Appco brings in hostage negotiator to highlight the power of listening",
			"date"		=> "April 20th",
			"source"	=> "appcogroup.co.uk",
			"excerpt"	=> "Business owners involved in face-to-face marketing will hear an expert hostage negotiator explain the essentials for good communication and the importance of listening at a two-day workshop hosted by Appco Group UK this weekend.",
		],
		[
			"title" 	=> "Cobra Group sponsors Royal Navy Medical Memorial charity cycling ...",
			"date"		=> "June 13th",
			"source"	=> "cobragroup.com",
			"excerpt"	=> "Cobra Group has sponsored a 300-mile charity bike ride to raise money for a Royal Navy Medical Service Memorial at the National Memorial Arboretum.hard on the web to find out about it.",
		],
		[
			"title" 	=> "Cobra Group continues to help Support Lao Children provide vital care to orph...",
			"date"		=> "May 26th",
			"source"	=> "cobragroup.com",
			"excerpt"	=> "The medical, nutritional and educational support being provided to orphans by Support Lao Children has been stepped up a level thanks to the charity’s regular donors, including Cobra Group’s",
		],
		[
			"title" 	=> "What does it take to be a successful leader?",
			"date"		=> "May 06th",
			"source"	=> "cobragroup.com",
			"excerpt"	=> "Cobra Group Founder and Chairman Chris Niarchos reflects on this very question in his latest blog. He says that, while many people can and will succeed in the business world, it takes a certain kind of person to be a truly successful leader.",
		]
	];
	?>
	<div class="home-cobra-news">
		<div class="container">
			<div class="row">
				<div class="col col-header col-xs-12 text-center">
					<h3 class="news-header-title">COBRA GROUP <span>NEWS</span></h3>
				</div>
			</div>
			<div class="row row-eq-height">
				<div class="col col-news-item col-news-item-blog col-md-6 col-xs-12">
					<a href="#">
						<img src="/img/cobra2016/blog-label.gif" class="blog-label">
						<img src="/img/cobra2016/news/1.jpg" class="news-image">
						<div class="news-text">
							<div class="news-date"><?php echo $companynews[0]["date"]; ?> | <span><?php echo $companynews[0]["source"]; ?></span></div>
							<h2><?php echo $companynews[0]["title"]; ?></h2>
							<div class="news-excerpt"><?php echo $companynews[0]["excerpt"]; ?></div>
						</div>
					</a>
				</div>
				<div class="col col-news-item col-md-6 col-xs-12">
					<a href="#">
						<img src="/img/cobra2016/news/2.jpg" class="news-image hidden-xs hidden-sm">
						<div class="news-text">
							<div class="news-date"><?php echo $companynews[1]["date"]; ?> | <span><?php echo $companynews[1]["source"]; ?></span></div>
							<h2><?php echo $companynews[1]["title"]; ?></h2>
							<div class="news-excerpt"><?php echo $companynews[1]["excerpt"]; ?></div>
						</div>
					</a>
				</div>
			</div>
			<div class="row row-eq-height">
<?php
for($i=2;$i<=4;$i++){
?>
				<div class="col col-news-item col-md-4 col-xs-12">
					<a href="#">
						<img src="/img/cobra2016/news/<?php echo $i+1; ?>.jpg" class="news-image hidden-xs hidden-sm">
						<div class="news-text">
							<div class="news-date"><?php echo $companynews[$i]["date"]; ?> | <span><?php echo $companynews[$i]["source"]; ?></span></div>
							<h2><?php echo $companynews[$i]["title"]; ?></h2>
							<div class="news-excerpt"><?php echo $companynews[$i]["excerpt"]; ?></div>
						</div>
					</a>
				</div>
<?php
}
?>
			</div>
		</div>
	</div>
