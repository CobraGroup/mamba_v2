  	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="/img/cobra2016/cobra-group-logo.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li<?php echo ($page=="about") ? ' class="active"' : '' ?>><a href="index.php?page=about">ABOUT US</a></li>
            <li<?php echo ($page=="news") ? ' class="active"' : '' ?>><a href="index.php?page=news">NEWS</a></li>
            <li<?php echo ($page=="directors") ? ' class="active"' : '' ?>><a href="index.php?page=directors">DIRECTORS</a></li>
            <li<?php echo ($page=="contact") ? ' class="active"' : '' ?>><a href="index.php?page=contact-us">CONTACT US</a></li>
          </ul>
        </div>
      </div>
    </nav>
