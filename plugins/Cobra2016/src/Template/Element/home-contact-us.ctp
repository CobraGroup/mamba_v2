	<div class="home-contact-us">
		<div class="container">
			<div class="row">
				<div class="col col-md-6 col-xs-12">
					<div class="contact-info">
						<h2><a href="/contact-us"><img src="/img/cobra2016/plus-icon.png"></a>CONTACT <span>US</span></h2>
						<p>
							<strong>The Cobra Group of Companies</strong><br>
							Cobra Group International (Hong Kong) Limited<br>
							Level 27 Worldwide House, 19 Des Voeux Road,<br>
							Central, Hong Kong<br>
						</p>
						<p>
							<strong>Robert Gibbs</strong><br>
							Director of Business Development<br>
							+44 (0) 20 7424 3700<br>
							<a href="mailto:rgibbs@cobragroup.com">rgibbs@cobragroup.com</a><br>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
