	<div class="cobra-intro-wrapper<?php if($is_home){ ?> is_home<?php }else{ ?> is_inner<?php } ?>">
		<div class="container cobra-intro cobra-logos">
			<?php 
			if(!$is_home){ 
			?>
			<div class="row cobra-logos-title">
				<div class="col col-xs-12">
					<h2><?php if(isset($exclude_company)){ ?><span>MORE</span> <?php } ?>COBRA GROUP <span>COMPANIES</span></h2>
				</div>
			</div>
			<?php
			}
			?>
			<div class="row">
		<?php
		$companies = ["mclaren-gt","wine-talk","appco-group","cobra-technology","amtrust-mobile-solutions","sg-global-support-services","sales-group-international","cube-online-services","cobra-group-investments","crs-racing","comparison-experts","global-fundraising-services"];
		foreach($companies as $company){
			if($exclude_company!=$company){
		?>
				<div class="col col-xs-4 col-md-3">
					<a href="/<?php echo $company; ?>">
		  			<div class="cobra-logo-item hidden-xs hidden-sm"><?php if($is_home){ ?><img src="/img/cobra2016/blue-corner.png" class="blue-corner"><img src="/img/cobra2016/company-logos/<?php echo $company ?>-desktop.png"><?php }else{ ?><img src="/img/cobra2016/company-logos/<?php echo $company ?>-internalpage.jpg"><?php } ?></div>
		  			<div class="cobra-logo-item-mobile cobra-logo-item-<?php echo $company ?> text-center hidden-md hidden-lg"><img src="/img/cobra2016/company-logos/<?php echo $company ?>-mobile.png"></div>
					</a>
				</div>
		<?php
			}
		}
		?>
			</div>
		</div>
	</div>