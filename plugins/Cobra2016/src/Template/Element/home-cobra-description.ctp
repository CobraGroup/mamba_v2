	<div class="container cobra-description">
		<div class="row">
			<div class="col col-md-6 hidden-xs hidden-sm">
				<div class="cobra-branches">
					<h2>CUSTOMER <span>ACQUISITION</span></h2>
					<h2>FACE TO FACE <span>MARKETING</span></h2>
					<h2>DATA <span>MANAGEMENT</span></h2>
					<h2>RETAIL <span>ENERGY</span></h2>
					<h2>AUTOMOTIVE <span>ENHANCEMENT</span></h2>
					<h2>PROPERTY <span>DEVELOPMENT</span></h2>
					<h2>BUSINESS MANAGEMENT <span>SYSTEMS</span></h2>
					<h2>SOFTWARE <span>DEVELOPMENT</span></h2>
					<h2>START-UP <span>INVESTMENT</span></h2>
					<h2>POINT OF SALE <span>TECHNOLOGY</span></h2>
					<h2>HOME <span>SERVICES</span></h2>
				</div>
			</div>
			<div class="col col-xs-12 col-md-6">
				<div class="cobra-history">
					<div class="intro">The <a href="#"><h1>Cobra Group of Companies</h1></a> has a long history of developing small, start-up businesses into successful companies, including the global sales and marketing organisation, <a href="#">Appco Group</a>.</div>
					<p>Over the last two decades, Cobra Group has successfully expanded its portfolio to include a wide range of companies that complement our core expertise of sales and marketing.</p>
					<p>The Cobra Group of Companies currently operates successful businesses specialising in the following industries and areas: face-to-face marketing & customer acquisition; data management & fulfilment; retail energy; automotive enhancement; property development; software development & business management systems; point-of-sale technology; management consultancy; online wine merchants; fundraising services; health & beauty; mobile device protection & enhanced banking solutions; start-up investment & business incubation.</p>
				</div>
				<div class="horizontal-coloured-bar"><img src="/img/cobra2016/coloured-bar.jpg"></div>
			</div>
		</div>
	</div>
