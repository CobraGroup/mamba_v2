<div class="directors">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
				<h1>Cobra Group <span>Board of Directors</span></h1>
			</div>
		</div>
		<?php
		$directors = [
			[
				"id" 		=> 1,
				"slug"		=> "chris-niarchos",
				"name" 		=> "Chris Niarchos",
				"title"		=> "Founder and Chairman",
				"summary"	=> "As the Founder and Chairman of Cobra Group International Ltd, Chris sets the strategic direction for all businesses within the Group and ensures critical objectives are met."
			],
			[
				"id" 		=> 2,
				"slug"		=> "richard-davison",
				"name" 		=> "Richard Davison",
				"title"		=> "Deputy Chairman &amp; CFO",
				"summary"	=> "Richard Davison was appointed Chief Financial Officer in July 1999, joining the Board of Cobra Group plc and leading a team of professional accountants who manage Cobra Group’s finances globally."
			],
			[
				"id" 		=> 3,
				"slug"		=> "paul-sanderson",
				"name" 		=> "Paul Sanderson",
				"title"		=> "Chief Executive Officer",
				"summary"	=> "As Chief Executive Officer, Paul is responsible for ensuring the Group’s strategic and tactical objectives are delivered. Reporting to the Chairman and Board of Directors, he oversees the corporate governance ... "
			],
			[
				"id" 		=> 4,
				"slug"		=> "stewart-hartley",
				"name" 		=> "Stewart Hartley",
				"title"		=> "Non-executive Director",
				"summary"	=> "As a result of his highly successful career working closely with Chris Niarchos to develop and expand Appco Group across Australia, Asia and Europe, Stewart was appointed as a non-executive director of Cobra Group."
			],
			[
				"id" 		=> 5,
				"slug"		=> "tony-fernandez",
				"name" 		=> "Tony Fernandez",
				"title"		=> "Non-executive Director",
				"summary"	=> "Tony was appointed to the Cobra Group Board of Directors, as a non-executive director, in 2013. He was one of the members of Chris Niarchos's first Appco office in Sydney in 1988. Since then, he has been an integral part of ... "
			],
			[
				"id" 		=> 6,
				"slug"		=> "frederique-schillern",
				"name" 		=> "Frederique Schillern",
				"title"		=> "Non-executive Director",
				"summary"	=> "Frederique was appointed to the Board of Cobra Group International Ltd, as non-executive director, in July 2013. Frederique started her career in the trust and corporate services field in Hong Kong in 1990 ... "
			],
			[
				"id" 		=> 7,
				"slug"		=> "linna-chang",
				"name" 		=> "Linna Chang",
				"title"		=> "Non-executive Director",
				"summary"	=> "Linna was appointed to the Board of Cobra Group International Ltd, as a non-executive director, in July 2013. After graduating with a Masters degree in Economics from Erasmus University in Rotterdam, Linna moved to Hong Kong in ... "
			]
		];
		?>
		<div class="row">
			<?php
			foreach($directors as $key => $director){
			?>
			<div class="col col-md-3 director-item">
				<div data-toggle="modal" data-target="#modal-<?php echo $director["id"]; ?>">
					<div class="row">
						<div class="col-xs-4 col-md-12 director-photo"><img src="/img/cobra2016/directors/<?php echo $director["slug"] ?>.jpg"></div>
						<div class="col-xs-8 col-md-12">
							<div class="director-info">
								<h2><?php echo $director["name"] ?></h2>
								<h3><?php echo $director["title"] ?></h3>
								<p class="summary"><?php echo $director["summary"] ?></p>
								<p class="read-more">Read more</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Lightbox/Modal pop-up for <?php echo $director["name"]; ?> -->
			<div class="modal fade" id="modal-<?php echo $director["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $director["slug"]; ?>-info">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="<?php echo $director["slug"]; ?>-info"><?php echo $director["name"]; ?></h4>
			      </div>
			      <div class="modal-body">
			      	<div class="text-center"><img class="img-thumbnail img-rounded" src="/img/cobra2016/directors/<?php echo $director["slug"] ?>.jpg"></div>
			        <p><?php echo $director["summary"] ?></p>
			        <p><?php echo $director["summary"] ?></p>
			        <p><?php echo $director["summary"] ?></p>
			      </div>
			    </div>
			  </div>
			</div>
			<?php
			}
			?>
		</div>
		<?php
		?>
	</div>
</div>
