	<div class="container company-details">
		<div class="row">
			<div class="col col-xs-12">
				<h1>Cobra Group of Companies / <span><?php echo $company_info["title"]; ?></span></h1>
			</div>
		</div>
		<div class="row row-eq-height">
			<div class="col col-md-6 company-logo cobra-logo-item-<?php echo $company ?> text-center">
				<img src="/img/cobra2016/company-logos/<?php echo $company ?>-detail.png">
			</div>
			<div class="col col-md-6 company-description">
				<?php echo $company_info["description"]; ?>
			</div>
		</div>
		<div class="row">
			<div class="col col-xs-12">
				<h2><?php echo $company_info["title"]; ?> <span>Statistics</span></h2>
			</div>
		</div>
		<div class="row row-eq-height">
			<div class="col col-md-6 company-stats company-description">
				<p>Acquired more than 10 million energy customers and more than 12 million telecommunications customers on behalf of our clients worldwide.</p>
			</div>
			<div class="col col-md-6 company-stats company-description">
				<p>Acquired more than 10 million energy customers and more than 12 million telecommunications customers on behalf of our clients worldwide.</p>
			</div>
		</div>
		<div class="row row-eq-height">
			<div class="col col-md-6 company-stats company-description">
				<p>Acquired more than 10 million energy customers and more than 12 million telecommunications customers on behalf of our clients worldwide.</p>
			</div>
			<div class="col col-md-6 company-stats company-description">
				<p>Acquired more than 10 million energy customers and more than 12 million telecommunications customers on behalf of our clients worldwide.</p>
			</div>
		</div>
		<div class="row row-eq-height">
			<div class="col col-md-6 company-stats company-description">
				<p>Acquired more than 10 million energy customers and more than 12 million telecommunications customers on behalf of our clients worldwide.</p>
			</div>
			<div class="col col-md-6 company-stats company-description">
				<p>Acquired more than 10 million energy customers and more than 12 million telecommunications customers on behalf of our clients worldwide.</p>
			</div>
		</div>
	</div>
