<div class="<?= $block['style'] ?>">
	<?php if($block['title']) echo $this->Html->tag($block['tag'], $block['title']) ?>

	<?php foreach($block['posts'] as $post): ?>
	<div class="person">
	    <div class="image col-md-4">
			<?php if($post->thumb) echo $this->Html->image($post->thumb, ['alt' => $post->title, 'url' => $post->slug] ) ?>
	    </div>
	    <div class="description  col-md-8">
	        <h2><?= $post->title ?></h2>
	        <?php if(!empty($post->subtitle)) :?>
	        <h3><?= $post->subtitle ?></h3>
	        <?php endif; ?>

			<p><?= $post->excerpt ?></p>
			<?= $this->Html->link(__('Read more'), $post->slug, ['class' => 'btn btn-primary']) ?>
	    </div>
	</div>
	<?php endforeach; ?>
</div>