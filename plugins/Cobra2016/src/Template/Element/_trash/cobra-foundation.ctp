	<div class="container cobra-foundation">
		<div class="row">
			<div class="col col-xs-12">
				<div class="foundation-description">
					<h1>Cobra Foundation</h1>
					<p><strong>Approximately 100,000 people work with us, either directly or indirectly, every day. We decided to harness this potential and develop the Cobra Foundation, with the aim of giving back to those who are in challenging circumstances, or who need a little extra support.</strong></p>
					<p>The Cobra Foundation was established in 2010 with a vision to create a global community of charitable causes that all businesses and employees within The Cobra Group can support. Our aim is for the Foundation to allow Cobra Group and Appco Group staff, as well as our global marketing company network, to engage in philanthropic work and make a difference in communities across the world.</p>
					<p>Since 2010, Cobra Foundation has donated hundreds of thousands of pounds to many causes – including the Support Lao Children charity, and more than £50,000 to the British Red Cross’s projects in Sierra Leone. Many of our staff members have also volunteered to rebuild communities who have been affected by natural disasters.</p>
					<p>Cobra Foundation projects include:</p>
					<ul class="list-unstyled">
						<li><h2>Projects in Sierra Leonne with the British Red Cross</h2></li>
						<li><h2>Cordaid partnership – Netherlands</h2></li>
						<li><h2>Earthquake appeal – Italy</h2></li>
						<li><h2>Bushfire fund – Australia</h2></li>
						<li><h2>Support Lao Children – UK and Ireland</h2></li>
						<li><h2>Shelter Home for Children – Asia</h2></li>
						<li><h2>Support to dozens of staff and associates of the group in individual fundraising pursuits</h2></li>
					</ul>
					For more information, please visit: <a href="http://www.cobragroupfoundation.org" target="_blank">www.cobragroupfoundation.org</a>
				</div>
			</div>
		</div>
	</div>
