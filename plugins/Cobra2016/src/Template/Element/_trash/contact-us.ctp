<div class="contact-us">
	<div class="container">
		<div class="row row-eq-height">
			<div class="col col-md-6 contact-info">

					<h2>CONTACT US <span>UK OFFICE</span></h2>
					<p class="address">
						<strong>The Cobra Group of Companies</strong><br>
						53-79 Highgate Rd, London NW5 1TL, UK<br>
					</p>
					<p>
						<strong>Robert Gibbs</strong><br>
						Director of Business Development<br>
						+44 (0) 20 7424 3700<br>
						<a href="mailto:rgibbs@cobragroup.com">rgibbs@cobragroup.com</a><br>
					</p>
					<p>
						<a href="https://www.google.com/maps/place/The+Cobra+Group+of+Companies/@51.5540822,-0.1466978,17z/data=!3m1!4b1!4m5!3m4!1s0x48761afefb4001c3:0xf395f38f3f540948!8m2!3d51.5540822!4d-0.1445038?hl=en-US" target="_blank"><button class="btn btn-lg btn-info">VIEW OUR MAP LOCATION</button></a>
					</p>

			</div>
			<div class="col col-md-6 contact-info">

					<h2>CONTACT US <span>HONG KONG OFFICE</span></h2>
					<p class="address">
						<strong>The Cobra Group of Companies</strong><br>
						Cobra Group International (Hong Kong) Limited<br>
						Level 27 Worldwide House, 19 Des Voeux Road,<br>
						Central, Hong Kong<br>
					</p>
					<p>
						<strong>Robert Gibbs</strong><br>
						Director of Business Development<br>
						+44 (0) 20 7424 3700<br>
						<a href="mailto:rgibbs@cobragroup.com">rgibbs@cobragroup.com</a><br>
					</p>
					<p>
						<a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.879547599447!2d114.15552971547808!3d22.28255218533273!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340400649b8a8c55%3A0x6dd8ad21746f1228!2sWorld+Wide+House%2C+19+Des+Voeux+Rd+Central%2C+Central%2C+Hong+Kong!5e0!3m2!1sen!2suk!4v1466160438943" target="_blank"><button class="btn btn-lg btn-info">VIEW OUR MAP LOCATION</button></a>
					</p>

			</div>
		</div>
	</div>
</div>

