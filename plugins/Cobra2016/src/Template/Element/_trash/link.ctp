<div class="cloud-tags">
	<hr>
	<h3>
	<?php
	if($block["section_title"]){
		echo $block["section_title"];
	}
	?>
	</h3>
	<?php
	// 	print_r($block);
	foreach($block["title"] as $key => $title){
		if($title){
		if(substr($block['slug'][$key],0,1) != "/") $block['slug'][$key] = "/".$block['slug'][$key];
		?>
		<a class="tag" <?= $block['link'][$key] ? 'href="'.$block['link'][$key].'" target="_blank"' : 'href="'.$block['slug'][$key].'"' ?>><?php echo $title ?></a>
		<?php
		}
	}
	?>
	<div class="clear"></div>
</div>