	<div class="cobra-news-bg hidden-xs hidden-sm"></div>
	<div class="container cobra-news">
		<div class="row">
			<div class="col col-xs-12">
				<h2>Cobra Group <span>latest news</span></h2>
			</div>
		</div>
		<div class="row news-item news-single">
			<div class="col col-xs-12 news-image">
				<img src="/img/cobra2016/news/main-1.jpg">
			</div>
			<div class="col col-xs-12 news-content">
				<p class="news-date">June 13th</p>
				<h1>Cobra Group continues to help Support Lao Children provide vital care to orphaned children</h1>
				<p>The medical, nutritional and educational support being provided to orphans by Support Lao Children has been stepped up a level thanks to the charity’s regular donors, including Cobra Group’s Cobra Group Foundation.</p>
				<p>As the charity’s longest-running supporter, the Cobra Foundation has contributed thousands of pounds to Support Lao Children’s projects, including the growing medical and food programmes.</p>
				<p>Since July 2015, 345 children have received medical help from the charity, due largely to the establishment of a new children’s hospital and an increased number of dedicated staff for the medical programme.</p>
				<p>Lack of medical support contributes to many premature deaths amongst Lao children, and the new facilities have been credited with saving a number of lives.</p>
				<p>In one particularly moving case, an orphan child was found delirious with sepsis but stabilised after six weeks’ hospital treatment that previously would not have been available to him.</p>
				<h2>Ambitious plans for 2016</h2>
				<p>During 2015, regular donations enabled the charity to extend its village support projects, and Support Lao Children founder Andrew Brown has set an ambitious target of providing support to 10 villages by the end of this year.</p>
				<p>With eight villages now under the charity’s umbrella, and a ninth set to be added shortly, the goal is well within reach.</p>
				<p>Education remains a core focus for the charity, and it has just celebrated enrolling another 200 students into tertiary education through its scholarship programme.</p>
				<p>Mr Brown said the feedback from the university graduates continued to be very positive, with most gaining work after graduation.</p>
				<p>“This programme offers a life-long gift for these children who have come from impoverished backgrounds. They now have the chance to lead a full and healthy life with secure government employment,” he said.</p>
				<p>The charity currently has almost 3,000 children under its support programmes, and aims to build that number to 5,000 in the next 18 months.</p>
				<p>“Lao is a long way from the western world, and I would understand if people think it is too far away, or too hard to help in a meaningful way,” Mr Brown said.</p>
				<p>“Fortunately, the opposite is true. Small donations that we receive add up to make a significant difference for these children. With funds in Lao, we are often able to buy five to 10 times more food, or medicine or shoes than what we could in any western country.”</p>
				<p><a class="read-more" href="#">Read more about Support Lao Children.</a></p>
			</div>
			<div class="col col-xs-12 news-button">
				<a href="index.php?page=news"><button class="btn btn-lg btn-info">BACK TO NEWS PAGE</button></a>
			</div>
		</div>
	</div>
