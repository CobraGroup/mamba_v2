	<?php $here = DS.current(explode('/', substr($this->request->here, 1)));   ?>
<?php if(strpos($_SERVER['SERVER_NAME'], 'appcogroupusa.com')): ?>
		<?php
				$country = 'appcogroup-usa';
				$menuItems = array(

					'1' => array('link' => '/about-us', 'text' =>  'About us', 'parent' => null ),
					'2' => array('link' => '/face-to-face-marketing', 'text' => 'Face-to-face marketing',  'parent' => null ),
					'3' => array('link' => '/our-expertise', 'text' => 'Our expertise',  'parent' => null ),
					'4' => array('link' => '/news', 'text' => 'News',  'parent' => null ),
					'5' => array('link' => '/appco-worldwide', 'text' => 'Appco worldwide',  'parent' => null ),
				    '6' =>array('link' => '/contact-us', 'text' => 'Contact us',  'parent' => null ),
				    '7' =>array('link' => '/clients', 'text' => 'Clients',  'parent' => null )
				);

			?>
		<?php elseif(strpos($_SERVER['SERVER_NAME'], '.uk')) : ?>

		<?php
				$country = 'appcogroup-uk';
				$menuItems = array(

					'1' => array('link' => '/about-us', 'text' =>  'About us', 'parent' => null ),
					'6' => array('link' => '/field-marketing', 'text' => 'Field marketing',  'parent' => null ),
// 					'7' => array('link' => '/our-expertise', 'text' => 'Our expertise',  'parent' => null ),
// 					'8' => array('link' => '/our-people/board-of-directors', 'text' => 'Our people',  'parent' => null ),
					'9' => array('link' => '/news', 'text' => 'News',  'parent' => null ),
// 					'10' => array('link' => '/appco-worldwide', 'text' => 'Appco worldwide',  'parent' => null ),
				    '11' =>array('link' => '/blog/', 'text' => 'Blog',  'parent' => null ),
// 				    '12' =>array('link' => '/clients', 'text' => 'Clients',  'parent' => null ),
					'13' =>array('link' => '/contact-us', 'text' => 'Contact us',  'parent' => null ),
					'14' =>array('link' => '/appco-uk-frequently-asked-questions', 'text' => 'FAQs',  'parent' => null , 'position' => 'last'  )
				);


			?>
			<?php endif; ?>
			<?php

			$new_array = array();
			foreach ($menuItems as $key => $item) {

				if (($item['parent'] != NULL) && ($item['parent'] != '')) {
					$new_array[$item['parent']]['Children'][$item['parent']][$key] = $item;

				} else {
			        $new_array[$key] = $item;
			    }
			}
		?>


		<div>
			<nav class="mobile-clearfix"  id="nav-mobile">
				<div class="mobile-home <?= $country ?>"><?= $this->Html->link(__('Home'), '/', ['class' => $here == '/' ? 'active' : null]) ?></div>
				<div class="mobile-menu"><span><a id="close-menu" class="close-button" href="#"></a></span><span><a id="pull" class="static-menu" href="#"></a></span></div>

				<ul class="nav ">

					<li class="deeper home <?= $country ?>"><?= $this->Html->link(__('Home'), '/', ['class' => $here == '/' ? 'active' : null]) ?></li>
						<?php
							foreach($new_array as $id => $menuitem):
								$attributes = array();
								$last = false;

								if(isset($menuitem['position']))
									$last = $menuitem['position'];
								if(substr_count($here, $menuitem['link'])){
									$attributes['class'] = 'active';
								}
						?>

						<li class="deeper <?= $last; ?> " ><?= $this->Html->link(__($menuitem['text']), $menuitem['link'], $attributes) ?>
						<?php if ((isset($menuitem['Children']) && $menuitem['Children'] != NULL) && ($menuitem['Children'] != '')) { ?>
									<ul class="submenu ">
										<?php foreach($menuitem['Children'][$id] as $subid => $subitem): ?>
										<?php
											$sublast = false;
											if(isset($subitem['position']))
												$sublast = $subitem['position'];

											if(isset($subitem['id'])) :
												$attributes['id'] = $subitem['id'];
											else :
												$attributes['id'] = '';
											endif;
									?>
											<li class="subdeeper <?= $sublast; ?>"><?= $this->Html->link(__($subitem['text']), $subitem['link'], $attributes) ?></li>
										<?php endforeach; ?>
									</ul>
									<?php } ?>
						<?php   endforeach; ?>
						</li>
				</ul>
			</nav>
		</div>

        <div class="clearfix"></div>