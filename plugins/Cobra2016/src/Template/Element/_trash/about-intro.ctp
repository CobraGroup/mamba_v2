<div class="cobra-intro-wrapper">
	<div class="container cobra-about">
		<div class="row row-eq-height">
			<div class="col col-md-6 cobra-about-text">
				<p><strong>Cobra Group was founded by Chris Niarchos, in Sydney, in 1988 with the aim of building one of the world’s leading direct sales and marketing companies.</strong></p>
				<p>By the early 1990s the business had grown significantly and had expanded into more than a dozen countries. By 2000, Chris was well on his way to achieving his original objective.</p>
				<p>Over the next decade, the company continued to expand and, by 2011, Cobra Group was established in over 20 countries, becoming the only truly global direct sales and marketing organisation. It was also recognised by its growing client base for its excellence in face-to-face customer acquisition.</p>
				<p>In 2011, all sales and marketing businesses were re-branded under Appco Group and, today, Appco remains a global leader in the industry.</p>
				<p>In 2015, Appco is on target to deliver over US$4bn in customer revenue for its commercial clients and will raise more than US$35 million every month for its charity partners.</p>
				<p>Alongside the tremendous growth and success of his direct sales business, Chris decided to diversify Cobra into various other businesses, which has resulted in what is The Cobra Group of Companies today.</p>
			</div>
			<div class="col col-md-6 cobra-about-text">
				<p>The Cobra Group of Companies is an entrepreneurial group of diversified companies, which specialises in incubating, developing and managing an exciting portfolio of businesses, all with a common theme of opening up a world of opportunity to the people who are working in the Group, or who are associated with one of our group companies.</p>
				<p>Our group businesses cover the following industries:</p>
				<ul>
					<li><h2>fundraising services</h2></li>
					<li><h2>aviation recruitment and training</h2></li>
					<li><h2>management consultancy</h2></li>
					<li><h2>home efficiency</h2></li>
					<li><h2>health and beauty</h2></li>
					<li><h2>home services</h2></li>
					<li><h2>insurance</h2></li>
					<li><h2>investments </h2></li>
					<li><h2>race-car manufacturing</h2></li>
					<li><h2>software development</h2></li>
				</ul>
				<p>Our company ethos is “Be something more”, which means striving for excellence every day.</p>
			</div>
    	</div>
    </div>
</div>
