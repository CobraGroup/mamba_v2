<?php //pr($block); ?>
<ul class="gallery">
	<?php foreach($block['gallery'] as $gallery):?>
     <li data-order="<?= $gallery['order']; ?>">
     	<?php
	     	if(!isset($gallery['class'])) {
		     	$gallery['class'] = '';
	     	}
	     	if(!isset($gallery['title'])) {
		     	$gallery['title'] = '';
	     	}
	     	if(!isset($gallery['caption'])) {
		     	$gallery['caption'] = '';
	     	}
	     ?>
	     <div class="<?= $gallery['class']; ?>">
	     	<?php
		     	$image = $this->Html->image($gallery['image'], ['alt' => $gallery['alternative_text']]);

		     	if($gallery['link']) {
			     	//echo $this->Html->link($gallery['title'], $gallery['link'], ['escape' => false ]);
			     	echo $this->Html->link($image, $gallery['link'], ['escape' => false ]);
		     	} else {
			     	echo $image;
		     	}
		     ?>
		     <?php if(isset($gallery['caption'])) { ?>
		     <a href="<?= $gallery['link']?>"><div class="imageInfo">
			     <div class="imageTitle"><?= $gallery['title'] ?></div>
			     <div class="imageCaption"><?= $gallery['caption']?></div>
		     </div>
		     </a>
		     <?php } ?>
	     </div>
    </li>
	<?php endforeach; ?>
</ul>
<div class="clear"></div>
