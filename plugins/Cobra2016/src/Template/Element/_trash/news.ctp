	<div class="cobra-news-bg hidden-xs hidden-sm"></div>
	<div class="container cobra-news">
		<div class="row">
			<div class="col col-xs-12">
				<h1>Cobra Group <span>latest news</span></h1>
			</div>
		</div>
		<?php
		for($i=1;$i<=5;$i++){
		?>
		<div class="row row-eq-height news-item">
			<div class="col col-md-4 news-image">
				<a href="index.php?page=news-detail"><img src="/img/cobra2016/news/featured-1.jpg"></a>
			</div>
			<div class="col col-md-8 news-summary">
				<a href="index.php?page=news-detail">
					<p class="news-date">June 13th</p>
					<h2>Cobra Group continues to help Support Lao Children provide vital care to orphaned children</h2>
					<p>The medical, nutritional and educational support being provided to orphans by Support Lao Children has been stepped up a level thanks to the charity’s regular donors, including Cobra Group’s Cobra Group Foundation.</p>
				</a>
			</div>
		</div>
		<?php
		}
		?>
		<nav>
			<ul class="pagination">
<!-- 			<li>
			  <a href="#" aria-label="Previous">
			    <span aria-hidden="true">&laquo;</span>
			  </a>
			</li> -->
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li>
			  <a href="#" aria-label="Next">
			    <span aria-hidden="true">&raquo;</span>
			  </a>
			</li>
			</ul>
		</nav>
	</div>
