<?php
$here = $this->request->here;

if(substr($here,0,1)=="/") $here = substr($here, 1);
if(substr($here,-1,1)=="/") $here = substr($here, 0, -1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Cobra Group">
    <meta name="author" content="">
    <link rel="icon" href="/cobragroup-favicon.ico">

    <title>Cobra Group</title>
    <link href="/css/cobra2016/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/css/cobra2016/bootstrap/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="/css/cobra2016/style.css" rel="stylesheet">
</head>

<body>

  	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="/img/cobra2016/cobra-group-logo.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li<?php echo ($here=="about-us") ? ' class="active"' : '' ?>><a href="/about-us/">ABOUT US</a></li>
            <li<?php echo ($here=="news") ? ' class="active"' : '' ?>><a href="/cobra-group-news/">NEWS</a></li>
            <li<?php echo ($here=="directors") ? ' class="active"' : '' ?>><a href="/directors/">DIRECTORS</a></li>
            <li<?php echo ($here=="contact-us") ? ' class="active"' : '' ?>><a href="/contact-us/">CONTACT US</a></li>
          </ul>
        </div>
      </div>
    </nav>


<?php echo $this->fetch('content', ['here' => $here]) ?>


    <div class="footer hidden-xs hidden-sm">
    	<div class="container">
    		<div class="row row-eq-height">
    			<div class="col col-md-3 text-center col-border-right cobra-group-logo">
    				<img src="/img/cobra2016/cobra-group-logo.png">
    			</div>
    			<div class="col col-md-3 hidden-xs hidden-sm col-border-right footer-menu">
    				<ul>
    					<li><h5><a href="/about-us">ABOUT US</a></h4></li>
    					<li><h5><a href="/news">NEWS</a></h4></li>
    					<li><h5><a href="/directors">DIRECTORS</a></h4></li>
    					<li><h5><a href="/contact-us">CONTACT US</a></h4></li>
    				</ul>
    			</div>
    			<div class="col col-md-3 col-border-right footer-contact">
	    			<h4>CONTACT US</h4>
    				<p>
						Robert Gibbs<br>
						Director of Business Development<br>
						+44 (0) 20 7424 3700<br>
						<a href="mailto:rgibbs@cobragroup.com">rgibbs@cobragroup.com</a><br>
					</p>
    			</div>
    			<div class="col col-md-3 footer-social">
    				<h4>FOLLOW US</h4>
    				<p>
	    				<a href="https://www.facebook.com/pages/Cobra-Group-of-Companies/196092030426933" target="_blank"><img src="/img/cobra2016/cobra-facebook.png"></a>
	    				<a href="https://www.linkedin.com/company/the-cobra-group-of-companies" target="_blank"><img src="/img/cobra2016/cobra-linkedin.png"></a>
	    				<a href="https://twitter.com/cobragroup" target="_blank"><img src="/img/cobra2016/cobra-twitter.png"></a>
    				</p>
    			</div>
    		</div>
    	</div>

	    <div class="footer-address hidden-xs hidden-sm">
	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-12 text-center">
	    				<p>Cobra Group International (Hong Kong) Limited: Level 27 Worldwide House, 19 Des Voeux Road, Central, Hong Kong. Registration number: HK F18858.</p>
	    			</div>
	    		</div>
	    	</div>
	    </div>
    </div>

    <div class="mobile-footer-social hidden-md hidden-lg">
    	<div class="container">
    		<div class="row">
    			<div class="col-xs-12 text-center">
    				<h4>FOLLOW US</h4> 
    				<a href="https://www.facebook.com/pages/Cobra-Group-of-Companies/196092030426933" target="_blank"><img src="/img/cobra2016/cobra-facebook.png"></a>
    				<a href="https://www.linkedin.com/company/the-cobra-group-of-companies" target="_blank"><img src="/img/cobra2016/cobra-linkedin.png"></a>
    				<a href="https://twitter.com/cobragroup" target="_blank"><img src="/img/cobra2016/cobra-twitter.png"></a>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="mobile-footer-cobra-group-logo hidden-md hidden-lg">
    	<div class="container">
    		<div class="row">
    			<div class="col-xs-12 text-center">
    				<img src="/img/cobra2016/cobra-group-logo.png">
    			</div>
    		</div>
    	</div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/cobra2016/jquery/jquery-3.0.0.min.js"><\/script>')</script>
<script src="/js/cobra2016/bootstrap/bootstrap.min.js"></script>
<script src="/js/cobra2016/bootstrap/ie10-viewport-bug-workaround.js"></script>
<script src="/js/cobra2016/scripts.js"></script>
</body>
</html>

