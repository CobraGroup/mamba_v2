<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}
?>

	<?php
	echo $this->element("home-intro", ["is_home" => true]);
	?>

	<div class="container cobra-description">
		<div class="row">
			<div class="col col-md-6 hidden-xs hidden-sm">
				<div class="cobra-branches">
					<?php echo $this->fetch('intro-left'); ?>
				</div>
			</div>
			<div class="col col-xs-12 col-md-6">
				<div class="cobra-history">
					<?php echo $this->fetch('intro-right'); ?>
				</div>
				<div class="horizontal-coloured-bar"><img src="/img/cobra2016/coloured-bar.jpg"></div>
			</div>
		</div>
	</div>

	<div class="home-cobra-news">
		<div class="container">
			<div class="row">
				<div class="col col-header col-xs-12 text-center">
					<h3 class="news-header-title">COBRA GROUP <span>NEWS</span></h3>
				</div>
			</div>
			<div class="row row-eq-height">
				<div class="col col-news-item col-news-item-blog col-md-6 col-xs-12">
					<a href="<?php echo $cnblogposts[0]->link; ?>" target="_blank">
						<img src="/img/cobra2016/blog-label.gif" class="blog-label">
						<img src="<?php echo $cnblogposts[0]->image; ?>" class="news-image">
						<div class="news-text">
							<div class="news-date"><?php echo substr($cnblogposts[0]->pubDate, 0, 16); ?> | <span><?php echo $cnblogposts[0]->category; ?></span></div>
							<h2><?php echo $cnblogposts[0]->title; ?></h2>
							<div class="news-excerpt"><?php echo strip_tags($cnblogposts[0]->description); ?></div>
						</div>
					</a>
				</div>
				<div class="col col-news-item col-md-6 col-xs-12">
					<a href="<?php echo $companynews[0]->link; ?>">
						<img src="<?php echo $companynews[0]->image; ?>" class="news-image hidden-xs hidden-sm">
						<div class="news-text">
							<div class="news-date"><?php echo substr($companynews[0]->pubDate, 0, 16); ?> | <span><?php echo $companynews[0]->category; ?></span></div>
							<h2><?php echo $companynews[0]->title; ?></h2>
							<div class="news-excerpt"><?php echo $companynews[0]->description; ?></div>
						</div>
					</a>
				</div>
			</div>
			<div class="row row-eq-height">
<?php
for($i=1;$i<=3;$i++){
?>
				<div class="col col-news-item col-md-4 col-xs-12">
					<a href="<?php echo $companynews[$i]->link; ?>">
						<img src="<?php echo $companynews[$i]->image; ?>" class="news-image hidden-xs hidden-sm">
						<div class="news-text">
							<div class="news-date"><?php echo substr($companynews[$i]->pubDate, 0, 16); ?> | <span><?php echo $companynews[$i]->category; ?></span></div>
							<h2><?php echo $companynews[$i]->title; ?></h2>
							<div class="news-excerpt"><?php echo $companynews[$i]->description; ?></div>
						</div>
					</a>
				</div>
<?php
}
?>
			</div>
		</div>
	</div>


<?php
echo $this->element("home-contact-us");
?>
