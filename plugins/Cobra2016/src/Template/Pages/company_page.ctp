<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}

$here = $this->request->here;

if(substr($here,0,1)=="/") $here = substr($here, 1);
if(substr($here,-1,1)=="/") $here = substr($here, 0, -1);
?>
	<div class="company-details-wrapper company-details-wrapper-<?php echo $here ?>">
		<div class="company-details-background hidden-xs hidden-sm">
			<?php echo $this->fetch('background'); ?>
		</div>
		<div class="container company-details">
			<div class="row">
				<div class="col col-xs-12">
					<h1>The Cobra Group of Companies / <span><?php echo $page->title; ?></span></h1>
				</div>
			</div>
			<div class="row row-eq-height">
				<div class="col col-md-6 company-description">
					<div class="company-logo" style="background-image:url(/img/cobra2016/company-logos/<?php echo $here ?>-desktop.png);"></div>
					<?php echo $this->fetch('left-sidebar'); ?>
				</div>
				<?php
				if($this->fetch('right-sidebar')){
				?>
				<div class="col col-md-6 company-description company-description-right-col">
					<?php echo $this->fetch('right-sidebar'); ?>
				</div>
				<?php
				}
				?>
			</div>
			<?php
			if(count($data["statistics"]) && isset($a_variable_that_is_not_set)){
			?>
			<div class="row">
				<div class="col col-xs-12">
					<h2><?php echo $page->title; ?> <span>Statistics</span></h2>
				</div>
			</div>
			<div class="row row-eq-height">
				<?php
				$stat_count = 0;
				foreach($data["statistics"] as $stat_block){
					$stat_count++;
					?>
					<div class="col col-md-6 company-stats">
						<div class="row">
							<div class="col-xs-3 text-center">
								<span class="glyphicon glyphicon-stats"></span>
							</div>
							<div class="col-xs-9">
								<?php
								echo $this->element($stat_block['element'], ['block' => $stat_block]);
								?>
							</div>
						</div>
	
					</div>
					<?php
					if(bcmod($stat_count,2)==0){
						?>
			</div>
			<div class="row row-eq-height">
						<?php
					}
				}
				if(bcmod($stat_count,2)!=0) {
					?>
				<div class="col col-md-6 company-stats">&nbsp;</div>
					<?php
				}
				?>
			</div>
			<?php
			}
			?>
		</div>
	</div>

<?php
echo $this->element("home-intro", ["is_home" => false, "exclude_company" => $here]);
?>
