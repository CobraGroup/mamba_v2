<?php  //pr($data);
	foreach($data as $region => $blocks) {
		$this->start($region);
			foreach ($blocks as $block) {
				echo $this->element($block['element'], ['block' => $block]);
			}
		$this->end();
	}
?>

<div id="container">

    <div id="banner">
	    <div class="row-fluid <?= $block['style'] ?>">
	        <div class="span12">
	            <div class="bg-image">
		            <?php foreach($data['banner'] as $gallery):?>
	                	<?= $this->Html->image($gallery['image'], ['alt' => 'Appco group UK'])?>
	                <?php endforeach; ?>
		            <div class="imageInfo">
			            <div class="imageTitle">APPCO GROUP</div>
			            <div class="imageCaption">FIELD MARKETING AGENCY</div>
		            </div>
	            </div>
	        </div>
	    </div>
		<div class="clearfix"></div>
	</div>

    <div class="layout1" id="content">
		<div class="modal-body first-row ">
		  <div class="col-md-6 text-center row-left-intro" >
		    <p class="first-row-mobile">Appco Group UK is</p>
		    <div class="intro-square">
				<h1>Appco Group <span>UK is</span></h1>
		    </div>
		  </div>
		  <div class="col-md-6 text-center parallax-right-column">

			<div id="textCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		        <li data-target="#textCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#textCarousel" data-slide-to="1"></li>
		        <li data-target="#textCarousel" data-slide-to="2"></li>
		        <li data-target="#textCarousel" data-slide-to="3"></li>
		        <li data-target="#textCarousel" data-slide-to="4"></li>
		        <li data-target="#textCarousel" data-slide-to="5"></li>
		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
					<p>A professional&nbsp;face-to-face field marketing agency with almost 30 years of global experience acquiring customers and regular-giving donors on behalf of our clients.</p>
					<p>In partnership with the independent&nbsp;brand ambassadors in our network, we help our clients to connect in a personal way with their target markets and acquire thousands&nbsp;of customers and donors for them every year. We call this process the Human Commercial&trade;.</p>
		        </div>
		        <div class="item">
					<p>Our Human Commercial&trade; is more than &ldquo;just door-to-door sales&rdquo;. It is a highly professional and interactive method of speaking directly with our clients&rsquo; target markets in an engaging and informative way &ndash; and it works for individuals and our clients alike.</p>
					<p>Every month, in the UK alone, we deliver more than &pound;1 million in revenue for our many clients.</p>
		        </div>
		        <div class="item">
					<p>We consistently achieve real&nbsp;results for them by developing an in-depth understanding of their products and services and communicating tailored&nbsp;messages to their chosen audiences.</p>
					<p>Globally, this approach has seen us generate more than $5 billion every year in shareholder value for our clients around the world.</p>
					<p>For over 25 years, we have made it our mission to build long-lasting relationships between our clients and their customers and donors.</p>
		        </div>
		        <div class="item">
					<p>Communicating with thousands of people every day across the UK via our unique Human Commercial&trade;, our professional teams of self-employed field representatives continually strive to ensure our clients receive the highest level of service and support while helping to promote their brands.</p>
					<p>Our drive and enthusiasm in what we do has allowed Appco Group to become one of the world&rsquo;s most successful sales and marketing companies.</p>
		        </div>
		        <div class="item">
					<p>Based in our London head office, Appco Group UK has multiple teams dedicated to each sector, specialising in everything from energy, home efficiency, charity fundraising and home delivery to product development, sales support, account management and communications.</p>
					<p>To ensure we provide the best possible service for our commercial clients and charity partners, we are continually building on our knowledge and expertise.</p>
		        </div>
		        <div class="item">
					<p>We pride ourselves on our face-to-face marketing skills, outstanding customer service and broad industry knowledge, and our achievements have seen us work with some of the world&rsquo;s most well-known brands.</p>
					<p>What&rsquo;s more, we operate on a performance-based model, meaning our clients only pay for the results we achieve. Take a look around our website to find out more about our business.</p>
		        </div>
		      </div>
		    </div>
			
		  </div>
		</div>
		<div class="clearfix"></div>

		<!---  social --->
		<div class="modal-body social-row ">
			<h2 class="social-header">LATEST NEW<span>S</span></h2>
			<div class="social-row-wrapper">
				<div class="col-md-6 row-two-left-column">
					<?= $this->fetch('social-row') ?>
				</div>
				<div class="col-md-6 row-two-right-column">
					<a height="469" data-chrome="nofooter noborders" class="twitter-timeline" href="https://twitter.com/appcogroup" data-widget-id="688026263815098369">Tweets by @appcogroup</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
		<div class="layout1 row second-row">
			<div class="row-two-left-column">
				<p class="second-row-mobile">WHAT WE DO</p>
				<div class="intro-square"><h2>What <span>we do</span></h2></div>
			</div>
			<div class="row-two-right-column">
			    <ul class="base-row">
				    <?= $this->fetch('second-row') ?>
			    </ul>
			</div>

		</div>


</div>


