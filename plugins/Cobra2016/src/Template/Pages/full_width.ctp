<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}

$here = $this->request->here;

if(substr($here,0,1)=="/") $here = substr($here, 1);
if(substr($here,-1,1)=="/") $here = substr($here, 0, -1);

switch($here){
	
	case("directors"):
	case("contact-us"):
	
		echo $this->fetch("content", ['block' => $block]);
	
	break;
	
	case("/"):
	default:

		echo $this->element("home-intro", ["is_home" => true]);
		echo $this->element("home-cobra-description");
		echo $this->element("home-cobra-news");
		echo $this->element("home-contact-us");
	
	break;
	
}
