<?php
foreach($data as $region => $blocks) {
	$this->start($region);
		foreach ($blocks as $block) {
			echo $this->element($block['element'], ['block' => $block]);
		}
	$this->end();
}
?>

	<div class="cobra-intro-wrapper cobra-intro-about">
		<div class="container cobra-about">
			<div class="row row-eq-height">
				<div class="col col-md-6 cobra-about-text">
					<?php echo $this->fetch('intro-left'); ?>
				</div>
				<div class="col col-md-6 cobra-about-text">
					<?php echo $this->fetch('intro-right'); ?>
				</div>
	    	</div>
	    </div>
	</div>
	<div class="container cobra-foundation">
		<div class="row">
			<div class="col col-xs-12">
				<div class="foundation-description">
					<?php echo $this->fetch('foundation'); ?>
				</div>
			</div>
		</div>
	</div>

<?php
echo $this->element("home-intro", ["is_home" => false]);
?>
