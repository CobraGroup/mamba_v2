<h1><?= __d('news_manager', 'News'); ?></h1>

<ul>
<?php foreach ($articles as $article): ?>
	<li>
		<?= $this->Html->link($article->title, $lang.'/news/'.$article->slug) ?>
	</li>
<?php endforeach; ?>
</ul>