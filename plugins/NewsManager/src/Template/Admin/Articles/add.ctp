<h3 style=" color:#e7e7e7; background: none repeat scroll 0 0 padding-box #666666;border-radius: 4px;margin-bottom: 20px;padding: 14px;">Create new Article</h3>

<?= $this->Form->create($article) ?>
<div class="row">
	<div class="col-lg-8">
			<div class="panel-body">

			<div class="page-block" style="float: left;padding-bottom: 20px;width: 100%;">
				<?= $this->Form->input('title') ?>
				<?= $this->Form->input('slug') ?>
				<?= $this->Form->input('description'); ?>

			</div>
			<div class="page-block" style="float: left;padding-bottom: 20px;width: 100%;">
				<?= $this->Form->input('body', ['rows' => '3']) ?>
				<?= $this->Form->input('external_link') ?>
				<?= $this->Form->input('link_title') ?>
			</div>
		</div>

	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('publish_date', [
					    'type'	=> 'text',
					    'class' => 'date-picker'
					])
				 ?>
				<?= $this->Form->input('status', array('options' => array_combine($status,$status))) ?>
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">SEO Details</div>
			<div class="panel-body">
				<?= $this->Form->input('meta_title') ?>
				<?= $this->Form->input('meta_description', ['rows' => '3']) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>

