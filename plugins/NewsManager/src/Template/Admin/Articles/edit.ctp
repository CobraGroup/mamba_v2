
<?= $this->Form->create($article) ?>
<div class="row">
	<div class="col-lg-8">
		<div class="panel panel-info">
		<div class="panel-heading">Edit Article</div>
			<div class="panel-body">
				<?= $this->Form->input('title') ?>
				<?= $this->Form->input('slug') ?>
				<?= $this->Form->input('description') ?>
				<?= $this->Form->input('body', ['rows' => '3']) ?>
				<?= $this->Form->input('external_link') ?>
				<?= $this->Form->input('link_title') ?>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-info">
			<div class="panel-heading">Publish</div>
			<div class="panel-body">
				<?= $this->Form->input('publish_date', [
					    'type'	=> 'text',
					    'class' => 'date-picker'
					])
				 ?>
				<?= $this->Form->input('status', array('options' => array_combine($status,$status))) ?>
				<?= $this->Form->button(__('Save')) ?>
			</div>
		</div>
		<div class="panel panel-info">
			<div class="panel-heading">SEO</div>
			<div class="panel-body">
				<?= $this->Form->input('meta_title') ?>
				<?= $this->Form->input('meta_description', ['rows' => '3']) ?>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>
<!--

<div class="panel panel-primary">...</div>
<div class="panel panel-success">...</div>
<div class="panel panel-info">...</div>
<div class="panel panel-warning">...</div>
<div class="panel panel-danger">...</div>
-->