<h1><?= __d('news_manager', 'News'); ?> articles</h1>
<p><?= $this->Html->link('Add Article', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-striped">
			<thead>
		    <tr>
		        <th>Title</th>
		        <th>Created</th>
		        <th colspan="2">Toolbar</th>

		    </tr>
			</thead>
		    <tbody>
		    <?php foreach ($articles as $article): ?>
		    <tr>
		        <td>
		            <?= $this->Html->link($article->title, ['action' => 'edit', $article->id]) ?>
		        </td>
		        <td>
		            <?= $article->created->format(DATE_RFC850) ?>
		        </td>
		        <td>
		            <?= $this->Html->link('Edit', ['action' => 'edit', $article->id], ['class' => 'btn btn-xs btn-primary']) ?>
		        </td>
		        <td>
		            <?= $this->Form->postLink(
		                'Delete',
		                ['action' => 'delete', $article->id],
		                ['confirm' => 'Are you sure?', 'class' => 'btn btn-xs btn-danger'])
		            ?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		    </tbody>
		</table>
	</div>
</div>
<p><?= $this->Html->link('Add Article', ['action' => 'add'], ['class' => 'btn btn-xs btn-primary']) ?></p>
