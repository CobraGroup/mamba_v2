<?php

namespace NewsManager\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticlesTable extends Table {

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
        $this->addBehavior('Translate', ['fields' => ['title', 'slug', 'body', 'description', 'meta_title', 'meta_description', 'meta_description','external_link', 'link_title']]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('title')
            ->notEmpty('slug')
            ->notEmpty('body');

        return $validator;
    }

}
