<?php

namespace NewsManager\Controller;

use NewsManager\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

class ArticlesController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'view']);
    }

    public function index() {
        $this->set('articles', $this->Articles->find('all'));

    }

    public function view() {
        if (!isset($this->request->params['slug'])) {
            throw new NotFoundException(__('Invalid article'));
        }

    	//$article = TableRegistry::get('Articles');
        //$data = $article->find()->where(['slug' => $this->request->params['slug']])->first();
        $article = $this->Articles->find()->where(['slug' => $this->request->params['slug']])->first();
        $this->set('article', $article);

       // pr(I18n::getDefault());
        //$this->Articles->test();

    }

}