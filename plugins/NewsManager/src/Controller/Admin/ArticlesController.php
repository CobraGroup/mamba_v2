<?php

namespace NewsManager\Controller\Admin;

use NewsManager\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ArticlesController extends AppController {

	public $helpers = [
		'Form' => [
			'templates' => 'bootstrap_form.php', // location: /config
		]
	];
	
    public function index() {
        $articles = $this->Articles->find('all');
        $this->set(compact('articles'));
    }

    public function add() {
        $article = $this->Articles->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your article.'));
        }
 	    $this->set('status', array('Published','Draft','Trash'));
 		$this->set('article', $article);
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid article'));
	    }
	    $article = $this->Articles->get($id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->Articles->patchEntity($article, $this->request->data);
	        if ($this->Articles->save($article)) {
	            $this->Flash->success(__('Your article has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your article.'));
	    }
	    $this->set('status', array('Published','Draft','Trash'));
	    $this->set('article', $article);
	}

	public function delete($id) {
	    $this->request->allowMethod(['post', 'delete']);
	    $article = $this->Articles->get($id);
	    if ($this->Articles->delete($article)) {
	        $this->Flash->success(__('The article with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

}