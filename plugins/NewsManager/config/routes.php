<?php
use Cake\Routing\Router;


/*
Router::connect('/news', ['plugin' => 'NewsManager', 'controller' => 'Articles', 'action' => 'index']);
Router::connect('/news/:slug', ['plugin' => 'NewsManager', 'controller' => 'Articles', 'action' => 'view']);

#Router::connect('/:lang/news', ['plugin' => 'NewsManager', 'controller' => 'Articles', 'action' => 'index'], ['_name' => 'articlesIndex', 'routeClass' => 'LangRoute']);

$langs = array('en','es');
foreach($langs as $lang) {
	Router::connect(DS.$lang.'/news', ['plugin' => 'NewsManager', 'controller' => 'Articles', 'action' => 'index', 'prefix' => null, 'lang' => $lang]);
	Router::connect(DS.$lang.'/news/:slug', ['plugin' => 'NewsManager', 'controller' => 'Articles', 'action' => 'view', 'prefix' => null, 'lang' => $lang]);
}

Router::prefix('admin', function($routes) {
	$routes->connect('/news', ['plugin' => 'NewsManager', 'controller' => 'Articles', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/news/:action', ['plugin' => 'NewsManager', 'controller' => 'Articles'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/news/:action/*', ['plugin' => 'NewsManager', 'controller' => 'Articles'], ['routeClass' => 'InflectedRoute']);
});
*/


Router::plugin('NewsManager', function($routes) {
#Router::plugin('/news', ['plugin' => 'NewsManager'], function($routes) {

/*
	$routes->connect('/', ['controller' => 'Articles', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/:slug', ['controller' => 'Articles', 'action' => 'view'], ['routeClass' => 'InflectedRoute']);
	
	$routes->prefix('admin', function($routes) {
		$routes->connect('/', ['controller'=> 'Articles', 'action' => 'index'], ['routeClass' => 'InflectedRoute']);
		$routes->connect('/:action', ['controller'=> 'Articles'], ['routeClass' => 'InflectedRoute']);
		$routes->connect('/:action/*', ['controller'=> 'Articles'], ['routeClass' => 'InflectedRoute']);
	});
*/
	
	$routes->fallbacks();
});