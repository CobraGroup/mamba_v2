CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dummy data
INSERT INTO `articles` (`title`, `slug`, `body`, `image`, `meta_title`, `meta_description`, `other_meta_tags`, `created`, `modified`)
VALUES
	('The title', 'the-title', 'This is the article body.', NULL, NULL, NULL, NULL, NOW(), NULL),
	('A title once again', 'title-one-again', 'And the article body follows.', NULL, NULL, NULL, NULL, NOW(), NULL),
	('Title strikes back', 'title-strikes-back', 'This is really exciting! Not.', NULL, NULL, NULL, NULL, NOW(), NULL);
