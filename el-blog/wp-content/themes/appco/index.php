<?php get_header(); ?>



<br />
<br />

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="post" id="post-<?php the_ID(); ?>">

	<div class="meta"><?php the_time('l, F jS, Y'); ?></div>

	<!-- TITLE -->
	<h2 class="storytitle">
		<a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
	</h2>
	<!-- / TITLE -->


	<!-- CONTENT -->
	<div class="storycontent"><?php the_excerpt(); ?></div>
	<!-- /CONTENT -->

	<div class="alignright readmore"><a href="<?php the_permalink() ?>" rel="bookmark">Leer más</a></div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>





<?php endwhile; else: ?>
	<h3><?php _e('Sorry, no posts matched your criteria.'); ?></h3>
<?php endif; ?>
<?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>




<?php get_footer(); ?>
