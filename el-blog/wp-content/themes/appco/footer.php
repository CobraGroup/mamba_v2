



					</div>
					<div class="wp-right">
						<?php get_sidebar(); ?>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>
		<div id="footer">
		    <div class="outter">
		        <div>
		            <div class="left">
		                <div class="logos">
		                    <ul>
		                        <li><a target="_blank" href="http://www.appcogroup.com/"><img itemprop="image" alt="Appco group - Field Marketing  Agency" src="/img/appco/appco_logo_footer.png"></a></li>
			            				                        </ul>
		                </div>
		            </div>
		            <div class="right">
		                	                   		                    <style type="text/css">
			                    #footer .right .logos li {
			                    	margin-left:0 !important;
								}
								@media (max-width: 768px) {
									.logos {
										margin-top: 20px;
									}
								}
								@media (max-width: 360px) {
									.logos {
										margin-top: 20px;
									}
								}
		                    </style>
							 <ul class="logos">
		               			<li style="margin: 0;">
		            			<a target="_blank" href="http://www.aefundraising.org/quienes/agencias.html"><img alt="Asociación Española de Fundraising" src="/img/appco/aefr.png"></a>                                </li>
		               			<li style="margin: 0;">
		            			<a target="_blank" href="http://www.apd.es/asociados"><img alt="APD" src="/img/appco/apd.png"></a>                                </li>
		                        <li style="margin: 0;">
		            			<a target="_blank" href="http://www.britishchamberspain.com/appco-group-espana.html"><img alt="BCCS" src="/img/appco/bccs.png"></a>                                </li>
		                        <li style="margin: 0;">
		            			<a target="_blank" href="http://www.corresponsables.com/"><img alt="corresponsables" src="/img/appco/corresponsables.png"></a>                                </li>
		                        <li style="margin: 0;">
		            			<a target="_blank" href="http://www.asociacionmkt.es/socios/appco-group-espana/"><img alt="MKT" src="/img/appco/mkt.png"></a>                                </li>
		                        <li style="margin: 0;">
		            			<a target="_blank" href="http://www.pimec.org/"><img alt="Pimec" src="/img/appco/pimec.png"></a>                                </li>
		            		</ul>
		                 	                     	                    	                    	                     	                </div>
		        </div>
		    </div>
		    <div class="grey">
		        <div class="outter">
		            <div class="copyright">
			        	<div class="footer-company-logo">
				        	<img alt="Cobra Group Company" src="/img/appco/cobra-group-company.png">			            				                            <div style="margin-left:15px;float:right;" class="besomething"><a target="_blank" href="https://www.linkedin.com/company/be-somethingmore"><img alt="Be something more with Appco" src="/img/appco/besomethingmore.png"></a>			            		</div>
			            						        	</div>
			        	<div class="footer-company-links">
							<ul>
				                									<li><a href="/sobre-appco">Sobre Appco</a></li>
									<li><a href="/que-hacemos">Qué hacemos</a></li>
									<li><a href="/appco-en-el-mundo">Appco en el mundo</a></li>
									<li><a href="/clientes">Clientes</a></li>
											                </ul>
			        	</div>
		            </div>
		            <div class="rights">
					   					    <p>Appco Group &copy; all rights reserved 2016 </p>


		            </div>
		        </div>
		     </div>
		</div>
	</div>


</body>
</html>


