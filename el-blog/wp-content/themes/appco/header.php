
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>Appco Group España | Casa</title>
	<link href="/favicon.ico" type="image/x-icon" rel="icon"/><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>



	<meta name="title" content="Appco Group España | Home">
	<meta name="description" content="Appco Group es la compañía líder en marketing face-to-face. Estamos presentes en 27 países desarrollando campañas de marketing directo.">

	<meta name="p:domain_verify" content="b75ea90b87e805da036b52d32e6d2d46"/>






	<link rel="stylesheet" href="/webroot/css/el-blog/reset.css" defer="defer"/>
	<link rel="stylesheet" href="/webroot/css/el-blog/all.css" defer="defer"/>
	<link rel="stylesheet" href="/webroot/css/el-blog/cookiecuttr.css" defer="defer"/>
	<link rel="stylesheet" href="/webroot/css/el-blog/blog.css" defer="defer"/>
	<link rel="stylesheet" href="/webroot/css/el-blog/custom.css" defer="defer"/>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="/js/jquery.cookie.js" defer="defer"></script>
	<script src="/js/jquery.cookiecuttr.js" defer="defer"></script>
	<script src="/js/appco.js" defer="defer"></script>

<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55741431-1', 'auto');
  ga('send', 'pageview');
</script>

       			<script type="text/javascript">
				$(document).ready(function() {
					//call cookie pocily
					$.cookieCuttr({
						cookieCutter: true,
						cookieAnalytics: false,
						cookiePolicyLink: '/politica-de-cookies',
					    cookieDeclineButton: false,
					    cookieMessage: 'Esta página web utiliza cookies,  <a href="{{cookiePolicyLink}}" title="click here">haga clic aquí para leer más sobre ellas</a>. Para usar la pagina web correctamente por favor acepte las cookies.',
					    cookieAcceptButtonText: ' Aceptar cookies',
					    cookiePolicyPageMessage: 'You can change the message here',
					    cookieResetButton: false
					});
				});
			</script>





</head>
<body class="blog">

	<div  id="wrapper" style="height: auto;">

		<div id="header">
	        <div class="logo-container">
	            <a rel="alternate" hreflang="x-default" href="/" class="logo-left" title="Appco Group">
	                <img alt="" src="/img/appco/appco-group-spain.png">	            </a>

	            <div class="social_icons">
								            <!-- Go to www.addthis.com/dashboard to customize your tools -->
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a2dce77bf1c151" async="async"></script>

		            	<ul>

			            	<li></li>


			            				            			<li>
									<a target="_blank" href="https://www.facebook.com/Appco-Group-Espa%C3%B1a-1438293629785516/"><img class="eiycgmdoqkquuellrbth" alt="Appco Spain Facebook" src="/img/appco/fb-es.png"></a>									</li>
									<li>
			            			<a target="_blank" href="https://twitter.com/appco_es"><img class="eiycgmdoqkquuellrbth" alt="Appco UK Twitter" src="/img/appco/tw.png"></a>									</li>




    	            	</ul>
	            </div>
					        </div>


		        <nav class="mobile-clearfix nav">
		         <div class="mobile-nav"><!-- <a href="/field-marketing" class="static-menu">Field marketing</a><a href="/news" class="static-menu">| News</a> --><a class="static-menu" id="pull" href="#"></a></div>
				<ul class="mobile-clearfix">
					<li class="home"><a class="active" href="/">Home</a></li>
										<li><a href="/sobre-appco">Sobre Appco</a></li>
										<li><a href="/que-hacemos">Qué hacemos</a></li>
										<li><a href="/quienes-somos">Quiénes somos</a></li>
										<li><a href="/noticias">Noticias</a></li>
										<li><a href="/el-blog">El blog</a></li>
										<li><a href="/appco-en-el-mundo">Appco en el mundo</a></li>
										<li><a href="/clientes">Clientes</a></li>
										<li><a href="/contacto">Contáctenos</a></li>
										<li><a href="/preguntas-frecuentes">Preguntas frecuentes</a></li>
									</ul>
			</nav>
	        <div class="clearfix"></div>

	    </div>
	<div id="container">
		<div id="banner">
			<div class="left">
				<img src="/uploads/2015/03/appco-complete-fundraising-solutions.jpg" alt=""/>
			</div>
			<div class="right">
				<img src="/uploads/2015/05/map.jpg" alt=""/>
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="row layout4" id="content">

			<div class="wp-left">

			<h1>Blog</h1>






