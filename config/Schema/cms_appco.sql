# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: cms_appco
# Generation Time: 2014-11-05 16:01:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT '',
  `original_filename` varchar(255) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `alternative_text` varchar(255) DEFAULT NULL,
  `content_type` varchar(100) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table block_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `block_types`;

CREATE TABLE `block_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `form` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `block_types` WRITE;
/*!40000 ALTER TABLE `block_types` DISABLE KEYS */;

INSERT INTO `block_types` (`id`, `name`, `description`, `form`)
VALUES
	(1,'Header','Title for view','header'),
	(2,'Image','Upload Assests','image'),
	(3,'Video','Add YouTube or Vimeo code','video'),
	(4,'Content','Add text','content'),
	(5,'HTML','HTML code','code'),
	(6,'News','List articles','news'),
	(7,'Banner','Image','banner'),
	(8,'Intro','Homepage introduction','intro'),
	(9,'Posts','select posts by category','posts');

/*!40000 ALTER TABLE `block_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `block_type_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `data` text,
  `link` varchar(255) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;

INSERT INTO `blocks` (`id`, `page_id`, `block_type_id`, `name`, `data`, `link`, `region`, `style`, `rank`, `active`, `created`, `modified`)
VALUES
	(1,1,1,'Hey Mani','a:1:{s:3:\"tag\";s:2:\"h1\";}',NULL,'banner','',0,1,'2014-10-29 14:02:25','2014-10-29 22:06:27'),
	(2,1,1,'does it really matter','a:1:{s:3:\"tag\";s:2:\"h2\";}',NULL,'left-column','',0,1,'2014-10-29 14:05:06','2014-10-29 22:05:32'),
	(3,1,2,'kids','a:1:{s:4:\"file\";a:5:{s:4:\"name\";s:0:\"\";s:4:\"type\";s:0:\"\";s:8:\"tmp_name\";s:0:\"\";s:5:\"error\";i:4;s:4:\"size\";i:0;}}',NULL,'right-column','',0,1,'2014-10-29 14:46:14','2014-10-29 21:42:20'),
	(4,1,6,'News','a:2:{s:5:\"limit\";s:1:\"5\";s:5:\"order\";s:4:\"DESC\";}',NULL,'news-section','',0,1,'2014-10-29 18:06:59','2014-10-29 19:09:46'),
	(5,18,7,'Banner','a:4:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:5:\"image\";s:31:\"/uploads/2014/10/contact-us.png\";s:8:\"captions\";s:5:\"bulbs\";}',NULL,'banner','',0,1,'2014-10-31 14:15:53','2014-11-05 10:37:03'),
	(6,18,8,'homepage introduction','a:6:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:3:\"tag\";s:2:\"h1\";s:5:\"title\";s:19:\"Welcome to Appco...\";s:7:\"content\";s:269:\"The industry leader in quality face-to-face marketing. Every day, across the world, we speak to more than one million people, which helps us to create a connection with our clients’ target markets and secure hundreds of thousands of new customers for them every year.\";s:7:\"element\";s:5:\"intro\";}',NULL,'left-column','',0,1,'2014-10-31 14:21:39','2014-10-31 14:29:44'),
	(13,18,6,'news section','a:5:{s:5:\"Model\";s:4:\"News\";s:7:\"element\";s:4:\"news\";s:5:\"limit\";s:1:\"5\";s:5:\"order\";s:3:\"ASC\";s:5:\"style\";s:0:\"\";}',NULL,'news-section','',0,1,'2014-11-03 11:47:22','2014-11-03 11:47:22'),
	(15,18,9,'Industry Expertise','a:5:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:4:\"post\";s:8:\"category\";s:1:\"1\";}',NULL,'center-column','',0,1,'2014-11-03 14:02:37','2014-11-04 14:10:24'),
	(16,18,9,'what we do','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"5\";s:5:\"posts\";a:4:{i:1;s:1:\"1\";i:3;s:1:\"1\";i:5;s:1:\"1\";i:7;s:1:\"1\";}s:5:\"style\";s:0:\"\";}',NULL,'what-we-do-section','',0,1,'2014-11-05 15:07:55','2014-11-05 15:07:55');

/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) DEFAULT 'en',
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `locale`, `name`, `slug`)
VALUES
	(1,'en','Industry Expertise','industry-expertise'),
	(2,'en','Board of Directors','board-of-directors'),
	(3,'en','Senior Executives','senior-executives'),
	(4,'en','Independent Contractors','independent-contractors'),
	(5,'en','What we do','what-we-do');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `webiste_id` int(11) unsigned NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `link_count` int(10) DEFAULT '0',
  `rank` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) DEFAULT 'en',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` varchar(20) DEFAULT NULL,
  `revised` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `locale`, `title`, `slug`, `body`, `image`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `revised`, `created`, `modified`)
VALUES
	(1,'en','The title','/news/the-title','Appco Group UK and its field sales representatives have dramatically increased sales for organic food supplier Riverford, signing up more customers in the first 10 months of 2014 than in the whole of 2013.\r\n\r\n“We have all been impressed with the volume and quality of customers acquired by Appco and the Field Representatives (FRs),” said Riverford Field Development Manager Ben Speed.\r\n\r\n“The FRs have shown integrity, professionalism and politeness in their approach to field sales, with great effect. Likewise, Appco has worked extensively to understand our business. All this has resulted in one of Riverford’s most successful drives to acquire new customers in recent years.”\r\n\r\nRiverford organic farms deliver fresh fruit and vegetable boxes straight to customers’ doors.','/uploads/2014/11/telecommunications100x100.jpg','','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-11-03 13:02:19'),
	(2,'en','A title once again','/news/title-one-again','And the article body follows.','/uploads/2014/11/telecommunications100x100.jpg','','',NULL,'Draft',NULL,'2014-09-22 12:26:17','2014-10-23 10:13:21'),
	(3,'en','Title strikes back','/news/title-strikes-back','This is really exciting! Not.','/uploads/2014/11/telecommunications100x100.jpg','','',NULL,'Unpublished',NULL,'2014-09-23 12:26:17','2014-10-31 11:12:55'),
	(4,'en_GB','test','/news/test','test','/uploads/2014/11/telecommunications100x100.jpg','','',NULL,'Published',NULL,'2014-10-23 10:14:30','2014-10-23 10:14:30'),
	(5,'en','test two','/news/test-two','test2','/uploads/2014/11/telecommunications100x100.jpg','','',NULL,'Published',NULL,'2014-11-03 11:17:22','2014-11-03 11:17:44');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `type` char(10) DEFAULT 'Page',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` enum('Published','Draft','Trash') DEFAULT 'Draft',
  `template` varchar(255) DEFAULT NULL,
  `revised` text,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `rank` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `website_id`, `locale`, `type`, `title`, `slug`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `template`, `revised`, `parent_id`, `lft`, `rght`, `rank`, `created`, `modified`)
VALUES
	(1,1,'en','Page','Home','/','Cobra Group of Companies','The Cobra Group of Companies started selling consumables more than 25 years ago with the ambition of building one of the world\'s largest and most successful sal',NULL,'Published','front-page',NULL,NULL,NULL,NULL,0,'2014-09-20 14:20:37','2014-10-29 11:01:45'),
	(2,1,'en','Page','About','/about','','',NULL,'Published','',NULL,NULL,NULL,NULL,1,'2014-10-22 11:32:51','2014-10-27 15:12:17'),
	(3,1,'en_GB','Page','Home','/','','',NULL,'Published','',NULL,NULL,NULL,NULL,NULL,'2014-10-22 11:47:12','2014-10-23 10:04:29'),
	(4,1,'en_GB','Page','About','/about','','',NULL,'Published','jkhkjhk',NULL,NULL,NULL,NULL,NULL,'2014-10-22 14:01:45','2014-10-23 10:04:41'),
	(15,1,'en','Page','Team','/team','','',NULL,'Published','',NULL,2,NULL,NULL,2,'2014-10-22 16:25:05','2014-10-27 15:12:17'),
	(16,1,'en_AU','Page','Home','/','Cobra Group of Companies','The Cobra Group of Companies started selling consumables more than 25 years ago with the ambition of building one of the world\'s largest and most successful sal',NULL,'Published','',NULL,NULL,NULL,NULL,NULL,'2014-09-20 14:20:37','2014-10-23 10:26:14'),
	(17,1,'en','Page','Contact','/contact','','',NULL,'Published','',NULL,NULL,NULL,NULL,3,'2014-10-23 11:27:43','2014-10-27 15:12:17'),
	(18,2,'en','Page','Home','/','','',NULL,'Published','front-page',NULL,NULL,1,NULL,0,'2014-10-30 14:58:10','2014-10-31 16:44:49');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `locale` varchar(10) DEFAULT 'en',
  `title` varchar(50) DEFAULT NULL,
  `menu_label` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` varchar(20) DEFAULT NULL,
  `revised` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `category_id`, `locale`, `title`, `menu_label`, `slug`, `slug_title`, `body`, `image`, `thumb`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `revised`, `created`, `modified`)
VALUES
	(1,1,'en','What we do','Industry Expertise','/industry-expertise/what-we-do',NULL,'<h3>Face-to-face marketing</h3>\r\n<p>We have developed a comprehensive results-driven sales campaign framework that is proven time and time again to be the most cost-effective channel in the marketing mix. Our teams of professional independent contractors receive ongoing support and product coaching. They also have regular contact with our clients, ensuring they are always at the heart of the businesses and charities they represent.</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg',NULL,'','',NULL,'Unpublished',NULL,'2014-09-23 12:26:17','2014-11-05 11:18:52'),
	(2,1,'en','Charities',NULL,'/industry-expertise/charities',NULL,'<p>\r\nAppco Group Support has rebranded to\r\n<a href=\"http://www.globalfundraisingservices.com\">AGS Global Fundraising Services</a>\r\nto better fulfil the complex and varied fundraising requirements of our charity partners.\r\n</p>\r\n<p>Over almost 20 years, the relationships we have built with our charity partners have allowed us to expand on the range of services we offer and we have rebranded to reflect our broader offer.</p>\r\n<p>As Appco Group Support we became the largest face-to-face donor recruitment agency in the world. As AGS Global Fundraising Services we will continue to provide the same standard of excellence in all of the services for which you are currently partnered with us.</p>\r\n<p>AGS Global Fundraising Services will also:</p>\r\n<ul>\r\n<li>\r\ndraw on our global expertise and resources, not only in face-to-face donor acquisition but also in fundraising solutions such as\r\n<a href=\"http://www.globalfundraisingservices.com/our-services/donor-management-services/\">donor management</a>\r\n,\r\n<a href=\"http://www.globalfundraisingservices.com/our-services/contact-centre-services/\">contact centre services</a>\r\nand\r\n<a href=\"http://www.globalfundraisingservices.com/our-services/consultancy-services/\">consultancy services</a>\r\n</li>\r\n<li>provide our clients with a bespoke, seamless package of services to meet all of their fundraising and donor management needs.</li>\r\n</ul>\r\n<p>\r\nPlease take a look at our new\r\n<a href=\"http://www.globalfundraisingservices.com/\">AGS Global Fundraising Services website</a>\r\nto find out more.\r\n</p>\r\n<p>\r\nVisit the \r\n<a href=\"http://www.appcogroup.com/appco-worldwide\">Appco Worldwide</a>\r\n page to get in touch with the Appco team in your country.\r\n</p>\r\n<p></p>','/uploads/2014/11/ec17d2d08e4f6c6df057a692724d2f28fbaf2335.jpg',NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-11-05 11:34:08'),
	(4,1,'en','Home efficiency',NULL,'/industry-expertise/home-efficiency',NULL,'<p>Governments and industries right across the globe are encouraging people to use energy more efficiently within their homes, which not only helps them save on their energy bills, but also reduces overall CO2 emissions.</p>\r\n\r\n<p>Appco Group\'s Home Efficiency division is driving change in this market by offering consumers a range of products and services – including solar panels, LED light solutions and home Insulation – that can help them achieve a more energy-efficient household.</p>\r\n\r\n<p>The division is a market leader in its investment in the provision of quality, comprehensive sales training and development programs for contracted sales teams, ensuring they are technically informed and the best possible brand ambassadors for our clients.</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg',NULL,'','',NULL,'Published',NULL,'2014-10-23 10:14:30','2014-11-05 11:03:10'),
	(8,1,'en','Energy',NULL,'/industry-expertise/energy',NULL,'<p>The Energy division is one of the foundations of Appco Group.</p>\r\n<p>As many governments have deregulated their utilities sectors, Appco has played a critical role in providing major energy companies with unrivalled customer acquisition volumes.</p>\r\n<p>Over 15 years, Appco Group has acquired more than 10 million new customers in 11 countries and has added billions of dollars in value for our energy clients.</p>\r\n<p>Appco specialises in face-to-face customer acquisition in the energy sector and offers a range of go-to-market channels, including residential door-to-door, business-to-business, in-store, shopping centres and events.</p>\r\n<p>Appco is also an industry leader in terms of quality and has comprehensive sales training and development programmes, a state-of-the-art straight-through processing solution, as well as our unique sales integrity programme, which includes a third party verification/validation service direct to the client at point of sale.</p>\r\n<p>\r\nVisit our\r\n<a href=\"/appco-worldwide\">Appco Worldwide</a>\r\npage to get in touch with a specialist in your local country.\r\n</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg',NULL,'','',NULL,'Published',NULL,'2014-11-04 14:12:31','2014-11-05 11:27:08'),
	(9,1,'en','Sports marketing & lotteries',NULL,'/industry-expertise/sports-marketing--lotteries',NULL,'<p></p>\r\n<p>Appco Group Sports started trading in 2000 as Allsports Direct (Australia) Pty Ltd and has earned a reputation as a professional, cost-effective means for sporting organisations and not for profits to achieve their revenue-raising objectives while raising their brand awareness.</p>\r\n<p>Appco Group Sports provides its clients with the resources to promote and sell their products in existing and new target markets via six main avenues:</p>\r\n<ol>\r\n<li>Major Lottery/Raffle Program</li>\r\n<li>Match Day Lottery/Raffle Program</li>\r\n<li>Scratch & Win Program</li>\r\n<li>Pin & Win Program</li>\r\n<li>Fundraising Voucher Program</li>\r\n<li>Membership Program</li>\r\n</ol>\r\n<p>\r\n<br>\r\nAppco Group Sports Australia represents sporting organisations across a range of sporting codes, including cricket, football, rugby league, rugby union, Australian rules football, basketball, netball and charitable sporting groups.\r\n</p>\r\n<p>The Australian Paralympic Committee was able to send the biggest Paralympic teams ever to the Beijing and Vancouver games due to the support of Appco Group Sports and the monies raised through the lottery/Pin & Win programs.</p>\r\n<p>The relationship between Appco Group Sports Australia and the Surf Life Saving Foundation in Australia has now extended to more than eight years and has been a fantastic relationship for both parties..</p>\r\n<p></p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg',NULL,'','',NULL,'Unpublished',NULL,'2014-11-04 14:22:02','2014-11-05 11:19:09'),
	(10,5,'en','Complete fundraising services','','what-we-do',NULL,'Complete fundraising services\r\n','/uploads/2014/11/b5b2cdd1a79598a7d4120f2972762e31c809b7db.png',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:26:50','2014-11-05 14:26:50'),
	(11,5,'en','Face-to-face marketing','','what-we-do',NULL,'Face-to-face marketing ','/uploads/2014/11/5af5aaf666da4d56c04269dccce452a4806d2c27.png',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:27:30','2014-11-05 14:27:30'),
	(12,5,'en','Door-to-door sales & services','','what-we-do',NULL,'Door-to-door sales & services','/uploads/2014/11/ae57613993fe09c2d85484b1b41ab1f92ea02f2a.jpg',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:30:00','2014-11-05 14:30:00'),
	(13,5,'en','Customer acquisition','','what-we-do',NULL,'Customer acquisition','/uploads/2014/11/afc22d978046058bbe549c8f3326b88c2a6858cd.png',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:30:28','2014-11-05 14:30:28');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `home_page_id` int(11) unsigned NOT NULL,
  `site_title` varchar(100) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `news_posts_per_page` int(10) NOT NULL DEFAULT '10',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `website_id`, `home_page_id`, `site_title`, `logo`, `news_posts_per_page`, `created`, `modified`)
VALUES
	(1,1,1,'',NULL,10,'2014-10-30 09:48:57','2014-10-30 09:51:12'),
	(2,2,18,'local.appco.group','appco/bg_logo_en.png',5,'2014-10-31 14:38:17','2014-10-31 14:38:17');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
