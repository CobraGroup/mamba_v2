# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: cms_cobra
# Generation Time: 2014-10-31 13:40:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT '',
  `original_filename` varchar(255) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `alternative_text` varchar(255) DEFAULT NULL,
  `content_type` varchar(100) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table block_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `block_types`;

CREATE TABLE `block_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `form` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `block_types` WRITE;
/*!40000 ALTER TABLE `block_types` DISABLE KEYS */;

INSERT INTO `block_types` (`id`, `name`, `description`, `form`, `created`, `modified`)
VALUES
	(1,'Header','Title for view','header','2014-10-23 14:48:16','2014-10-23 14:48:16'),
	(2,'Image','Upload Assests','image','2014-10-23 14:48:40','2014-10-23 14:48:40'),
	(3,'Video','Add YouTube or Vimeo code','video','2014-10-29 17:24:09','2014-10-29 17:24:09'),
	(4,'Content','Add text','content','2014-10-29 17:32:19','2014-10-29 17:32:19'),
	(5,'HTML','HTML code','code','2014-10-29 17:33:22','2014-10-29 17:33:22'),
	(6,'News','List articles','news','2014-10-29 17:55:28','2014-10-29 17:55:28'),
	(7,'Banner','Image','banner','2014-10-30 12:09:34','2014-10-30 12:09:34');

/*!40000 ALTER TABLE `block_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `block_type_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `data` text,
  `link` varchar(255) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;

INSERT INTO `blocks` (`id`, `page_id`, `block_type_id`, `name`, `data`, `link`, `region`, `style`, `rank`, `active`, `created`, `modified`)
VALUES
	(1,1,1,'Hey Mani','a:1:{s:3:\"tag\";s:2:\"h1\";}',NULL,'banner','',0,1,'2014-10-29 14:02:25','2014-10-29 22:06:27'),
	(2,1,1,'does it really matter','a:1:{s:3:\"tag\";s:2:\"h2\";}',NULL,'left-column','',0,1,'2014-10-29 14:05:06','2014-10-29 22:05:32'),
	(3,1,2,'kids','a:1:{s:4:\"file\";a:5:{s:4:\"name\";s:0:\"\";s:4:\"type\";s:0:\"\";s:8:\"tmp_name\";s:0:\"\";s:5:\"error\";i:4;s:4:\"size\";i:0;}}',NULL,'right-column','',0,1,'2014-10-29 14:46:14','2014-10-29 21:42:20'),
	(4,1,6,'News','a:2:{s:5:\"limit\";s:1:\"5\";s:5:\"order\";s:4:\"DESC\";}',NULL,'news-section','',0,1,'2014-10-29 18:06:59','2014-10-29 19:09:46');

/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carousels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carousels`;

CREATE TABLE `carousels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT '',
  `rank` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`)
VALUES
	(1,'Industry Expertise'),
	(2,'Board of Directors'),
	(3,'Senior Executives'),
	(4,'Independent Contractors');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `webiste_id` int(11) unsigned NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `link_count` int(10) DEFAULT '0',
  `rank` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) DEFAULT 'en',
  `title` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` varchar(20) DEFAULT NULL,
  `revised` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `locale`, `title`, `slug`, `body`, `image`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `revised`, `created`, `modified`)
VALUES
	(1,'en','The title','/news/the-title','This is the article body.',NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-10-31 11:28:58'),
	(2,'en','A title once again','/news/title-one-again','And the article body follows.',NULL,'','',NULL,'Draft',NULL,'2014-09-22 12:26:17','2014-10-23 10:13:21'),
	(3,'en','Title strikes back','/news/title-strikes-back','This is really exciting! Not.',NULL,'','',NULL,'Unpublished',NULL,'2014-09-23 12:26:17','2014-10-31 11:12:55'),
	(4,'en_GB','test','/news/test','test',NULL,'','',NULL,'Published',NULL,'2014-10-23 10:14:30','2014-10-23 10:14:30');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `type` char(10) DEFAULT 'Page',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` enum('Published','Draft','Trash') DEFAULT 'Draft',
  `template` varchar(255) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT '0',
  `revised` text,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `rank` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `website_id`, `locale`, `type`, `title`, `slug`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `template`, `weight`, `revised`, `parent_id`, `rank`, `created`, `modified`)
VALUES
	(1,1,'en','Page','Home','/','Cobra Group of Companies','The Cobra Group of Companies started selling consumables more than 25 years ago with the ambition of building one of the world\'s largest and most successful sal',NULL,'Published','front-page',0,NULL,NULL,0,'2014-09-20 14:20:37','2014-10-29 11:01:45'),
	(2,1,'en','Page','About','/about','','',NULL,'Published','full-width',0,NULL,NULL,1,'2014-10-22 11:32:51','2014-10-27 15:12:17'),
	(3,1,'en_GB','Page','Home','/','','',NULL,'Published','full-width',0,NULL,NULL,NULL,'2014-10-22 11:47:12','2014-10-23 10:04:29'),
	(4,1,'en_GB','Page','About','/about','','',NULL,'Published','full-width',0,NULL,NULL,NULL,'2014-10-22 14:01:45','2014-10-23 10:04:41'),
	(15,1,'en','Page','Team','/team','','',NULL,'Published','full-width',0,NULL,2,2,'2014-10-22 16:25:05','2014-10-27 15:12:17'),
	(16,1,'en_AU','Page','Home','/','Cobra Group of Companies','The Cobra Group of Companies started selling consumables more than 25 years ago with the ambition of building one of the world\'s largest and most successful sal',NULL,'Published','full-width',0,NULL,NULL,NULL,'2014-09-20 14:20:37','2014-10-23 10:26:14'),
	(17,1,'en','Page','Contact','/contact','','',NULL,'Published','full-width',0,NULL,NULL,3,'2014-10-23 11:27:43','2014-10-27 15:12:17');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned DEFAULT NULL,
  `locale` varchar(10) DEFAULT 'en',
  `title` varchar(50) DEFAULT NULL,
  `menu_label` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` varchar(20) DEFAULT NULL,
  `revised` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `website_id`, `category_id`, `locale`, `title`, `menu_label`, `slug`, `body`, `image`, `thumb`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `revised`, `created`, `modified`)
VALUES
	(1,1,1,'en','What we do','Industry Expertise','what-we-do','<h3>Face-to-face marketing</h3>\n<p>We have developed a comprehensive results-driven sales campaign framework that is proven time and time again to be the most cost-effective channel in the marketing mix. Our teams of professional independent contractors receive ongoing support and product coaching. They also have regular contact with our clients, ensuring they are always at the heart of the businesses and charities they represent.</p>',NULL,NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-10-23 10:13:00'),
	(2,1,1,'en','Charities',NULL,'charities','<p>Appco Group’s charities division, Appco Group Support, started in Australia in 1997, and is now the largest face-to-face donor recruitment agency in the world.</p>\n<p>Appco Group Support Australia understands the importance of providing our charity partners with long-term, quality donors.</p>\n<p>Between 2000 and 2013, <strong>we have acquired more than 1.2 million donors, who have collectively donated more than AU$800 million</strong>.</p>\n<p>Our fundraisers engage every individual in a conversation about the charity they are representing, and our expert approach, insistence on quality and passion for the causes we represent continually turns otherwise unreachable people into loyal donors.</p>\n<p>Each and every month, Appco Group Support Australia continues to raise millions of dollars in donations for our partners, which is testament to our fundraisers and the responsiveness the public continues to have to this form of fundraising.</p>\n<p>Our focus on attracting committed, quality donors, has seen us become the first-choice donor recruitment agency for non-profit organisations.</p>',NULL,NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-10-23 10:13:21'),
	(3,1,1,'en','Energy',NULL,'energy','<p>The Energy division has been one of the foundations of Appco Group.</p>\r\n\r\n<p>As many governments have deregulated their utilities sectors, Appco has been a key player by providing major energy companies with unrivalled customer acquisition volumes.</p>\r\n\r\n<p>Appco Group has acquired more than 10 million new customers over 15 years in 11 countries and has added billions of dollars in value for our clients.</p>\r\n\r\n<p>Appco specialises in face-to-face new customer acquisition in the energy sector and offers a range of channels of communication, including residential door-to-door, business-to-business, in-store, call centre, shopping centres and events.</p>\r\n\r\n<p>Appco is also an industry leader in terms of quality and has comprehensive sales training and development programs, a state-of-the-art straight-through processing solution, as well as our unique sales integrity program which includes a third party verification/validation direct to the client at point of sale.</p>\r\n',NULL,NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-10-23 10:13:26'),
	(4,1,1,'en','Home efficiency',NULL,'home-efficiency','<p>Governments and industries right across the globe are encouraging people to use energy more efficiently within their homes, which not only helps them save on their energy bills, but also reduces overall CO2 emissions.</p>\n\n<p>Appco Group\'s Home Efficiency division is driving change in this market by offering consumers a range of products and services – including solar panels, LED light solutions and home Insulation – that can help them achieve a more energy-efficient household.</p>\n\n<p>The division is a market leader in its investment in the provision of quality, comprehensive sales training and development programs for contracted sales teams, ensuring they are technically informed and the best possible brand ambassadors for our clients.</p>',NULL,NULL,'','',NULL,'Unpublished',NULL,'2014-10-23 10:14:30','2014-10-23 10:14:30');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `home_page_id` int(11) unsigned NOT NULL,
  `site_title` varchar(100) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `news_posts_per_page` int(10) NOT NULL DEFAULT '10',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `website_id`, `home_page_id`, `site_title`, `logo`, `news_posts_per_page`, `created`, `modified`)
VALUES
	(1,1,1,'',NULL,10,'2014-10-30 09:48:57','2014-10-30 09:51:12');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
