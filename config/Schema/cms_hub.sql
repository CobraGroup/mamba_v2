# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: cms_hub
# Generation Time: 2014-10-31 13:40:40 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `alternative_text` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT '',
  `type` char(20) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;

INSERT INTO `assets` (`id`, `name`, `original_name`, `alternative_text`, `path`, `type`, `published`, `created`, `modified`)
VALUES
	(3,'pics.jpg','pics.jpg','kids','/uploads/2014/10','image/jpeg',1,'2014-10-28 16:19:42','2014-10-28 17:38:05'),
	(4,'home.jpg','home.jpg','','/uploads/2014/10','image/jpeg',1,'2014-10-30 13:55:09','2014-10-30 13:55:09'),
	(5,'abouts-us.png','abouts-us.png','','/uploads/2014/10','image/png',1,'2014-10-30 13:55:25','2014-10-30 14:02:52'),
	(6,'contact-us.png','contact-us.png','','/uploads/2014/10','image/png',1,'2014-10-30 14:07:45','2014-10-30 14:07:45'),
	(7,'door-selling.png','door-selling.png','','/uploads/2014/10','image/png',1,'2014-10-30 14:08:00','2014-10-30 14:08:00'),
	(8,'our-services.png','our-services.png','','/uploads/2014/10','image/png',1,'2014-10-30 14:08:18','2014-10-30 14:08:18'),
	(9,'our-team.png','our-team.png','','/uploads/2014/10','image/png',1,'2014-10-30 14:08:45','2014-10-30 14:08:45');

/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assets_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assets_tags`;

CREATE TABLE `assets_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `assets_tags` WRITE;
/*!40000 ALTER TABLE `assets_tags` DISABLE KEYS */;

INSERT INTO `assets_tags` (`id`, `asset_id`, `tag_id`)
VALUES
	(3,3,2),
	(4,4,3),
	(5,5,3),
	(6,6,3),
	(7,7,3),
	(8,8,3),
	(9,9,3);

/*!40000 ALTER TABLE `assets_tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `theme` varchar(50) DEFAULT NULL,
  `screenshot` varchar(100) DEFAULT 'screenshot.png',
  `datasource` varchar(50) NOT NULL,
  `templates` text,
  `rank` tinyint(4) DEFAULT '0',
  `websites_count` int(10) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;

INSERT INTO `companies` (`id`, `name`, `theme`, `screenshot`, `datasource`, `templates`, `rank`, `websites_count`, `created`, `modified`)
VALUES
	(1,'Cobra','Cobra','screenshots/cgc.png','cobra','{\n	\"front-page\": {\n		\"name\": \"Front Page\",\n		\"template\": \"front-page\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"left-column\": \"Left Column\",\n			\"right-column\": \"Right Column\",\n			\"news-section\": \"News Section\"\n		}\n	},\n	\"full-width\": {\n		\"name\": \"Full Width\",\n		\"template\": \"full-width\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"content\": \"Content\"\n		}\n	},\n	\"left-sidebar\": {\n		\"name\": \"Left Sidebar\",\n		\"template\": \"left-sidebar\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"content\": \"Content\",\n			\"left-sidebar\": \"Left Sidebar\"\n		}\n	},\n	\"right-sidebar\": {\n		\"name\": \"Right Sidebar\",\n		\"template\": \"right-sidebar\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"content\": \"Content\",\n			\"right-sidebar\": \"Right Sidebar\"\n		}\n	},\n	\"three-column\" : {\n		\"name\": \"Three Column\",\n		\"template\" : \"three-column\",\n		\"regions\" : {\n			\"col1\": \"Column 1\",\n			\"col2\": \"Column 2\",\n			\"col3\": \"Column 3\"\n		}\n	},\n	\"four-column\": {\n		\"name\": \"Four Column\",\n		\"template\": \"four-column\",\n		\"regions\": {\n			\"col1\": \"Column 1\",\n			\"col2\": \"Column 2\",\n			\"col3\": \"Column 3\",\n			\"col4\": \"Column 4\"\n		}\n	}\n}',0,0,'2014-09-30 15:00:52','2014-09-30 15:01:00'),
	(2,'Appco','Appco','screenshots/appco.png','appco','{\n	\"front-page\": {\n		\"name\": \"Front Page\",\n		\"template\": \"front-page\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"left-column\": \"Left Column\",\n			\"right-column\": \"Right Column\",\n			\"news-section\": \"News Section\"\n		}\n	},\n	\"full-width\": {\n		\"name\": \"Full Width\",\n		\"template\": \"full-width\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"content\": \"Content\"\n		}\n	},\n	\"left-sidebar\": {\n		\"name\": \"Left Sidebar\",\n		\"template\": \"left-sidebar\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"content\": \"Content\",\n			\"left-sidebar\": \"Left Sidebar\"\n		}\n	},\n	\"right-sidebar\": {\n		\"name\": \"Right Sidebar\",\n		\"template\": \"right-sidebar\",\n		\"regions\": {\n			\"banner\": \"Banner\",\n			\"content\": \"Content\",\n			\"right-sidebar\": \"Right Sidebar\"\n		}\n	},\n	\"three-column\" : {\n		\"name\": \"Three Column\",\n		\"template\" : \"three-column\",\n		\"regions\" : {\n			\"col1\": \"Column 1\",\n			\"col2\": \"Column 2\",\n			\"col3\": \"Column 3\"\n		}\n	},\n	\"four-column\": {\n		\"name\": \"Four Column\",\n		\"template\": \"four-column\",\n		\"regions\": {\n			\"col1\": \"Column 1\",\n			\"col2\": \"Column 2\",\n			\"col3\": \"Column 3\",\n			\"col4\": \"Column 4\"\n		}\n	}\n}',1,0,'2014-10-02 12:43:28','2014-10-02 12:43:28');

/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table domains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domains`;

CREATE TABLE `domains` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `domains` WRITE;
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;

INSERT INTO `domains` (`id`, `website_id`, `name`, `created`, `modified`)
VALUES
	(1,1,'local.cgc.app','2014-10-06 17:51:20','2014-10-06 17:51:20'),
	(2,2,'local.appco.group','2014-10-06 17:52:14','2014-10-06 17:52:14');

/*!40000 ALTER TABLE `domains` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `locale` varchar(5) DEFAULT NULL,
  `slug` varchar(5) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `locale` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `name`, `country`, `locale`, `slug`, `active`)
VALUES
	(1,'International','International','en',NULL,1),
	(2,'English (UK)','United Kingdom','en_GB','gb',1),
	(3,'Spanish','Spain','es','es',1),
	(4,'Italian','Italy','it','it',1),
	(5,'French','France','fr','fr',0),
	(6,'Russian','Russia','ru','ru',0),
	(7,'German','German','gr','gr',0),
	(8,'Arabic',NULL,'ar','ar',0),
	(9,'Bulgarian','Bulgaria','bg','bg',0),
	(10,'Catalan','Spain','ca','ca',0),
	(11,'Chinese','China','zh','zh',0),
	(12,'Croatian','Croatia','hr','hr',0),
	(13,'Czech','Czech Republic','cs','cs',0),
	(14,'Danish','Danish','ds','ds',0),
	(15,'Dutch','Netherlands','nl','nl',0),
	(16,'English','United States','en_US','us',1),
	(17,'English','South Africa','en_ZA','za',0),
	(18,'English','Singapore','en_SG','sg',0),
	(19,'English','Philippines','en_PH','ph',0),
	(20,'English','New Zealand','en_NZ','nz',0),
	(21,'English','Malta','en_MT','en-mt',0),
	(22,'English','Ireland','en_IE','ie',0),
	(23,'English','India','en_IN','en-in',0),
	(24,'English','Canada','en_CA','en-ca',0),
	(25,'English','Australia','en_AU','au',0),
	(26,'Estonian','Estonia','et','et',0),
	(27,'Finnish','Finland','fi','fi',0),
	(28,'Greek','Greece','el','el',0),
	(29,'Hebrew','Isreal','iw','iw',0),
	(30,'Hindi','India','hi_IN','hi',0),
	(31,'Hungarian','Hungary','hu','hu',0),
	(32,'Icelandic','Iceland','is','is',0),
	(33,'Indonesian','Indonesia','in','in',0),
	(34,'Japanese','Japan','ja','ja',0),
	(35,'Korean','Korea','ko','ko',0),
	(36,'Latvian','Latvia','lv','lv',0),
	(37,'Lithuanian','Lithuania','lt','lt',0),
	(38,'Macedonian','Macedonia','mk','mk',0),
	(39,'Malay','Malaysia','ms','ms',0),
	(40,'Maltese','Malta','mt','mt',0),
	(41,'Norwegian','Norway','no','no',0),
	(42,'Polish','Poland','pl','pl',0),
	(43,'Portuguese','Prtugal','pt','pt',0),
	(44,'Romanian','Romania','ro','ro',0),
	(45,'Serbian','Serbia','sr','sr',0),
	(46,'Slovak','Slovakia','sk','sk',0),
	(47,'Slovenian','Slovenia','sl','sl',0),
	(48,'Swedish','Sweden','sv','sv',0),
	(49,'Thai','Thailand','th','th',0),
	(50,'Turkish','Turkey','tr','tr',0),
	(51,'Ukrainian','Ukraine','uk','uk',0),
	(52,'Vietnamese','Vietnam','vi','vi',0);

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`id`, `name`)
VALUES
	(1,'Cobra'),
	(2,'Appco'),
	(3,'Banner');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` char(10) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `username`, `password`, `role`, `blocked`, `last_login`, `created`, `modified`)
VALUES
	(1,'Mani','mani@cobracreative.co.uk','$2y$10$GjE3Z03MOyy0kpLo/P1IIu6xN99nNvQsgsDQKY/0dWJ1kDcY1tMWm','Admin',0,'2014-10-09 12:16:00','2014-09-24 14:06:09','2014-09-24 14:06:09');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table websites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `websites`;

CREATE TABLE `websites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `theme` varchar(50) NOT NULL DEFAULT '',
  `analytics` text,
  `live` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `websites` WRITE;
/*!40000 ALTER TABLE `websites` DISABLE KEYS */;

INSERT INTO `websites` (`id`, `company_id`, `country_id`, `name`, `theme`, `analytics`, `live`, `created`, `modified`)
VALUES
	(1,1,1,'Cobra Group Consultancy','Cobra','',1,'2014-09-30 15:00:52','2014-10-27 10:27:51'),
	(2,2,1,'Appco Group','Appco','',1,'2014-10-02 12:43:28','2014-10-02 12:43:28'),
	(3,1,3,'Spain','','',1,'2014-10-27 10:28:36','2014-10-27 10:28:36');

/*!40000 ALTER TABLE `websites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table websites_languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `websites_languages`;

CREATE TABLE `websites_languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `websites_languages` WRITE;
/*!40000 ALTER TABLE `websites_languages` DISABLE KEYS */;

INSERT INTO `websites_languages` (`id`, `website_id`, `language_id`)
VALUES
	(13,1,1),
	(14,3,3);

/*!40000 ALTER TABLE `websites_languages` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
