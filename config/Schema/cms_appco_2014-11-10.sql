# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: cms_appco
# Generation Time: 2014-11-10 18:12:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT '',
  `original_filename` varchar(255) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `alternative_text` varchar(255) DEFAULT NULL,
  `content_type` varchar(100) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table block_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `block_types`;

CREATE TABLE `block_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `form` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `block_types` WRITE;
/*!40000 ALTER TABLE `block_types` DISABLE KEYS */;

INSERT INTO `block_types` (`id`, `name`, `description`, `form`)
VALUES
	(1,'Header','Title for view','header'),
	(2,'Image','Upload Assests','image'),
	(3,'Video','Add YouTube or Vimeo code','video'),
	(4,'Content','Add text','content'),
	(5,'HTML','HTML code','code'),
	(6,'News','List articles','news'),
	(7,'Banner','Image','banner'),
	(8,'Intro','Homepage introduction','intro'),
	(9,'Posts','select posts by category','posts'),
	(10,'Gallery','Select images for the gallery','gallery');

/*!40000 ALTER TABLE `block_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `block_type_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `data` text,
  `link` varchar(255) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;

INSERT INTO `blocks` (`id`, `page_id`, `block_type_id`, `name`, `data`, `link`, `region`, `style`, `rank`, `active`, `created`, `modified`)
VALUES
	(1,1,1,'Hey Mani','a:1:{s:3:\"tag\";s:2:\"h1\";}',NULL,'banner','',0,1,'2014-10-29 14:02:25','2014-10-29 22:06:27'),
	(2,1,1,'does it really matter','a:1:{s:3:\"tag\";s:2:\"h2\";}',NULL,'left-column','',0,1,'2014-10-29 14:05:06','2014-10-29 22:05:32'),
	(3,1,2,'kids','a:1:{s:4:\"file\";a:5:{s:4:\"name\";s:0:\"\";s:4:\"type\";s:0:\"\";s:8:\"tmp_name\";s:0:\"\";s:5:\"error\";i:4;s:4:\"size\";i:0;}}',NULL,'right-column','',0,1,'2014-10-29 14:46:14','2014-10-29 21:42:20'),
	(4,1,6,'News','a:2:{s:5:\"limit\";s:1:\"5\";s:5:\"order\";s:4:\"DESC\";}',NULL,'news-section','',0,1,'2014-10-29 18:06:59','2014-10-29 19:09:46'),
	(5,18,7,'Banner','a:4:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:5:\"image\";s:31:\"/uploads/2014/10/contact-us.png\";s:8:\"captions\";s:5:\"bulbs\";}',NULL,'banner','',0,1,'2014-10-31 14:15:53','2014-11-05 10:37:03'),
	(6,18,8,'homepage introduction','a:6:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:3:\"tag\";s:2:\"h1\";s:5:\"title\";s:19:\"Welcome to Appco...\";s:7:\"content\";s:269:\"The industry leader in quality face-to-face marketing. Every day, across the world, we speak to more than one million people, which helps us to create a connection with our clients’ target markets and secure hundreds of thousands of new customers for them every year.\";s:7:\"element\";s:5:\"intro\";}',NULL,'left-column','',0,1,'2014-10-31 14:21:39','2014-10-31 14:29:44'),
	(13,18,6,'news section','a:5:{s:5:\"Model\";s:4:\"News\";s:7:\"element\";s:4:\"news\";s:5:\"limit\";s:1:\"5\";s:5:\"order\";s:3:\"ASC\";s:5:\"style\";s:0:\"\";}',NULL,'news-section','',0,1,'2014-11-03 11:47:22','2014-11-03 11:47:22'),
	(15,18,9,'Industry Expertise','a:5:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:4:\"post\";s:8:\"category\";s:1:\"1\";}',NULL,'center-column','',0,1,'2014-11-03 14:02:37','2014-11-04 14:10:24'),
	(16,18,9,'what we do','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"5\";s:5:\"posts\";a:4:{i:1;s:1:\"1\";i:3;s:1:\"1\";i:5;s:1:\"1\";i:7;s:1:\"1\";}s:5:\"style\";s:0:\"\";}',NULL,'what-we-do-section','',0,1,'2014-11-05 15:07:55','2014-11-05 15:07:55'),
	(18,19,7,'banner','a:3:{s:5:\"image\";s:43:\"/uploads/2014/11/london-thames-about-us.png\";s:8:\"captions\";s:9:\"about us \";s:5:\"style\";s:0:\"\";}',NULL,'banner','',0,1,'2014-11-06 13:47:43','2014-11-06 13:47:43'),
	(19,19,9,'Industry Expertise','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"1\";s:5:\"posts\";a:4:{i:1;s:1:\"1\";i:4;s:1:\"1\";i:6;s:1:\"1\";i:8;s:1:\"1\";}s:5:\"style\";s:0:\"\";}',NULL,'right-column','',0,1,'2014-11-06 13:55:44','2014-11-06 13:55:44'),
	(20,21,7,NULL,'a:3:{s:5:\"image\";s:35:\"/uploads/2014/11/high-standards.png\";s:8:\"captions\";s:0:\"\";s:5:\"style\";s:0:\"\";}',NULL,'banner','',0,1,'2014-11-06 16:05:31','2014-11-06 16:05:31'),
	(21,19,9,'About us','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"7\";s:5:\"posts\";a:1:{i:1;s:1:\"1\";}s:5:\"style\";s:0:\"\";}',NULL,'center-column','',0,1,'2014-11-07 09:01:16','2014-11-07 09:01:16'),
	(23,19,9,'sidebar','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"7\";s:5:\"posts\";a:8:{i:0;s:1:\"0\";i:1;s:2:\"21\";i:2;s:1:\"0\";i:3;s:2:\"22\";i:4;s:1:\"0\";i:5;s:2:\"23\";i:6;s:1:\"0\";i:7;s:2:\"24\";}s:5:\"style\";s:0:\"\";}',NULL,'left-column','',NULL,1,'2014-11-07 11:45:01','2014-11-07 17:15:49'),
	(24,20,7,NULL,'a:4:{s:4:\"link\";N;s:5:\"style\";s:0:\"\";s:5:\"image\";s:31:\"/uploads/2014/10/contact-us.png\";s:8:\"captions\";s:5:\"bulbs\";}',NULL,'banner','',NULL,1,'2014-11-07 14:38:58','2014-11-07 14:38:58'),
	(25,20,9,'what we do post','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"7\";s:5:\"posts\";a:1:{i:1;s:1:\"1\";}s:5:\"style\";s:0:\"\";}',NULL,'left-column','',NULL,1,'2014-11-07 15:02:37','2014-11-07 15:02:37'),
	(26,20,9,'Industry Expertise','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"1\";s:5:\"posts\";a:4:{i:1;s:1:\"1\";i:4;s:1:\"1\";i:6;s:1:\"1\";i:8;s:1:\"1\";}s:5:\"style\";s:0:\"\";}',NULL,'right-column','',NULL,1,'2014-11-07 15:35:49','2014-11-07 15:35:49'),
	(27,25,7,'what-we-do banner','a:3:{s:5:\"image\";s:35:\"/uploads/2014/11/high-standards.png\";s:8:\"captions\";s:0:\"\";s:5:\"style\";s:0:\"\";}',NULL,'banner','',NULL,1,'2014-11-07 15:41:50','2014-11-07 15:41:50'),
	(28,25,9,'what we do post','a:5:{s:5:\"Model\";s:5:\"Posts\";s:7:\"element\";s:5:\"posts\";s:11:\"category_id\";s:1:\"7\";s:5:\"posts\";a:1:{i:0;s:2:\"21\";}s:5:\"style\";s:0:\"\";}',NULL,'left-column','',NULL,1,'2014-11-07 15:43:49','2014-11-10 14:11:50'),
	(29,26,7,NULL,'a:3:{s:7:\"element\";s:6:\"banner\";s:5:\"image\";s:50:\"/uploads/2014/11/field-marketing-opportunities.png\";s:5:\"style\";s:0:\"\";}',NULL,'banner',NULL,NULL,1,'2014-11-10 12:41:18','2014-11-10 12:41:18'),
	(35,26,10,NULL,'a:4:{s:7:\"element\";s:7:\"gallery\";s:5:\"title\";s:9:\"Charities\";s:5:\"style\";s:0:\"\";s:7:\"gallery\";a:2:{i:0;a:3:{s:5:\"image\";s:30:\"/uploads/2014/11/dan-brisk.jpg\";s:16:\"alternative_text\";s:3:\"Dan\";s:5:\"order\";s:1:\"1\";}i:1;a:3:{s:5:\"image\";s:35:\"/uploads/2014/11/chris-niarchos.png\";s:16:\"alternative_text\";s:5:\"Chris\";s:5:\"order\";s:1:\"2\";}}}',NULL,'content',NULL,NULL,1,'2014-11-10 16:58:30','2014-11-10 16:58:30');

/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) DEFAULT 'en',
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `locale`, `name`, `slug`)
VALUES
	(1,'en','Industry Expertise','industry-expertise'),
	(2,'en','Board of Directors','board-of-directors'),
	(3,'en','Senior Executives','senior-executives'),
	(4,'en','Independent Contractors','independent-contractors'),
	(5,'en','What we do','what-we-do'),
	(7,'en','about-us','about-us'),
	(8,'en','what we do post','what-we-do');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `webiste_id` int(11) unsigned NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `link_count` int(10) DEFAULT '0',
  `rank` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) DEFAULT 'en',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` varchar(20) DEFAULT NULL,
  `revised` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `locale`, `title`, `slug`, `body`, `image`, `thumb`, `slug_title`, `excerpt`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `revised`, `created`, `modified`)
VALUES
	(1,'en','Appco UK promotes renewable energy growth','/news/appco-uk-promotes-renewable-energy-growth','Appco Group is supporting the UK government’s renewable energy projects by promoting free solar photovoltaic (PV) systems to households across the country.\r\n\r\nSince June 2013, field sales representatives working with Appco Group UK have helped to sign up around 2,700 customers in the UK for free solar panel installations, which will collectively save them at least £17million on their electricity bills over the next 20 years*. \r\n\r\nAccording to Appco client A Shade Greener, this has helped them to prevent more than 134,000 tonnes of CO2 from being emitted into the environment since the company began in 2010.\r\n\r\n“It’s great to see so many people sign up for the free solar panel installations as we’re saving homeowners money, we’re supporting an independent business – A Shade Greener – and we’re protecting the environment so it’s a win-win situation for everyone,” said Nathan Brown General Manager Appco Group Solar.\r\n\r\nA Shade Greener generates revenue from the government’s Feed-In Tariff  scheme (FITs), which pays energy users for the renewable electricity they produce. In return, A Shade Greener provides households with solar power at no cost. \r\n\r\n*Calculation based on average energy consumption for one household, equating to approximately £325\r\nper year.\r\n\r\n“We have all been impressed with the volume and quality of customers acquired by Appco and the Field Representatives (FRs),” said Riverford Field Development Manager Ben Speed.\r\n\r\n“The FRs have shown integrity, professionalism and politeness in their approach to field sales, with great effect. Likewise, Appco has worked extensively to understand our business. All this has resulted in one of Riverford’s most successful drives to acquire new customers in recent years.”\r\n\r\nRiverford organic farms deliver fresh fruit and vegetable boxes straight to customers’ doors.','/uploads/2014/11/telecommunications100x100.jpg',NULL,NULL,NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-11-05 17:56:17'),
	(2,'en','A title once again','/news/title-one-again','And the article body follows.','/uploads/2014/11/telecommunications100x100.jpg',NULL,NULL,NULL,'','',NULL,'Draft',NULL,'2014-09-22 12:26:17','2014-10-23 10:13:21'),
	(3,'en','Title strikes back','/news/title-strikes-back','This is really exciting! Not.','/uploads/2014/11/telecommunications100x100.jpg',NULL,NULL,NULL,'','',NULL,'Unpublished',NULL,'2014-09-23 12:26:17','2014-10-31 11:12:55'),
	(4,'en_GB','test','/news/test','test','/uploads/2014/11/telecommunications100x100.jpg',NULL,NULL,NULL,'','',NULL,'Published',NULL,'2014-10-23 10:14:30','2014-10-23 10:14:30'),
	(5,'en','Appco Australia supports Breast Cancer Awareness Month','/news/','<p>The National Breast Cancer Foundation (NBCF) has recognised Appco Australia&rsquo;s key role in generating regular-giving donors for them and its efforts in helping to raise support throughout <a href=\"http://www.breastcancercampaign.org/support-us/breast-cancer-awareness-month-2014\" target=\"_blank\">Breast Cancer Awareness Month</a> this October.</p>\r\n<p>Since 2012, Appco Australia and its fundraisers have recruited nearly 17,000 monthly donors for NBCF.</p>\r\n<p>NBCF Senior Direct Marketing Officer Dan Peyton has thanked Appco for their support, particularly for promoting Breast Cancer Awareness Month throughout October.</p>\r\n<p>&ldquo;Thanks to funds raised from these generous donors, researchers around the country are hard at work, helping to address some of the most complex breast cancer issues and helping to save lives,&rdquo; he said.</p>\r\n<p>&ldquo;October marks Breast Cancer Awareness Month where we share in the hope for a future free from breast cancer. These researchers will focus their efforts towards the goal of zero deaths by 2030.</p>\r\n<p>&ldquo;Thank you to Appco for being a key part of the NBCF team and for making breast cancer research possible.&rdquo;</p>\r\n<p>The charity recognises an ongoing need to raise awareness and promote early detection, and provides a supportive community for those affected by the disease.</p>\r\n<p>Visit the <a href=\"http://www.nbcf.org.au/\" target=\"_blank\">National Breast Cancer Foundation website</a> for more information on Breast Cancer Awareness Month, their work and how you can get involved.</p>','/uploads/2014/11/telecommunications100x100.jpg','','','The charity recognises an ongoing need to raise awareness and promote early detection, and provides a supportive community for those affected by the disease.','','',NULL,'Published',NULL,'2014-11-03 11:17:22','2014-11-06 17:12:05'),
	(6,'en','Face-to-face fundraiser hailed “a hero” for preventing house fire','/news/','<p>A quick-thinking fundraiser working with <a href=\"http://www.globalfundraisingservices.com/\" target=\"_blank\">AGS Global Fundraising Services</a> * prevented a fire from spreading across a number of terraced houses.</p>\r\n<p>Rhianne Saville smelled smoke when she knocked on the door of a property in Neath, Wales, while out raising funds for and awareness of British Red Cross through a face-to-face campaign run by AGS Global Fundraising Services.</p>\r\n<p>&ldquo;I instantly thought something was wrong as I could smell burning, the smoke alarm was going off and no one was reacting,&rdquo; said Rhianne.</p>\r\n<p>&ldquo;I was worried someone was trapped inside so, after I knocked on the door three times, I called the fire brigade.&rdquo;</p>\r\n<p>The homeowner had accidently left her gas cooker on and without Rhianne&rsquo;s quick response, a number of houses in the street would have caught fire as the gas pipes are connected.</p>\r\n<p>&ldquo;I can&rsquo;t represent the British Red Cross and not help &ndash; it&rsquo;s part of the job,&rdquo; Rhianne added.</p>\r\n<p>Rhianne returned to the property later to speak to homeowner Frances Jordan, who was so grateful for Rhianne&rsquo;s assistance that she immediately signed up to support the British Red Cross.</p>\r\n<p>Frances said: &ldquo;I was so lucky Rhianne happened to be in the area when the fire started &ndash; I owe her a tremendous amount.</p>\r\n<p>&ldquo;If she hadn&rsquo;t phoned the fire brigade when she did, it could have been a disaster on a huge level, but a tragedy was averted because of her quick thinking. She saved the day and I can&rsquo;t thank her enough &ndash; she&rsquo;s a hero!&rdquo;</p>\r\n<p>*Appco Group Support has recently rebranded to AGS Global Fundraising Services to better reflect the range of fundraising solutions it offers.</p>\r\n<div class=\"clearfix\">&nbsp;</div>','','','','A quick-thinking fundraiser working with AGS Global Fundraising Services * prevented a fire from spreading across a number of terraced houses.','','',NULL,'Published',NULL,'2014-11-05 17:58:26','2014-11-06 17:11:29'),
	(7,'en','Appco sponsors HRI 25th anniversary event','/news/','<p>Appco Group Australia sponsored the Heart Research Institute&rsquo;s 25th anniversary dinner in Sydney on 27 September.</p>\r\n<p>The black tie event recognised the charity&rsquo;s achievements and its continuing research into cardiovascular disease.</p>\r\n<p>Appco was a platinum award sponsor at the event, presenting an award to 2014 Young Achiever Dr Yuen Ting (Monica) Lam, with the funds going towards her research into angiogenesis.</p>\r\n<p>&ldquo;We are so grateful for Appco&rsquo;s generous sponsorship of the event,&rdquo; said HRI Fundraising and Events Coordinator Samantha Holt.</p>\r\n<p>&ldquo;We are delighted to announce that over $200,000 was raised for heart research. The generosity of Appco and our guests on the evening will help to support the work of the 2014 Young Achievers&hellip; ensuring the continuation of vital research into cardiovascular disease at the Heart Research Institute.</p>\r\n<p>&ldquo;Thank you once again for Appco&rsquo;s involvement in our gala dinner.&rdquo;</p>\r\n<p>The event was held at the Sydney Opera House Point Marquee. Visit the <a href=\"http://www.hri.org.au/international-fundraising\" target=\"_blank\">HRI website</a> for more information on their initiatives across the early detection, prevention and treatment of heart disease.</p>\r\n<div class=\"clearfix\">&nbsp;</div>','','','','Appco was a platinum award sponsor at the event, presenting an award to 2014 Young Achiever Dr Yuen Ting (Monica) Lam, with the funds going towards her research into angiogenesis.','','',NULL,'Published',NULL,'2014-11-05 17:59:13','2014-11-06 17:10:48');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `type` char(10) DEFAULT 'Page',
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` enum('Published','Draft','Trash') DEFAULT 'Draft',
  `template` varchar(255) DEFAULT NULL,
  `revised` text,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `rank` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `website_id`, `locale`, `type`, `title`, `slug`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `template`, `revised`, `parent_id`, `rank`, `created`, `modified`)
VALUES
	(1,1,'en','Page','Home','/','Cobra Group of Companies','The Cobra Group of Companies started selling consumables more than 25 years ago with the ambition of building one of the world\'s largest and most successful sal',NULL,'Published','front-page',NULL,NULL,0,'2014-09-20 14:20:37','2014-10-29 11:01:45'),
	(2,1,'en','Page','About','/about','','',NULL,'Published','',NULL,NULL,1,'2014-10-22 11:32:51','2014-10-27 15:12:17'),
	(3,1,'en_GB','Page','Home','/','','',NULL,'Published','',NULL,NULL,NULL,'2014-10-22 11:47:12','2014-10-23 10:04:29'),
	(4,1,'en_GB','Page','About','/about','','',NULL,'Published','jkhkjhk',NULL,NULL,NULL,'2014-10-22 14:01:45','2014-10-23 10:04:41'),
	(15,1,'en','Page','Team','/team','','',NULL,'Published','',NULL,2,2,'2014-10-22 16:25:05','2014-10-27 15:12:17'),
	(16,1,'en_AU','Page','Home','/','Cobra Group of Companies','The Cobra Group of Companies started selling consumables more than 25 years ago with the ambition of building one of the world\'s largest and most successful sal',NULL,'Published','',NULL,NULL,NULL,'2014-09-20 14:20:37','2014-10-23 10:26:14'),
	(17,1,'en','Page','Contact','/contact','','',NULL,'Published','',NULL,NULL,3,'2014-10-23 11:27:43','2014-10-27 15:12:17'),
	(18,2,'en','Page','Home','/','','',NULL,'Published','front-page',NULL,NULL,0,'2014-10-30 14:58:10','2014-10-31 16:44:49'),
	(19,2,'en','Page','About us','about-us','','',NULL,'Published','full-width',NULL,18,1,'2014-11-06 10:48:26','2014-11-07 15:31:38'),
	(20,2,'en','Page','contact us','contact','','',NULL,'Published','full-width',NULL,18,3,'2014-11-06 10:53:30','2014-11-07 15:31:38'),
	(21,2,'en','Page','news','news','','',NULL,'Published','full-width',NULL,18,4,'2014-11-06 15:38:01','2014-11-07 16:24:37'),
	(22,2,'en','Page','Our people','our-people/board-of-directors','','',NULL,'Published','full-width',NULL,18,6,'2014-11-06 15:50:04','2014-11-10 12:30:02'),
	(23,2,'en','Page','Appco worldwide','appco-worldwide','','',NULL,'Published','full-width',NULL,18,7,'2014-11-06 15:50:37','2014-11-10 12:30:02'),
	(24,2,'en','Page','Blog','blog','','',NULL,'Published','full-width',NULL,18,8,'2014-11-06 15:51:18','2014-11-10 12:30:02'),
	(25,2,'en','Page','what we do','what-we-do','','',NULL,'Published','full-width',NULL,18,2,'2014-11-07 15:31:24','2014-11-07 15:31:38'),
	(26,2,'en','Page','clients','clients','','',NULL,'Published','full-width',NULL,18,5,'2014-11-10 12:27:58','2014-11-10 12:30:02');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `locale` varchar(10) DEFAULT 'en',
  `title` varchar(50) DEFAULT NULL,
  `menu_label` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `body` text,
  `image` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `rank` int(10) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(160) DEFAULT NULL,
  `other_meta_tags` text,
  `status` varchar(20) DEFAULT NULL,
  `revised` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `category_id`, `locale`, `title`, `menu_label`, `slug`, `slug_title`, `excerpt`, `body`, `image`, `thumb`, `rank`, `meta_title`, `meta_description`, `other_meta_tags`, `status`, `revised`, `created`, `modified`)
VALUES
	(1,1,'en','Face-to-face marketing','Industry Expertise','/industry-expertise/what-we-do','what-we-do','','<p>We have developed a comprehensive results-driven sales campaign framework that is proven time and time again to be the most cost-effective channel in the marketing mix. Our teams of professional independent contractors receive ongoing support and product coaching. They also have regular contact with our clients, ensuring they are always at the heart of the businesses and charities they represent.</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg','/uploads/2014/11/ec17d2d08e4f6c6df057a692724d2f28fbaf2335.jpg',NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-11-07 09:25:33'),
	(2,1,'en','Charities',NULL,'/industry-expertise/','','','<p>Appco Group Support has rebranded to <a href=\"http://www.globalfundraisingservices.com\">AGS Global Fundraising Services</a> to better fulfil the complex and varied fundraising requirements of our charity partners.</p>\r\n<p>Over almost 20 years, the relationships we have built with our charity partners have allowed us to expand on the range of services we offer and we have rebranded to reflect our broader offer.</p>\r\n<p>As Appco Group Support we became the largest face-to-face donor recruitment agency in the world. As AGS Global Fundraising Services we will continue to provide the same standard of excellence in all of the services for which you are currently partnered with us.</p>\r\n<p>AGS Global Fundraising Services will also:</p>\r\n<ul>\r\n<li>draw on our global expertise and resources, not only in face-to-face donor acquisition but also in fundraising solutions such as <a href=\"http://www.globalfundraisingservices.com/our-services/donor-management-services/\">donor management</a> , <a href=\"http://www.globalfundraisingservices.com/our-services/contact-centre-services/\">contact centre services</a> and <a href=\"http://www.globalfundraisingservices.com/our-services/consultancy-services/\">consultancy services</a></li>\r\n<li>provide our clients with a bespoke, seamless package of services to meet all of their fundraising and donor management needs.</li>\r\n</ul>\r\n<p>Please take a look at our new <a href=\"http://www.globalfundraisingservices.com/\">AGS Global Fundraising Services website</a> to find out more.</p>\r\n<p>Visit the <a href=\"http://www.appcogroup.com/appco-worldwide\">Appco Worldwide</a> page to get in touch with the Appco team in your country.</p>\r\n<p>&nbsp;</p>','/uploads/2014/11/ec17d2d08e4f6c6df057a692724d2f28fbaf2335.jpg','',NULL,'','',NULL,'Published',NULL,'2014-09-23 12:26:17','2014-11-07 09:25:41'),
	(4,1,'en','Home efficiency',NULL,'/industry-expertise/','','','<p>Governments and industries right across the globe are encouraging people to use energy more efficiently within their homes, which not only helps them save on their energy bills, but also reduces overall CO2 emissions.</p>\r\n<p>Appco Group\'s Home Efficiency division is driving change in this market by offering consumers a range of products and services &ndash; including solar panels, LED light solutions and home Insulation &ndash; that can help them achieve a more energy-efficient household.</p>\r\n<p>The division is a market leader in its investment in the provision of quality, comprehensive sales training and development programs for contracted sales teams, ensuring they are technically informed and the best possible brand ambassadors for our clients.</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg','',NULL,'','',NULL,'Published',NULL,'2014-10-23 10:14:30','2014-11-07 09:25:48'),
	(8,1,'en','Energy',NULL,'/industry-expertise/','','','<p>The Energy division is one of the foundations of Appco Group.</p>\r\n<p>As many governments have deregulated their utilities sectors, Appco has played a critical role in providing major energy companies with unrivalled customer acquisition volumes.</p>\r\n<p>Over 15 years, Appco Group has acquired more than 10 million new customers in 11 countries and has added billions of dollars in value for our energy clients.</p>\r\n<p>Appco specialises in face-to-face customer acquisition in the energy sector and offers a range of go-to-market channels, including residential door-to-door, business-to-business, in-store, shopping centres and events.</p>\r\n<p>Appco is also an industry leader in terms of quality and has comprehensive sales training and development programmes, a state-of-the-art straight-through processing solution, as well as our unique sales integrity programme, which includes a third party verification/validation service direct to the client at point of sale.</p>\r\n<p>Visit our <a href=\"../../../appco-worldwide\">Appco Worldwide</a> page to get in touch with a specialist in your local country.</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg','',NULL,'','',NULL,'Published',NULL,'2014-11-04 14:12:31','2014-11-07 09:25:54'),
	(9,1,'en','Sports marketing & lotteries',NULL,'/industry-expertise/sports-marketing-n-lotteries','sports-marketing-n-lotteries',NULL,'<p>Appco Group Sports started trading in 2000 as Allsports Direct (Australia) Pty Ltd and has earned a reputation as a professional, cost-effective means for sporting organisations and not for profits to achieve their revenue-raising objectives while raising their brand awareness.</p>\r\n<p>Appco Group Sports provides its clients with the resources to promote and sell their products in existing and new target markets via six main avenues:</p>\r\n<ol>\r\n<li>Major Lottery/Raffle Program</li>\r\n<li>Match Day Lottery/Raffle Program</li>\r\n<li>Scratch &amp; Win Program</li>\r\n<li>Pin &amp; Win Program</li>\r\n<li>Fundraising Voucher Program</li>\r\n<li>Membership Program</li>\r\n</ol>\r\n<p><br /> Appco Group Sports Australia represents sporting organisations across a range of sporting codes, including cricket, football, rugby league, rugby union, Australian rules football, basketball, netball and charitable sporting groups.</p>\r\n<p>The Australian Paralympic Committee was able to send the biggest Paralympic teams ever to the Beijing and Vancouver games due to the support of Appco Group Sports and the monies raised through the lottery/Pin &amp; Win programs.</p>\r\n<p>The relationship between Appco Group Sports Australia and the Surf Life Saving Foundation in Australia has now extended to more than eight years and has been a fantastic relationship for both parties..</p>\r\n<p>&nbsp;</p>','/uploads/2014/11/f2574236fa2523346f9d32970dcf3786cd7c7501.jpg','/uploads/2014/11/telecommunications100x100.jpg',NULL,'','',NULL,'Published',NULL,'2014-11-04 14:22:02','2014-11-05 18:21:56'),
	(10,5,'en','Complete fundraising services','','/what-we-do/','','','<p>Complete fundraising services</p>','/uploads/2014/11/b5b2cdd1a79598a7d4120f2972762e31c809b7db.png','',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:26:50','2014-11-07 09:26:29'),
	(11,5,'en','Face-to-face marketing','','/what-we-do/','','','<p>Face-to-face marketing</p>','/uploads/2014/11/5af5aaf666da4d56c04269dccce452a4806d2c27.png','',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:27:30','2014-11-07 09:26:51'),
	(12,5,'en','Door-to-door sales & services','','/what-we-do/','','','<p>Door-to-door sales &amp; services</p>','/uploads/2014/11/ae57613993fe09c2d85484b1b41ab1f92ea02f2a.jpg','',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:30:00','2014-11-07 09:26:44'),
	(13,5,'en','Customer acquisition','','/what-we-do/','','','<p>Customer acquisition</p>','/uploads/2014/11/afc22d978046058bbe549c8f3326b88c2a6858cd.png','',NULL,'','',NULL,'Published',NULL,'2014-11-05 14:30:28','2014-11-07 09:26:38'),
	(21,7,'en','About us',NULL,'/about-us/about-us','about-us','','<p>Appco Group is part of the&nbsp;<a href=\"http://www.cobragroup.com/\" target=\"_blank\">Cobra Group of Companies</a>. From humble beginnings in 1988 in Sydney, Australia, Appco has expanded across the globe and become one of the world\'s leading face-to-face sales and marketing companies, with more than 800 locations in 27 countries on five continents.</p>\r\n<p>Appco Group sells products and services on behalf of blue-chip companies and acquires long-term donors for well-known charities.</p>\r\n<p>We take pride in helping to add value to our clients&rsquo; bottom line by securing new customers, or raising funds for their vital causes.<br /> And our performance-based model means they only pay for the results we deliver.</p>\r\n<p>Our proven face-to-face sales methods and outcome-based fee structure ensures we deliver to our clients in excess of $US4 billion<br /> in revenue every year via the new customers or donors we acquire for them.</p>\r\n<p>We believe responsible face-to-face marketing <strong>creates a connection </strong>with consumers, <strong>delivers value </strong>to clients, and <strong>supports small businesses </strong><strong>and entrepreneurs</strong>.</p>\r\n<p>For more information, visit our&nbsp;<a href=\"http://www.appcogroup.co.uk/contact-us\">Contact us</a>&nbsp;page and one of our new business specialists will be in touch.</p>','','',NULL,'','',NULL,'Published',NULL,'2014-11-07 09:00:33','2014-11-07 09:00:33'),
	(22,7,'en','Global coverage, local expertise',NULL,'/about-us/global-coverage-local-expertise','global-coverage-local-expertise','','<p>Appco Group continues to expand on a global basis, but always with a focus on maintaining&nbsp;local expertise in every city, province, region&nbsp;and country in which we operate.</p>\r\n<p>Our mission is to continue to use our global knowledge and experience to add value to local campaigns.</p>\r\n<p>This approach ensures we understand the&nbsp;local business environment and, more importantly,&nbsp;the local customers and donors we interact with every day.</p>\r\n<p>Our ability to draw on global experience and awareness of marketing trends, has most recently allowed us to begin&nbsp;operations in Russia, Brazil, China, the USA,&nbsp;Canada and Hong Kong, and we plan to continue our expansion across the Americas, Asia and&nbsp;Europe.</p>','','',NULL,'','',NULL,'Published',NULL,'2014-11-07 10:58:06','2014-11-07 10:58:06'),
	(23,7,'en','Meeting the highest standards of quality',NULL,'/about-us/meeting-the-highest-standards-of-quality','meeting-the-highest-standards-of-quality','','<p>Appco Group insists on the highest possible standards of integrity and accountability at all times: with consumers on the doorstep, with the clients we serve, and with the people contracted to us.</p>\r\n<p>Not only do we adhere to the specific regulatory requirements and codes for each of the sectors in which we operate, but we have also developed our own Appco Group Code of Conduct and Practice.</p>\r\n<p>We require <a href=\"http://www.appcogroup.co.uk/our-people-1/field-representatives\">field representatives</a> to comply with the highest standards of personal conduct, best sales practice and accountability.</p>\r\n<p>It demonstrates how we operate above and beyond industry requirements, implementing our own quality-control mechanisms to help ensure that we demonstrate the highest degree of quality and professionalism at all times.</p>','','',NULL,'','',NULL,'Published',NULL,'2014-11-07 10:59:24','2014-11-07 10:59:24'),
	(24,7,'en','Campaign management',NULL,'/about-us/campaign-management','campaign-management','','<p>At Appco Group, we have invested heavily in developing our business to provide our clients with exceptional returns on their investment and demonstrated results.</p>\r\n<p>We have developed a comprehensive results-driven sales<br /> campaign framework and we have a range of tools at our fingertips, including our:</p>\r\n<ul>\r\n<li>five and eight-step sales methodology, <em>Human Commercial</em>&trade;</li>\r\n<li>industry-leading sales coaching system</li>\r\n<li>leadership and management academies</li>\r\n<li>territory planning and management systems</li>\r\n<li>campaign planning and management framework</li>\r\n<li>unique product testing and market assessment programmes</li>\r\n<li>quality assurance system</li>\r\n<li>state-of-the-art back-office processing centres in 27 countries.</li>\r\n</ul>','','',NULL,'','',NULL,'Published',NULL,'2014-11-07 10:59:56','2014-11-07 10:59:56'),
	(25,8,'en','What we do',NULL,'/what-we-do/what-we-do','what-we-do','','<p>❯ <a href=\"http://www.appcogroup.co.uk/what-we-do#name%201\">Face-to-face marketing</a><br /> ❯ <a href=\"http://www.appcogroup.co.uk/what-we-do#name%202\">Door-to-door products and services</a><br /> ❯ <a href=\"http://www.appcogroup.co.uk/what-we-do#name%203\">Customer acquisition</a><br /> ❯ <a href=\"http://www.appcogroup.co.uk/what-we-do#name%204\">Complete fundraising solutions</a></p>\r\n<h3><a name=\"name 1\"></a>Face-to-face marketing</h3>\r\n<p>We have developed a comprehensive results-driven sales campaign framework that is proven time and time again to be the most cost-effective channel in the marketing mix. Our teams of professional field representatives receive ongoing support and product coaching. They also have regular contact with our clients, ensuring they are always at the heart of the businesses and charities they represent.</p>\r\n<h3><a name=\"name 2\"></a>Door-to-door products and services</h3>\r\n<p>Appco Group sells a range of products and services on behalf of blue-chip companies and we take pride in the fact that we<strong> generate US$4 billion+ per year in new customer revenu </strong>for them. Our service allows businesses to speak directly to potential customers at their homes, their places of work, in store and at private sites and public venues. And our performance-based model means they only pay for the results we deliver.</p>\r\n<h3><a name=\"name 3\"></a>Customer acquisition</h3>\r\n<p>We specialise in customer acquisition across <strong>11 industry sectors</strong> and, with more than 10,000 field representatives in 27 countries, we are one of the largest and most successful sales and marketing organisations in the world today. For our energy clients alone, we have acquired more than 10 million new customers across the globe.</p>\r\n<h3><a name=\"name 4\"></a>Complete fundraising solutions</h3>\r\n<p>Face-to-face donor acquisition is just one part of our fundraising offer. <strong>Appco Group&rsquo;s fundraising solutions bring together a range of services that can fulfill our clients&rsquo; needs at every stage</strong>of their fundraising and donor-acquisition journey. Other services include:​<br /> Online fundraising services<br /> Complete outsourced donor management services<br /> Consultancy services<br /> Contact centre services<br /> Point-of-sale and mobile fundraising solutions<br /> Lottery and scratch-card campaigns</p>','/uploads/2014/11/creating-a-connection.png','',NULL,'','',NULL,'Published',NULL,'2014-11-07 14:46:42','2014-11-07 14:46:42'),
	(26,8,'en','Face-to-face customer acquisition',NULL,'/what-we-do/face-to-face-customer-acquisition','face-to-face-customer-acquisition','','<h3>Responsible face-to-face marketing</h3>\r\n<p>We believe that responsible face-to-face marketing creates a connection with consumers, delivers value to clients and supports small businesses and entrepreneurs.</p>\r\n<p>And, with more than 25 years of experience in the industry, we know that responsible face-to-face sales and marketing is the most cost-effective channel in the marketing mix.</p>\r\n<p>Appco Group&rsquo;s proven methods consistently deliver results for our clients and ensure their products and services receive the best possible face-to-face marketing strategy and reach.</p>\r\n<p><a href=\"http://www.appcogroup.co.uk/what-we-do/face-to-face-customer-acquisition#top\">Back to the top</a></p>\r\n<h3><a name=\"name 2\"></a>Successful customer acquisition</h3>\r\n<p>We believe the first step to successful face-to-face customer acquisition is to build a relationship with each of our clients so that we, and our teams of field marketing representatives, understand their product or service and the message they need to deliver via our door-to-door marketing method.</p>\r\n<p>Specialising in face-to-face customer acquisition across 14 industry sectors, including fundraising, we continually strive to ensure that we offer both our clients and the field representatives who work with us the highest level of service and support.</p>\r\n<p>Our insistence on excellence in everything we do has seen Appco Group grow into a leading face-to-face customer acquisition company, as well as the largest and most successful sales and marketing organisation in the world today.</p>\r\n<p><a href=\"http://www.appcogroup.co.uk/what-we-do/face-to-face-customer-acquisition#top\">Back to the top</a></p>\r\n<h3><a name=\"name 3\"></a>Our face-to-face customer acquisition experts</h3>\r\n<p>Our field representatives are specialists in face-to-face customer acquisition, using our proven Human CommercialTM framework to achieve results for our clients.</p>\r\n<p>All field representatives are offered ongoing support and product coaching to help them deliver a professional and interactive face-to-face selling experience for every customer and achieve our clients&rsquo; acquisition goals.</p>\r\n<p>They also have regular contact with our clients to ensure they are always at the heart of the businesses and charities they represent.</p>\r\n<p>Results-driven face-to-face marketing solutions</p>\r\n<p>Appco Group&rsquo;s face-to-face marketing services are proven to work and guaranteed to deliver results for our clients.</p>\r\n<p>Our comprehensive sales-campaign framework is performance based, which means clients only pay for the results we deliver.</p>\r\n<p>As a result, our face-to-face sales and marketing strategies achieve a higher level of retention and reach.</p>\r\n<p>It&rsquo;s one more reason why Appco is the leading face-to-face marketing company in the field marketing industry.</p>','/uploads/2014/11/face-to-face-customer-acquisition.png','',NULL,'','',NULL,'Published',NULL,'2014-11-07 14:50:40','2014-11-07 14:50:40'),
	(27,8,'en','Door-to-door sales and services',NULL,'/what-we-do/door-to-door-sales-and-services','door-to-door-sales-and-services','','<p>❯ <a href=\"http://www.appcogroup.co.uk/what-we-do/door-to-door-sales-and-services#name%201\">Unique door-to-door sales solutions</a><br /> ❯ <a href=\"http://www.appcogroup.co.uk/what-we-do/door-to-door-sales-and-services#name%202\">The door-to-door marketing experts</a><br /> ❯ <a href=\"http://www.appcogroup.co.uk/what-we-do/door-to-door-sales-and-services#name%203\">Guaranteed return on investment</a></p>\r\n<h3><a name=\"name 1\"></a>Unique door-to-door sales solutions</h3>\r\n<p>Our door-to-door sales team helps us to create a connection with our clients&rsquo; target markets and secure hundreds of thousands of new customers for them every year,&nbsp; and their strength lies in our unique field sales solutions.</p>\r\n<p>Selling products and services on behalf of blue-chip companies and well-known charities for over 25 years, we have become an industry leader in door-to-door sales and marketing, delivering value to our clients and offering field sales services and fundraising solutions that support small businesses and entrepreneurs all over the world.</p>\r\n<p><a href=\"http://www.appcogroup.co.uk/what-we-do/door-to-door-sales-and-services#top\">Back to the top</a></p>\r\n<h3><a name=\"name 2\"></a>The door-to-door marketing experts</h3>\r\n<p>We take pride in helping to add value to our clients&rsquo; businesses and our door-to-door sales method is proven to secure the very best results.</p>\r\n<p>We have developed face-to-face customer acquisition solutions and door-to-door marketing techniques that promote our clients&rsquo; products and services in an interactive and personalised way, ensuring the right message reaches the right people.</p>\r\n<p>Appco Group&rsquo;s door-to-door marketing specialists speak directly to, and create a connection with, our clients&rsquo; potential customers at their homes, places of work, in store and at private sites and public venues, gaining the maximum reach possible for our clients.</p>\r\n<p>Our door-to-door sales strategy is so successful that we generate US$4 billion+ per year in new customer revenue for our clients.</p>\r\n<p><a href=\"http://www.appcogroup.co.uk/what-we-do/door-to-door-sales-and-services#top\">Back to the top</a></p>\r\n<h3><a name=\"name 3\"></a>Guaranteed return on investment</h3>\r\n<p>As an experienced door-to-door marketing company, we understand how frustrating it can be for businesses when they don&rsquo;t receive the results they have been promised.</p>\r\n<p>This is why our door-to-door sales strategy is based purely on our performance, which means our clients only pay for the results that we deliver.</p>\r\n<p>It&rsquo;s just one more reason that charities and blue-chip companies trust us time and again to deliver value &ndash; and superior quality customers or donors &ndash; to their businesses.</p>','/uploads/2014/11/creating-a-connection.png','',NULL,'','',NULL,'Published',NULL,'2014-11-07 14:51:22','2014-11-07 14:52:31'),
	(28,8,'en','Complete fundraising solutions',NULL,'/what-we-do/complete-fundraising-solutions','complete-fundraising-solutions','','<p>As an experienced face-to-face fundraising agency, we understand that our charity partners have a diverse range of fundraising and donor management needs.</p>\r\n<p>Since 1997, the successful relationships we have built with our charity partners have allowed us to expand on the range of services we offer, which is why Appco Group Support has now rebranded to <a href=\"http://www.globalfundraisingservices.com\">AGS Global Fundraising Services</a> to reflect our broader offer.</p>\r\n<p>AGS Global Funbdraising Services brings together a range of fundraising solutions that can meet our clients&rsquo; needs at every stage of their fundraising and donor-acquisition journey.</p>\r\n<h3>Alongside face-to-face donor acquisition, AGS Global Fundraising Services also includes:<br /> &nbsp;</h3>\r\n<ul>\r\n<li><a href=\"http://www.globalfundraisingservices.com/our-services/donor-management-services/\">Complete outsourced donor management services</a></li>\r\n<li><a href=\"http://www.globalfundraisingservices.com/our-services/contact-centre-services/\">Contact centre services</a></li>\r\n<li><a href=\"http://www.globalfundraisingservices.com/our-services/consultancy-services/\">Consultancy services</a></li>\r\n<li><a href=\"http://www.globalfundraisingservices.com/our-services/point-of-sale-and-mobile-fundraising-solutions/\">Point-of-sale and mobile fundraising solutions</a></li>\r\n<li><a href=\"http://www.globalfundraisingservices.com/our-services/lotteries-and-vouchers/\">Lottery and voucher campaigns</a></li>\r\n</ul>\r\n<h3><br /> Our fundraisers</h3>\r\n<p>We understand that brand and reputation are of ultimate importance to all our charity partners, so all fundraisers are offered ongoing support and coaching &ndash; and have regular contact with our charity partners &ndash; to ensure they are worthy and well-informed ambassadors for our charity partners.</p>\r\n<p>AGS Global Funbdraising Services also captures daily feedback from a range of different sources to ensure that our field marketing teams of fundraisers maintain our highly professional standards, which we believe is an important part of our role as fundraising consultants.&nbsp;</p>\r\n<p>It all helps us to support our fundraisers&rsquo; passion for the causes they represent, and maintain highly motivated teams with the capacity to deliver committed, regular-giving donors.</p>\r\n<p>Every week, our worldwide network of 3,500 fundraisers acquires 32,500 new donors for our charity clients. Globally, we generate US$40 million in donations every month for our clients, making our fundraising services some of the best in the world.</p>','/uploads/2014/11/meeting-the-highest-standard.png','',NULL,'','',NULL,'Published',NULL,'2014-11-07 14:52:05','2014-11-07 14:52:05'),
	(29,8,'en','Field marketing opportunities',NULL,'/what-we-do/field-marketing-opportunities','field-marketing-opportunities','','<p>Appco Group UK provides a unique and fantastic opportunity for motivated people of all ages to develop their field sales careers locally, regionally and internationally.</p>\r\n<p>Appco Group UK has a variety of <a href=\"http://www.appcogroup.co.uk/clients\">clients</a> spanning many industries. As a Field Representative, you will promote and sell products and services for, and on behalf of, Appco Group\'s clients, and you will be contracted to one of the Marketing Companies within our network.</p>\r\n<p>To assist you in promoting and selling our clients\' products and services, which may also include providing face-to-face fundraising services, you will be provided with client product training and all the materials and support you need, based on the clients\' door-to-door marketing or donor-acquisition requirements.</p>\r\n<p>As a successful Field Representative, unique opportunities exist to enter the Appco Group Business Development Programme. This provides you with an opportunity to mentor, guide and product train other field sales representatives with a view to moving through the Programme and running your own Marketing Company within the Appco Group network.</p>\r\n<p>If you are interested in opportunities within the Appco Group network or Marketing Companies &ndash; or you are already a Field Representative in the UK &ndash; our <a href=\"http://www.appcogroup.co.uk/our-people-1/field-representatives\">Field Representatives section</a> is designed to answer any questions you may have about operating your own business as a self-employed field sales representative.</p>','/uploads/2014/11/field-marketing-opportunities.png','',NULL,'','',NULL,'Published',NULL,'2014-11-07 14:54:00','2014-11-07 14:54:00'),
	(30,2,'en','Chris Niarchos',NULL,'/board-of-directors/chris-niarchos','chris-niarchos','','<h2>Chairman and Founder</h2>\r\n<p>As the Founder and Chairman of the <a href=\"http://www.cobragroup.com/\" target=\"_blank\">Cobra Group of Companies</a>, Chris sets the strategic direction and ensures the objectives of every business within the Group are met.</p>\r\n<p>Appco Group is the direct sales and marketing subsidiary of the Cobra Group of Companies. In 1987, as a 22-year-old, Chris started his direct sales business in Sydney, Australia, with the vision of one day building it into the world&rsquo;s largest and most successful sales and marketing company.</p>\r\n<p>By 2010, he had well and truly achieved this goal and now runs a billion-dollar enterprise that is continuing to build on a global scale.</p>\r\n<p>There is no doubt Chris is one of the world&rsquo;s most successful entrepreneurs and he has just as much energy and passion today as he did at the beginning of his journey. As well as direct sales, the Cobra portfolio also includes interests in insurance, energy, financial services, high-tech security, business process outsourcing, mid-range super yachts and motorsport.</p>\r\n<p>Chris is an inspirational leader and has an innate ability to invest in people and businesses, and harness their potential through his unique partnership approach.</p>\r\n<p>Chris continues to bring incredible leadership, vision and execution to what is one of the world&rsquo;s fastest growing entrepreneurial groups of businesses.</p>','','/uploads/2014/11/chris-niarchos.png',NULL,'','',NULL,'Published',NULL,'2014-11-07 14:58:43','2014-11-07 14:58:43'),
	(31,3,'en','Dan Brisk',NULL,'/senior-executives/dan-brisk','dan-brisk','','<p><strong>Head of Product Development, The Lab</strong></p>\r\n<p>Dan joined the Appco Group in 2012 as Head of Product Development.</p>\r\n<p> He brought with him over 10 years of direct sales and product development experience within<br /> a variety of business sectors covering three continents.</p>\r\n<p>This made him the ideal person to head up one of Appco Group&rsquo;s newest strategic divisions,<br /> The Lab.</p>\r\n<p>Specialising in the research, development and the implementation of new and existing products<br /> for the business, The Lab is the key to a new era for Appco.</p>','','/uploads/2014/11/dan-brisk.jpg',NULL,'','',NULL,'Published',NULL,'2014-11-07 15:00:45','2014-11-07 15:00:55');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(11) unsigned NOT NULL,
  `home_page_id` int(11) unsigned NOT NULL,
  `site_title` varchar(100) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `news_posts_per_page` int(10) NOT NULL DEFAULT '10',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `website_id`, `home_page_id`, `site_title`, `logo`, `news_posts_per_page`, `created`, `modified`)
VALUES
	(1,1,1,'',NULL,10,'2014-10-30 09:48:57','2014-10-30 09:51:12'),
	(2,2,18,'local.appco.group','appco/bg_logo_en.png',5,'2014-10-31 14:38:17','2014-10-31 14:38:17');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
