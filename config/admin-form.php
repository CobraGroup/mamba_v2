<?php

$config = [
	'formstart' => '<form{{attrs}} role="form" novalidate="novalidate">',
	'button' => '<button{{attrs}} class="btn btn-primary">{{text}}</button>',
	//'checkboxFormGroup' => '{{input}} {{label}}',
	//'checkboxWrapper' => '<div class="form-group">{{input}} {{label}}</div>',
	'formGroup' => '{{label}} {{input}}',
    //'inputContainer' => '<div class="form-group">{{content}}</div>',
	//'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}>',
	'inputContainerError' => '<div class="input {{type}}{{required}} has-error has-feedback">{{content}}{{error}}</div>',
	//'select' => '<select name="{{name}}"{{attrs}} class="form-control">{{content}}</select>',
	'selectMultiple' => '<select name="{{name}}[]" multiple="multiple"{{attrs}}  class="form-control">{{content}}</select>',
	'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',


	'inputContainer'	=> '<div class="form-group">{{content}}</div>',
	'input'				=> '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',
	'select'			=> '<select name="{{name}}" {{attrs}} class="form-control">{{content}}</select>',
	'nestingLabel'		=> '<div class="checkbox">{{hidden}}<label {{attrs}}>{{input}} {{text}}</label></div>',
	'submitContainer'	=> '{{content}}',
	'dateWidget'		=> '{{day}} {{month}} {{year}} {{hour}} {{minute}} {{second}} {{meridian}}'
];

/*
	'button' => '<button{{attrs}}>{{text}}</button>',
	'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
	'checkboxFormGroup' => '{{input}}{{label}}',
	'checkboxWrapper' => '<div class="checkbox">{{input}}{{label}}</div>',
	'dateWidget' => '{{year}}{{month}}{{day}}{{hour}}{{minute}}{{second}}{{meridian}}',
	'error' => '<div class="error-message">{{content}}</div>',
	'errorList' => '<ul>{{content}}</ul>',
	'errorItem' => '<li>{{text}}</li>',
	'file' => '<input type="file" name="{{name}}"{{attrs}}>',
	'fieldset' => '<fieldset>{{content}}</fieldset>',
	'formstart' => '<form{{attrs}}>',
	'formend' => '</form>',
	'formGroup' => '{{label}}{{input}}',
	'hiddenblock' => '<div style="display:none;">{{content}}</div>',
	'input' => '<input type="{{type}}" name="{{name}}"{{attrs}}>',
	'inputsubmit' => '<input type="{{type}}"{{attrs}}>',
	'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>',
	'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
	'label' => '<label{{attrs}}>{{text}}</label>',
	'legend' => '<legend>{{text}}</legend>',
	'option' => '<option value="{{value}}"{{attrs}}>{{text}}</option>',
	'optgroup' => '<optgroup label="{{label}}"{{attrs}}>{{content}}</optgroup>',
	'select' => '<select name="{{name}}"{{attrs}}>{{content}}</select>',
	'selectMultiple' => '<select name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select>',
	'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
	'radioWrapper' => '{{input}}{{label}}',
	'textarea' => '<textarea name="{{name}}"{{attrs}}>{{value}}</textarea>',
	'submitContainer' => '<div class="submit">{{content}}</div>',
*/

