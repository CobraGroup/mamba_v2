<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\Router;
use Cake\Routing\Dispatcher;
/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');

Router::prefix('admin', function($routes) {
	$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/:controller/:action', [], ['routeClass' => 'InflectedRoute']);
	$routes->connect('/:controller/:action/*', [], ['routeClass' => 'InflectedRoute']);
});


Router::scope('/', function($routes) {
/**
 * Here, we are connecting '/' (base path) to a controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, src/Template/Pages/home.ctp)...
 */
	$routes->redirect('/webroot/blog', '/blog/', ['status' => 302]);
	$routes->redirect('/webroot/blog/', '/blog/', ['status' => 302]);

	$routes->redirect('/webroot/el-blog', '/blog/', ['status' => 302]);
	$routes->redirect('/webroot/el-blog/', '/blog/', ['status' => 302]);

	$routes->connect('/', ['controller' => 'Pages', 'action' => 'index']);

	$routes->redirect('/admin', '/login', ['status' => 302]);

	$routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
	$routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);

	$routes->connect('/thanks', ['controller' => 'Communications', 'action' => 'apply']);
	$routes->connect('/contact', ['controller' => 'Contact', 'action' => 'index']);

	#Router::connect('/es/news/:slug', ['controller' => 'News', 'action' => 'view', 'lang' => 'es']);

	$routes->connect('/news-and-blog', ['controller' => 'News', 'action' => 'index']);
	$routes->connect('/news-and-blog/:slug', ['controller' => 'News', 'action' => 'view']);

	$routes->connect('/news', ['controller' => 'News', 'action' => 'index']);
	$routes->connect('/news/:slug', ['controller' => 'News', 'action' => 'view']);

	$routes->connect('/nyheter', ['controller' => 'News', 'action' => 'index']);
	$routes->connect('/nyheter/:slug', ['controller' => 'News', 'action' => 'view']);

	$routes->connect('/notizie', ['controller' => 'News', 'action' => 'index']);
	$routes->connect('/notizie/:slug', ['controller' => 'News', 'action' => 'view']);

	$routes->connect('/noticias', ['controller' => 'News', 'action' => 'index']);
	$routes->connect('/noticias/:slug', ['controller' => 'News', 'action' => 'view']);

	$routes->connect('/contact', ['controller' => 'Contact', 'action' => 'index']);

	Router::connect('/:lang', ['controller' => 'Pages', 'action' => 'index'], ['lang' => '[a-z]{2}', 'routeClass' => 'InflectedRoute']);
	Router::connect('/:lang/:controller', ['action' => 'index'], ['lang' => '[a-z]{2}', 'routeClass' => 'InflectedRoute']);
	Router::connect('/:lang/:controller/:slug', ['action' => 'view'], ['routeClass' => 'InflectedRoute']);

	$routes->connect( '/:slug',  ['controller' => 'Pages', 'action' => 'view']);
	$routes->connect( '/:parent/:slug',  ['controller' => 'Pages', 'action' => 'view', 'slug' => ':slug', 'parent' => ':parent' ]);
	$routes->connect( '/:grandparent/:parent/:slug',  ['controller' => 'Pages', 'action' => 'view', 'slug' => ':slug', 'parent' => ':parent', 'grandparent' => ':grandparent' ]);



    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
