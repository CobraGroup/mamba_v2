<?php
	
$config = [
    'number' => '<li><a href="{{url}}">{{text}}</a></li>',
	'first' => '<li class="first"><a href="{{url}}">&laquo;</a></li>',
	'last' => '<li class="last"><a href="{{url}}">&raquo;</a></li>',
];
