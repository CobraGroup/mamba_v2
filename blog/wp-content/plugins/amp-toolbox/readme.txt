=== Plugin Name ===
Contributors: deano1987
Tags: amp, amp schema, amp header, amp link, amp original, schema, header, header logo, link
Requires at least: 3.0
Tested up to: 4.5.2
Stable tag: 1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin provides extra features and fixes for use with the AMP Wordpress plugin. You can change the header and it fixes schema as well as allowing you to give users a link to your amp/original versions.

== Description ==

This plugin provides extra features and fixes for use with the AMP Wordpress plugin, which must also be installed. https://wordpress.org/plugins/amp

- You can change the header CSS, specify a header image, change the size and colours of the header.
- You can add a link to normal posts page "there is a AMP version of this page" - so users can opt to view the AMP version.
- You can add a link to AMP versions of posts "this is an AMP version of this page" - so users can opt to view the normal version.
- You can fix the schema problems with publisher logo by setting your publisher logo and dimensions.

Fixes new Schema rules by Google for Width and Height.

== Frequently Asked Questions ==

= Will there ever be more options added? =

Yes, we are in early development so expect lots of features! Please let us know if you have a feature suggestion.


== Installation ==

1. Upload the folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.5 =
* Fixed a bug where AMP version links were not being removed if you turned the option off - Thanks Robert.

= 1.4 =
* Now stripping illegal tag <font> from any content being served on an AMP page.

= 1.3 =
* Google have updated schema testing, and AMP plugin breaks image width and height, this is now automagically fixed by AMP Toolbox.

= 1.2 =
* Now stripping illegal tags such as <embed> and replacing <quote> with <em>

= 1.0 =
* Inital release