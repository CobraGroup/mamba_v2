<!DOCTYPE html>
<html>
<head>
	<base href="http://www.appcogroup.co.uk/" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="/favicon.ico" type="image/x-icon" rel="icon"/>
	<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" defer="defer"></script>




	<link rel="stylesheet" href="/css/reset.css" defer="defer"/>
	<link rel="stylesheet" href="/css/blog.css" defer="defer"/>
	<link rel="stylesheet" href="/css/jquery.bxslider.css" defer="defer"/>
	<link rel="stylesheet" href="/css/ag/css/all.css" defer="defer"/>
	<link rel="stylesheet" href="/css/ag/css/theme.css" defer="defer"/>

	<?php

		if($_SERVER["REQUEST_URI"] =='/webroot/blog/') :
			error_reporting(E_ALL | E_WARNING | E_NOTICE);
			ini_set('display_errors', TRUE);
			wp_redirect( 'http://www.appcogroup.co.uk/blog/', 301 ); exit;

		endif;
	?>

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-17876900-9', 'auto');
	ga('send', 'pageview');
	</script>
	<style type="text/css">.at4-share-title { font-size: 9px; }</style>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title(); ?></title>

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php //comments_popup_script(); // off by default ?>
	<?php wp_head(); ?>



</head>
<body class="blog">
	<div id="wrapper">
	<div id="menu-header">
		<div>
			<nav id="nav-mobile" class="mobile-clearfix nav">
				<div class="mobile-home appcogroup-uk"><a class="active" href="/">Home</a></div>
				<div class="mobile-menu"><span><a href="#" class="close-button" id="close-menu"></a></span><span><a href="#" class="static-menu" id="pull"></a></span></div>

				<ul>

					<li class="deeper home appcogroup-uk"><a class="active" href="/">Home</a></li>

							<li class="deeper  "><a href="/about-us">About us</a>
							</li><li class="deeper  "><a href="/field-marketing">Field marketing</a>
							</li><li class="deeper  "><a href="/our-expertise">Our expertise</a>
							</li><li class="deeper  "><a href="/our-people/board-of-directors">Our people</a>
							</li><li class="deeper  "><a href="/news">News</a>
							</li><li class="deeper  "><a href="/appco-worldwide">Appco worldwide</a>
							</li><li class="deeper  "><a href="/blog/">Blog</a>
							</li><li class="deeper  "><a href="/clients">Clients</a>
							</li><li class="deeper  "><a href="/contact-us">Contact us</a>
							</li><li class="deeper last "><a href="/appco-uk-frequently-asked-questions">FAQs</a></li>
				</ul>
			</nav>
		</div>
		<div class="google-search">
			<div class="google-search-close"><img src="/img/appco/close-btn.png" alt="close google search" class="google-close-button"/></div>
			<div class="google-search-box">

			<script>
			  (function() {
			    var cx = '007974598654637237179:cpkesk7mqr8';
			    var gcse = document.createElement('script');
			    gcse.type = 'text/javascript';
			    gcse.async = true;
			    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
			        '//cse.google.com/cse.js?cx=' + cx;
			    var s = document.getElementsByTagName('script')[0];
			    s.parentNode.insertBefore(gcse, s);
			  })();
			</script>
			<gcse:search linktarget="_parent"></gcse:search>
			<!-- <gcse:searchresults linktarget="_parent"></gcse:searchresults> -->
		</div>
	</div>
	</div>
	<div id="container">
		<div id="banner">
			<div class="" style="clear:both; padding-top:70px; display: block;"></div>
			<div class="left">
				<img src="/uploads/2015/03/appco-complete-fundraising-solutions.jpg" alt=""/>
			</div>
			<div class="right">
				<img src="/uploads/2015/05/map.jpg" alt=""/>
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="row layout4" id="content">

			<div class="wp-left">

			<h1>Blog</h1>






