



					</div>
					<div class="wp-right">
						<?php get_sidebar(); ?>
					</div>

				</div>
			</div>
		</div>
	</div>

			<div style="margin: 0 auto; width: 960px; " class="" id="">
  				<div class="wp-left">
					<div class="clearfix"></div>
					<div class="row"><h3 style="font-size: 14px;">Read more of our blogs <a target="_blank" href="http://appcogroupuk.tumblr.com/">here</a>.</h3></div>
				</div>
				<div class="wp-right"></div>
			</div>
	<div class="clearfix"></div>
<div id="footer">
		    <div class="outter">
	            <div>
	                <div class="left">
	                    <div class="logos">
	                        <ul>
	                            <li><a target="_blank" href="http://www.appcogroup.com/"><img itemprop="image" alt="Appco group - Field Marketing  Agency" src="/img/appco/appco_logo_footer.png"></a></li>
	                            <li><a target="_blank" href="https://www.linkedin.com/company/be-somethingmore"><img alt="Be something more with Appco" src="/img/appco/besomethingmore.png"></a></li>
	                        </ul>
	                    </div>
	                </div>
	                <div class="right">
		                	                    	                     							 <ul class="logos">
	                   			<li>
		            			<a target="_blank" href="http://www.dma.org.uk/"><img alt="DSA Member" src="/img/appco/appco-dsa-membr_huge.png"></a>                                </li>
	                            <li>
		            			<a target="_blank" href="http://www.lotteriescouncil.org.uk/"><img alt="The Lotteries Council" src="/img/appco/the-lotteries-council.png"></a>								</li>
                                <li>
		            			<a target="_blank" href="http://www.pfra.org.uk/"><img alt="PFRA Approved" src="/img/appco/pfra.png"></a>			            		</li>
	                            <li>
			            			<a target="_blank" href="http://www.institute-of-fundraising.org.uk/find-a-supplier/appco-group-support/"><img alt="Instiitute of Fundraisers" src="/img/appco/institute-of-fundraising.png"></a>			            		</li>
                    		</ul>
		                 	                    	                    	                </div>
	            </div>
		    </div>
		    <div class="brand-colours">
			    <span class="red">&nbsp;</span>
			    <span class="blue">&nbsp;</span>
			    <span class="brown">&nbsp;</span>
			    <span class="purple">&nbsp;</span>
			    <span class="orange">&nbsp;</span>
			    <span class="red">&nbsp;</span>
			</div>
		    <div class="grey">
		        <div class="outter">

		            <div class="left">
			        	<div class="cobra-company">
				        	<a target="_blank" href="http://www.cobragroup.com/"><img alt="Cobra Group Company" src="/img/appco/ag/cobra-group-company.png"></a>			        	</div>
		            </div>
					<div class="right">
		                <ul>
			                	<li><a href="/about-us/appco-worldwide">Appco worldwide</a></li><li class="seperator">&nbsp;|&nbsp;</li>
                                <li><a href="/news">News</a></li><li class="seperator">&nbsp;|&nbsp;</li>
								<li><a href="/privacy-policy">Privacy policy</a></li><li class="seperator">&nbsp;|&nbsp;</li>
                                <li><a href="/sitemap-gb.html">Sitemap</a></li>
									                </ul>
					</div>
		        </div>
		            <div class="rights">
			            <div class="bottom-links-wrapper">
					   						   	<div class="bottom-links-left">
						    	<p>Appco Group &copy; all rights reserved 2016 </p>
			            	</div>
		            		<div class="bottom-links-right">

			            		<!-- AddThis Follow BEGIN -->
			            		<div class="bottom-links-right-title">
									<h3 style="color:#fff;">FOLLOW US: </h3>
			            		</div>
			            		<div class="bottom-links-right-links">
									<!-- Go to www.addthis.com/dashboard to customize your tools -->
									<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a2dce77bf1c151" async="async"></script>

									<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<p class="addthis_horizontal_follow_toolbox">
									<a addthis:userid="appcogroup" class="addthis_button_facebook_follow at300b" href="http://www.facebook.com/appcogroup" target="_blank" title="Follow on Facebook"><img src="/img/ag/buttons/facebook.png"><span class="addthis_follow_label">Facebook</span></a>

									<a addthis:userid="appcogroup" class="addthis_button_twitter_follow at300b" href="//twitter.com/appcogroup" target="_blank" title="Follow on Twitter"><img src="/img/ag/buttons/twitter.png"><span class="addthis_follow_label">Twitter</span></a>

									<a addthis:usertype="company" addthis:userid="appco-group" class="addthis_button_linkedin_follow at300b" href="http://www.linkedin.com/company/appco-group" target="_blank" title="Follow on LinkedIn"><img src="/img/ag/buttons/linkedin.png"><span class="addthis_follow_label">LinkedIn</span></a>

									<a addthis:userid="+appcogroup" class="addthis_button_google_follow at300b" title="Follow on Google" href="https://plus.google.com/+appcogroup" target="_blank"><img src="/img/ag/buttons/gplus.png"><span class="addthis_follow_label">Google Follow</span></a>

									<a addthis:userid="appcogroup" class="addthis_button_pinterest_follow at300b" href="http://www.pinterest.com/appcogroup" target="_blank" title="Follow on Pinterest"><img src="/img/ag/buttons/pinterest.png"><span class="addthis_follow_label">Pinterest</span></a>

									<a addthis:userid="appcogrouptv" class="addthis_button_youtube_follow at300b" href="http://www.youtube.com/user/appcogrouptv?sub_confirmation=1" target="_blank" title="Follow on YouTube"><img src="/img/ag/buttons/youtube.png"><span class="addthis_follow_label">YouTube</span></a>


								<div id="atftbx" class="at-follow-tbx-element addthis-smartlayers addthis-animated at4-show"><p><span> </span></p><div class="addthis_toolbox addthis_default_style addthis_32x32_style "><div class="atclear"></div></div></div></p>
			            		</div>

		            		</div>

					   </div>

		            </div>

		     </div>
		</div>
	</div>



	<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NL735P"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NL735P');</script>
	<!-- End Google Tag Manager -->
<script src="/js/ag/min/appco-min.js" defer="defer"></script>

</body>
</html>

