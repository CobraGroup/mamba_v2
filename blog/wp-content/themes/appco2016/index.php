<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="post blog-item" id="post-<?php the_ID(); ?>">

	<div class="meta">
		<div class="hidden-xs hidden-sm">
			<div class="date-item"><strong><?php the_time('d'); ?></strong>|<?php the_time('m'); ?></div>
			<div class="date-item date-year"><span><?php the_time('Y'); ?></span></div>
		</div>
		<div class="date-mobile visible-xs-block visible-sm hidden-md hidden-lg"><?php the_time('d/m/Y'); ?></div>
	</div>
	
	<div class="summary">
		<!-- TITLE -->
		<h2 class="storytitle">
			<?php
			$title = get_the_title();
			$excerpt = get_the_excerpt("");
			$title_length = strlen(strip_tags($title));
			$excerpt_length = strlen(strip_tags($excerpt));
			$excerpt_limit = 280 - $title_length;
			$excerpt = substr($excerpt, 0, $excerpt_limit)." [...]";
			?>
			<a href="<?php the_permalink() ?>" rel="bookmark"><?php echo $title; ?></a>
		</h2>
		<!-- / TITLE -->
	
	
		<!-- CONTENT -->
		<div class="storycontent"><?php echo $excerpt; ?></div>
		<!-- /CONTENT -->
	
		<div class="readmore"><a href="<?php the_permalink() ?>" rel="bookmark">Read more</a></div>
	</div>
	<div class="clearfix">&nbsp;</div>

</div>
<?php endwhile; else: ?>
	<h3><?php _e('Sorry, no posts matched your criteria.'); ?></h3>
<?php endif; ?>

<?php // posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>




<?php get_footer(); ?>
