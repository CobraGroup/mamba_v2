
<?php get_header(); ?>

<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="blogback">◄ Blog Home</a><br />

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="post" id="post-<?php the_ID(); ?>">
	
	<br />
	
	<div class="meta"><?php the_time('l, F jS, Y'); ?></div>
	
		
	<!-- TITLE -->
	<h1 class="storytitle-single">
		<?php the_title(); ?>
	</h1>
	<!-- / TITLE -->



	<!-- CONTENT -->
	<div class="storycontent-single"><?php the_content(__('(more...)')); ?></div>
	<!-- /CONTENT -->
	
	<div class="alignright">
		<small>Posted at <?php the_time() ?></small><br />
		<small><?php the_tags(__('Tags: '), ', ', ' &#8212; '); ?></small><br />
		<!--<small><?php _e("Category: "); ?> <?php the_category(',') ?></small>-->
	</div>

</div>



<?php endwhile; else: ?>
	<h3><?php _e('Sorry, no posts matched your criteria.'); ?></h3>
<?php endif; ?>
<?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>





<?php get_footer(); ?>
