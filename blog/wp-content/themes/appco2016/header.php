<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="/favicon.ico" type="image/x-icon" rel="icon"/>
	<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" defer="defer"></script>


	<link rel="stylesheet" href="/blog/wp-content/themes/appco2016/style.css" async="async" media="all" onload="if(media!='all')media='all'">
	<link rel="stylesheet" href="/css/bootstrap/bootstrap.css" async="async" media="all" onload="if(media!='all')media='all'">
	<link rel="stylesheet" href="/css/ag/less/css/abovefold.css" async="async" media="all" onload="if(media!='all')media='all'">
	<link rel="stylesheet" href="/css/ag/less/css/internal.css" async="async" media="all" onload="if(media!='all')media='all'">

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-17876900-9', 'auto');
	ga('send', 'pageview');
	</script>
	<style type="text/css">.at4-share-title { font-size: 9px; }</style>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title(); ?></title>

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php //comments_popup_script(); // off by default ?>
	<?php wp_head(); ?>

</head>
<body class="blog">
	<div id="wrapper" data-role="page">
			
		<div>
			<nav class="mobile-clearfix nav" id="nav-mobile">
				<div class="mobile-home appcogroup-uk"><a href="/">Home</a></div>
				<div class="mobile-menu"><span><a id="close-menu" class="close-button" href="#"></a></span><span><a id="pull" class="static-menu" href="#"></a></span></div>

				<ul class="nav ">
					<li class="deeper home appcogroup-uk"><a href="/">Home</a></li>
					<li class="deeper  "><a href="/about-us">About us</a></li>
					<li class="deeper  "><a href="/field-marketing">Field marketing</a></li>
					<li class="deeper  "><a href="/news">News</a></li>
					<li class="deeper  "><a href="/blog/" class="active">Blog</a></li>
					<li class="deeper  "><a href="/contact-us">Contact us</a></li>
					<li class="deeper last "><a href="/appco-uk-frequently-asked-questions">FAQs</a></li>
				</ul>
			</nav>
		</div>

        <div class="clearfix"></div>				
		<div class="banner">
			<div class="row-fluid">
				<div class="span12">
					<div class="bg-image"><img src="/uploads/2016/06/blog-appco-group-uk.jpg" alt="Appco Blog"></div>
        		</div>
    		</div>
		</div>
		<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" data-role="header">
			<div class="widecrumb"><ul class="breadcrumb "><li><a href="/" itemprop="/"><span itemprop="Home">Home</span></a></li><span class="divider"> / </span>Blog</ul></div>
		</div>


	<div id="container" data-role="main">
		<div class="container-wrapper news">
		    <div class="row layout4" id="content">
	
		      <div class="col-md-8 content wp-left">
	
			  		<?php
			  		if(!is_single()){
			  		?>
				  	<div class="row-left-intro">
					  	<div class="intro-square">
							<h1>Appco Group UK Blog</h1>
						</div>
					</div>
					<?php
					}
					?>






