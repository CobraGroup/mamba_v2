var position = [22.282552, 114.157718];

function showGoogleMaps() {
 
    var latLng = new google.maps.LatLng(position[0], position[1]);
    var latLngCenter = new google.maps.LatLng(position[0] - 0.001, position[1] - 0.006);
 
    var mapOptions = {
        zoom: 16, // initialize zoom level - the max value is 21
        streetViewControl: false, // hide the yellow Street View pegman
        scaleControl: true, // allow users to zoom the Google Map
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latLngCenter
    };
 
    map = new google.maps.Map(document.getElementById('googlemaps'),
        mapOptions);

    marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP
    });
}
 
google.maps.event.addDomListener(window, 'load', showGoogleMaps);
