
$(window).load(function() {

	$(window.location.hash).modal('show');
	$('div[data-toggle="modal"]').click(function(){
		window.location.hash = $(this).data('target');
	});
	
    function revertToOriginalURL() {
        var original = window.location.href.substr(0, window.location.href.indexOf('#'))
        history.replaceState({}, document.title, original);
    }

    $('.modal').on('hidden.bs.modal', function () {
        revertToOriginalURL();
    });

});
