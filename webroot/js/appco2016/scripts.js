
$(document).ready(function() {

	var map_fs_on = false;
	var map_off_height = $(".home-intro-map").outerHeight();

	$(".map-fullscreen-button").on("click", function(){
		if(map_fs_on != true){
			$(".home-intro-map").animate({
				"height": $(window).height()
			});
			$(".google-map-bg").height($(window).height() + 48);
			$(".google-map-bg iframe").height($(window).height() + 48);
			$(".google-map-bg iframe").attr('src', $(".google-map-bg iframe").attr('src'));
			$(".map-fullscreen-button").html('<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span> <span class="text">collapse the map</span>');
			map_fs_on = true;
		}else{
			$(".home-intro-map").animate({
				"height": map_off_height
			});
			$(".google-map-bg").outerHeight(map_off_height + 48);
			$(".google-map-bg iframe").outerHeight(map_off_height + 48);
			$(".map-fullscreen-button").html('<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> <span class="text">expand the map</span>');
			map_fs_on = false;
		}
	});

	$('.dropdown').on('show.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown(250);
	});

	$('.dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(125);
	});
	
	$(".ww-filter-buttons button").on("click", function(){
		filter_type = $(this).data("filter");
		$(".ww-filter-buttons button").removeClass("active");
		$(".ww-filter-buttons button[data-filter=" + filter_type + "]").addClass("active");
		$("div.appco-ww-countries").hide();
		$("div.appco-ww-filter-" + filter_type).show();
	});
	$(".ww-filter-buttons button[data-filter=2]").trigger("click");
	$(".appco-ww-countries div.post-item").each(function(){
	 	$(this).find("a").removeAttr("style");
	 	$(this).find("p:first").removeAttr("style").css({
			'width': '100%',
			'line-height': '18px',
			'min-height': '42px',
			'padding-bottom': '6px',
			'margin-bottom': '6px',
			'border-bottom': '1px solid #575a5d'
		});
	});

	$(window.location.hash).modal('show');
	$('div[data-toggle="modal"]').click(function(){
		window.location.hash = $(this).data('target');
	});
	
    function revertToOriginalURL() {
        var original = window.location.href.substr(0, window.location.href.indexOf('#'))
        history.replaceState({}, document.title, original);
    }

    $('.modal').on('hidden.bs.modal', function () {
        revertToOriginalURL();
    });
    
	$('.carousel').carousel({
		interval: 1000 * 10
	});
	
	$("#home-carousel, #home-carousel-services").swiperight(function() { $(this).carousel('prev'); });
	$("#home-carousel, #home-carousel-services").swipeleft(function() { $(this).carousel('next'); });

});
