$(document).ready(function() {

    var country_label =  $("<option></option>")
                                .attr("value","-1")
                                .text($("#countries_filter").data('countrylabel'));
    
    /* region */
    $('#region').change(function() {
        //for all regions
        if($('#region option:selected').index() == 0) {
            //recreate an element for IE (bug)
	    var newSelect = jQuery.extend({}, $("#countries").html("").clone());
            
            $(newSelect).append($("<option></option>")
                                .attr("value","-1")
                                .text($("#countries_filter").data('countrylabel')));
            
            $("#countries_all optgroup").each(function(index,el) {

                if($(el).is('optgroup')) {
                        var optgroup = $('<optgroup>').attr('label',$(el).attr('label'));

                        $(el).find('option').each(function(option,index2) {
                            $(optgroup).append(
                            $("<option></option>")
                                .attr("value",option)
                                .text($(index2).text())
                            );
                            
                        });
                        $(newSelect).append(optgroup);
                }

            });
            var parent = $("#countries").parent();
            $("#countries").remove();
 
            $(parent).append($(newSelect));
            
            $("#countries_filter table").fadeOut("slow");
            $("#Allregions").fadeIn();
            $("#Allregions td").fadeIn();
        }
        else { //for single region
            var region = $('#region option:selected').text();
            
            //recreate an element for IE (bug)
	    var newSelect = jQuery.extend({}, $("#countries").html("").clone());
            $(newSelect).append($("<option></option>")
                                .attr("value","-1")
                                .text($("#countries_filter").data('countrylabel')));
            $("#countries_all optgroup[label='" + region + "'] option").each(function(index,el) {
                $(newSelect).append(
                    $("<option></option>")
                        .attr("value",index)
                        .text($(el).text())
                );
                
            });
            var parent = $("#countries").parent();
            $("#countries").remove();
 
            $(parent).append($(newSelect));
            
            $("#countries_filter table").fadeOut("slow");
            $("#" + region).fadeIn("slow");
            $("#" + region + " td").fadeIn("slow");
        }
    });
  
    /* country */
    $('div#content.row').on('change','#countries',function() {
        country = $('#countries option:selected').text();
        $("#countries_filter table td").fadeOut();
        var region = $('#region option:selected').text();

        // for all regions
        if($('#region option:selected').index() === 0) {
            region = $('#countries option:selected').parent().attr('label');
        }
       
       //load country
        if($('#countries > option:selected').index() === 0) {
             $("#countries_filter table td").fadeIn("slow"); //data-country
        }
       
        $("#countries_filter table#" + region).fadeIn("slow");
        $("#countries_filter table:not(#Allregions) td[data-country='" + country + "']").fadeIn("slow"); //data-country
    });
 
    //submenu
    $(".menu li a").each(function(){
        if($(this).attr('href') === location.pathname) {
            $(this).parent().addClass('active');
        }
    });

});