
/*
function loadStyleSheet(src) {
    if (document.createStyleSheet){
        document.createStyleSheet(src);
    }
    else {
        $("head").append($("<link rel='stylesheet' href='"+src+"' type='text/css' media='screen' />"));
    }
};
*/

$( window ).load(function() {

	$("body").show();

/*
	if($(window).width() > 640) {

		//dynamically reset the height
	    var dynamic = $('.parallax-right-column');
	    var statica = $('.row-left-intro');
    	statica.height(dynamic.height());
	}
*/

/*
	loadCSS("/css/bootstrap/bootstrap.min.css");
	loadCSS("/css/ag/less/css/abovefold.css");
*/
	
    var container = $(".google-close-button");
    container.click(function(){
	    $('.google-search').empty();
    });
    
// 	if($('body').width() > 640) { 
		$('.bottom-links-right-links').append('<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a2dce77bf1c151" defer="defer" async="async" id="remove_me_on_mobile"><\/script>');
// 	}
 	
	if($('.submenu').length > 0) {
		var parentMenu = $('.submenu').parent('.deeper');
		parentMenu.addClass('hasSubmenu');
	}
	$('.hasSubmenu>a').append('<span class="submenu-accordion-button">+</span>');
	$(".hasSubmenu.active").find(".submenu").show();
	$(".submenu>li.active").parent().show();
	$(".hasSubmenu.active").find(".submenu-accordion-button").html("-");
	$(".submenu>li.active").parent().parent().find(".submenu-accordion-button").html("-");


	$(window.location.hash).modal('show');
	$('a[data-toggle="modal"]').click(function(){
		window.location.hash = $(this).data('target');
	});
	
    function revertToOriginalURL() {
        var original = window.location.href.substr(0, window.location.href.indexOf('#'))
        history.replaceState({}, document.title, original);
    }

    $('.modal').on('hidden.bs.modal', function () {
        revertToOriginalURL();
    });

});


$(document).ready(function() {

	$("#textCarousel").swiperight(function() {  
		$(this).carousel('prev');  
	}); 

	$("#textCarousel").swipeleft(function() {  
		$(this).carousel('next');  
	});

	var $wrapper = $('ul.gallery');
	$wrapper.find('li').sort(function(a, b) {
		return +a.dataset.order - +b.dataset.order;
	}).appendTo($wrapper);

	// find first parallax-intro class and add class to first div after that
	if($('.parallax-intro').length > 0) {
	
		$('.row-1-intro').find('h1').addClass('parallax-left-column');
		$('.parallax-intro').find('div:nth-child(2)').addClass('parallax-right-column');
		$('.parallax-intro').find('div:last').addClass('parallax-text-column');
	}

/*
 		$('.hasSubmenu>a').click(function(e) {
	 		e.preventDefault();
// 	 		if($(this).parent().find("li.active").length == 0){
		 		$(this).parent().find('.submenu').toggle();
		 		$(this).parent().parent('ul').toggleClass('hideTray');
		 		if($(this).parent().find('.submenu:hidden').length == 0){
			 		$(this).parent().find("span.submenu-accordion-button").html("-");
		 		}else{
			 		$(this).parent().find("span.submenu-accordion-button").html("+");
		 		}
// 		 	}
 		});
*/
 		
	$(".ww-filter-buttons button").on("click", function(){
		filter_type = $(this).data("filter");
		console.log(filter_type);
		$(".ww-filter-buttons button").removeClass("active");
		$(".ww-filter-buttons button[data-filter=" + filter_type + "]").addClass("active");
		$("div.appco-ww-countries").hide();
		$("div.appco-ww-filter-" + filter_type).show();
	});
	$(".ww-filter-buttons button[data-filter=2]").trigger("click");
	
	$(".appco-ww-countries div.description div.text").each(function(){
	 	$(this).find("a").removeAttr("style");
	 	$(this).find("p:first").removeAttr("style").css({
			'width': '100%',
			'line-height': '18px',
			'min-height': '42px',
			'padding-bottom': '6px',
			'margin-bottom': '6px',
			'border-bottom': '1px solid #575a5d'
		});
	});

	$(function() {
		var pull 		= $('#pull'),
			menu 		= $('nav ul'),
			//menuHeight	= menu.height(),
			pulldown	= $('#pulldown'),
			submenu		= $('.menu ul');
			//submenuHeight	= submenu.height();

			var w = $(window).width();
    		if(w >= 210 && w <= 1000) {
        		$( "nav" ).removeClass('nav');
    		} else {
        		$( "nav" ).addClass('nav');
    		}


		$(pull).on('click', function(e) {
			e.preventDefault();
			menu.slideToggle();

			$('.submenu').css('display', 'none');

			if($('.hideTray').length > 0) {
				$('.hideTray').toggleClass('hideTray');
			}

		});

		$(pulldown).on('click', function(e) {
			e.preventDefault();
			submenu.slideToggle();

		});

		$(window).resize(function(){
    		var w = $(window).width();

    		if(w < 1000) {
        		$( "nav" ).removeClass('nav');
    		} else {
        		$( "nav" ).addClass('nav');
    		}
    		if(w > 1000 && menu.is(':hidden')) {
    			menu.removeAttr('style');

    		}
    		if(w > 1000 && submenu.is(':hidden')) {
    			submenu.removeAttr('style');
    		}

		});
	});

	$(function() {
		$( ".accordion" ).accordion({
			  collapsible: true,
			  active: false,
			  heightStyle: "content"
		});
	});


 });

