

$(function(){
	 var mySlider, mainBanner, paraSlider;
	 var w = $(window).width();
		      var minNumber,
		            maxNumber,
		            swidth;


		        if (w <= 360) {
		            minNumber = 1;
		            maxNumber = 1;
		            swidth = 360;
		        } else if (w <= 768) {
		            minNumber = 2;
		            maxNumber = 2;
		            swidth = 384;
		        }  else if (w <= 1000) {
		            minNumber = 3;
		            maxNumber = 3;
		            swidth = 384;
		        } else {
		            minNumber = 5;
		            maxNumber = 5;
		            swidth = 500;
		        }



		        if($('#banner > .list-inline').get(0)) {
		            mainBanner = $('#banner > .list-inline').bxSlider({
						adaptiveHeight: true,
						autoStart : true,
						autoHover : false,
						controls: true,
						pager: true,
			            minSlides: 1,
			            maxSlides: 1,


		            });
				}
				if($('.third-row > .list-inline').get(0)) {
				    mySlider = $('.third-row > .list-inline').bxSlider({
				 				adaptiveHeight: true,
								autoStart : false,
								autoHover : true,
								controls: true,
					            minSlides: minNumber,
					            maxSlides: maxNumber,

								slideWidth: swidth,
								slideMargin: 0,
								pager: false,
				    });
				}


				$(window).resize(function(){
	        		var w = $(window).width();
			      var minNumber,
			            maxNumber,
			            swidth;


			        if (w <= 360) {
			            minNumber = 1;
			            maxNumber = 1;
			            swidth = 360;
			        } else if (w <= 768) {
			            minNumber = 2;
			            maxNumber = 2;
			            swidth = 384;
			        }  else if (w <= 1000) {
			            minNumber = 3;
			            maxNumber = 3;
			            swidth = 384;
			        } else {
			            minNumber = 5;
			            maxNumber = 5;
			            swidth = 500;
			        }

					if($('.third-row > .list-inline').get(0)) {
					    mySlider.reloadSlider({
					 				adaptiveHeight: true,
									autoStart : false,
									autoHover : true,
									controls: true,
						            minSlides: minNumber,
						            maxSlides: maxNumber,

									slideWidth: swidth,
									slideMargin: 0,
									pager: true,
					    });
					}


					if($('.parallax-text-column').length > 0) {

					paraSlider.reloadSlider({
				 				adaptiveHeight: true,
								autoStart : false,
								autoHover : true,
								controls: false,
					            minSlides: 1,
					            maxSlides: 1,

								slideWidth: swidth,
								slideMargin: 0,
								pager: true,
				    		});
					}

 				});


});

$( window ).load(function() {

				if($(window).width() > 640) {

					//dynamically reset the height
				    var dynamic = $('.parallax-right-column');
				    var statica = $('.row-left-intro');
			    	statica.height(dynamic.height());
				}
    var container = $(".google-close-button");
    container.click(function(){
	    $('.google-search').empty();
    });

});


 $(document).ready(function() {

 		if($('.submenu').length > 0) {

	 		var parentMenu = $('.submenu').parent('.deeper');
	 		parentMenu.addClass('hasSubmenu');
	 		parentMenu.find('a:first').append('<span></span>');
 		}

 		// find first parallax-intro class and add class to first div after that
 		if($('.parallax-intro').length > 0) {

	 		$('.row-1-intro').find('h1').addClass('parallax-left-column');
	 		$('.parallax-intro').find('div:nth-child(2)').addClass('parallax-right-column');
	 		$('.parallax-intro').find('div:last').addClass('parallax-text-column');
 		}

 		$('.hasSubmenu').click(function(e) {

	 		e.preventDefault();
	 		$(this).find('.submenu').toggle();
	 		$(this).parent('ul').toggleClass('hideTray');
 		});

		$(function() {
			var pull 		= $('#pull'),
				menu 		= $('nav ul'),
				//menuHeight	= menu.height(),
				pulldown	= $('#pulldown'),
				submenu		= $('.menu ul');
				//submenuHeight	= submenu.height();

				var w = $(window).width();
        		if(w >= 210 && w <= 1000) {
	        		$( "nav" ).removeClass('nav');
        		} else {
	        		$( "nav" ).addClass('nav');
        		}



				var swidth = $('.parallax-text-column').width();
				$('.parallax-text-column > .list-inline').bxSlider({
				 				adaptiveHeight: true,
								autoStart : false,
								autoHover : true,
								controls: false,
					            minSlides: 1,
					            maxSlides: 1,

								slideWidth: swidth,
								slideMargin: 0,
								pager: true,
				    		});

			$(pull).on('click', function(e) {
				e.preventDefault();
				menu.slideToggle();

				$('.submenu').css('display', 'none');

				if($('.hideTray').length > 0) {
					$('.hideTray').toggleClass('hideTray');
				}

			});

			$(pulldown).on('click', function(e) {
				e.preventDefault();
				submenu.slideToggle();

			});

			$(window).resize(function(){
        		var w = $(window).width();

        		if(w < 1000) {
	        		$( "nav" ).removeClass('nav');
        		} else {
	        		$( "nav" ).addClass('nav');
        		}
        		if(w > 1000 && menu.is(':hidden')) {
        			menu.removeAttr('style');

        		}
        		if(w > 1000 && submenu.is(':hidden')) {
        			submenu.removeAttr('style');
        		}

    		});
		});

	$(function() {
		$( ".accordion" ).accordion({
			  collapsible: true,
			  active: false,
			  heightStyle: "content"
		});
	});


 });

